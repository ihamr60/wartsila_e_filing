$(document).ready(function () {
    $('li.menu-overtime, li.menu-overtime li.menu-overtime-sub-overtime-request').addClass('active');

    var date_dp = $("input[name=inputOverTimeDate]");

    date_dp.datepicker({
        language: 'en',
        dateFormat : 'yyyy-mm-dd'
    });

    $('select#inputDayStatus, select#inputPlantManager,select#inputMinute,select#inputHour,select#inputEmployee').select2({
        width: 'resolve'
    });

    for (let index = 0; index <= 12; index++) {
        var o = new Option(index, index);
        $('select#inputHour').append(o);
    }

    for (let index = 0; index < 60; index++) {
        var o = new Option((index < 10 ? '0' + index : index), index);
        $('select#inputMinute').append(o);
    }

    $('form#overTimeRequest').submit(function (event) {
        var data = {
            'inputEmployee' : $('select#inputEmployee').val(),
            'inputPlantManager' : $('select#inputPlantManager').val(),
            'inputDate': $('input[name=inputOverTimeDate]').val(),
            'inputHours': $('select#inputHour').val(),
            'inputMinutes': $('select#inputMinute').val(),
            'inputPurpose': $('textarea[name=inputPurpose]').val()
        };

        if($('select#inputDayStatus').val()){
            data['inputDayStatus'] = $('select#inputDayStatus').val();
        }

        if($('input[name=inputOverTimeDate]').val() && $('select#inputHour').val() != -1 && $('select#inputMinute').val() != -1){
            $.ajax({
                type: 'POST',
                url: host + 'ajax/overtime_request_by_superior',
                data: data,
                beforeSend  : function(){
                    Swal.fire({
                        imageUrl: host + '../assets/images/load.gif',
                        imageAlt: ' ',
                        showConfirmButton: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        title: 'Loading ...'
                    });
                }
            })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    Toast.fire({
                        type: 'success',
                        title: 'Request overtime has been sent.'
                    });
                    $(this).trigger('reset');
                    setTimeout(function(){ location.reload(); }, 1000);
                }else{
                    Toast.fire({
                        type: 'warning',
                        title: 'Overtime request cannot be sent.'
                    });
                }
            })
            .fail(function(){
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments."
                });
            });
        }else{
            Swal.fire({ type: 'info', title: 'The form is not fully filled' });
        }

        event.preventDefault();
    });
});