$(document).ready(function() {
    $('li.menu-shift, li.menu-shift li.menu-shift-sub-shift-time').addClass('active');

    $('select#siteSelect').select2({
        width: 'resolve'
    });
    
    $('.datepicker').css('z-index',9999);
    site_id = $('select[name=siteSelect]').val();
    var shift_time_datatable = {
        table : $('#shiftTimeDataTable').DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            ajax: {
                url: host + 'ajax_filing/datatable_karyawan',
                method: "POST",
                data: function (d) {
                    d.site_id = site_id;
                }
            },
            columns: [
                { 
                    data: "PersNo",
                    width: "45%"
                },
                { 
                    data: "Known_As",
                    
                    width: "45%"
                },
                { 
                    data: null,
                    render: function ( data, type, row ) {
                        return    '<button id="'+ data.shift_time_id +'" type="button" class="btn btn-info btn-sm btn-edit-shift-time">'
                                +   '<i class="far fa-edit"></i>'
                                + '</button>'
                                + '<button id="'+ data.shift_time_id +'" type="button" class="btn btn-danger btn-sm btn-delete-shift-time">'
                                +   '<i class="fas fa-trash"></i>'
                                + '</button>';
                    },
                    width: "10%"
                }
            ]
        })
    };

    $('form#add-new-shift-time').submit(function(event){
        var data = {
            'inputShiftName' : $(this).find('input[name=inputShiftName]').val(),
            'inputTimeIn' : $(this).find('input[name=inputTimeIn]').val(),
            'inputTimeOut' : $(this).find('input[name=inputTimeOut]').val(),
            'site_id' : site_id
        };

        $.ajax({
            type        : 'POST',
            url         : host + 'ajax/add_shift_time',
            data        : data,
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function(data){
            Swal.close();
            if(data.status){
                $('#addNewShiftTimeModal').modal('hide');
                shift_time_datatable.table.ajax.reload();
                shift_time_datatable.table.draw();
                    Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
              });
        });

        event.preventDefault();

    });

    $(document).on('click', '.btn-edit-shift-time', function (e) {
        var selected_row = $(this).parent('td');
        var cell = shift_time_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;

        $('#editShiftTimeModal').modal('show');

        var form = $('form#edit-shift-time');
        form.trigger('reset');
        form.find('input[name=inputShiftName]').val(data.shift_time_name);
        form.find('input[name=inputTimeIn]').val(data.start_time);
        form.find('input[name=inputTimeOut]').val(data.finish_time);
    });

    $('form#edit-shift-time').submit(function (event) {
        var data = {
            'inputShiftName': $(this).find('input[name=inputShiftName]').val(),
            'inputTimeIn': $(this).find('input[name=inputTimeIn]').val(),
            'inputTimeOut': $(this).find('input[name=inputTimeOut]').val(),
            'shiftTimeId': last_selected.shift_time_id,
            'site_id': site_id
        };

        $.ajax({
                type: 'POST',
                url: host + 'ajax/update_shift_time',
                data: data,
                beforeSend  : function(){
                    Swal.fire({
                        imageUrl: host + '../assets/images/load.gif',
                        imageAlt: ' ',
                        showConfirmButton: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        title: 'Loading ...'
                    });
                }
            })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    $('#editShiftTimeModal').modal('hide');
                    shift_time_datatable.table.ajax.reload();
                    shift_time_datatable.table.draw();
                        Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                }
            })
            .fail(function(){
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                  });
            });

        event.preventDefault();

    });

    $('select[name=siteSelect]').on('change', function () {
        site_id = $('select[name=siteSelect]').val();
        // shift_time_datatable.table.ajax.reload();
        shift_time_datatable.table.draw();
    });

    $(document).on('click', '.btn-delete-shift-time', function(){
        var shift_time_id = $(this).attr('id');
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success ml-1',
              cancelButton: 'btn btn-danger mr-1'
            },
            buttonsStyling: false,
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type        : 'POST',
                    url         : host + 'ajax/delete_shift_time',
                    data        : {shift_time_id:shift_time_id}
                })
                .done(function(data){
                    if(data.result){
                        Toast.fire({
                            type: 'success',
                            title: 'Success.'
                          });
                        shift_time_datatable.table.ajax.reload();                        
                        shift_time_datatable.table.draw();
                    }else{
                        Toast.fire({
                            type: 'error',
                            title: 'Data cannot be deleted.'
                          });
                    }
                })
                .fail(function(){
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                      });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          })
    });
});