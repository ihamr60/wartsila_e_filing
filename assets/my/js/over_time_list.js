$(document).ready(function () {
    $('li.menu-overtime, li.menu-overtime li.menu-overtime-sub-overtime-list-request').addClass('active');
    var overtime_time = new Array();
    var overtime_extend_time = new Array();
    function format ( d ) {
        var html = `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="3"><i class="fas fa-info-circle"></i> DETAILS</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <tr>
                            <td>Overtime Date</td>
                            <td>${d.overtime_date_formated}</td>
                            <td></td>
                        </tr>  
                        <tr>
                            <td>Day Status</td>
                            <td>${(d.day_status == '0' ? 'HOLIDAY' : 'NORMAL')}</td>
                            <td></td>
                        </tr>                        
                        <tr>
                            <td>Employee ID</td>
                            <td>${d.employee_id}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Employee Name</td>
                            <td>${d.employee_name}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Employee Position</td>
                            <td>${d.employee_position}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Purpose</td>
                            <td>${d.purpose}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Overtime Hour(s)</td>
                            <td><input class='form-control datepicker-here datepicker-over-time-hours' data-timepicker='true' data-only-timepicker='true' data-time-format='h hour(s) and i minute(s)' data-min-hours='0' data-max-hours = '12' data-language='en' id='${d.over_time_id}' name='inputOverTimeHours' value='${d.time_formated}'></input></td>
                            <td><button id='${d.over_time_id}' type='button' class='btn btn-info btn-sm btn-edit-overtime-time'><i class='far fa-edit'></i></button></td>
                        </tr>
                        <tr>
                            <td>Request Date</td>
                            <td>${d.created_date}</td>
                            <td></td>
                        </tr>
                    </tbody>
                    </table>`;

        if(d.extend_request != '-'){ 
            html += `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="3"><i class="fas fa-info-circle"></i> Extend DETAILS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Extend Purpose</td>
                            <td>${d.extend_purpose}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Extend Overtime Hour(s)</td>
                            <td><input class='form-control datepicker-here datepicker-over-time-hours' data-timepicker='true' data-only-timepicker='true' data-time-format='h hour(s) and i minute(s)' data-min-hours='0' data-max-hours = '12' data-language='en' id='${d.extend_id}' name='inputOverTimeHours' value='${d.extend_time}'></input></td>
                            <td><button id='${d.extend_id}' type='button' class='btn btn-info btn-sm btn-edit-overtime-extend-time'><i class='far fa-edit'></i></button></td>
                        </tr>
                        <tr>
                            <td>Request Extend Date</td>
                            <td>${d.extend_request}</td>
                            <td></td>
                        </tr>                                                
                    </tbody>
                </table>`;
        }

        html += `<table class="table w-100">
                    <thead class="thead-dark">
                        <tr>
                            <th colspan="2"><i class="fas fa-info-circle"></i> Shift Information</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td>Normal Work Hours</td>
                            <td>${d.info.shift ? d.info.shift.start + ' - ' + d.info.shift.finish : '-'}</td>
                        </tr>
                        <tr>
                            <td>Shift</td>
                            <td>${d.info.shift ? d.info.shift.name : '-'}</td>
                        </tr>
                    </tbody>
                </table>`;

        html += `<table class="table w-100">
                    <thead class="thead-dark">
                        <tr>
                            <th colspan="3"><i class="fas fa-info-circle"></i> Attendance Information</th>
                        </tr>
                    </thead>
                    
                    <tbody>`;

        if(d.info.attendance.length > 0){
            for (let index = 0; index < d.info.attendance.length; index++) {
                html += `<tr>
                            <td>${d.info.attendance[index]['date']}</td>
                            <td>Coming</td>
                            <td>${d.info.attendance[index]['coming']}</td>
                        </tr>
                        <tr>
                            <td>${d.info.attendance[index]['date']}</td>
                            <td>Leaving</td>
                            <td>${d.info.attendance[index]['leaving']}</td>
                        </tr>`;
            }
        }else{
            html += `<tr>
                        <td colspan="2">Data not found!</td>
                    </tr>`;
        }

        html += `</tbody>
                </table>`;
        return html;
    }

    var overtimeListDataTable = $('#overtimeDataTable').DataTable({
        'ordering': false,
        'processing': true,
        'serverSide': true,
        ajax: {
            url: host + 'ajax/overtime_datatable_list',
            method: "POST"
        },
        columns: [
            {
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: '<button type="button" class="btn btn-info btn-sm"><i class="fas fa-chevron-down"></i></button>',
                width: "5%"
            },
            { 
                data: "overtime_date_formated",
                width: "25%"
            },
            { 
                data: "employee_name",
                width: "25%"
            },
            { 
                data: "employee_position",
                width: "25%"
            },
            { 
                data: null,
                "render": function (data, type, row) {
                    var return_html = '-';
                    if(data.status == 'APPROVED') {
                        return_html = `<span class="badge bg-success text-white">APPROVED</span>`;
                    }else if(data.status == 'DECLINED' || data.status == 'CANCEL'){
                        return_html = `<span class="badge bg-danger text-white">CANCELED</span>`;
                    }else if(data.status == 'ON REQUEST'){
                        return_html = `<span class="badge bg-info text-white">ON REQUEST</span>`;
                    }

                    if(data.extend_status == 'APPROVED') {
                        return_html += `<br><span class="badge bg-success text-white">EXTEND APPROVED</span>`;
                    } else if (data.extend_status == 'DECLINED' || data.extend_status == 'CANCEL'){
                        return_html += `<br><span class="badge bg-danger text-white">EXTEND DECLINED</span>`;
                    }else if(data.extend_status == 'ON REQUEST'){
                        return_html += `<br><span class="badge bg-info text-white">EXTEND REQUEST</span>`;
                    }
                    return return_html;
                },
                width: "10%"
            },
            {
                data: null,
                "render": function (data, type, row){
                    if (data.status == "ON REQUEST"){
                        return `<div class="btn-group-vertical">
                                    <button id="${data.over_time_id}" type="button" class="btn btn-info btn-response btn-sm btn-approve-overtime" data-button='{"response": "APPROVED"}'>
                                        <i>Approve</i>
                                    </button>
                                    <button id="${data.over_time_id}" type="button" class="btn btn-danger btn-response btn-sm btn-decline-overtime" data-button='{"response": "DECLINED"}'>
                                        <i>Decline</i>
                                    </button>
                                </div>`;
                    } else if (data.extend_status == "ON REQUEST"){
                        return `<div class="btn-group-vertical">
                                    <button id="${data.extend_status}" type="button" class="btn btn-info btn-sm btn-response btn-approve-extend-overtime" data-button='{"response_extend": "APPROVED"}'>
                                        <i>Approve Extend</i>
                                    </button>
                                    <button id="${data.extend_status}" type="button" class="btn btn-danger btn-sm btn-response btn-decline-extend-overtime" data-button='{"response_extend": "DECLINED"}'>
                                    <i>Decline Extend</i>
                                    </button>
                                </div>`;
                    }else{
                        return null;
                    }

                },
                width: "10%"
            }
        ]
    });

    $(document).on('click', '#overtimeDataTable tbody td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = overtimeListDataTable.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }else {
            // Open this row
            console.log(row.data());
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
            var data = row.data();
            console.log(data.time);
            var time = data.time;
            var formattedTime = time.split(":");

            var dp = $(`input[id=${data.over_time_id}]`).datepicker().data('datepicker');
            var new_hours = new Date();
            new_hours.setHours(formattedTime[0], formattedTime[1], 0, 0);
            dp.selectDate(new_hours);

            dp.update({
                toggleSelected: false,
                onSelect: function (formattedDate, date, inst) {

                    overtime_time[`${data.over_time_id}`] = date.getHours() + ":" + date.getMinutes();
                    // console.log(date);
                    console.log(overtime_time);
                }
            });

            if (data.extend_request != '-') {
                console.log(data.extend_time);
                var time_ex = data.extend_time_full;
                var formattedTime_ex = time_ex.split(":");
                var dpex = $(`input[id=${data.extend_id}]`).datepicker().data('datepicker');
                var new_hours_ex = new Date();
                new_hours_ex.setHours(formattedTime_ex[0], formattedTime_ex[1], 0, 0);
                dpex.selectDate(new_hours_ex);

                dpex.update({
                    toggleSelected: false,
                    onSelect: function (formattedDate, date, inst) {

                        overtime_extend_time[`${data.extend_id}`] = date.getHours() + ":" + date.getMinutes();
                        // console.log(date);
                        console.log(overtime_extend_time);
                    }
                });
            }
        }
    });

    $(document).on('click', '.btn-response', function (e) {
        var data = $.parseJSON($(this).attr('data-button'));
        if (typeof data.response != "undefined") {
            var selected_data = overtimeListDataTable.cell($(this).parents('td')).data();
            var data = {
                'overtime_id': selected_data.over_time_id,
                'status': data.response,
                'user_request_id': selected_data.employee_id
            };
            $.ajax({
                type: 'POST',
                url: host + 'ajax/overtime_update_status',
                data: data,
                beforeSend  : function(){
                    Swal.fire({
                        imageUrl: host + '../assets/images/load.gif',
                        imageAlt: ' ',
                        showConfirmButton: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        title: 'Loading ...'
                    });
                }
            })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    overtimeListDataTable.ajax.reload();
                    overtimeListDataTable.draw();
                        Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                }
            })
            .fail(function(){
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                  });
            });
        }
        else if (typeof data.response_extend != "undefined") {
            var selected_data = overtimeListDataTable.cell($(this).parents('td')).data();
            var data = {
                'extend_id': selected_data.extend_id,
                'status': data.response_extend,
                'user_request_id': selected_data.employee_id
            };
            $.ajax({
                type: 'POST',
                url: host + 'ajax/overtime_extend_update_status',
                data: data,
                beforeSend  : function(){
                    Swal.fire({
                        imageUrl: host + '../assets/images/load.gif',
                        imageAlt: ' ',
                        showConfirmButton: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        title: 'Loading ...'
                    });
                }
            })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    overtimeListDataTable.ajax.reload();
                    overtimeListDataTable.draw();
                        Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                }
            })
            .fail(function(){
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                  });
            });
        }
    });

    
    $(document).on('click', '.btn-edit-overtime-time', function (e) {
        console.log($(this).attr("id"));
        var overtime_id = $(this).attr("id");
        var data = {
            'overtime_id' : overtime_id,
            'inputHours'        : overtime_time[`${overtime_id}`]
        }
        $.ajax({
            type: 'POST',
            url: host + 'ajax/update_overtime_time',
            data: data,
            beforeSend: function () {
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    overtimeListDataTable.ajax.reload();
                    overtimeListDataTable.draw();
                    Toast.fire({
                        type: 'success',
                        title: 'Success.'
                    });
                }
            })
            .fail(function () {
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                });
            });
    });
    
    $(document).on('click', '.btn-edit-overtime-extend-time', function (e) {
        console.log($(this).attr("id"));
        var extend_id = $(this).attr("id");
        var data = {
            'extend_id': extend_id,
            'inputHours': overtime_extend_time[`${extend_id}`]
        }
        $.ajax({
            type: 'POST',
            url: host + 'ajax/update_overtime_extend_time',
            data: data,
            beforeSend: function () {
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
            .done(function (data) {
                Swal.close();
                if (data.status) {
                    overtimeListDataTable.ajax.reload();
                    overtimeListDataTable.draw();
                    Toast.fire({
                        type: 'success',
                        title: 'Success.'
                    });
                }
            })
            .fail(function () {
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                });
            });
    });


});