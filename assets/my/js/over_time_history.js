$(document).ready(function () {
    $('li.menu-overtime, li.menu-overtime li.menu-overtime-sub-overtime-history').addClass('active');
    $('.datepicker').css('z-index', 99999);

    var overtime_hours = "00:00";
    var dp = $("input[name=inputOverTimeHours]").datepicker().data('datepicker');
    var new_hours = new Date();
    var over_time_id_selected;
    new_hours.setHours(0, 0, 0, 0);
    dp.selectDate(new_hours);

    dp.update({
        toggleSelected: false,
        onSelect: function (formattedDate, date, inst) {
            overtime_hours = date.getHours() + ":" + date.getMinutes();
        }
    });

    function format(d) {
        var html = '';
        if(d.requestor_name){ 
            html += `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2"><i class="fas fa-info-circle"></i> Requestor</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Personal Number</td>
                            <td>${d.requestor_id}</td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>${d.requestor_name}</td>
                        </tr>                                              
                    </tbody>
                </table>`;
        }

        html += `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2"><i class="fas fa-info-circle"></i> OT DETAILS</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <tr>
                            <td>Overtime Date</td>
                            <td>${d.overtime_date_formated}</td>
                        </tr>                        
                        <tr>
                            <td>Employee ID</td>
                            <td>${d.employee_id}</td>
                        </tr>
                        <tr>
                            <td>Employee Name</td>
                            <td>${d.employee_name}</td>
                        </tr>
                        <tr>
                            <td>Employee Position</td>
                            <td>${d.employee_position}</td>
                        </tr>
                        <tr>
                            <td>Purpose</td>
                            <td>${d.purpose}</td>
                        </tr>
                        <tr>
                            <td>Overtime Hour(s)</td>
                            <td>${d.time_formated}</td>
                        </tr>
                        <tr>
                            <td>Request Date</td>
                            <td>${d.created_date}</td>
                        </tr>
                    </tbody>
                    </table>`;

        if(d.extend_request != '-'){ 
            html += `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th colspan="2"><i class="fas fa-info-circle"></i> Extend DETAILS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Extend Purpose</td>
                            <td>${d.extend_purpose}</td>
                        </tr>
                        <tr>
                            <td>Extend Overtime Hour(s)</td>
                            <td>${d.extend_time}</td>
                        </tr>
                        <tr>
                            <td>Request Extend Date</td>
                            <td>${d.extend_request}</td>
                        </tr>                                                
                    </tbody>
                </table>`;
        }
        return html;
    }

    var overtimeHistoryDataTable = $('#overtimeHistoryDataTable').DataTable({
        ordering: false,
        processing: true,
        serverSide: true,
        ajax: {
            url: host + 'ajax/overtime_datatable_history',
            method: "POST"
        },
        columns: [
            {
                width: "5%",
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: '<button type="button" class="btn btn-info btn-sm"><i class="fas fa-chevron-down"></i></button>'
            },
            { width: "18%", data: "overtime_date_formated" },
            { width: "42%", data: "purpose" },
            {
                width: "20%",
                data: null,
                render: function (data, type, row) {
                    var return_html = '';
                    if(data.status == 'APPROVED') {
                        return_html = `<span class="badge bg-success text-white">APPROVED</span>`;
                    }else if(data.status == 'DECLINED'){
                        return_html = `<span class="badge bg-danger text-white">DECLINED</span>`;
                    }else if(data.status == 'ON REQUEST'){
                        return_html = `<span class="badge bg-info text-white">ON REQUEST</span>`;
                    }else if (data.status == 'CANCEL'){
                        return_html += `<span class="badge bg-danger text-white">CANCELED</span>`;
                    }

                    if(data.extend_status == 'APPROVED') {
                        return_html += `<br><span class="badge bg-success text-white">EXTEND APPROVED</span>`;
                    } else if (data.extend_status == 'DECLINED'){
                        return_html += `<br><span class="badge bg-danger text-white">EXTEND DECLINED</span>`;
                    }else if(data.extend_status == 'ON REQUEST'){
                        return_html += `<br><span class="badge bg-info text-white">EXTEND REQUEST</span>`;
                    }else if (data.extend_status == 'CANCEL'){
                        return_html += `<br><span class="badge bg-danger text-white">EXTEND CANCELED</span>`;
                    }
                    return return_html;
                }
            },
            {
                width: "25%",
                data: null,
                render: function (data, type, row) {
                    if(data.allow_request != 1)
                        return '';

                    var html = '<div class="btn-group-vertical">';

                    if (data.status == 'APPROVED' || data.status == 'ON REQUEST'){
                        html += `<button type="button" id="${data.over_time_id}" data-button='{"response": "CANCEL"}' class="btn btn-danger btn-sm btn-overtime-request-cancel"><i class="fas fa-times"></i> Cancel OT</button>`;
                    }

                    if(data.extend_status == '-' && data.status != 'DECLINED' && data.status != 'CANCEL'){
                        html += `<button type="button" id="${data.over_time_id}" class="btn btn-info btn-sm btn-overtime-request-extend"><i class="far fa-file-alt"></i> Extend</button>`;
                    }else if (data.extend_status == 'APPROVED' || data.extend_status == 'ON REQUEST'){
                        html += `<button type="button" id="${data.extend_id}" data-button='{"response": "CANCEL"}' class="btn btn-danger btn-sm btn-overtime-extend-request-cancel"><i class="fas fa-times"></i> Cancel OT Extend</button>`;
                    }

                    return html;


                    // if (data.status == 'ON REQUEST' && data.extend_status == '-') {
                    //     return (data.allow_request == 1) ? 
                    //         `<div class="btn-group-vertical">
                    //             <button type="button" id="${data.over_time_id}" data-button='{"response": "CANCEL"}' class="btn btn-danger btn-sm btn-overtime-request-cancel"><i class="fas fa-times"></i> Cancel</button>
                    //             <button type="button" id="${data.over_time_id}" class="btn btn-info btn-sm btn-overtime-request-extend"><i class="far fa-file-alt"></i> Extend</button>
                    //         </div>` : null;
                    // }
                    // if (data.status == 'APPROVED' && data.extend_status == '-') {
                    //     return (data.allow_request == 1) ?  
                    //         `<div class="btn-group-vertical">
                    //             <button type="button" id="${data.over_time_id}" data-button='{"response": "CANCEL"}' class="btn btn-danger btn-sm btn-overtime-request-cancel"><i class="fas fa-times"></i> Cancel</button>
                    //             <button type="button" id="${data.over_time_id}" class="btn btn-info btn-sm btn-overtime-request-extend"><i class="far fa-file-alt"></i> Extend</button>
                    //         </div>`  : null;
                    // }
                    // else if (data.extend_status == 'APPROVED' || data.extend_status == 'ON REQUEST') {
                    //     return (data.allow_request == 1) ? `<button type="button" id="${data.extend_id}" data-button='{"response": "CANCEL"}' class="btn btn-danger btn-sm btn-overtime-extend-request-cancel"><i class="fas fa-times"></i> Cancel Extend</button>`  : null;
                    // }
                    // else {
                    //     return null;
                    // }
                }
            }
        ]
    });

    $(document).on('click', '.btn-overtime-extend-request-cancel', function (e) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-danger ml-1',
              cancelButton: 'btn btn-light mr-1'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, cancel extend request!',
            cancelButtonText: 'No, cancel this action!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var data = {
                    'extend_id': $(this).attr('id')
                };
                $.ajax({
                    type: 'POST',
                    url: host + 'ajax/overtime_extend_cancel',
                    data: data,
                    beforeSend  : function(){
                        Swal.fire({
                            imageUrl: host + '../assets/images/load.gif',
                            imageAlt: ' ',
                            showConfirmButton: false,
                            showCancelButton: false,
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            title: 'Loading ...'
                        });
                    }
                })
                .done(function (data) {
                    Swal.close();
                    if (data.status) {
                        overtimeHistoryDataTable.ajax.reload();
                        overtimeHistoryDataTable.draw();
                        Toast.fire({
                            type: 'success',
                            title: 'Overtime extend has been canceled.'
                            });
                    }else{
                        Toast.fire({
                            type: 'warning',
                            title: 'Overtime extend failed to be canceled.'
                            });
                    }
                })
                .fail(function(){
                    Swal.close();
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                        });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          });
        // var data = $.parseJSON($(this).attr('data-button'));
        // var selected_data = overtimeHistoryDataTable.cell($(this).parent('td')).data();
        // var data = {
        //     'extend_id': selected_data.extend_id
        // };
        // $.ajax({
        //     type: 'POST',
        //     url: host + 'ajax/overtime_extend_cancel',
        //     data: data,
        //     beforeSend  : function(){
        //         Swal.fire({
        //             imageUrl: host + '../assets/images/load.gif',
        //             imageAlt: ' ',
        //             showConfirmButton: false,
        //             showCancelButton: false,
        //             allowOutsideClick: false,
        //             allowEscapeKey: false,
        //             title: 'Loading ...'
        //         });
        //     }
        // })
        // .done(function (data) {
        //     Swal.close();
        //     if (data.status) {
        //         overtimeHistoryDataTable.ajax.reload();
        //         overtimeHistoryDataTable.draw();
        //         Toast.fire({
        //             type: 'success',
        //             title: 'Success.'
        //         });
        //     }
        // })
        // .fail(function(){
        //     Swal.close();
        //     Toast.fire({
        //         type: 'error',
        //         title: "The server doesn't respond, try again in a few moments"
        //       });
        // });
    });

    $(document).on('click', '.btn-overtime-request-cancel', function() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-danger ml-1',
              cancelButton: 'btn btn-light mr-1'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, cancel request!',
            cancelButtonText: 'No, cancel this action!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                var data = {
                    'overtime_id': $(this).attr('id')
                };
                $.ajax({
                    type: 'POST',
                    url: host + 'ajax/overtime_cancel_request',
                    data: data,
                    beforeSend  : function(){
                        Swal.fire({
                            imageUrl: host + '../assets/images/load.gif',
                            imageAlt: ' ',
                            showConfirmButton: false,
                            showCancelButton: false,
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            title: 'Loading ...'
                        });
                    }
                })
                .done(function (data) {
                    Swal.close();
                    if (data.status) {
                        overtimeHistoryDataTable.ajax.reload();
                        overtimeHistoryDataTable.draw();
                        Toast.fire({
                            type: 'success',
                            title: 'Overtime has been canceled.'
                            });
                    }else{
                        Toast.fire({
                            type: 'warning',
                            title: 'Overtime failed to be canceled.'
                            });
                    }
                })
                .fail(function(){
                    Swal.close();
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                        });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          });
    });
    // $(document).on('click', '.btn-overtime-request-cancel', function (e) {
    //     var data = $.parseJSON($(this).attr('data-button'));
    //     var selected_data = overtimeHistoryDataTable.cell($(this).parent('td')).data();
    //     var data = {
    //         'overtime_id': selected_data.over_time_id,
    //         'status': data.response,
    //         'user_request_id': selected_data.employee_id
    //     };
    //     $.ajax({
    //         type: 'POST',
    //         url: host + 'ajax/overtime_update_status',
    //         data: data,
    //         beforeSend  : function(){
    //             Swal.fire({
    //                 imageUrl: host + '../assets/images/load.gif',
    //                 imageAlt: ' ',
    //                 showConfirmButton: false,
    //                 showCancelButton: false,
    //                 allowOutsideClick: false,
    //                 allowEscapeKey: false,
    //                 title: 'Loading ...'
    //             });
    //         }
    //     })
    //     .done(function (data) {
    //         Swal.close();
    //         if (data.status) {
    //             overtimeHistoryDataTable.ajax.reload();
    //             overtimeHistoryDataTable.draw();
    //             Toast.fire({
    //                 type: 'success',
    //                 title: 'Success.'
    //                 });
    //         }
    //     })
    //     .fail(function(){
    //         Swal.close();
    //         Toast.fire({
    //             type: 'error',
    //             title: "The server doesn't respond, try again in a few moments"
    //             });
    //     });
    // });

    $(document).on('click', '#overtimeHistoryDataTable tbody td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = overtimeHistoryDataTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });


    $(document).on('click', '.btn-overtime-request-extend', function (e) {
        var selected_row = $(this).parents('td');
        var cell = overtimeHistoryDataTable.cell(selected_row);
        var data = cell.data();
        over_time_id_selected = data.over_time_id;
        $('#ExtendRequestModal').modal('show');
    });

    $("form#extend-request").submit(function (event) {
        var data = {
            'overTimeId': over_time_id_selected,
            'inputHours': overtime_hours,
            'inputPurpose': $(this).find('textarea[name=inputPurpose]').val()
        };
        $.ajax({
            type: 'POST',
            url: host + 'ajax/overtime_extend_add',
            data: data,
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function (data) {
            Swal.close();
            if (data.status) {
                Toast.fire({
                    type: 'success',
                    title: 'Success.'
                });
                overtimeHistoryDataTable.ajax.reload();
                overtimeHistoryDataTable.draw();
                $('#ExtendRequestModal').modal('hide');
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
              });
        });
        event.preventDefault();
    });
});