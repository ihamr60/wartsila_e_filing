$(document).ready(function() {
    $('li.menu-shift, li.menu-shift li.menu-shift-sub-shift-group').addClass('active');

    var site_id = $('select[name=siteSelect]').val();
    var last_selected_group = "";

    $(".btn-new-shift-group").attr("href", host + '/shift_group/new_shift_group/' + site_id);

    $('select#siteSelect').select2({
        width: 'resolve'
    });
    
    function format ( members ) {
        var html = `<table class="table w-100">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Pers No</th>
                                <th scope="col">Name</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>`;

        members.forEach(function(e) {
            var name = e.last_name ? e.first_name + ' ' + e.last_name : e.first_name;
            html += `<tr>
                        <td>`+e.employee_id+`</td>
                        <td>`+name+`</td>
                        <td>
                            <button id="`+e.employee_id+`" type="button" class="btn btn-danger btn-sm btn-delete-shift-group-member">
                                <i class="fas fa-user-alt-slash"></i>
                            </button>
                        </td>
                    </tr>`;
        });

        html += `</tbody>
                </table>`;

        return html;
    }

    var shift_datatable = $('#shiftDataTable').DataTable({
        'ordering' : false,
        'processing': true,
        'serverSide': true,
        ajax: {
            url: host + 'ajax/get_shift_group_datatable',
            method: "POST",
            data: function( d ){
                d.site_id = site_id;
            }
        },
        columns: [
            {
                className: 'details-control',
                orderable: false,
                data: null,
                defaultContent: '<button type="button" class="btn btn-info btn-sm"><i class="fas fa-plus"></i></button>',
                width: "10%"
            },
            {
                className: 'details-control',
                data: "shift_group_name",
                width: "80%"
            },
            { 
                data: null,
                render: function ( data, type, row ) {
                    return '<a href="./shift_group/edit/' + data.shift_group_id +'" class="btn btn-info btn-sm text-white btn-new-shift-group" target="_blank">'
                            +  '<i class="far fa-edit"></i>'
                            +  '</a>'
                            + '<button id="'+ data.shift_group_id +'" type="button" class="btn btn-danger btn-sm btn-delete-shift-group">'
                            +   '<i class="fas fa-trash"></i>'
                            + '</button>';
                },
                width: "10%"
            }
        ]
    });

    $('#shiftDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = shift_datatable.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            var selected_row = $(this);
            var cell = shift_datatable.cell(selected_row);
            last_selected_group = cell.data()['shift_group_id'];
            console.log(last_selected_group);
            row.child( format(row.data().members) ).show();
            tr.addClass('shown');
        }
    } );
    
    $('select[name=siteSelect]').on('change', function(){
        site_id = $('select[name=siteSelect]').val();
        $(".btn-new-shift-group").attr("href", host + '/shift_group/new_shift_group/' + site_id);
        shift_datatable.draw();
    });

    $(document).on('click', '.btn-delete-shift-group-member', function(){
        $.ajax({
            type: 'POST',
            url: host + 'ajax/delete_shift_group_member',
            data: {employee_id:$(this).attr('id'), shift_group_id:last_selected_group},
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function (data) {
            Swal.close();
            if (data.result) {
                shift_datatable.draw();
                Toast.fire({
                    type: 'success',
                    title: 'Success.'
                });
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
              });
        });
    });

    $(document).on('click', '.btn-delete-shift-group', function(){
        var shift_group_id = $(this).attr('id');
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success ml-1',
              cancelButton: 'btn btn-danger mr-1'
            },
            buttonsStyling: false,
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type        : 'POST',
                    url         : host + 'ajax/delete_shift_group',
                    data        : {shift_group_id:shift_group_id}
                })
                .done(function(data){
                    if(data.result){
                        Toast.fire({
                            type: 'success',
                            title: 'Success.'
                          });
                        shift_datatable.draw();
                    }else{
                        Toast.fire({
                            type: 'error',
                            title: 'Data cannot be deleted.'
                          });
                    }
                })
                .fail(function(){
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                      });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          })
    });
});