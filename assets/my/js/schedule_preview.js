$(document).ready(function() {
    $('li.menu-schedule').addClass('active');

    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        noEventsMessage: 'No schedule found!',
        allDayText: '',
        plugins: [ 'dayGrid', 'list' ],
        buttonText: {list:'View as list', month:'View as calendar'},
        header: {
            left: 'listWeek,dayGridMonth',
            center: 'title',
            right: 'prev,next'
        },
        defaultDate: view_date,
        events: event_list
    });

    calendar.render();

    $(document).on('click','.btnPrint',function(e) {
        window.print();
    });

});