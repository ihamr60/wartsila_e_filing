$(document).ready(function () {
    $('li.menu-overtime-report, li.menu-overtime-report  li.menu-overtime-sub-overtime-summary').addClass('active');
    
    var select_year = $('select[name=selectYear]');
    var preview = $('a[name=preview]');
    var d = new Date();
    var year = d.getFullYear();
    select_year.html('');
    select_year.append(o);
    year_limit = year - 10;
    for (index = year; index > year_limit; index--) {
        var o = new Option(index, index);
        select_year.append(o);
    }

    var selected_year = select_year.val();
    preview.attr("href", host + 'over_time_summary/index/' + selected_year);

    select_year.on('change', function () {
        selected_year = select_year.val();
        preview.attr("href", host + 'over_time_summary/index/' + selected_year);
        console.log(selected_year);
    });
});