$(document).ready(function() {
    $('li.menu-schedule').addClass('active');

    $('.datepicker').css('z-index', '99999');

    $('select#siteSelect').select2();
    var select_site = $('select[name=siteSelect]');
    var datepicker = $('#datepicker');
    var now_date = new Date();
    var selected_date = now_date.getFullYear() + '-'+ (now_date.getMonth()+1) + '-' + now_date.getDate();
    var current_schedule_id;

    $('input[name=inputScheduleDate]').datepicker({
        language: 'en',
        multipleDatesSeparator: ', ',
        multipleDates: true,
        dateFormat: "yyyy/mm/dd",
        todayButton: true,
        navTitles: {
            days: 'mm-yyyy'
        }
    });

    function load_group_and_shift(){
        $.ajax({
            type: "POST",
            url: host + 'ajax/get_all_groups_by_site',
            data: {site_id : select_site.val()}
            }).done(function (data){
                var select_shift_group_name = $("form#add-bulk-schedule-time #select-shift-group");
                select_shift_group_name.html('');
                var o = new Option('Select', '0');
                $(o).attr('selected','selected');
                select_shift_group_name.append(o);
                var shift_group = data.data;
                shift_group.forEach(function(val){
                    var o = new Option(val.shift_group_name, val.shift_group_id);
                    select_shift_group_name.append(o);
                });
            });
    
        $.ajax({
            type: "POST",
            url: host + 'ajax/get_all_shifts_by_site',
            data: {site_id: select_site.val()}
            }).done(function (data){
                var select_shift_time = $("form#add-bulk-schedule-time #select-shift-time");
                select_shift_time.html('');
                var o = new Option('OFF', '0');
                $(o).attr('selected','selected');
                select_shift_time.append(o);
                var shift_time = data.data;
                shift_time.forEach(function(val){
                    var o = new Option(val.shift_time_name, val.shift_time_id);
                    select_shift_time.append(o);
                });

                var advance_option_modal = $("form#advance-option #select-shift-time");
                advance_option_modal.html($("#select-shift-time").clone().html());
                advance_option_modal.find('option:first').remove();

                var on_leave = new Option('OFF', '-1');
                var select = new Option('Select', '0');
                
                $("form#advance-option #select-shift-time option:first").before(select);
                $("form#advance-option #select-shift-time option:first").after(on_leave);
            });
    }

    load_group_and_shift();

    var schedule_datatable = $('#scheduleDataTable').DataTable({
        lengthChange: false,
        searching: false,
        ordering: false,
        processing: true,
        serverSide: true,
        language: {
            emptyTable: "No group found"
          },
        ajax: {
            url: host + 'ajax/schedule_datatable',
            method: "POST",
            data: function( d ){
                d.site_id = select_site.val();
                d.date = selected_date;
            }
        },
        columns: [
            { 
                data: null,
                render: function ( data, type, row ) {
                    return data.shift_group_name;
                },
                width: "45%"
            },
            { 
                data: null,
                render: function ( data, type, row ) {
                    // return data.shift_name ? data.shift_name : '-';
                    var opt = $("<select>").attr('name','shiftSelect').attr('id', 'shiftSelect');
                    opt.html($("#select-shift-time").clone().html());
                    opt.find('option[value='+ data.shift_time_id +']').attr('selected','selected');
                    return opt[0].outerHTML;
                },
                width: "45%"
            },
            { 
                data: null,
                render: function ( data, type, row ) {
                    var disabled = '';
                    if(!data.schedule_id){
                        disabled = 'disabled';
                    }
                    return '<button id="'+ data.shift_group_id +'" type="button" class="btn btn-primary btn-sm btn-advance-option" '+disabled+'>'
                    +   '<i class="fas fa-user-cog"></i>'
                    + '</button>';
                },
                width: "10%"
            }
        ]
    });

    var schedule_advanced_datatable = $('#scheduleAdvanceDataTable').DataTable({
        lengthChange: false,
        searching: false,
        ordering: false,
        processing: true,
        serverSide: true,
        language: {
            emptyTable: "No data available"
          },
        ajax: {
            url: host + 'ajax/datatable_schedule_advanced',
            method: "POST",
            data: function( d ){
                d.site_id = select_site.val();
                d.date = selected_date;
            }
        },
        columns: [
            { 
                data: "employee_id",
                width: "20%"
            },
            { 
                data: null,
                "render": function ( data, type, row ) {
                    return data.last_name ? data.first_name + ' ' + data.last_name : data.first_name;
                },
                width: "35%"
            },
            { 
                data: null,
                "render": function ( data, type, row ) {
                    return data.shift_time_id ? data.shift_time_name : 'Select';
                },
                width: "35%"
            },
            { 
                data: null,
                "render": function ( data, type, row ) {
                    return '<button id="'+ data.schedule_advanced_id +'" type="button" class="btn btn-danger btn-sm btn-delete-advance-option">'
                    +   '<i class="fas fa-trash"></i>'
                    + '</button>';
                },
                width: "10%"
            }
        ]
    });

    select_site.on('change', function(){
        last_selected_site = $(this).val();
        load_group_and_shift();
        schedule_datatable.draw();
        schedule_advanced_datatable.draw();
    });

    datepicker.datepicker({
        onSelect: function (fd, d, picker) {
            selected_date = d.getFullYear() + '-'+ (d.getMonth()+1) + '-' + d.getDate();
            schedule_datatable.draw();
            schedule_advanced_datatable.draw();
        }
    });

    $('form#add-bulk-schedule-time').submit(function (event) {
        var data = {
            'inputScheduleDate': $(this).find('input[name=inputScheduleDate]').val(),
            'site_id': select_site.val(),
            'inputShiftGroupId': $(this).find('select[name=inputShiftGroup]').val(),
            'inputShiftTimeId': $(this).find('select[name=inputShiftTime]').val()
        };
        $.ajax({
            type: 'POST',
            url: host + 'ajax/add_bulk_schedule',
            data: data
        })
        .done(function (data) {
            if (data.status) {
                $('#addBulkScheduleModal').modal('hide');
                schedule_datatable.draw();
                Toast.fire({
                    type: 'success',
                    title: 'Success.'
                    });
                }else{
                    Toast.fire({
                        type: 'info',
                        title: 'There is no data stored, maybe data is already available.'
                        });
                }
        })
        .fail(function () {
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
                });
        });
        event.preventDefault();
    });

    $(document).on('click','button.btn-advance-option', function(){
        var selected_row = $(this).parent('td');
        var cell = schedule_datatable.cell(selected_row);
        var data = cell.data();

        current_schedule_id = data.schedule_id;

        $.ajax({
            type: "POST",
            url: host + 'ajax/get_all_employee_by_shift_group',
            data: {shift_group_id : data.shift_group_id}
            }).done(function (data){
                var select_employee = $("form#advance-option #select-employee");
                select_employee.html('');
                var o = new Option('Select', '0');
                $(o).attr('selected','selected');
                select_employee.append(o);
                var members = data.members;
                members.forEach(function(val){
                    var name = val.last_name ? val.first_name + ' ' + val.last_name : val.first_name;
                    var o = new Option(name, val.employee_id);
                    select_employee.append(o);
                });
            });
        
        $('form#advance-option').trigger('reset');
        $('form#advance-option select[name=inputEmployee]').removeClass('is-invalid');
        $('form#advance-option select[name=inputShiftTime]').removeClass('is-invalid');

        $('#advanceOptionModal').modal('show');
    });

    $('form#advance-option').submit(function(event){
        var employee_id = $(this).find('select[name=inputEmployee]').val();
        var shift_time_id = $(this).find('select[name=inputShiftTime]').val();
        
        if(employee_id == 0){
            $(this).find('select[name=inputEmployee]').addClass('is-invalid');
        }else{
            $(this).find('select[name=inputEmployee]').removeClass('is-invalid');
        }
        
        if(shift_time_id == 0){
            $(this).find('select[name=inputShiftTime]').addClass('is-invalid');
        }else{
            $(this).find('select[name=inputShiftTime]').removeClass('is-invalid');
        }
        
        if(shift_time_id != 0 && employee_id != 0){
            $.ajax({
                type: "POST",
                url: host + 'ajax/add_schedule_advanced',
                data: {
                    schedule_id: current_schedule_id,
                    employee_id : employee_id,
                    shift_time_id : shift_time_id
                }
                }).done(function (data){
                    if(data.status){
                        Toast.fire({
                            type: 'success',
                            title: 'Success.'
                          });
                        schedule_advanced_datatable.draw(); 
                        $('#advanceOptionModal').modal('hide');                       
                    }else{
                        Toast.fire({
                            type: 'error',
                            title: 'Error.'
                          });
                    }
                }).fail(function(){
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                      });
                });;
        }
        event.preventDefault();
    });

    var previous_shift;
    $(document).on('focus', 'select#shiftSelect', function () {
        // Store the current value on focus and on change
        previous_shift = this.value;
    }).on('change', 'select#shiftSelect', function () {
        var shift_id = this.value;
        var selected_row = $(this).parent('td');
        var cell = schedule_datatable.cell(selected_row);
        var data = cell.data();

        var data = {
            schedule_id : data.schedule_id,
            schedule_date : selected_date,
            site_id : data.site_id,
            shift_group_id : data.shift_group_id,
            shift_time_id : shift_id
        };

        if(shift_id == 0){
            $.ajax({
                type: "POST",
                url: host + 'ajax/delete_shift_group_schedule',
                data: data
                }).done(function (data){
                    if(data.status){
                            Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                        schedule_datatable.draw();
                        schedule_advanced_datatable.draw();
                    }
                    else
                         Swal.fire({ type: 'error', title: 'Error' });
                });
        }else if(previous_shift == 0){
            $.ajax({
                type: "POST",
                url: host + 'ajax/add_shift_group_schedule',
                data: data
                }).done(function (data){
                    if(data.status){
                            Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                        schedule_datatable.draw();
                        schedule_advanced_datatable.draw();
                    }
                    else
                         Swal.fire({ type: 'error', title: 'Error' });
                });
        }else{
            $.ajax({
                type: "POST",
                url: host + 'ajax/update_shift_group_schedule',
                data: data
                }).done(function (data){
                    if(data.status){
                            Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
                        schedule_datatable.draw();
                        schedule_advanced_datatable.draw();
                    }
                    else
                         Swal.fire({ type: 'error', title: 'Error' });
                });
        }
        previous_shift = shift_id;
    });

    $(document).on('click', '.btn-delete-advance-option', function(){
        var schedule_advanced_id = $(this).attr('id');
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success ml-1',
              cancelButton: 'btn btn-danger mr-1'
            },
            buttonsStyling: false,
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type        : 'POST',
                    url         : host + 'ajax/delete_schedule_advaced',
                    data        : {schedule_advanced_id:schedule_advanced_id}
                })
                .done(function(data){
                    if(data.result){
                        Toast.fire({
                            type: 'success',
                            title: 'Success.'
                          });
                        schedule_advanced_datatable.draw();
                    }else{
                        Toast.fire({
                            type: 'error',
                            title: 'Data cannot be deleted.'
                          });
                    }
                })
                .fail(function(){
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                      });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          })
    });

    $(document).on('click', '.btn-preview', function(){
        window.open(`${host}schedule/preview/${select_site.val()}/${selected_date}`,'_blank');
    });
});