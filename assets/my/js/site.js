$(document).ready(function() {
    $('li.menu-site').addClass('active');

    var last_selected = null;

    var site_datatable = {
        table : $('#siteDataTable').DataTable({
            searchDelay: 2000,
            ordering : false,
            processing: true,
            serverSide: true,
            language: {
                emptyTable: "No group found"
              },
            ajax: {
                url: host + 'ajax/site_datatable',
                method: "POST"
            },
            columns: [
                { 
                    data: "site_name",
                    width: "20%"
                },
                { 
                    data: "site_address",
                    width: "70%"
                },
                { 
                    data: null,
                    render: function ( data, type, row ) {
                        return    '<button id="'+ data.site_id +'" type="button" class="btn btn-primary btn-sm btn-edit-site">'
                                +   '<i class="far fa-edit"></i>'
                                + '</button>'
                                + '<button id="'+ data.site_id +'" type="button" class="btn btn-danger btn-sm btn-delete-site">'
                                +   '<i class="fas fa-trash"></i>'
                                + '</button>';
                    },
                    width: "10%"
                }
            ]
        })
    };

    $('form#edit-site').submit(function(event){
        var data = {
            'inputSiteName' : $(this).find('input[name=inputName]').val(),
            'inputSiteAddress' : $(this).find('input[name=inputAddress]').val(),
            'inputSiteID' : last_selected.site_id
        };

        $.ajax({
            type        : 'POST',
            url         : host + 'ajax/update_site',
            data        : data,
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function(data){
            Swal.close();
            if(data.status){
                $('#editSiteModal').modal('hide');
                site_datatable.table.ajax.reload();
                site_datatable.table.draw();
                Toast.fire({
                    type: 'success',
                    title: 'Update success.'
                  });
            }else{
                Toast.fire({
                    type: 'info',
                    title: 'No data changed.'
                  });
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
              });
        });
        event.preventDefault();
    });

    $('form#add-new-site').submit(function(event){
        var data = {
            'inputSiteName' : $(this).find('input[name=inputName]').val(),
            'inputSiteAddress' : $(this).find('input[name=inputAddress]').val()
        };

        $.ajax({
            type        : 'POST',
            url         : host + 'ajax/new_site',
            data        : data,
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function(data){
            Swal.close();
            if(data.status){
                $('#addNewSiteModal').modal('hide');
                site_datatable.table.ajax.reload();
                site_datatable.table.draw();
                Toast.fire({
                    type: 'success',
                    title: 'Success.'
                  });
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
              });
        });

        event.preventDefault();
    });

    $(document).on('shown.bs.modal', '#addNewSiteModal', function () {
        $('form#add-new-site').trigger('reset');
    });

    $(document).on('click','.btn-edit-site',function(e) {
        var selected_row = $(this).parent('td');
        var cell = site_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;

        $('#editSiteModal').modal('show');
        
        var form = $('form#edit-site');
        form.trigger('reset');
        form.find('input[name=inputName]').val(data.site_name);
        form.find('input[name=inputAddress]').val(data.site_address);
    });

    $(document).on('click', '.btn-delete-site', function(e){
        var selected_row = $(this).parent('td');
        var cell = site_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success ml-1',
              cancelButton: 'btn btn-danger mr-1'
            },
            buttonsStyling: false,
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                $.ajax({
                    type        : 'POST',
                    url         : host + 'ajax/delete_office',
                    data        : {site_id:last_selected.site_id}
                })
                .done(function(data){
                    if(data.result){
                        Toast.fire({
                            type: 'success',
                            title: 'Success.'
                          });
                        site_datatable.table.ajax.reload();
                        site_datatable.table.draw();
                    }else{
                        Toast.fire({
                            type: 'error',
                            title: 'Data cannot be deleted.'
                          });
                    }
                })
                .fail(function(){
                    Toast.fire({
                        type: 'error',
                        title: "The server doesn't respond, try again in a few moments"
                      });
                });
            } else if (
              result.dismiss === Swal.DismissReason.cancel
            ) {
                Toast.fire({
                    type: 'error',
                    title: 'Cancelled.'
                  });
            }
          })
    });
});