$(document).ready(function() {
    $('li.menu-shift, li.menu-shift li.menu-shift-sub-shift-group').addClass('active');

    var selected_employee = [];

    // EDIT
    var options = $('select[name=input-employees] option');
    for (let index = 0; index < options.length; index++) {
        selected_employee.push(options[index].value);
    }

    console.log(selected_employee);

    var employee_datatable = {
        table : $('#employeeDataTable').DataTable({
            'lengthMenu': [[5, 10], [5, 10]],
            'ordering' : false,
            'processing': true,
            'serverSide': true,
            ajax: {
                url: host + 'ajax/get_employee_data_table',
                method: "POST"
            },
            columns: [
                { data: "employee_id" },
                { 
                    data: null,
                    "render": function ( data, type, row ) {
                        return data.last_name ? data.first_name + ' ' + data.last_name : data.first_name;
                    } 
                },
                { 
                    data: null,
                    "render": function ( data, type, row ) {
                        var btn = '';
                        if (selected_employee.indexOf(data.employee_id) != -1) {
                            btn = '<button id="' + data.employee_id + '" type="button" class="btn btn-sm btn-select-delete btn-danger">' +
                                'Unselect' +
                                '</button>';
                        }else{
                            btn = '<button id="' + data.employee_id + '" type="button" class="btn btn-success btn-sm btn-select-add">' +
                                'Select' +
                                '</button>';
                        }
                        return btn;
                    } 
                }
            ]
        })
    };

    $('form#add-new-shift-group').submit(function(event){
        if(selected_employee.length >0){
            var data = [];

            selected_employee.forEach(function(val) {
                data.push(val);
            });

            var variables = {
                shift_name : $(this).find('input[name=inputName]').val(),
                site_id : $(this).find('select[name=siteSelect]').val(),
                employee : data
            }

            $.ajax({
                type        : 'POST',
                url         : host + 'ajax/add_shift_group',
                data        : variables,
                beforeSend  : function(){
                    Swal.fire({
                        imageUrl: host + '../assets/images/load.gif',
                        imageAlt: ' ',
                        showConfirmButton: false,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        title: 'Loading ...'
                    });
                }
            })
            .done(function(data){
                Swal.close();
                if(data.status){
                    setTimeout(function(){ location.reload(); }, 1000);
                    Toast.fire({
                        type: 'success',
                        title: 'Success.'
                    });
                }
            })
            .fail(function(){
                Swal.close();
                Toast.fire({
                    type: 'error',
                    title: "The server doesn't respond, try again in a few moments"
                    });
            });
        }else{
            Toast.fire({
                type: 'warning',
                title: 'No employee seleted'
                });
        }

        event.preventDefault();
    });

    $('select#input-employees').on('click', function(){
        $('#selectEmployeeModal').modal('show');
    });

    $(document).on('click', '.btn-select-add', function(e){
        var selected_row = $(this).parent('td');
        var cell = employee_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;

        var string = data.employee_id + " - " + data.first_name;

        if(data.last_name)
            string += ' ' + data.last_name;
        
        if(!selected_employee.includes(data.employee_id)){
            selected_employee.push(data.employee_id);
            $('select[name=input-employees]').append('<option value="'+data.employee_id+'">'+string+'</option>');
            $('span .badge.badge-light').html(selected_employee.length);
        }

        $(this).removeClass('btn-select-add btn-success').addClass('btn-select-delete btn-danger').html('Unselect');

        console.log(selected_employee);
    });


    $(document).on('click', '.btn-select-delete', function(e){
        var selected_row = $(this).parent('td');
        var cell = employee_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;
        
        if(selected_employee.includes(data.employee_id)){
            delete selected_employee[selected_employee.indexOf(data.employee_id)];
            selected_employee = selected_employee.filter(function (el) {
                return el != null;
              });
              $('select[name=input-employees]').find('option[value="'+data.employee_id+'"]').remove();
              $('span .badge.badge-light').html(selected_employee.length);
        }

        $(this).removeClass('btn-select-delete btn-danger').addClass('btn-select-add btn-success').html('Select');
        
        console.log(selected_employee);
    });
});