$(document).ready(function() {
    $('li.menu-shift, li.menu-shift li.menu-shift-sub-shift-group').addClass('active');
    
    var selected_employee_count = $('span .badge.badge-light');
    var selected_employee = [];
    var selected_employee_old = [];

    // EDIT
    var options = $('select[name=input-employees] option');
    for (let index = 0; index < options.length; index++) {
        selected_employee.push(options[index].value);
    }

    selected_employee_old = selected_employee.slice();

    selected_employee_count.html(selected_employee.length);

    var employee_datatable = {
        table : $('#employeeDataTable').DataTable({
            'lengthMenu': [[5, 10], [5, 10]],
            'ordering' : false,
            'processing': true,
            'serverSide': true,
            ajax: {
                url: host + 'ajax/get_employee_data_table',
                method: "POST"
            },
            columns: [
                { data: "employee_id" },
                { 
                    data: null,
                    "render": function ( data, type, row ) {
                        return data.last_name ? data.first_name + ' ' + data.last_name : data.first_name;
                    } 
                },
                { 
                    data: null,
                    "render": function ( data, type, row ) {
                        var btn = '';
                        if (selected_employee.indexOf(data.employee_id) != -1) {
                            btn = '<button id="' + data.employee_id + '" type="button" class="btn btn-sm btn-select-delete btn-danger">' +
                                'Unselect' +
                                '</button>';
                            console.log('include');
                        }else{
                            btn = '<button id="' + data.employee_id + '" type="button" class="btn btn-success btn-sm btn-select-add">' +
                                'Select' +
                                '</button>';
                            console.log('not include');
                        }
                        return btn;
                    } 
                }
            ]
        })
    };

    $('form#update-shift-group').submit(function(event){
        var update_members = [];
        var delete_members = [];

        var selected_employee_new = selected_employee;

        console.log(selected_employee_old);
        console.log(selected_employee_new);

        selected_employee_new.forEach(function(val) {
            if(!selected_employee_old.includes(val)){
                update_members.push(val);
            }
        });
        
        selected_employee_old.forEach(function(val) {
            if(!selected_employee_new.includes(val)){
                delete_members.push(val);
            }
        });

        var variables = {
            shift_name : $(this).find('input[name=inputName]').val(),
            group_id : $(this).find('input[name=groupId]').val(),
            update_members : update_members,
            delete_members : delete_members
        }

        $.ajax({
            type        : 'POST',
            url         : host + 'ajax/update_shift_group',
            data        : variables,
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function(data){
            Swal.close();
            Toast.fire({
                type: 'success',
                title: 'Success.'
            });
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
                });
        });
        event.preventDefault();
    });

    $('select#input-employees').on('click', function(){
        $('#selectEmployeeModal').modal('show');
    });

    $(document).on('click', '.btn-select-add', function(e){
        var selected_row = $(this).parent('td');
        var cell = employee_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;

        var string = data.employee_id + " - " + data.first_name;

        if(data.last_name)
            string += ' ' + data.last_name;
        
        if(!selected_employee.includes(data.employee_id)){
            selected_employee.push(data.employee_id);
            $('select[name=input-employees]').append('<option value="'+data.employee_id+'">'+string+'</option>');
            selected_employee_count.html(selected_employee.length);
        }

        $(this).removeClass('btn-select-add btn-success').addClass('btn-select-delete btn-danger').html('Unselect');
    });


    $(document).on('click', '.btn-select-delete', function(e){
        var selected_row = $(this).parent('td');
        var cell = employee_datatable.table.cell(selected_row);
        var data = cell.data();
        last_selected = data;
        
        if(selected_employee.includes(data.employee_id)){
            delete selected_employee[selected_employee.indexOf(data.employee_id)];
            selected_employee = selected_employee.filter(function () { return true });
            $('select[name=input-employees]').find('option[value="'+data.employee_id+'"]').remove();
            selected_employee_count.html(selected_employee.length);
        }

        $(this).removeClass('btn-select-delete btn-danger').addClass('btn-select-add btn-success').html('Select');
    });
});