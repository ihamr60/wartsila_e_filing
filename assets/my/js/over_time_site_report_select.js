$(document).ready(function () {
    $('li.menu-overtime-report, li.menu-overtime-report  li.menu-overtime-sub-overtime-site-report').addClass('active');
    var select_site = $('select[name=selectSite]');
    var select_month = $('select[name=selectMonth]');
    var select_year = $('select[name=selectYear]');
    var preview = $('a[name=preview]');
    var selected_site = select_site.val();
    var selected_month = select_month.val();
    

    select_site.on('change', function () {
        selected_site = select_site.val();
        
        // console.log(selected_site);
        $.ajax({
            type        : 'POST',
            url         : host + 'ajax/get_existing_month_and_year_site_report',
            data        : {site_id:selected_site},
            beforeSend  : function(){
                Swal.fire({
                    imageUrl: host + '../assets/images/load.gif',
                    imageAlt: ' ',
                    showConfirmButton: false,
                    showCancelButton: false,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    title: 'Loading ...'
                });
            }
        })
        .done(function(data){
            Swal.close();
            if(data.years && data.months){
                select_month.html('');
                select_month.append(new Option('Select Month', '-1'));
                for (let index = 0; index < data.months.length; index++) {
                    select_month.append(new Option(data.months[index]['formated_month'], data.months[index]['month']));
                    if(index == 0){
                        selected_month = data.months[index]['month'];
                        select_month.val(selected_month);
                    }
                }
                select_year.html('');
                select_year.append(new Option('Select Year', '-1'));
                for (let index = 0; index < data.months.length; index++) {
                    select_year.append(new Option(data.years[index]['year'], data.years[index]['year']));
                    if(index == 0){
                        selected_year = data.years[index]['year'];
                        select_year.val(selected_year);
                    }
                }
            }else{
                Toast.fire({
                    type: 'info',
                    title: "Data Not Found"
                    });
            }
        })
        .fail(function(){
            Swal.close();
            Toast.fire({
                type: 'error',
                title: "The server doesn't respond, try again in a few moments"
                });
        });
    });
    
    $('form#selection-form').submit(function (event) {
        var selected_site = select_site.val();
        var selected_month = select_month.val();
        var selected_year = select_year.val();

        if(selected_year == -1 && selected_month == -1)
            Toast.fire({
                type: 'error',
                title: "No Selected Month/Year"
                });
        else
            window.open(host + 'over_time_site_report/index/' + selected_site + '/' + selected_month + '/' + selected_year,"_self");
                
        event.preventDefault();
    });
});