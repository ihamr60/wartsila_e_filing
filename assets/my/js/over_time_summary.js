$(document).ready(function () {
    $('li.menu-overtime-report, li.menu-overtime-report  li.menu-overtime-sub-overtime-summary').addClass('active');
    var base_url = window.location.pathname;
    var get_start = base_url.lastIndexOf('/');
    var lastchar = base_url.length;
    var year = base_url.substr((get_start+1), (lastchar -1));
    console.log(year);
    $.ajax({
        type: 'POST',
        url: host + 'ajax/overtime_report_summary',
        data : {
            'year' : year
        }
    }).done(function (data){
        var chart_data = new Array();
        var summary_per_dept = data.per_dept_per_month;
        var labels = Object.keys(data.per_dept_per_month[0]);
        labels = labels.filter(function (key) {
            return (key != 'date');
        });
        date_label = summary_per_dept.map(function (name) {
            return name.date;
        });
        date_label.forEach(axes => {
            var index = summary_per_dept.map(function (e) { return e.date; }).indexOf(axes);
            var index_value = summary_per_dept[index];
            var current_data = new Array();
            labels.forEach(value => {
                current_data.push(index_value[value]);
            });
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }        
            var current_data_chart = {
                label: axes,
                data: current_data,
                backgroundColor: color,
                borderWidth: 0
            };
            chart_data.push(current_data_chart);
            });
        
        var yAxesData = new Array();
        var set_zero = { ticks: { beginAtZero: true}};
        yAxesData.push(set_zero);
        var chartOptions = {
            scales: {
                xAxes: [{
                    barPercentage: 1,
                    categoryPercentage: 0.6
                }],
                yAxes: yAxesData
            }
        };

        var summary_chart_data = {
            labels: labels,
            datasets: chart_data
        };
        var ot_summary = document.getElementById('chartOvertimeSummary').getContext('2d');
        var ot_summary_chart = new Chart(ot_summary, {
            type: 'bar',
            data: summary_chart_data,
            options: chartOptions
        });
        
        console.log(data);
        var summary_per_month = data.per_month;
        var month_labels = summary_per_month.map(function (e) { return e.date; }) ;
        var month_value = summary_per_month.map(function (e) { return e.total_works; });
        console.log(month_labels);
        var bgcolor = new Array();
        month_labels.forEach(value => {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            bgcolor.push(color);
        });
        var summary_per_month_data_sets = new Array();
        var current_data_per_month = {
            label: 'Report',
            data: month_value,
            backgroundColor: bgcolor,
            borderWidth: 0
        };
        summary_per_month_data_sets.push(current_data_per_month);
        var summary_per_month_data_chart = {
            labels: month_labels,
            datasets: summary_per_month_data_sets
        };
        var ot_summary_per_month = document.getElementById('chartOvertimeSummaryPerMonth').getContext('2d');
        var ot_summary_chart_per_month = new Chart(ot_summary_per_month, {
            type: 'bar',
            data: summary_per_month_data_chart,
            options: chartOptions
        });
    });

    $(document).on('click','.btnPrint',function(e) {
        window.print();
    });
});