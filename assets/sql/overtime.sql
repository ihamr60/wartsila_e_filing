-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2019 at 10:40 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `overtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` varchar(25) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `first_name`, `last_name`) VALUES
('001201500001', 'Ersapta', NULL),
('001201500002', 'Arie', 'Sukma'),
('001201500003', 'Desriel', 'Muhammad'),
('001201500028', 'Teuku', 'Hilman');

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `office_id` varchar(32) NOT NULL,
  `office_name` varchar(255) NOT NULL,
  `office_address` varchar(255) DEFAULT NULL,
  `line_manager_id` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`office_id`, `office_name`, `office_address`, `line_manager_id`) VALUES
('3CE8C8937F3011E9A9C0ACE010195876', 'Kantor cabang jaksel', 'Jln. Anyer Raya No 384 D', '001201500002'),
('89E5D10C7F1F11E9A9C0ACE010195876', 'Timor Timur', 'Ibukota timor leste', '001201500002'),
('CD96CFAF86EC11E98FB0887873212BEE', 'Kantor Cabang Jakut', 'Jalan Kelapa Gading', '001201500001'),
('DABB4D207F3011E9A9C0ACE010195876', 'Kantor cabang jakbar', 'Jln. Anyer Raya', '001201500028'),
('DEE4F09C7F1E11E9A9C0ACE010195876', 'Main Site', 'Halim perdana kusuma', '001201500002');

-- --------------------------------------------------------

--
-- Table structure for table `office_hours`
--

CREATE TABLE `office_hours` (
  `office_hours_id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `office_id` varchar(32) CHARACTER SET utf8 NOT NULL,
  `sunday_in` time NOT NULL DEFAULT '00:00:00',
  `monday_in` time NOT NULL DEFAULT '00:00:00',
  `tuesday_in` time NOT NULL DEFAULT '00:00:00',
  `wednesday_in` time NOT NULL DEFAULT '00:00:00',
  `thursday_in` time NOT NULL DEFAULT '00:00:00',
  `friday_in` time NOT NULL DEFAULT '00:00:00',
  `saturday_in` time NOT NULL DEFAULT '00:00:00',
  `sunday_out` time NOT NULL DEFAULT '00:00:00',
  `monday_out` time NOT NULL DEFAULT '00:00:00',
  `tuesday_out` time NOT NULL DEFAULT '00:00:00',
  `wednesday_out` time NOT NULL DEFAULT '00:00:00',
  `thursday_out` time NOT NULL DEFAULT '00:00:00',
  `friday_out` time NOT NULL DEFAULT '00:00:00',
  `saturday_out` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Dumping data for table `office_hours`
--

INSERT INTO `office_hours` (`office_hours_id`, `office_id`, `sunday_in`, `monday_in`, `tuesday_in`, `wednesday_in`, `thursday_in`, `friday_in`, `saturday_in`, `sunday_out`, `monday_out`, `tuesday_out`, `wednesday_out`, `thursday_out`, `friday_out`, `saturday_out`) VALUES
('4EAD751183AC11E9AD75ACE010195876', 'DEE4F09C7F1E11E9A9C0ACE010195876', '00:00:00', '08:00:00', '08:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '17:00:00', '17:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'),
('92B7557882B011E9AD75ACE010195876', '89E5D10C7F1F11E9A9C0ACE010195876', '00:00:00', '04:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '10:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'),
('A2C3554485C911E9B8B7887873212BEE', '3CE8C8937F3011E9A9C0ACE010195876', '00:00:00', '08:00:00', '08:00:00', '08:00:00', '08:00:00', '08:00:00', '00:00:00', '00:00:00', '17:00:00', '17:00:00', '17:00:00', '17:00:00', '17:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `schedule_id` varchar(32) NOT NULL,
  `schedule_date` date NOT NULL,
  `office_id` varchar(32) NOT NULL,
  `shift_group_id` varchar(32) NOT NULL,
  `shift_time_id` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_id`, `schedule_date`, `office_id`, `shift_group_id`, `shift_time_id`) VALUES
('1D23EA79896411E98AC5887873212BEE', '2019-06-02', '3CE8C8937F3011E9A9C0ACE010195876', 'E35FEDF0846E11E9AD75ACE010195876', '000093EE86EC11E98FB0887873212BEE'),
('1D246164896411E98AC5887873212BEE', '2019-06-12', '3CE8C8937F3011E9A9C0ACE010195876', 'E35FEDF0846E11E9AD75ACE010195876', '000093EE86EC11E98FB0887873212BEE'),
('1D246C6E896411E98AC5887873212BEE', '2019-06-27', '3CE8C8937F3011E9A9C0ACE010195876', 'E35FEDF0846E11E9AD75ACE010195876', '000093EE86EC11E98FB0887873212BEE');

-- --------------------------------------------------------

--
-- Table structure for table `schedule_advanced`
--

CREATE TABLE `schedule_advanced` (
  `schedule_advanced_id` varchar(32) NOT NULL,
  `schedule_id` varchar(32) NOT NULL,
  `employee_id` varchar(25) NOT NULL,
  `shift_time_id` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shift_group`
--

CREATE TABLE `shift_group` (
  `shift_group_id` varchar(32) NOT NULL,
  `office_id` varchar(32) NOT NULL,
  `shift_group_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift_group`
--

INSERT INTO `shift_group` (`shift_group_id`, `office_id`, `shift_group_name`) VALUES
('E35FEDF0846E11E9AD75ACE010195876', '3CE8C8937F3011E9A9C0ACE010195876', 'Test Group'),
('FA5040C8855111E983A9ACE010195876', '3CE8C8937F3011E9A9C0ACE010195876', 'Group Baru');

-- --------------------------------------------------------

--
-- Table structure for table `shift_group_detail`
--

CREATE TABLE `shift_group_detail` (
  `shift_group_detail_id` varchar(32) NOT NULL,
  `shift_group_id` varchar(32) NOT NULL,
  `employee_id` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift_group_detail`
--

INSERT INTO `shift_group_detail` (`shift_group_detail_id`, `shift_group_id`, `employee_id`) VALUES
('E3774753846E11E9AD75ACE010195876', 'E35FEDF0846E11E9AD75ACE010195876', '001201500001'),
('E3775A0B846E11E9AD75ACE010195876', 'E35FEDF0846E11E9AD75ACE010195876', '001201500002'),
('FA653566855111E983A9ACE010195876', 'FA5040C8855111E983A9ACE010195876', '001201500001');

-- --------------------------------------------------------

--
-- Table structure for table `shift_time`
--

CREATE TABLE `shift_time` (
  `shift_time_id` varchar(32) NOT NULL,
  `shift_time_name` varchar(255) NOT NULL,
  `office_id` varchar(32) NOT NULL,
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `finish_time` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift_time`
--

INSERT INTO `shift_time` (`shift_time_id`, `shift_time_name`, `office_id`, `start_time`, `finish_time`) VALUES
('000093EE86EC11E98FB0887873212BEE', 'Shift Jaksel A', '3CE8C8937F3011E9A9C0ACE010195876', '13:23:00', '21:11:00'),
('089924A286ED11E98FB0887873212BEE', 'Shift F', '3CE8C8937F3011E9A9C0ACE010195876', '20:15:00', '04:15:00'),
('0D532B8286EC11E98FB0887873212BEE', 'Shift Jaksel B', '3CE8C8937F3011E9A9C0ACE010195876', '18:12:00', '01:12:00'),
('268CFF70891F11E98D77887873212BEE', 'Shift B', '89E5D10C7F1F11E9A9C0ACE010195876', '00:00:00', '05:30:00'),
('450BB01886EF11E98FB0887873212BEE', 'Shift B', '3CE8C8937F3011E9A9C0ACE010195876', '16:00:00', '23:59:00'),
('8F5EEA6E86ED11E98FB0887873212BEE', 'Shift S', '3CE8C8937F3011E9A9C0ACE010195876', '06:23:00', '15:23:00'),
('9EA0B1F686F011E98FB0887873212BEE', 'Shift B', '3CE8C8937F3011E9A9C0ACE010195876', '16:00:00', '08:35:00'),
('D78AF94C86F011E98FB0887873212BEE', 'Shift A', '3CE8C8937F3011E9A9C0ACE010195876', '06:31:00', '21:11:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`office_id`),
  ADD KEY `line_manager_id` (`line_manager_id`);

--
-- Indexes for table `office_hours`
--
ALTER TABLE `office_hours`
  ADD PRIMARY KEY (`office_hours_id`),
  ADD UNIQUE KEY `office_id` (`office_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`),
  ADD KEY `shift_time_id` (`shift_time_id`),
  ADD KEY `shift_group_id` (`shift_group_id`),
  ADD KEY `schedule_ibfk_5` (`office_id`);

--
-- Indexes for table `schedule_advanced`
--
ALTER TABLE `schedule_advanced`
  ADD PRIMARY KEY (`schedule_advanced_id`),
  ADD KEY `schedule_id` (`schedule_id`),
  ADD KEY `shift_time_id` (`shift_time_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `shift_group`
--
ALTER TABLE `shift_group`
  ADD PRIMARY KEY (`shift_group_id`),
  ADD KEY `office_id` (`office_id`);

--
-- Indexes for table `shift_group_detail`
--
ALTER TABLE `shift_group_detail`
  ADD PRIMARY KEY (`shift_group_detail_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `shift_group_id` (`shift_group_id`);

--
-- Indexes for table `shift_time`
--
ALTER TABLE `shift_time`
  ADD PRIMARY KEY (`shift_time_id`),
  ADD KEY `shift_time_ibfk_1` (`office_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `office`
--
ALTER TABLE `office`
  ADD CONSTRAINT `office_ibfk_1` FOREIGN KEY (`line_manager_id`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `office_hours`
--
ALTER TABLE `office_hours`
  ADD CONSTRAINT `office_hours_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_ibfk_3` FOREIGN KEY (`shift_group_id`) REFERENCES `shift_group` (`shift_group_id`),
  ADD CONSTRAINT `schedule_ibfk_4` FOREIGN KEY (`shift_time_id`) REFERENCES `shift_time` (`shift_time_id`),
  ADD CONSTRAINT `schedule_ibfk_5` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`);

--
-- Constraints for table `schedule_advanced`
--
ALTER TABLE `schedule_advanced`
  ADD CONSTRAINT `schedule_advanced_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `schedule_advanced_ibfk_4` FOREIGN KEY (`shift_time_id`) REFERENCES `shift_time` (`shift_time_id`),
  ADD CONSTRAINT `schedule_advanced_ibfk_5` FOREIGN KEY (`schedule_id`) REFERENCES `schedule` (`schedule_id`);

--
-- Constraints for table `shift_group`
--
ALTER TABLE `shift_group`
  ADD CONSTRAINT `shift_group_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`);

--
-- Constraints for table `shift_group_detail`
--
ALTER TABLE `shift_group_detail`
  ADD CONSTRAINT `shift_group_detail_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `shift_group_detail_ibfk_3` FOREIGN KEY (`shift_group_id`) REFERENCES `shift_group` (`shift_group_id`);

--
-- Constraints for table `shift_time`
--
ALTER TABLE `shift_time`
  ADD CONSTRAINT `shift_time_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
