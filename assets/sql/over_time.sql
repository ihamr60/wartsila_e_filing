-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2019 at 10:08 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `overtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `over_time`
--

CREATE TABLE `over_time` (
  `over_time_id` varchar(32) NOT NULL,
  `employee_id` varchar(32) NOT NULL,
  `office_id` varchar(32) NOT NULL,
  `overtime_date` date NOT NULL,
  `time` time NOT NULL DEFAULT '00:00:00',
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `over_time`
--

INSERT INTO `over_time` (`over_time_id`, `employee_id`, `office_id`, `overtime_date`, `time`, `reason`) VALUES
('1070E24E8B8A11E9B4EC887873212BEE', '001201500001', '3CE8C8937F3011E9A9C0ACE010195876', '2019-06-11', '02:12:00', ' update deadline kejar cepat selesai');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
