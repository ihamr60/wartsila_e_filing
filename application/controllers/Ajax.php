<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Ajax extends MY_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
    }
    
    public function new_site()
    {
        if ($this->form_validation->run() == TRUE){
            $this->load->model('Site_model');

            $site_name = $this->input->post('inputSiteName');
            $site_address = $this->input->post('inputSiteAddress');

            $status = $this->Site_model->create($site_name, $site_address);

            $this->renderJson(array(
                'status' => $status
            ));
        }else{
            $this->renderJson(array(
                'validation' => $this->form_validation->error_array()
            ));
        }
    }

    public function update_site()
    {
        if ($this->form_validation->run() == TRUE){
            $this->load->model('Site_model');
            
            $site_id = $this->input->post('inputSiteID');
            $site_name = $this->input->post('inputSiteName');
            $site_address = $this->input->post('inputSiteAddress');

            $status = $this->Site_model->update($site_id, $site_name, $site_address);

            $this->renderJson(array(
                'status' => $status
            ));
        }else{
            $this->renderJson(array(
                'validation' => $this->form_validation->error_array()
            ));
        }
    }

    public function employee_datalist()
    {
        if ($this->form_validation->run() == TRUE){
            $this->load->model('Employee_model');

            $keyword = $this->input->post('search');

            $employee = $this->Employee_model->datalist_employee($keyword);

            $this->renderJson(array(
                'employee' => $employee
            ));
        }else{
            $this->renderJson(array(
                'validation' => $this->form_validation->error_array()
            ));
        }
    }

    public function site_datatable()
    {
        $this->load->model('Site_model');

        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $total = $this->Site_model->total();
        
        $list_site = $this->Site_model->get($start, $length, $search);

        $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => ($search == '') ? $total : sizeof($list_site),
            'data' => $list_site
        ));
    }

    public function get_employee_data_table()
    {
        $this->load->model('Employee_model');

        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');

        $employees = $this->Employee_model->get($start, $length, $search);
        $total = $this->Employee_model->total();
      
        $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => ($search == '') ? $total : sizeof($employees),
            'data' => $employees
        ));
    }

    public function add_shift_group()
    {
        $this->load->model('Shift_group_model');

        $group_name = $this->input->post('shift_name');
        $site_id = $this->input->post('site_id');
        $employees_id = $this->input->post('employee');

        $this->renderJson(array(
            'status' => $this->Shift_group_model->create($group_name,$site_id,$employees_id)
        ));
    }

    public function get_shift_group_datatable()
    {
        $this->load->model('Shift_group_model');

        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $site_id = $this->input->post('site_id');

        $groups = $this->Shift_group_model->get($site_id, $start,$length,$search);
        $total = $this->Shift_group_model->total();

        for ($i=0; $i < sizeof($groups); $i++) { 
            $groups[$i]['members'] = $this->Shift_group_model->get_members($groups[$i]['shift_group_id']);
        }

        $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => sizeof($groups),
            'data' => $groups
        ));
    }

    public function update_shift_group()
    {
        $this->load->model('Shift_group_model');

        $shift_name = $this->input->post('shift_name');
        $group_id = $this->input->post('group_id');
        $delete_members = $this->input->post('delete_members');
        $update_members = $this->input->post('update_members');

        $return = array();

        if(isset($delete_members) && sizeof($delete_members) > 0)
            $return['delete_members'] = $this->Shift_group_model->delete_shift_members($group_id, $delete_members);
        
        if(isset($update_members) && sizeof($update_members) > 0)
            $return['update_members'] = $this->Shift_group_model->add_shift_members($group_id, $update_members);

        $return['shift_name'] = $this->Shift_group_model->update_shift_group($group_id, $shift_name);

        $this->renderJson($return);
    }

    public function add_shift_time() {

        $this->load->model('Shift_time_model');


        $shift_name = $this->input->post('inputShiftName');
        $time_in = $this->input->post('inputTimeIn');
        $time_out = $this->input->post('inputTimeOut');
        $site_id = $this->input->post('site_id');

        $status = $this->Shift_time_model->create($site_id, $shift_name, $time_in, $time_out);

        $this->renderJson(array(
                'status' => $status
        ));
    }

    public function shift_time_datatable()
    {
            $this->load->model('Shift_time_model');

            $draw = $this->input->post('draw');
            $search = $this->input->post('search')['value'];
            $start = $this->input->post('start');
            $length = $this->input->post('length');
            $site_id = $this->input->post('site_id');
            $total = $this->Shift_time_model->total();
            
            $shift_times = $this->Shift_time_model->get($site_id, $start,$length,$search);

            $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => $total,
            'recordsFiltered' => sizeof($shift_times),
            'data' => $shift_times
            ));
    }

    public function update_shift_time()
    {
        $this->load->model('Shift_time_model');

        $shift_name = $this->input->post('inputShiftName');
        $shift_time_id = $this->input->post('shiftTimeId');
        $time_in = $this->input->post('inputTimeIn');
        $time_out = $this->input->post('inputTimeOut');
        $site_id = $this->input->post('site_id');
        $status = $this->Shift_time_model->update($site_id, $shift_time_id, $shift_name, $time_in, $time_out);

        $this->renderJson(array(
                'status' => $status
        ));
    }

    public function schedule_datatable()
    {
        $this->load->model(array('Schedule_model', 'Shift_group_model'));

        $not_set = false;

        $draw = $this->input->post('draw');
        $site_id = $this->input->post('site_id');
        $date = $this->input->post('date');

        $data = $this->Schedule_model->get_group_shift_time_schedule($site_id,$date);

        if(sizeof($data) == 0){
            $not_set = true;
            $data = $this->Shift_group_model->get_all_groups_by_site($site_id);
        }

        $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => sizeof($data),
            'recordsFiltered' => sizeof($data),
            'data' => $data,
            'notSet' => $not_set
        ));
    }

    public function get_all_groups_by_site() 
    {
        $this->load->model('Shift_group_model');
        $site_id = $this->input->post('site_id');
        $data = $this->Shift_group_model->get_all_groups_by_site($site_id);
        $this->renderJson(array(
                'data' => $data
        ));
    }

    public function get_all_shifts_by_site()
    {
        $this->load->model('Shift_time_model');
        $site_id = $this->input->post('site_id');
        $data = $this->Shift_time_model->get_all_shifts_by_site($site_id);
        $this->renderJson(array(
                'data' => $data
        ));
    }

    public function add_bulk_schedule()
    {
        $this->load->model('Schedule_model');
        $schedule_date = $this->input->post('inputScheduleDate');
        $site_id = $this->input->post('site_id');
        $shift_group_id = $this->input->post('inputShiftGroupId');
        $shift_time_id = $this->input->post('inputShiftTimeId');

        $status = $this->Schedule_model->add_bulk_schedule($schedule_date, $site_id, $shift_group_id, $shift_time_id);
        $this->renderJson(array(
            'status' => $status
        ));
    }

    public function get_all_employee_by_shift_group()
    {
        $this->load->model('Shift_group_model');

        $shift_group_id = $this->input->post('shift_group_id');

        $this->renderJson(array(
            'members' => $this->Shift_group_model->get_members($shift_group_id)
        ));
    }

    public function add_over_time()  {
        $this->load->model(array('Overtime_model', 'Employee_model', 'Shift_group_model'));
        
        $employee_id = $_SESSION['user_id'];
        $site_id = $this->Shift_group_model->get_site_id_by_employee_id($employee_id);
        $superior_id = $this->Employee_model->getSuperior($employee_id);
        
        $overtime_date = $this->input->post('inputDate');
        $hours = $this->input->post('inputHours');
        $minutes = $this->input->post('inputMinutes');
        $purpose = $this->input->post('inputPurpose');

        if(!isset($_POST['inputDayStatus']))
            $day_status = $this->Overtime_model->is_holiday($overtime_date, $employee_id);
        else
            $day_status = $this->input->post('inputDayStatus');

        if ($this->Overtime_model->add_over_time($site_id, $employee_id, $superior_id, $overtime_date , ($hours.':'.$minutes), $purpose, $day_status)) {
            $employee = $this->Employee_model->get_by_id($employee_id);
            $superior = $this->Employee_model->get_by_id($superior_id);

            $values = array(
                'to' => $superior->mail_addr,
                'subject' => 'Request Overtime',
                'view' => 'email/emailTemplate.php'
            );
    
            $data = array(
                'to' => "Hi $superior->Known_As",
                'msg' => "$employee->Known_As just request overtime, please click link below to read more details.",
                'link' => array('url' => site_url(), 'text' => 'More details')
            );
    
            $sendEmailtoSuperior =  $this->sendEmail($values, $data);
            
            $this->session->set_flashdata('message', 'Your overtime request has been sent!');
            $this->renderJson(array(
                'status' => TRUE,
                'email' => $sendEmailtoSuperior
            ));
        }
        else {
            $this->renderJson(array(
                'status' => FALSE
            ));
        }
        
    }

    public function overtime_datatable_list()
    {
        $this->load->model(array('Overtime_model', 'Shift_time_model', 'Attandance_model'));

        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');

        $superior_id = $_SESSION['user_id'];
        $total = $this->Overtime_model->total_by_superior_id($superior_id);
        
        $overtimes = $this->Overtime_model->get_by_superior_id($superior_id,$start,$length,$search);

        $overtimes_new = [];

        foreach ($overtimes as $key => $overtime) {
            $overtime['info']['shift'] = $this->Shift_time_model->get_employee_shift_time($overtime['employee_id'],$overtime['overtime_date']);
            $overtime['info']['attendance'] = $this->Attandance_model->get_attendance($overtime['employee_id'],$overtime['overtime_date'],$overtime['info']['shift']['is_day_plus']);
            $overtimes_new[] =  $overtime;
        }

        $this->renderJson(array(
        'draw' => $draw,
        'recordsTotal' => $total,
        'recordsFiltered' => ($search == '') ? $total : sizeof($overtimes),
        'data' => $overtimes_new
        ));
    }

    public function overtime_datatable_history()
    {
        $this->load->model('Overtime_model');

        $draw = $this->input->post('draw');
        $search = $this->input->post('search')['value'];
        $start = $this->input->post('start');
        $length = $this->input->post('length');

        $employee_id = $_SESSION['user_id'];
        $total = $this->Overtime_model->total_by_employee_id($employee_id);
        
        $overtimes = $this->Overtime_model->get_by_employee_id($employee_id,$start,$length,$search);

        $this->renderJson(array(
        'draw' => $draw,
        'recordsTotal' => $total,
        'recordsFiltered' => ($search == '') ? $total : sizeof($overtimes),
        'data' => $overtimes
        ));
    }

    public function overtime_cancel_request()
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));
        $overtime_id = $this->input->post('overtime_id');

        return $this->renderJson(array(
            'status' => $this->Overtime_model->update_status($overtime_id, 'CANCEL')
        ));
    }

    public function overtime_update_status()
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));

        $overtime_id = $this->input->post('overtime_id');
        $status = $this->input->post('status');
        $employee_id = $this->input->post('user_request_id');
        
        if ($this->Overtime_model->update_status($overtime_id, $status)) {
            $employee = $this->Employee_model->get_by_id($employee_id);
            
            $superior = $this->Employee_model->get_by_id(($employee->Field26));
            if ($status == "CANCEL") {
                return $this->renderJson(array(
                    'status' => TRUE,
                ));
            }
            $values = array(
                'to' => $employee->mail_addr,
                'subject' => 'Request Overtime',
                'view' => 'email/emailTemplate.php'
            );
    
            $data = array(
                'to' => "Hi $employee->Known_As.",
                'msg' => "$superior->Known_As just $status your overtime request."
            );
            
            $sendEmailtoSuperior =  $this->sendEmail($values, $data);
            
            $this->renderJson(array(
                'status' => TRUE,
                'email' => $sendEmailtoSuperior
            ));
        }
        else {
            $this->renderJson(array(
                'status' => FALSE
            ));
        }
    }

    public function update_overtime_time()
    {
        $this->load->model('Overtime_model');
        $overtime_id = $this->input->post('overtime_id');
        $time = $this->input->post('inputHours');

        return $this->renderJson(array(
            'status' => $this->Overtime_model->update_overtime_time($overtime_id, $time)
        ));
    }

    public function update_overtime_extend_time()
    {
        $this->load->model('Overtime_model');
        $extend_id = $this->input->post('extend_id');
        $time = $this->input->post('inputHours');

        return $this->renderJson(array(
            'status' => $this->Overtime_model->update_overtime_extend_time($extend_id, $time)
        ));
    }

    public function overtime_extend_cancel()
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));
        $extend_id = $this->input->post('extend_id');

        return $this->renderJson(array(
            'status' => $this->Overtime_model->update_extend_status($extend_id, 'CANCEL'),
        ));
    }
    
    public function overtime_extend_update_status()
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));
        $employee_id = $this->input->post('user_request_id');
        $extend_id = $this->input->post('extend_id');
        $status = $this->input->post('status');
        
        if($this->Overtime_model->update_extend_status($extend_id, $status)){
            $employee = $this->Employee_model->get_by_id($employee_id);
            $superior = $this->Employee_model->get_by_id(($employee->Field26));

            if ($status == "CANCEL") {
                return $this->renderJson(array(
                    'status' => TRUE,
                ));
            }

            $values = array(
                'to' => $employee->mail_addr,
                'subject' => 'Request Overtime',
                'view' => 'email/emailTemplate.php'
            );
    
            $data = array(
                'to' => "Hi $employee->Known_As.",
                'msg' => "$superior->Known_As just $status your Extend Overtime request."
            );
    
            $sendEmailtoSuperior =  $this->sendEmail($values, $data);
            
            $this->renderJson(array(
                'status' => TRUE,
                'email' => $sendEmailtoSuperior
            ));
        }
        else {
            $this->renderJson(array(
                'status' => FALSE
            ));
        }
    }

    public function overtime_extend_add()
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));
        $employee_id = $this->session->user_id;
        $superior_id = $this->session->isHasSuperior;
        $overtime_id = $this->input->post('overTimeId');
        $time = $this->input->post('inputHours');
        $purpose = $this->input->post('inputPurpose');

        if ($this->Overtime_model->add_extend($overtime_id, $time, $purpose)) {
            $employee = $this->Employee_model->get_by_id($employee_id);
            $superior = $this->Employee_model->get_by_id($superior_id);

            $values = array(
                'to' => $superior->mail_addr ,
                'subject' => 'Request Extend Overtime',
                'view' => 'email/emailTemplate.php'
            );
    
            $data = array(
                'to' => "Hi $superior->Known_As",
                'msg' => "$employee->Known_As just request extend overtime, please click link below to read more details.",
                'link' => array('url' => site_url(), 'text' => 'More details')
            );
    
            $sendEmailtoSuperior =  $this->sendEmail($values, $data);
            
            $this->renderJson(array(
                'status' => TRUE,
                'email' => $sendEmailtoSuperior
            ));
        }
        else {
            $this->renderJson(array(
                'status' => FALSE
            ));
        }
    }

    public function add_schedule_advanced()
    {
        $this->load->model('Schedule_advance_model');

        $employee_id = $this->input->post('employee_id');
        $shift_time_id = $this->input->post('shift_time_id');
        $schedule_id = $this->input->post('schedule_id');

        if($shift_time_id == -1){
            $this->renderJson(array(
                'status' => $this->Schedule_advance_model->create($schedule_id, $employee_id),
            ));
        }else{
            $this->renderJson(array(
                'status' => $this->Schedule_advance_model->create($schedule_id, $employee_id, $shift_time_id)
            ));
        }
    }

    

    public function add_shift_group_schedule()
    {
        $this->load->model('Schedule_model');

        $schedule_date = $this->input->post('schedule_date');
        $site_id = $this->input->post('site_id');
        $shift_group_id = $this->input->post('shift_group_id');
        $shift_time_id = $this->input->post('shift_time_id');

        $this->renderJson(array(
            'status' => $this->Schedule_model->create($schedule_date, $site_id, $shift_group_id, $shift_time_id)
        ));
    }

    public function update_shift_group_schedule()
    {
        $this->load->model('Schedule_model');

        $schedule_id = $this->input->post('schedule_id');
        $schedule_date = $this->input->post('schedule_date');
        $site_id = $this->input->post('site_id');
        $shift_group_id = $this->input->post('shift_group_id');
        $shift_time_id = $this->input->post('shift_time_id');

        $this->renderJson(array(
            'status' => $this->Schedule_model->update($schedule_id, $schedule_date, $site_id, $shift_group_id, $shift_time_id)
        ));
    }

    public function delete_shift_group_schedule()
    {
        $this->load->model('Schedule_model');

        $schedule_id = $this->input->post('schedule_id');

        $this->renderJson(array(
            'status' => $this->Schedule_model->delete($schedule_id)
        ));
    }

    // public function test_disabled_date()
    // {
    //     $this->load->model('Schedule_model');

    //     $month = $this->input->post('month');
    //     $year = $this->input->post('year');
    //     $shift_group_id = $this->input->post('shift_group_id');
    //     $site_id = $this->input->post('site_id');

    //     $day = $this->Schedule_model->test_disabled_date($site_id, $shift_group_id, $month, $year);

    //     $data = array();
    //     foreach ($day as $val) {
    //         $data[] = intval($val['day']) - 1;
    //     }

    //     $this->renderJson(array(
    //         'disabled_day' => $data
    //     ));
    // }

    public function test_email()
    {
        $values = array(
            'to' => 'thilmanrevanda@gmail.com',
            'subject' => 'TestEmail',
            'view' => 'email/emailTemplate.php'
        );

        $data = array(
            'preheader' => '[preheader]',
            'to' => 'Hilman',
            'msg' => 'Sapta just requesting to overtime. please click the link below. :)',
            'link' => array('url' => './testlink', 'text' => '[button text]')
        );

        $this->renderJson(array(
            'email' => $this->sendEmail($values, $data)
        ));
    }

    public function delete_office()
    {
        $this->load->model('Site_model');

        $site_id = $this->input->post('site_id');
        $result = $this->Site_model->delete($site_id);

        $this->renderJson(array(
            'result' => $result
        ));
    }

    public function delete_shift_group()
    {
        $this->load->model('Shift_group_model');

        $shift_group_id = $this->input->post('shift_group_id');
        $result = $this->Shift_group_model->delete($shift_group_id);

        $this->renderJson(array(
            'result' => $result
        ));
    }

    public function delete_shift_time()
    {
        $this->load->model('Shift_time_model');

        $shift_time_id = $this->input->post('shift_time_id');
        $result = $this->Shift_time_model->delete($shift_time_id);

        $this->renderJson(array(
            'result' => $result
        ));
    }

    public function datatable_schedule_advanced()
    {
        $this->load->model('Schedule_advance_model');

        $draw = $this->input->post('draw');
        $site_id = $this->input->post('site_id');
        $date = $this->input->post('date');

        $data = $this->Schedule_advance_model->get_by_site_and_date($site_id,$date);

        $this->renderJson(array(
            'draw' => $draw,
            'recordsTotal' => sizeof($data),
            'recordsFiltered' => sizeof($data),
            'data' => $data
        ));
    }

    public function delete_schedule_advaced()
    {
        $this->load->model('Schedule_advance_model');

        $schedule_advanced_id = $this->input->post('schedule_advanced_id');
        $result = $this->Schedule_advance_model->delete($schedule_advanced_id);

        $this->renderJson(array(
            'result' => $result
        ));
    }

    public function overtime_report_summary()
    {
        $this->load->model('Overtime_model');
        $year = $this->input->post('year');
        $this->renderJson(array(
            'per_dept_per_month' => $this->Overtime_model->overtime_report_summary_per_dept_per_month($year),
            'per_month' => $this->Overtime_model->overtime_report_summary_per_month($year)
        ));
    }

    public function delete_shift_group_member()
    {
        $this->load->model('Shift_group_model');

        $shift_group_id = $this->input->post('shift_group_id');
        $employee_id = $this->input->post('employee_id');

        $this->renderJson(array(
            'result' => $this->Shift_group_model->delete_shift_members($shift_group_id, $employee_id)
        ));
    }

    public function get_existing_month_and_year_site_report()
    {
        $this->load->model('Overtime_model');

        $site_id = $this->input->post('site_id');

        $years = array();
        $months = array();

        $years = $this->Overtime_model->get_existing_year_by_site($site_id);
        if(sizeof($years) > 0)
            $months = $this->Overtime_model->get_existing_month_by_site_and_year($site_id, $years[0]['year']);

        $this->renderJson(array(
            'years' => $years,
            'months' => $months,
            'site_id' => $site_id
        ));
    }

    public function overtime_request_by_superior()
    {
        $this->load->model(array('Overtime_model', 'Employee_model', 'Shift_group_model'));
        
        $plant_manager = $this->input->post('inputPlantManager');
        $requestor = $this->session->user_id;
        $to_employee = $this->input->post('inputEmployee');
        $site_id = $this->Shift_group_model->get_site_id_by_employee_id($to_employee);
        $overtime_date = $this->input->post('inputDate');
        $hours = $this->input->post('inputHours');
        $minutes = $this->input->post('inputMinutes');
        $purpose = $this->input->post('inputPurpose');
        $time = (String) ($hours.':'.$minutes);

        if(!isset($_POST['inputDayStatus']))
            $day_status = $this->Overtime_model->is_holiday($overtime_date, $to_employee);
        else
            $day_status = $this->input->post('inputDayStatus');

        if ($this->Overtime_model->add_over_time_by_superior($site_id, $requestor, $to_employee, $plant_manager, $overtime_date , $time, $purpose, $day_status)) {
            $requestor_info = $this->Employee_model->get_by_id($requestor);
            $plant_manager_info = $this->Employee_model->get_by_id($plant_manager);
            $employee_info = $this->Employee_model->get_by_id($to_employee);

            $values = array(
                'to' => $plant_manager_info->mail_addr,
                'subject' => 'Request Overtime',
                'view' => 'email/emailTemplate.php'
            );
    
            $data = array(
                'to' => "Hi $plant_manager_info->Known_As",
                'msg' => "$requestor_info->Known_As just request overtime for $employee_info->Known_As, please click link below to read more details.",
                'link' => array('url' => site_url(), 'text' => 'More details')
            );
    
            $sendEmailtoSuperior =  $this->sendEmail($values, $data);
            
            $this->session->set_flashdata('message', 'Your overtime request has been sent to Plant Manager!');
            $this->renderJson(array(
                'status' => TRUE,
                'email' => $sendEmailtoSuperior
            ));
        }
        else {
            $this->renderJson(array(
                'status' => FALSE
            ));
        }
    }

    // public function dummy()
    // {
    //     $this->load->model(array('Employee_model', 'Shift_group_model', 'Overtime_model', 'Site_model'));

    //     $sites = $this->Site_model->get_all();

    //     $start_date = '2019-01-01';
    //     $end_date = '2019-08-01';

    //     $arry = [1,2,3];

    //     $count_extend = 0;
    //     $count = 0;
        
    //     $site = $sites[0];

    //     $members = $this->Shift_group_model->get_all_employee_by_site($site['site_id']);
        
    //     $i = 0;
    //     while (strtotime($start_date) <= strtotime($end_date)) {
    //         $member = $members[$i];
    //         $day_status = $this->Overtime_model->is_holiday($start_date, $member['employee_id'] );
    //         $superior_id = $this->Employee_model->getSuperior($member['employee_id']);
    //         if(rand(1,200) % 2){
    //             $data = $this->Overtime_model->add_over_time($site['site_id'], $member['employee_id'], $superior_id, $start_date , "0".rand(1, 8).":00", 'dummy', $day_status, TRUE);
    //             if(in_array(rand(1,10), $arry)){
    //                 $this->Overtime_model->add_extend($data['over_time_id'], "0".rand(1, 8).":".rand(0, 60), 'dummy test 2');
    //                 $count_extend++;              
    //             }
    //             $count++;
    //         }
    //         $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
    //         $i++;
    //         if($i >= sizeof($members))
    //             $i=0;
    //     }

    //     echo "$count ot added and $count_extend ot extend added";

    //     // echo sizeof($members);
    //     // print_r($members);
    // }

    // public function test()
    // {
    //     $month = strftime( "%m", time()) - 1;
    //     $year =  strftime( "%Y", time());
    //     echo "Saat ini: ".$month;

    //     $a_date = "2009-07-15";
    //     echo date("Y-m-t", strtotime($a_date));
    // }
    
    // public function add_new_group()
    // {
    //     if ($this->form_validation->run() == TRUE){
    //         $this->load->model('Site_model');

    //         $this->Site_model->create($this->input->post('inputSiteName').
    //                                     $this->input->post('inputSiteAddress'),
    //                                     $this->input->post('inputLineManagerId'));

    //         $this->renderJson(array(
    //             'status' => TRUE
    //         ));
    //     }else{
    //         $this->renderJson(array(
    //             'validation' => $this->form_validation->error_array()
    //         ));
    //     }
    // }
        
}
        
    /* End of file  Ajax.php */
        
                            