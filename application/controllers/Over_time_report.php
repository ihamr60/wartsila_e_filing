<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Over_time_report extends MY_Controller {

    public function index()
    {
        $this->load->model('Overtime_model');
        $user_id = $_SESSION['user_id'];
        $year = date("Y");
        $month = date("m");
        $data = [
            'scripts' => ['my/js/over_time_report'],
            'ot_res' => $this->Overtime_model->overtime_report_individual($user_id, $year, $month),
            'ot_sum' => $this->Overtime_model->overtime_report_summary_individual($user_id, $year, $month),
        ];
        $this->render('over-time/over_time_report_my_view', $data);
    }

    public function view($employee_id, $month, $year)
    {
        $this->load->model(array('Overtime_model', 'Employee_model'));
        $data = [
            'scripts' => ['my/js/over_time_report'],
            'ot_res' => $this->Overtime_model->overtime_report_individual($employee_id, $year, $month),
            'ot_sum' => $this->Overtime_model->overtime_report_summary_individual($employee_id, $year, $month),
            'start_date' => date("Y M d", strtotime("$year-$month-01")),
            'end_date' => date("Y M t", strtotime("$year-$month-01")),
            'employee' => $this->Employee_model->get_by_id($employee_id)
        ];
        $this->render('over-time/over_time_report_view', $data);
    }
        
}
        
    /* End of file  Over_time_report.php.php */
        
                            