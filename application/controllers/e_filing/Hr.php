<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends MY_Controller {

	 public function index()
     {   	
 		$this->load->model('web_app_model');
		$bc['dataKaryawan'] = $this->web_app_model->getAllData('employee');
		$this->render('e_filing/hr/bg_employee', $bc);
			
     }
	 
	 public function bg_detailEmployee()
     {   	
 		$this->load->model('web_app_model');
		$bc['data_file'] 			= $this->web_app_model->getWhereAllItem($this->uri->segment(4),'PersNo','tbl_filing');
		$bc['data_file_req'] 		= $this->web_app_model->getJoin('uuid_filing_category','uuid_filing_category','tbl_employee_required_file','tbl_filing_category');
		$bc['filing_req'] 			= $this->web_app_model->getAllData('tbl_filing_required');
		$bc['category'] 			= $this->web_app_model->getAllData('tbl_filing_category');
		$bc['ALLemployee'] 			= $this->web_app_model->getWhereAllItem($this->uri->segment(4),'PersNo','employee');
		$bc['employee'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(4),'PersNo','employee');
		$bc['organisasi'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_organisasi');
		$bc['job_ex'] 				= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_job_ex');
		$bc['pendidikan'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_education');
		$bc['kompetensi'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_competence');
		$bc['language'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_language');
		$bc['family'] 				= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_family');
		$bc['training'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_training');
		$bc['achievement'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_achievement');
		$bc['refference'] 			= $this->web_app_model->getWhere($this->uri->segment(4),'PersNo','tbl_refference');
		
		if (isset($_GET['person']) && $_GET['person']==1) 
		{
			$this->session->set_flashdata("person2","background-color: #e9f5fe; border-radius: 1%;");
			$this->session->set_flashdata("person","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
		}
		if (isset($_GET['person_inf']) && $_GET['person_inf']==1) 
		{
			$this->session->set_flashdata("person_inf","background-color: #f0f6ff; border-radius: 1%;");
			$this->session->set_flashdata("person_inf2","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
		}
		if (isset($_GET['photo']) && $_GET['photo']==1) 
		{
			$this->session->set_flashdata("photo","background-color: #f0f6ff; border-radius: 1%;");
			$this->session->set_flashdata("photo2","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
		}
		if (isset($_GET['attach']) && $_GET['attach']==1) 
		{
			$this->session->set_flashdata("attach","background-color: #f0f6ff; border-radius: 1%;");
			$this->session->set_flashdata("attach2","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
		}

		//TAB TAG NOTIF
		if (isset($_GET['org']) && $_GET['org']==1) 
		{
			$this->session->set_flashdata("org","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("org1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['work']) && $_GET['work']==1) 
		{
			$this->session->set_flashdata("work","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("work1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['edu']) && $_GET['edu']==1) 
		{
			$this->session->set_flashdata("edu","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("edu1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['cert']) && $_GET['cert']==1) 
		{
			$this->session->set_flashdata("cert","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("cert1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['lang']) && $_GET['lang']==1) 
		{
			$this->session->set_flashdata("lang","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("lang1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['fam']) && $_GET['fam']==1) 
		{
			$this->session->set_flashdata("fam","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("fam1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['train']) && $_GET['train']==1) 
		{
			$this->session->set_flashdata("train","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("train1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['achi']) && $_GET['achi']==1) 
		{
			$this->session->set_flashdata("achi","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("achi1","background-color: #f0f6ff; border-radius: 1%;");
		}
		if (isset($_GET['ref']) && $_GET['ref']==1) 
		{
			$this->session->set_flashdata("ref","<img class='blinktext' src='".base_url()."/assets/images/warning.png'>");
			$this->session->set_flashdata("ref1","background-color: #f0f6ff; border-radius: 1%;");
		}

		if (isset($_GET['stts_notif']) && $_GET['stts_notif']==0) 
		{
			$notif = array(		
			'stts' 				=> '1',
			);

		$this->web_app_model->updateData($notif,'tbl_notif');
		}
		
		$this->render('e_filing/hr/bg_detailEmployee', $bc);		
     }

     public function bg_notif()
     {   	
 		$this->load->model('web_app_model');
 		//$this->web_app_model->deleteNotif('tbl_notif','date','2'); AUTO DELETE NOTIF
		$bc['dataNotif'] 		= $this->web_app_model->getJoin('Uploader','PersNo','tbl_notif','employee');
		$this->render('e_filing/hr/bg_notif',$bc);
     }

     public function bg_notif_unread()
     {   	
 		$this->load->model('web_app_model');
		$bc['dataNotif_unread'] 		= $this->web_app_model->getJoinWhere('Uploader','PersNo','tbl_notif','employee','stts','0');
		$this->render('e_filing/hr/bg_notif_unread',$bc);
     }

     public function bg_notif_read()
     {   	
 		$this->load->model('web_app_model');
		$bc['dataNotif_read'] 		= $this->web_app_model->getJoinWhere('Uploader','PersNo','tbl_notif','employee','stts','1');
		$this->render('e_filing/hr/bg_notif_read',$bc);
     }

	 public function bg_holiday()
     {   	
 		$this->load->model('web_app_model');
		$bc['dataHoliday'] 		= $this->web_app_model->getJoinFour();
		$bc['dataCountry'] 		= $this->web_app_model->getAllData('tbl_country');
		$bc['dataProvince'] 	= $this->web_app_model->getJoin('country_id','country_id','tbl_province','tbl_country');
		$bc['dataCity'] 		= $this->web_app_model->getJoin('province_id','province_id','tbl_province','tbl_city');
		$this->render('e_filing/hr/bg_holiday',$bc);
     }
	 
	 public function bg_list_country()
     {   	
 		$this->load->model('web_app_model'); 
	 	$bc['dataCountry'] 		= $this->web_app_model->getAllData('tbl_country');
		$this->render('e_filing/hr/bg_list_country',$bc);
     }

     public function bg_filing_category()
     {   	
 		$this->load->model('web_app_model'); 
	 	$bc['dataFilingCategory']= $this->web_app_model->getAllData('tbl_filing_category');
		$this->render('e_filing/hr/bg_filing_category',$bc);
     }

     public function bg_required_file()
     {   	
 		$this->load->model('web_app_model'); 
 		$bc['dataFilingCategory']= $this->web_app_model->getAllData('tbl_filing_category');
	 	$bc['dataRequiredFile']	 = $this->web_app_model->getJoin('uuid_filing_category','uuid_filing_category','tbl_employee_required_file','tbl_filing_category');
		$this->render('e_filing/hr/bg_required_file',$bc);
     }
	 
	 public function bg_list_province()
     {   	
 		$this->load->model('web_app_model');
		$bc['dataCountry'] 		= $this->web_app_model->getAllData('tbl_country');
		$bc['dataProvince'] 	= $this->web_app_model->getJoin('country_id','country_id','tbl_province','tbl_country');
		$this->render('e_filing/hr/bg_list_province',$bc);
     }
	 
	 public function bg_list_city()
     {   	
 		$this->load->model('web_app_model'); 
		$bc['dataProvince'] 	= $this->web_app_model->getJoin('country_id','country_id','tbl_province','tbl_country');
	 	$bc['dataCity'] 		= $this->web_app_model->getJoin('province_id','province_id','tbl_province','tbl_city');
		$this->render('e_filing/hr/bg_list_city',$bc);
     }
	 
	 public function bg_editCountry()
     {   	
 		$this->load->model('web_app_model'); 
	 	$bc['dataCountry'] 		= $this->web_app_model->getWhere($this->uri->segment(4),'country_id','tbl_country');
		$this->render('e_filing/hr/bg_editCountry',$bc);
     }
	 
	 public function bg_editProvince()
     {   	
 		$this->load->model('web_app_model'); 
	 	$bc['dataCountry'] 		= $this->web_app_model->getAllData('tbl_country');
		$bc['dataProvince'] 	= $this->web_app_model->getJoinWhere('country_id','country_id','tbl_province','tbl_country','province_id',$this->uri->segment(4));
		$this->render('e_filing/hr/bg_editProvince',$bc);
     }
	 
	 public function bg_editCity()
     {   	
 		$this->load->model('web_app_model'); 
	 	$bc['dataCity'] 		= $this->web_app_model->getJoinWhere('province_id','province_id','tbl_province','tbl_city','city_id',$this->uri->segment(4));
		$bc['dataProvince'] 	= $this->web_app_model->getJoin('country_id','country_id','tbl_province','tbl_country');
		$this->render('e_filing/hr/bg_editCity',$bc);
     }
	 
	 public function bg_editHoliday()
     {   	
 		$this->load->model('web_app_model'); 
		$bc['dataCountry'] 		= $this->web_app_model->getAllData('tbl_country');
		$bc['dataHoliday'] 		= $this->web_app_model->getJoinFourWhere($this->uri->segment(4));
	 	$bc['dataCity'] 		= $this->web_app_model->getJoin('province_id','province_id','tbl_province','tbl_city');
		$bc['dataProvince'] 	= $this->web_app_model->getJoin('country_id','country_id','tbl_province','tbl_country');
		$this->render('e_filing/hr/bg_editHoliday',$bc);
     }
	 
	 public function add_city()
	 {
		
		$this->load->model('web_app_model'); 
		$cek 	  	 	= $this->web_app_model->getWhere($this->input->post('city_code'),'city_code','tbl_city');
		if (count($cek) > 0) {
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_city');
			$this->session->set_flashdata("info","
								<div class='alert alert-danger'>
										<strong>
											<i class='icon-ok'></i>
											Maaf!
										</strong>
										Code / ID already exists.
								</div>");
		}
		else
		{
			$province_id			= $this->input->post('province_id');
			$city_code				= $this->input->post('city_code');
			$city_name				= $this->input->post('city_name');
			$city_id				= $this->get_id();
			$data = array(
				'city_code' 	  	 => $city_code,
				'city_name' 	  	 => $city_name,
				'city_id'			 => $city_id,
				'province_id'		 => $province_id,
				);
			$this->web_app_model->insertData($data,'tbl_city');
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_city');
			$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												City data added successfully!
										</div>");
		}
	}
	
	public function add_holiday()
	 {
		$this->load->model('web_app_model'); 
		$holiday_id				= $this->get_id();
		$country_id				= $this->input->post('country_id');
		$province_id			= $this->input->post('province_id');
		$city_id				= $this->input->post('city_id');
		$holiday_date			= $this->input->post('holiday_date');
		$holiday_date			= date('Y-m-d', strtotime($holiday_date));
		$description			= $this->input->post('description');	

		$data = array(	
			'holiday_id' 	  	 => $holiday_id,
			'country_id' 	  	 => $country_id,
			'province_id'		 => $province_id,
			'city_id'			 => $city_id,
			'date_holiday'		 => $holiday_date,
			'description'		 => $description,
			);

		$this->web_app_model->insertData($data,'tbl_holiday');	
		header('location:'.base_url().'index.php/e_filing/hr/bg_holiday');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Holiday data added successfully!
									</div>");
	}

	public function add_requiredFile()
	 {
		$this->load->model('web_app_model'); 
		$uuid_employee_required_file 		= $this->get_id();
		$uuid_filing_category				= $this->input->post('uuid_filing_category');
		$required_file_name					= $this->input->post('required_file_name');

		$data = array(	
			'uuid_employee_required_file' 	=> $uuid_employee_required_file,
			'uuid_filing_category'			=> $uuid_filing_category,
			'required_file_name'			=> $required_file_name,
			);

		$this->web_app_model->insertData($data,'tbl_employee_required_file');	
		header('location:'.base_url().'index.php/e_filing/hr/bg_required_file');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Required File added successfully!
									</div>");
	}

	public function edit_requiredFile()
	 {
		$this->load->model('web_app_model'); 
		$uuid_employee_required_file 		= $this->input->post('uuid_employee_required_file');
		$uuid_filing_category				= $this->input->post('uuid_filing_category');
		$required_file_name					= $this->input->post('required_file_name');

		$data = array(	
			'uuid_filing_category'			=> $uuid_filing_category,
			'required_file_name'			=> $required_file_name,
			);

		$where = array(
			'uuid_employee_required_file'	=> $uuid_employee_required_file,
			);

		$this->web_app_model->updateDataWhere($data,$where,'tbl_employee_required_file');	
		header('location:'.base_url().'index.php/e_filing/hr/bg_required_file');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Required File has been Updated!
									</div>");
	}

	public function deleteRequiredFile()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('uuid_employee_required_file'	=>	$id);
		$this->web_app_model->deleteData('tbl_employee_required_file',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_required_file');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										Country Data successfully deleted!
								</div>");
	}

	public function add_filingCategory()
	 {
		$this->load->model('web_app_model'); 
		$uuid_filing_category 	= $this->get_id();
		$category_name			= $this->input->post('category_name');

		$data = array(	
			'category_name' 	  	=> $category_name,
			'uuid_filing_category'	=> $uuid_filing_category,
			);

		$this->web_app_model->insertData($data,'tbl_filing_category');	
		header('location:'.base_url().'index.php/e_filing/hr/bg_filing_category');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Filng Category added successfully!
									</div>");
	}

	public function edit_filingCategory()
	 {
		$this->load->model('web_app_model'); 
		$uuid_filing_category 	= $this->input->post('uuid_filing_category');
		$category_name			= $this->input->post('category_name');

		$data = array(	
			'category_name' 	  	=> $category_name,
			);

		$where = array(
			'uuid_filing_category'	=> $uuid_filing_category,
			);

		$this->web_app_model->updateDataWhere($data,$where,'tbl_filing_category');	
		header('location:'.base_url().'index.php/e_filing/hr/bg_filing_category');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Filing Category has been updated!
									</div>");
	}
	
	public function editHoliday()
	{		
		$this->load->model('web_app_model'); 
		$holiday_id				= $this->input->post('holiday_id');
		$country_id				= $this->input->post('country_id');
		$province_id			= $this->input->post('province_id');
		$city_id				= $this->input->post('city_id');
		$date_holiday			= $this->input->post('date_holiday');
		$description			= $this->input->post('description');
			
		$data = array(
			'country_id' 	  	 => $country_id,
			'province_id' 	  	 => $province_id,
			'city_id' 	  		 => $city_id,
			'date_holiday' 	  	 => $date_holiday,
			'description' 	  	 => $description,
			);
			
		$where = array(
			'holiday_id'		 => $holiday_id,
			);
		
		$this->web_app_model->updateDataWhere($data,$where,'tbl_holiday');
		header('location:'.base_url().'index.php/e_filing/hr/bg_holiday');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Holiday data has been updated!
									</div>");
	}
	
	public function editCountry()
	{		
		$this->load->model('web_app_model'); 
		$country_code				= $this->input->post('country_code');
		$country_name				= $this->input->post('country_name');
		$country_id					=  $this->input->post('country_id');
			
		$data = array(
			'country_code' 	  	 => $country_code,
			'country_name' 	  	 => $country_name,
			);
			
		$where = array(
			'country_id'		=> $country_id,
			);
		
		$this->web_app_model->updateDataWhere($data,$where,'tbl_country');
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_country');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Country data has been updated!
									</div>");
	}
	
	public function editCity()
	 {
		$this->load->model('web_app_model'); 
		$province_id			= $this->input->post('province_id');
		$city_code				= $this->input->post('city_code');
		$city_name				= $this->input->post('city_name');
		$city_id				= $this->input->post('city_id');
			
		$data = array(
			'city_code' 	  	 => $city_code,
			'city_name' 	  	 => $city_name,
			'province_id' 	  	 => $province_id,
			);
			
		$where = array(
			'city_id'		=> $city_id,
			);
		
		$this->web_app_model->updateDataWhere($data,$where,'tbl_city');
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_city');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											City data has been updated!
									</div>");
	}
	 
	 public function add_country()
	 {
		$this->load->model('web_app_model'); 
		$cek 	  	 	= $this->web_app_model->getWhere($this->input->post('country_code'),'country_code','tbl_country');
		if (count($cek) > 0) {
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_country');
			$this->session->set_flashdata("info","
								<div class='alert alert-danger'>
										<strong>
											<i class='icon-ok'></i>
											Maaf!
										</strong>
										ID already exists.
								</div>");
		}
		else
		{
			$country_code				= $this->input->post('country_code');
			$country_name				= $this->input->post('country_name');
			$country_id					= $this->get_id();
			$data = array(
				'country_code' 	  	 => $country_code,
				'country_name' 	  	 => $country_name,
				'country_id'		 => $country_id,
				);
			$this->web_app_model->insertData($data,'tbl_country');
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_country');
			$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												City data added successfully!
										</div>");
		}
	}
	
	public function deleteCountry()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('country_id'	=>	$id);
		$this->web_app_model->deleteData('tbl_country',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_country');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										Country Data successfully deleted!
								</div>");
	}
	
	public function deleteHoliday()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('holiday_id'	=>	$id);
		$this->web_app_model->deleteData('tbl_holiday',$hapus);
		//$this->web_app_model->deleteData('tbl_dosen_wali',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_holiday');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										Holiday Data successfully deleted!
								</div>");
	}
	
	public function deleteFilingCategory()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('uuid_filing_category'	=>	$id);
		$this->web_app_model->deleteData('tbl_filing_category',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_filing_category');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										Filing Category successfully deleted!
								</div>");
	}

	public function deleteProvince()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('province_id'	=>	$id);
		$this->web_app_model->deleteData('tbl_province',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_province');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										Province Data successfully deleted!
								</div>");
	}
	
	public function deleteCity()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('city_id'	=>	$id);
		$this->web_app_model->deleteData('tbl_city',$hapus);
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_city');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Oke!
										</strong>
										City Data successfully deleted!
								</div>");
	}
	
	public function add_province()
	 {
		$this->load->model('web_app_model'); 
		$cek 	  	 	= $this->web_app_model->getWhere($this->input->post('province_code'),'province_code','tbl_province');
		if (count($cek) > 0) {
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_province');
			$this->session->set_flashdata("info","
								<div class='alert alert-danger'>
										<strong>
											<i class='icon-ok'></i>
											Maaf!
										</strong>
										Code ID already exists.
								</div>");
		}
		else
		{
			$province_code				= $this->input->post('province_code');
			$province_name				= $this->input->post('province_name');
			$country_id					= $this->input->post('country_id');
			$province_id				= $this->get_id();
			
			$data = array(
				'province_code' 	  	 => $province_code,
				'province_name' 	  	 => $province_name,
				'province_id'			 => $province_id,
				'country_id'			 => $country_id,
				);
	
			$this->web_app_model->insertData($data,'tbl_province');
			header('location:'.base_url().'index.php/e_filing/hr/bg_list_province');
			$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Province data added successfully!
										</div>");
		}
	}
	
	public function editProvince()
	 {
		$this->load->model('web_app_model'); 
		$province_code				= $this->input->post('province_code');
		$province_name				= $this->input->post('province_name');
		$country_id					= $this->input->post('country_id');
		$province_id				= $this->input->post('province_id');
		
		$data = array(
			'province_code' 	  	 => $province_code,
			'province_name' 	  	 => $province_name,
			'province_id'			 => $province_id,
			'country_id'			 => $country_id,
			);
			
		$where = array (
			'province_id'			=> $province_id,
		);

		$this->web_app_model->updateDataWhere($data,$where,'tbl_province');
		header('location:'.base_url().'index.php/e_filing/hr/bg_list_province');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Province data has been updated!
									</div>");
	}
}