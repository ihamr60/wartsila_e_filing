<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MY_Controller {

	public function index()
    {   	
 		$this->load->model('web_app_model');
 		$bc['jumlah_belumBaca']		= $this->web_app_model->hitungJumlahBelumBaca();
 		$bc['data_city']			= $this->web_app_model->getAllData('tbl_city');
		$bc['data_province']		= $this->web_app_model->getAllData('tbl_province');
		$bc['category'] 			= $this->web_app_model->getAllData('tbl_filing_category');
		$bc['employee'] 			= $this->web_app_model->getWhereOneItem($this->session->user_id,'PersNo','employee');
		$bc['data_file'] 			= $this->web_app_model->getWhereAllItem($this->session->user_id,'PersNo','tbl_filing');
		$bc['organization']			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_organisasi');
		$bc['job_ex'] 				= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_job_ex');
		$bc['education'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_education');
		$bc['competence'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_competence');
		$bc['language'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_language');
		$bc['family'] 				= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_family');
		$bc['training'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_training');
		$bc['achievement'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_achievement');
		$bc['refference'] 			= $this->web_app_model->getWhere($this->session->user_id,'PersNo','tbl_refference');
		$bc['data_file_req'] 		= $this->web_app_model->getJoin('uuid_filing_category','uuid_filing_category','tbl_employee_required_file','tbl_filing_category');
		$bc['filing_req'] 			= $this->web_app_model->getAllData('tbl_filing_required');
		$bc['id'] 					= $this->get_id();

		// START - PANGGIL TAB MENU
		if (isset($_GET['tabPersonData']) && $_GET['tabPersonData']==1) 
		{
			$this->session->set_flashdata("person-tab-active","in active");
		}

		if (isset($_GET['tabOrganiz']) && $_GET['tabOrganiz']==1) 
		{
			$this->session->set_flashdata("organiz-tab-active","in active");
		}

		if (isset($_GET['tabWork']) && $_GET['tabWork']==1) 
		{
			$this->session->set_flashdata("work-tab-active","in active");
		}

		if (isset($_GET['tabEdu']) && $_GET['tabEdu']==1) 
		{
			$this->session->set_flashdata("edu-tab-active","in active");
		}

		if (isset($_GET['tabCert']) && $_GET['tabCert']==1) 
		{
			$this->session->set_flashdata("cert-tab-active","in active");
		}

		if (isset($_GET['tabLang']) && $_GET['tabLang']==1) 
		{
			$this->session->set_flashdata("lang-tab-active","in active");
		}

		if (isset($_GET['tabFam']) && $_GET['tabFam']==1) 
		{
			$this->session->set_flashdata("fam-tab-active","in active");
		}

		if (isset($_GET['tabTraining']) && $_GET['tabTraining']==1) 
		{
			$this->session->set_flashdata("training-tab-active","in active");
		}

		if (isset($_GET['tabAchiev']) && $_GET['tabAchiev']==1) 
		{
			$this->session->set_flashdata("achiev-tab-active","in active");
		}

		if (isset($_GET['tabReff']) && $_GET['tabReff']==1) 
		{
			$this->session->set_flashdata("reff-tab-active","in active");
		}

		if (isset($_GET['tabFilling']) && $_GET['tabFilling']==1) 
		{
			$this->session->set_flashdata("filling-tab-active","in active");
		}
		// END - PANGGIL TAB MENU

		$this->render('e_filing/employee/my_cv', $bc);
    }

    public function updateDataPersonal()
	{
		$this->load->model('web_app_model');
		$First_name					= $this->input->post('First_name');
		$PersNo						= $this->input->post('PersNo');
		$gender						= $this->input->post('gender');
		$Last_name					= $this->input->post('Last_name');
		$Birthdate					= $this->input->post('Birthdate');
		$religion					= $this->input->post('religion');	
		$height						= $this->input->post('height');
		$tribe						= $this->input->post('tribe');	
		$passion					= $this->input->post('passion');
		$marital_stats				= $this->input->post('marital_stats');
		$weight						= $this->input->post('weight');
		$blood_type					= $this->input->post('blood_type');
		$Nationality				= $this->input->post('Nationality');
		$address_current			= $this->input->post('address_current');
		$city_current				= $this->input->post('city_current');
		$province_current			= $this->input->post('province_current');
		$address_permanent			= $this->input->post('address_permanent');
		$city_permanent				= $this->input->post('city_permanent');
		$province_permanent			= $this->input->post('province_permanent');
		$no_ktp						= $this->input->post('no_ktp');
		$npwp						= $this->input->post('npwp');
		$bpjs_kes					= $this->input->post('bpjs_kes');
		$passport					= $this->input->post('passport');
		$no_kk						= $this->input->post('no_kk');
		$dplk						= $this->input->post('dplk');
		$bpjs_naker					= $this->input->post('bpjs_naker');
		$jamsostek					= $this->input->post('jamsostek');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Personal Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		$stts						= '0';

		$data = array(			
			'First_name'			=> $First_name,
			'PersNo'				=> $PersNo,
			'gender'				=> $gender,
			'Last_name'				=> $Last_name,
			'Birthdate'				=> $Birthdate,
			'religion'				=> $religion,
			'height'				=> $height,
			'tribe'					=> $tribe,
			'passion'				=> $passion,
			'marital_stats'			=> $marital_stats,
			'weight'				=> $weight,
			'blood_type'			=> $blood_type,
			'Nationality'			=> $Nationality,
			'address_current'		=> $address_current,
			'city_current'			=> $city_current,
			'province_current'		=> $province_current,
			'address_permanent'		=> $address_permanent,
			'city_permanent'		=> $city_permanent,
			'province_permanent'	=> $province_permanent,
			'no_ktp'				=> $no_ktp,
			'npwp'					=> $npwp,
			'bpjs_kes'				=> $bpjs_kes,
			'passport'				=> $passport,
			'no_kk'					=> $no_kk,
			'dplk'					=> $dplk,
			'bpjs_naker'			=> $bpjs_naker,
			'jamsostek'				=> $jamsostek,			
			);
			
		$where = array(		
			'PersNo' 				=> $PersNo,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'stts' 					=> $stts,
			'person_tab'			=> "1",
			'person_inf'			=> "1",
			);
		
		$this->web_app_model->updateDataWhere($data,$where,'employee');
		$this->web_app_model->insertData($notif,'tbl_notif');
		header('location:'.base_url().'index.php/e_filing/employee?tabPersonData=1');
		$this->session->set_flashdata("info","
											<div class='alert alert-success'>
													<strong>
														<i class='icon-ok'></i>
														Success!
													</strong>
													Data Pegawai berhasil diupdate!
											</div>");
	}

	public function updatePhotoPersonal()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->input->post('PersNo');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Photo Profile <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/photo';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif';
		$config['max_size'] 		= '4480';
		$config['overwrite']		= true;
		$config['file_name'] 		= $PersNo;

		$this->load->library('upload', $config);
		
		if(!empty($_FILES['photo']['name'])) 
		{
	        if ( $this->upload->do_upload('photo') ) 
	        {
	            $foto = $this->upload->data();	

				$data = array(		
					'photo'					=> $foto['file_name'],
					);

				$where = array(		
					'PersNo'				=> $PersNo,
					);

				$notif = array(		
					'uuid_notif' 			=> $id_notif,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $PersNo,
					'message' 				=> $message,
					'date' 					=> $date,
					'person_tab'			=> "1",
					'person_photo'			=> "1",
					);
		
				$this->web_app_model->updateDataWhere($data,$where,'employee');
				$this->web_app_model->insertData($notif,'tbl_notif');
				header('location:'.base_url().'index.php/e_filing/employee?tabPersonData=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Photo Profile berhasil ditambahkan!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/employee?tabPersonData=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/employee?tabPersonData=1');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												File Tidak Masuk !!
										</div>");
	    }
	}

	public function addOrganizationEx()
	{
		$this->load->model('web_app_model');

		date_default_timezone_set('Asia/Jakarta');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$message					= 'Organization Experience <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		$id_organisasi				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$organisasi					= $this->input->post('organisasi');
		$jabatan					= $this->input->post('jabatan');
		$thn_mulai					= $this->input->post('thn_mulai');
		$thn_berakhir				= $this->input->post('thn_berakhir');
		
		$data = array(		
			'id_organisasi'			=> $id_organisasi,
			'organisasi'			=> $organisasi,
			'PersNo'				=> $PersNo,
			'jabatan'				=> $jabatan,
			'thn_mulai'				=> $thn_mulai,
			'thn_berakhir'			=> $thn_berakhir,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'org'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_organisasi');
		header('location:'.base_url().'index.php/e_filing/employee?tabOrganiz=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data organisasi berhasil ditambahkan!
								</div>");
	}

	public function editOrganization()
	{
		$this->load->model('web_app_model');

		date_default_timezone_set('Asia/Jakarta');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$message					= 'Organization Experience <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		$id_organisasi				= $this->input->post('id_organisasi');
		$PersNo 					= $this->session->user_id;
		$organisasi					= $this->input->post('organisasi');
		$jabatan					= $this->input->post('jabatan');
		$thn_mulai					= $this->input->post('thn_mulai');
		$thn_berakhir				= $this->input->post('thn_berakhir');
		
		$data = array(		
			'organisasi'			=> $organisasi,
			'PersNo'				=> $PersNo,
			'jabatan'				=> $jabatan,
			'thn_mulai'				=> $thn_mulai,
			'thn_berakhir'			=> $thn_berakhir,
			);
			
		$where = array(		
			'id_organisasi' 		=> $id_organisasi,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'org'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_organisasi');
		header('location:'.base_url().'index.php/e_filing/employee?tabOrganiz=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data organisasi berhasil diupdate!
								</div>");
	}

	public function addWorkEx()
	{
		$this->load->model('web_app_model');
		$id_job						= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$perusahaan					= $this->input->post('perusahaan');
		$jabatan					= $this->input->post('jabatan');
		$thn_mulai					= $this->input->post('thn_mulai');
		$thn_berakhir				= $this->input->post('thn_berakhir');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Work Experience <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_job'				=> $id_job,
			'perusahaan'			=> $perusahaan,
			'PersNo'				=> $PersNo,
			'jabatan'				=> $jabatan,
			'thn_mulai'				=> $thn_mulai,
			'thn_berakhir'			=> $thn_berakhir,
			);
		
		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'work'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_job_ex');
		header('location:'.base_url().'index.php/e_filing/employee?tabWork=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Pengalaman Kerja berhasil ditambahkan!
								</div>");
	}

	public function editWorkEx()
	{
		$this->load->model('web_app_model');
		
		$id_job						= $this->input->post('id_job');
		$PersNo 					= $this->session->user_id;
		$perusahaan					= $this->input->post('perusahaan');
		$jabatan					= $this->input->post('jabatan');
		$thn_mulai					= $this->input->post('thn_mulai');
		$thn_berakhir				= $this->input->post('thn_berakhir');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Work Experience <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_job'				=> $id_job,
			'perusahaan'			=> $perusahaan,
			'PersNo'				=> $PersNo,
			'jabatan'				=> $jabatan,
			'thn_mulai'				=> $thn_mulai,
			'thn_berakhir'			=> $thn_berakhir,
			);

		$where = array(		
			'id_job' 				=> $id_job,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'work'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_job_ex');
		header('location:'.base_url().'index.php/e_filing/employee?tabWork=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Pengalaman Kerja berhasil diupdate!
								</div>");
	}

	public function addEducation()
	{
		$this->load->model('web_app_model');
		
		$id_education				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$institusi					= $this->input->post('institusi');
		$jurusan					= $this->input->post('jurusan');
		$ipk						= $this->input->post('ipk');
		$thn_masuk					= $this->input->post('thn_masuk');
		$thn_lulus					= $this->input->post('thn_lulus');
		$no_ijazah					= $this->input->post('no_ijazah');
		$lampiran					= $this->input->post('lampiran');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Education Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_education'			=> $id_education,
			'institusi'				=> $institusi,
			'PersNo'				=> $PersNo,
			'jurusan'				=> $jurusan,
			'ipk'					=> $ipk,
			'thn_masuk'				=> $thn_masuk,
			'thn_lulus'				=> $thn_lulus,
			'no_ijazah'				=> $no_ijazah,
			);
		
		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'edu'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_education');
		header('location:'.base_url().'index.php/e_filing/employee?tabEdu=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Pendidikan berhasil ditambahkan!
								</div>");
	}

	public function editEducation()
	{
		$this->load->model('web_app_model');
		$id_education				= $this->input->post('id_education');
		$PersNo 					= $this->session->user_id;
		$institusi					= $this->input->post('institusi');
		$jurusan					= $this->input->post('jurusan');
		$ipk						= $this->input->post('ipk');
		$thn_masuk					= $this->input->post('thn_masuk');
		$thn_lulus					= $this->input->post('thn_lulus');
		$no_ijazah					= $this->input->post('no_ijazah');
		$lampiran					= $this->input->post('lampiran');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Education Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'institusi'				=> $institusi,
			'PersNo'				=> $PersNo,
			'jurusan'				=> $jurusan,
			'ipk'					=> $ipk,
			'thn_masuk'				=> $thn_masuk,
			'thn_lulus'				=> $thn_lulus,
			'no_ijazah'				=> $no_ijazah,
			);

		$where = array(		
			'id_education' 			=> $id_education,
			);
		
		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'edu'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_education');
		header('location:'.base_url().'index.php/e_filing/employee?tabEdu=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Pendidikan berhasil diupdate!
								</div>");
	}

	public function addCertification()
	{
		$this->load->model('web_app_model');
		$id_competence				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$kompetensi					= $this->input->post('kompetensi');
		$level						= $this->input->post('level');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Certification Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_competence'			=> $id_competence,
			'kompetensi'			=> $kompetensi,
			'PersNo'				=> $PersNo,
			'level'					=> $level,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'cert'					=> "1",
			);

			$this->web_app_model->insertData($notif,'tbl_notif');
			$this->web_app_model->insertData($data,'tbl_competence');
			header('location:'.base_url().'index.php/e_filing/employee?tabCert=1');
			$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Data Sertifikasi & kompetensi berhasil ditambahkan!
									</div>");
	}

	public function editCertification()
	{
		$this->load->model('web_app_model');	
		$id_competence				= $this->input->post('id_competence');
		$PersNo 					= $this->session->user_id;
		$kompetensi					= $this->input->post('kompetensi');
		$level						= $this->input->post('level');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Certification Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'kompetensi'			=> $kompetensi,
			'PersNo'				=> $PersNo,
			'level'					=> $level,
			);

		$where = array(		
			'id_competence' 		=> $id_competence,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'cert'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_competence');
		header('location:'.base_url().'index.php/e_filing/employee?tabCert=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Sertifikasi & kompetensi berhasil diupdate!
								</div>");
	}

	public function addLanguage()
	{
		$this->load->model('web_app_model');
		$id_language				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$bahasa						= $this->input->post('bahasa');
		$level						= $this->input->post('level');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Language Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_language'			=> $id_language,
			'bahasa'				=> $bahasa,
			'PersNo'				=> $PersNo,
			'level'					=> $level,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'lang'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_language');
		header('location:'.base_url().'index.php/e_filing/employee?tabLang=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Skill Bahasa berhasil ditambahkan!
								</div>");
	}

	public function editLanguage()
	{
		$this->load->model('web_app_model');	
		$id_language				= $this->input->post('id_language');
		$PersNo 					= $this->session->user_id;
		$bahasa						= $this->input->post('bahasa');
		$level						= $this->input->post('level');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Language Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'bahasa'				=> $bahasa,
			'PersNo'				=> $PersNo,
			'level'					=> $level,
			);

		$where = array(		
			'id_language'			=> $id_language,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'lang'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_language');
		header('location:'.base_url().'index.php/e_filing/employee?tabLang=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Skill Bahasa berhasil diupdate!
								</div>");
	}

	public function addFamily()
	{
		$this->load->model('web_app_model');
		$id_family					= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$nama_sdr					= $this->input->post('nama_sdr');
		$tgl_lhr_sdr				= $this->input->post('tgl_lhr_sdr');
		$hubungan					= $this->input->post('hubungan');
		$pekerjaan					= $this->input->post('pekerjaan');
		$no_hp						= $this->input->post('no_hp');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Family Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
	
		$data = array(		
			'id_family'				=> $id_family,
			'nama_sdr'				=> $nama_sdr,
			'PersNo'				=> $PersNo,
			'tgl_lhr_sdr'			=> $tgl_lhr_sdr,
			'hubungan'				=> $hubungan,
			'pekerjaan'				=> $pekerjaan,
			'no_hp'					=> $no_hp,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'fam'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_family');
		header('location:'.base_url().'index.php/e_filing/employee?tabFam=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Keluarga berhasil ditambahkan!
								</div>");
	}

	public function editFamily()
	{
		$this->load->model('web_app_model');	
		$id_family					= $this->input->post('id_family');
		$PersNo 					= $this->session->user_id;
		$nama_sdr					= $this->input->post('nama_sdr');
		$tgl_lhr_sdr				= $this->input->post('tgl_lhr_sdr');
		$hubungan					= $this->input->post('hubungan');
		$pekerjaan					= $this->input->post('pekerjaan');
		$no_hp						= $this->input->post('no_hp');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Family Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
	
		$data = array(		
			'nama_sdr'				=> $nama_sdr,
			'PersNo'				=> $PersNo,
			'tgl_lhr_sdr'			=> $tgl_lhr_sdr,
			'hubungan'				=> $hubungan,
			'pekerjaan'				=> $pekerjaan,
			'no_hp'					=> $no_hp,
			);

		$where = array(		
			'id_family'				=> $id_family,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'fam'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_family');
		header('location:'.base_url().'index.php/e_filing/employee?tabFam=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Keluarga berhasil diupdate!
								</div>");
	}

	public function addTraining()
	{
		$this->load->model('web_app_model');
		$id_training				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$training					= $this->input->post('training');
		$tmpt_training				= $this->input->post('tmpt_training');
		$mulai_training				= $this->input->post('mulai_training');
		$selesai_training			= $this->input->post('selesai_training');
		$penyelenggara				= $this->input->post('penyelenggara');
		$lampiran					= $this->input->post('lampiran');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		
		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Training Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");

		$data = array(		
			'id_training'			=> $id_training,
			'training'				=> $training,
			'PersNo'				=> $PersNo,
			'tmpt_training'			=> $tmpt_training,
			'mulai_training'		=> $mulai_training,
			'selesai_training'		=> $selesai_training,
			'penyelenggara'			=> $penyelenggara,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'train'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_training');
		header('location:'.base_url().'index.php/e_filing/employee?tabTraining=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Training berhasil ditambahkan!
								</div>");
	}

	public function editTraining()
	{
		$this->load->model('web_app_model');
		$id_training				= $this->input->post('id_training');
		$PersNo 					= $this->session->user_id;
		$training					= $this->input->post('training');
		$tmpt_training				= $this->input->post('tmpt_training');
		$mulai_training				= $this->input->post('mulai_training');
		$selesai_training			= $this->input->post('selesai_training');
		$penyelenggara				= $this->input->post('penyelenggara');
		$lampiran					= $this->input->post('lampiran');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Training Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'training'				=> $training,
			'PersNo'				=> $PersNo,
			'tmpt_training'			=> $tmpt_training,
			'mulai_training'		=> $mulai_training,
			'selesai_training'		=> $selesai_training,
			'penyelenggara'			=> $penyelenggara,
			);

		$where = array(		
			'id_training'			=> $id_training,
		);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'train'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_training');
		header('location:'.base_url().'index.php/e_filing/employee?tabTraining=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Training berhasil diupdate!
								</div>");
	}

	public function addAchievement()
	{
		$this->load->model('web_app_model');
		$id_achievement				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$achievement				= $this->input->post('achievement');
		$year						= $this->input->post('year');
		$organizer					= $this->input->post('organizer');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Achievement Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_achievement'		=> $id_achievement,
			'achievement'			=> $achievement,
			'PersNo'				=> $PersNo,
			'year'					=> $year,
			'organizer'				=> $organizer,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'achi'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_achievement');
		header('location:'.base_url().'index.php/e_filing/employee?tabAchiev=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Achievement berhasil ditambahkan!
								</div>");
	}

	public function editAchievement()
	{
		$this->load->model('web_app_model');
		$id_achievement				= $this->input->post('id_achievement');
		$PersNo 					= $this->session->user_id;
		$achievement				= $this->input->post('achievement');
		$year						= $this->input->post('year');
		$organizer					= $this->input->post('organizer');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Achievement Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");

		$data = array(		
			'achievement'			=> $achievement,
			'PersNo'				=> $PersNo,
			'year'					=> $year,
			'organizer'				=> $organizer,
			);

		$where = array(		
			'id_achievement'		=> $id_achievement,
		);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'achi'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_achievement');
		header('location:'.base_url().'index.php/e_filing/employee?tabAchiev=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Achievement berhasil diupdate!
								</div>");
	}

	public function addRefference()
	{
		$this->load->model('web_app_model');
		$id_refference				= $this->get_id();
		$PersNo 					= $this->session->user_id;
		$fullname					= $this->input->post('fullname');
		$relation					= $this->input->post('relation');
		$no_hp						= $this->input->post('no_hp');
		$email						= $this->input->post('email');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Refference Data <b>'.$First_name.' '.$Last_name.'</b> has been added';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'id_refference'			=> $id_refference,
			'fullname'				=> $fullname,
			'PersNo'				=> $PersNo,
			'relation'				=> $relation,
			'no_hp'					=> $no_hp,
			'email'					=> $email,
			);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'ref'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->insertData($data,'tbl_refference');
		header('location:'.base_url().'index.php/e_filing/employee?tabReff=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Referensi berhasil ditambahkan!
								</div>");
	}

	public function editRefference()
	{
		$this->load->model('web_app_model');
		$id_refference				= $this->input->post('id_refference');
		$PersNo 					= $this->session->user_id;
		$fullname					= $this->input->post('fullname');
		$relation					= $this->input->post('relation');
		$no_hp						= $this->input->post('no_hp');
		$email						= $this->input->post('email');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'Refference Data <b>'.$First_name.' '.$Last_name.'</b> has been updated';
		$id_notif					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$data = array(		
			'fullname'				=> $fullname,
			'PersNo'				=> $PersNo,
			'relation'				=> $relation,
			'no_hp'					=> $no_hp,
			'email'					=> $email,
			);

		$where = array(		
			'id_refference'			=> $id_refference,
		);

		$notif = array(		
			'uuid_notif' 			=> $id_notif,
			'PersNo_notif' 			=> $PersNo,
			'Uploader' 				=> $PersNo,
			'message' 				=> $message,
			'date' 					=> $date,
			'ref'					=> "1",
			);

		$this->web_app_model->insertData($notif,'tbl_notif');
		$this->web_app_model->updateDataWhere($data,$where,'tbl_refference');
		header('location:'.base_url().'index.php/e_filing/employee?tabReff=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success!
										</strong>
										Data Referensi berhasil diupdate!
								</div>");
	}

	public function hapusOrganization()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_organisasi'=>$id);

		$this->web_app_model->deleteData('tbl_organisasi',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabOrganiz=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Organisasi berhasil dihapus...!
								</div>");
	}

	public function hapusWorkEx()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_job'=>$id);
		
		$this->web_app_model->deleteData('tbl_job_ex',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabWork=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data pekerjaan berhasil dihapus...!
								</div>");
	}

	public function hapusEducation()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_education'=>$id);
		
		$this->web_app_model->deleteData('tbl_education',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabEdu=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Pendidikan berhasil dihapus...!
								</div>");
	}

	public function hapusCompetence()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_competence'=>$id);
		
		$this->web_app_model->deleteData('tbl_competence',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabCert=1');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data kompetensi/Sertifikasi berhasil dihapus...!
								</div>");
	}

	public function hapusLanguage()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_language'=>$id);
		
		$this->web_app_model->deleteData('tbl_language',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabLang=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Language berhasil dihapus...!
								</div>");
	}

	public function hapusFamily()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_family'=>$id);
		
		$this->web_app_model->deleteData('tbl_family',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabFam=1');
		$this->session->set_flashdata("info","
								<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Keluarga berhasil dihapus...!
								</div>");
	}

	public function hapusTraining()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_training'=>$id);
		
		$this->web_app_model->deleteData('tbl_training',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabTraining=1');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Training berhasil dihapus...!
								</div>");
	}

	public function hapusAchievement()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_achievement'=>$id);
		
		$this->web_app_model->deleteData('tbl_achievement',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabAchiev=1');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Prestasi berhasil dihapus...!
								</div>");
	}

	public function hapusRefference()
	{
		$this->load->model('web_app_model');
		$id 			= $this->uri->segment(4);
		$hapus 			= array('id_refference'=>$id);
		
		$this->web_app_model->deleteData('tbl_refference',$hapus);
		header('location:'.base_url().'index.php/e_filing/employee?tabReff=1');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Data Referensi berhasil dihapus...!
								</div>");
	}
}