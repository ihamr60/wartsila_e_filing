<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller {

	public function userUploadDoc()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->session->user_id;
		$file_name					= $this->input->post('file_name');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$category 					= $this->input->post('category');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$id_notif					= $this->get_id();
		$uuid_file					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480';  
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name.'';
		$this->load->library('upload', $config);

		if(!empty($_FILES['attachment']['name'])) 
		{
	        if ( $this->upload->do_upload('attachment') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'uuid_filing'			=> $uuid_file,
					'attachment'			=> $foto['file_name'],
					'file_name'				=> $file_name,
					'PersNo'				=> $PersNo,
					'category'				=> $category,
					);
		
				$notif = array(		
					'uuid_notif' 			=> $id_notif,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $PersNo,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->insertData($data,'tbl_filing');
				header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Dokumen berhasil diupload!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function userUploadDoc_req_add()
	{
		$this->load->model('web_app_model');	
		$PersNo 					= $this->input->post('PersNo');
		$Uploader 					= $this->session->user_id;
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$uuid_employee_required_file= $this->input->post('uuid_employee_required_file');
		$uuid_filing_category 		= $this->input->post('uuid_filing_category');
		$file_name_req  			= $this->input->post('file_name_req');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$get_id						= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480';  
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name_req.'';
		$this->load->library('upload', $config);
		
		if(!empty($_FILES['attach_req']['name'])) 
		{
	        if ( $this->upload->do_upload('attach_req') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'uuid_filing_req'			  => $get_id,
					'attach_req'				  => $foto['file_name'],
					'file_name_req'				  => $file_name_req,
					'PersNo_req'				  => $PersNo,
					'uuid_filing_category'		  => $uuid_filing_category,
					'uuid_employee_required_file' => $uuid_employee_required_file,
					);
		
				$notif = array(		
					'uuid_notif' 			=> $get_id,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $Uploader,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->insertData($data,'tbl_filing_required');
				header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Dokumen berhasil diupload!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function userUploadDoc_req_update()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->input->post('PersNo');
		$Uploader 					= $this->session->user_id;
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$uuid_filing_req 			= $this->input->post('uuid_filing_req');
		$file_name_req  			= $this->input->post('file_name_req');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$get_id						= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480'; 
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name_req.'';
		$this->load->library('upload', $config);
		
		if(!empty($_FILES['attach_req']['name'])) 
		{
	        if ( $this->upload->do_upload('attach_req') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'attach_req'			=> $foto['file_name'],
					);
		
				$notif = array(		
					'uuid_notif' 			=> $get_id,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $Uploader,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$where = array(
					'uuid_filing_req'		=> $uuid_filing_req,
				);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->updateDataWhere($data,$where,'tbl_filing_required');
				header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Docuement has been changed!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function HRUploadDoc()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->input->post('PersNo');
		$Uploader 					= $this->session->user_id;
		$file_name					= $this->input->post('file_name');
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$category 					= $this->input->post('category');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$id_notif					= $this->get_id();
		$uuid_file					= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480'; 
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name.'';
		$this->load->library('upload', $config);
		
		if(!empty($_FILES['attachment']['name'])) 
		{
	        if ( $this->upload->do_upload('attachment') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'uuid_filing'			=> $uuid_file,
					'attachment'			=> $foto['file_name'],
					'file_name'				=> $file_name,
					'PersNo'				=> $PersNo,
					'category'				=> $category,
					);
		
				$notif = array(		
					'uuid_notif' 			=> $id_notif,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $Uploader,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->insertData($data,'tbl_filing');
				header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Dokumen berhasil diupload!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function HRUploadDoc_req_add()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->input->post('PersNo');
		$Uploader 					= $this->session->user_id;
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$uuid_employee_required_file= $this->input->post('uuid_employee_required_file');
		$uuid_filing_category 		= $this->input->post('uuid_filing_category');
		$file_name_req  			= $this->input->post('file_name_req');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$get_id						= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480'; 
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name_req.'';

		$this->load->library('upload', $config);
		
		if(!empty($_FILES['attach_req']['name'])) 
		{
	        if ( $this->upload->do_upload('attach_req') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'uuid_filing_req'		=> $get_id,
					'attach_req'			=> $foto['file_name'],
					'file_name_req'			=> $file_name_req,
					'PersNo_req'			=> $PersNo,
					'uuid_filing_category'	=> $uuid_filing_category,
					'uuid_employee_required_file' => $uuid_employee_required_file,
					);
		
				$notif = array(		
					'uuid_notif' 			=> $get_id,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $Uploader,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->insertData($data,'tbl_filing_required');
				header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Dokumen Required berhasil diupload!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function HRUploadDoc_req_update()
	{
		$this->load->model('web_app_model');
		$PersNo 					= $this->input->post('PersNo');
		$Uploader 					= $this->session->user_id;
		$First_name 				= $this->input->post('First_name');
		$Last_name 					= $this->input->post('Last_name');
		$uuid_filing_req 			= $this->input->post('uuid_filing_req');
		$file_name_req  			= $this->input->post('file_name_req');

		date_default_timezone_set('Asia/Jakarta');
		$message					= 'E-Filing Data <b>'.$First_name.' '.$Last_name.'</b> has been uploaded';
		$get_id						= $this->get_id();
		$date						= date("Y-n-d G:i:s");
		
		$config['upload_path'] 		= './upload/others';
		$config['allowed_types'] 	= 'jpg|png|jpeg|gif|pdf|doc';
		$config['max_size'] 		= '4480';  
		$config['overwrite']		= true;
		$config['file_name'] 		= ''.$PersNo.'-'.$file_name_req.'';

		$this->load->library('upload', $config);
		
		if(!empty($_FILES['attach_req']['name'])) 
		{
	        if ( $this->upload->do_upload('attach_req') ) 
	        {
	            $foto = $this->upload->data();		
				$data = array(		
					'attach_req'			=> $foto['file_name'],
					);
		
				$notif = array(		
					'uuid_notif' 			=> $get_id,
					'PersNo_notif' 			=> $PersNo,
					'Uploader' 				=> $Uploader,
					'message' 				=> $message,
					'date' 					=> $date,
					'attach'				=> "1",
					'person_tab'			=> "1",
					);

				$where = array(
					'uuid_filing_req'		=> $uuid_filing_req,
				);

				$this->web_app_model->insertData($notif,'tbl_notif');
				$this->web_app_model->updateDataWhere($data,$where,'tbl_filing_required');
				header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-success'>
												<strong>
													<i class='icon-ok'></i>
													Success!
												</strong>
												Dokumen Required has been Changed!
										</div>");
			}
			else 
			{
           		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
				$this->session->set_flashdata("info","
										<div class='alert alert-danger'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Gagal Upload
										</div>");
	    	}
	    }
		else 
		{
      		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
			$this->session->set_flashdata("info","
										<div class='alert alert-warning'>
												<strong>
													<i class='icon-ok'></i>
													Ups!
												</strong>
												Try Again !!
										</div>");
		}
	}

	public function deleteDoc()
	{
		$this->load->model('web_app_model');
		$PersNo 			= $this->session->user_id;
		$lampiran 			= $this->uri->segment(5);
		$uuid_filing 		= $this->uri->segment(4);
		$path 				= './upload/others/';
		@unlink($path.$lampiran);
		
		$where = array(		
			'uuid_filing'			=> $uuid_filing,
			);
		
		$this->web_app_model->deleteData('tbl_filing', $where);
		header('location:'.base_url().'index.php/e_filing/employee?tabFilling=1');
		$this->session->set_flashdata("info","
							<div class='alert alert-success'>
								<strong>
									<i class='icon-ok'></i>
									Okay!
								</strong>
								Data Lampiran E-Filing berhasil dihapus...!
							</div>");
	}

	public function HRdeleteDoc()
	{
		$this->load->model('web_app_model');
		$PersNo 			= $this->uri->segment(6);
		$lampiran 			= $this->uri->segment(5);
		$uuid_filing 		= $this->uri->segment(4);
		$path 				= './upload/others/';
		@unlink($path.$lampiran);

		$where = array(		
			'uuid_filing'			=> $uuid_filing,
			);
		
		$this->web_app_model->deleteData('tbl_filing', $where);
		header('location:'.base_url().'index.php/e_filing/hr/bg_detailEmployee/'.$PersNo.'');
		$this->session->set_flashdata("info","
							<div class='alert alert-success'>
								<strong>
									<i class='icon-ok'></i>
									Okay!
								</strong>
								Data Lampiran E-Filing berhasil dihapus...!
							</div>");
	}
}