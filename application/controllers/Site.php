<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Site extends MY_Controller {
    
    public function index()
    {   
        $data = [
            'scripts' => ['my/js/site']
        ];

        $this->render('site/site_view', $data);
    }
        
}
        
    /* End of file  Site.php */
        
                            