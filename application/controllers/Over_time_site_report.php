<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');

require('./exportexcel/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Supervisor;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnDimension;
use PhpOffice\PhpSpreadsheet\Worksheet\RowDimension;
use PhpOffice\PhpSpreadsheet\Worksheet\Dimension;

class Over_time_site_report extends MY_Controller {

    public function index($site_id, $month, $year)
    {
        $this->load->model(array('Employee_model', 'Shift_group_model', 'Overtime_model', 'Site_model'));
        $employee_list = $this->Shift_group_model->get_all_employee_by_site($site_id);
        $ot_sum_list = array();
        foreach ($employee_list as $value) {
            $current_ot_sum = array();
            $current_ot_sum = $value;
            $current_employee_id = $value['employee_id'];
            $current_ot_sum = $current_ot_sum + (array) $this->Overtime_model->overtime_report_summary_individual($current_employee_id, $year, $month);
            
            array_push($ot_sum_list, $current_ot_sum);
        }
            $data = [
                'scripts' => ['my/js/over_time_site_report'],
                'employee' => $employee_list,
                'ot_sum' => $ot_sum_list,
                'year' => $year,
                'month' => $month,
                'start_date' => date("Y M d", strtotime("$year-$month-01")),
                'end_date' => date("Y M t", strtotime("$year-$month-01")),
                'site' => $this->Site_model->get_by_id($site_id),
                'site_id' => $site_id
            ];
            
            $this->render('over-time/over_time_site_report_view', $data);                
    }
    
    public function select()
    {
        $this->load->model(array('Site_model', 'Overtime_model'));

        $sites = $this->Site_model->get_by_persmission();
        $years = array();
        $months = array();
        if(sizeof($sites) > 0){
            $years = $this->Overtime_model->get_existing_year_by_site($sites[0]['site_id']);
            if(sizeof($years) > 0)
                $months = $this->Overtime_model->get_existing_month_by_site_and_year($sites[0]['site_id'], $years[0]['year']);
        }
        $data = [
                'scripts' => ['my/js/over_time_site_report_select'],
                'site_list' => $sites,
                'default_years' => $years,
                'default_months' => $months
        ];
        $this->render('over-time/over_time_report_selection_view',$data);
    }

    public function exportToExcel($site_id, $month, $year)
    {
        $spreadsheet = new Spreadsheet();
        $this->load->model(array('Employee_model', 'Shift_group_model', 'Overtime_model', 'Site_model'));
        $employee_list = $this->Shift_group_model->get_all_employee_by_site($site_id);
        $ot_sum_list = array();
        foreach ($employee_list as $value) {
            $current_ot_sum = array();
            $current_ot_sum = $value;

            $current_employee_id = $value['employee_id'];
            $current_ot_sum = $current_ot_sum + (array) $this->Overtime_model->overtime_report_summary_individual($current_employee_id, $year, $month);
            array_push($ot_sum_list, $current_ot_sum);
        }
        $start_date = date("d M Y", strtotime("$year-$month-01"));
        $end_date = date("t M Y", strtotime("$year-$month-01"));
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A4', 'SUMMARY OF OVERTIME RECORD')
        ->setCellValue('I6', "Period : {$start_date} - {$end_date}")
        ->setCellValue('A8', 'No')
        ->setCellValue('B8', 'Pers No')
        ->setCellValue('C8', 'Name')
        ->setCellValue('D8', 'Period')
        ->setCellValue('E8', 'Total Real Hours')
        ->setCellValue('F8', 'Overtime Hours')
        ->setCellValue('F9', 'Factor Time')
        ->setCellValue('K8', 'Multipple/ Acc. Hours')
        ->setCellValue('L8', 'Employee Signatures');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('A4:L4');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('F8:J8');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('F9:J9');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('F8:J8');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('A8:A10');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('B8:B10');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('C8:C10');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('D8:E9');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('F9:J9');
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('K8:K10');
        $spreadsheet->setActiveSheetIndex(0)->getStyle('K8:K10')->getAlignment()->setWrapText(true); 
        $spreadsheet->setActiveSheetIndex(0)->mergeCells('L8:L10');
        $spreadsheet->setActiveSheetIndex(0)->getStyle('L8:L10')->getAlignment()->setWrapText(true); 
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('D10', 'From')
        ->setCellValue('E10', 'To')
        ->setCellValue('F10', '1.5')
        ->setCellValue('G10', '2')
        ->setCellValue('H10', '3')
        ->setCellValue('I10', '4')
        ->setCellValue('J10', 'Total Hours');
        $i=12;
        $border_style_else =  array(
                        'bottom' => 
                        array( 
                            'borderStyle' => Border::BORDER_DOTTED,
                            'color' => array( 
                                'rgb' => '808080'
                            ) 
                        ), 
                        'top' => array( 
                            'borderStyle' => Border::BORDER_DOTTED, 
                            'color' => array( 
                                'rgb' => '808080' 
                            ) 
                        ),
                        'right' => array( 
                            'borderStyle' => Border::BORDER_THIN, 
                            'color' => array( 
                                'rgb' => '000000' 
                            ) 
                        ) 
                            );
        foreach($ot_sum_list as $ot_sum_list) {
            $acc = sprintf("%.2f",(($ot_sum_list['oneofive'] * 1.5) + ($ot_sum_list['two'] * 2) + ($ot_sum_list['three'] * 3) + ($ot_sum_list['four'] * 4)));
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, ($i-11))
            ->setCellValue('B'.$i, $ot_sum_list['employee_id'])
            ->setCellValue('C'.$i, $ot_sum_list['full_name'])
            ->setCellValue('D'.$i, date("Y M d", strtotime("$year-$month-01")))
            ->setCellValue('E'.$i, date("Y M t", strtotime("$year-$month-01")))
            ->setCellValue('J'.$i, $ot_sum_list['total_works'] ? $ot_sum_list['total_works'] : '0.0' )
            ->setCellValue('F'.$i, $ot_sum_list['oneofive'] ? $ot_sum_list['oneofive'] : '0.0' )
            ->setCellValue('G'.$i, $ot_sum_list['two'] ? $ot_sum_list['two'] : '0.0' )
            ->setCellValue('H'.$i, $ot_sum_list['three'] ? $ot_sum_list['three'] : '0.0' )
            ->setCellValue('I'.$i, $ot_sum_list['four'] ? $ot_sum_list['four'] : '0.0' )
            ->setCellValue('K'.$i, $acc );
            $spreadsheet->getActiveSheet()
            ->getStyle("A{$i}")->
            getBorders()->
            applyFromArray(
                 array(
                        'bottom' => 
                        array( 
                            'borderStyle' => Border::BORDER_DOTTED,
                            'color' => array( 
                                'rgb' => '808080'
                            ) 
                        ), 
                        'top' => array( 
                            'borderStyle' => Border::BORDER_DOTTED, 
                            'color' => array( 
                                'rgb' => '808080' 
                            ) 
                        ),
                        'left' => array( 
                            'borderStyle' => Border::BORDER_MEDIUM, 
                            'color' => array( 
                                'rgb' => '000000' 
                            ) 
                        ),
                        'right' => array( 
                            'borderStyle' => Border::BORDER_THIN, 
                            'color' => array( 
                                'rgb' => '000000' 
                            ) 
                        ) 
                    ) 
            );
            $column = "B";
            for ($k = 0; $k<11; $k++){
                $spreadsheet->getActiveSheet()->getStyle("{$column}{$i}")->getBorders()->applyFromArray($border_style_else);
                $column++;
            }

            $i++;
        }

        $border_style_separate_else = array( 
                                        'top' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'right' => array( 
                                            'borderStyle' => Border::BORDER_THIN, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ) 
                                      );
        $border_style_separate_a = array( 
                                        'top' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'left' => array( 
                                            'borderStyle' => Border::BORDER_MEDIUM, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ),
                                        'right' => array( 
                                            'borderStyle' => Border::BORDER_THIN, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ) 
                                    );
        $spreadsheet->getActiveSheet()->getStyle("A11")->getBorders()->applyFromArray($border_style_separate_a);
        
        $column = 'B';
        for ($j=0; $j < 11; $j++) { 
            $spreadsheet->getActiveSheet()->getStyle("{$column}11")->getBorders()->applyFromArray($border_style_separate_else);
            $column++;
        }

        $border_style_header_a = array( 
                                        'top' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'bottom' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'left' => array( 
                                            'borderStyle' => Border::BORDER_MEDIUM, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ),
                                        'right' => array( 
                                            'borderStyle' => Border::BORDER_THIN, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ) 
                                    );
        $border_style_header_else = array( 
                                        'top' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'bottom' => array( 
                                            'borderStyle' => Border::BORDER_DOUBLE, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        ),
                                        'right' => array( 
                                            'borderStyle' => Border::BORDER_THIN, 
                                            'color' => array( 
                                                'rgb' => '000000' 
                                            ) 
                                        ) 
                                    );

        $merge_style_header = array( 
                                        'bottom' => array( 
                                            'borderStyle' => Border::BORDER_THIN, 
                                            'color' => array( 
                                                'rgb' => '808080' 
                                            ) 
                                        )
                                    );
        $spreadsheet->getActiveSheet()->getStyle("A8:A10")->getBorders()->applyFromArray($border_style_header_a);
        $column = 'B';
        for ($j=0; $j < 11; $j++) { 
            $spreadsheet->getActiveSheet()->getStyle("{$column}8:{$column}10")->getBorders()->applyFromArray($border_style_header_else);
            $column++;
        }
        $spreadsheet->getActiveSheet()->getStyle("D8:E9")->getBorders()->applyFromArray($merge_style_header);
        $spreadsheet->getActiveSheet()->getStyle("F8:J8")->getBorders()->applyFromArray($merge_style_header);
        $spreadsheet->getActiveSheet()->getStyle("F9:J9")->getBorders()->applyFromArray($merge_style_header);
        $spreadsheet->getActiveSheet()->getStyle("L8:L10")->getBorders()->applyFromArray(array(
             'right' => array(
                'borderStyle' => Border::BORDER_THIN,
                'color' => array( 
                    'rgb' => '000000' 
                )
            )));
        $i = $i - 1;
        $spreadsheet->setActiveSheetIndex(0)->getStyle("A1:L{$i}")->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle("A{$i}:L{$i}")->getBorders()->applyFromArray(array(
             'bottom' => array(
                'borderStyle' => Border::BORDER_THIN,
                'color' => array( 
                    'rgb' => '808080' 
                )
            )));
        $spreadsheet->setActiveSheetIndex(0)->getStyle("A6")->getAlignment()->setHorizontal('left');
        $spreadsheet->setActiveSheetIndex(0)->getStyle("A4:L4")->getFont()->applyFromArray(array( 'bold' => TRUE, 'size' => 14 ));
        $spreadsheet->setActiveSheetIndex(0)->getStyle("A8:L10")->getAlignment()->setVertical('center');
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("A")->setWidth(floatval(4));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("B")->setWidth(floatval(15));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("C")->setWidth(floatval(30));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("D")->setWidth(floatval(15));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("E")->setWidth(floatval(15));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("F")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("G")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("H")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("I")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("J")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("K")->setWidth(floatval(10));
        $spreadsheet->setActiveSheetIndex(0)->getColumnDimension("L")->setWidth(floatval(19));
        $spreadsheet->setActiveSheetIndex(0)->getRowDimension("11")->setRowHeight(floatval(2));
        $site = $this->Site_model->get_by_id($site_id);
        $site_name = $site->site_name;
        $spreadsheet->setActiveSheetIndex(0)->setCellValue('A6', "Project : {$site_name}");
        $title = $site_name . '-' . $month . '-' . $year;
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=\"$title.xlsx\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
        $data = [
            'scripts' => ['my/js/over_time_site_report'],
            'employee' => $employee_list,
            'ot_sum' => $ot_sum_list,
            'year' => $year,
            'month' => $month,
            'start_date' => date("Y M d", strtotime("$year-$month-01")),
            'end_date' => date("Y M t", strtotime("$year-$month-01")),
            'site' => $this->Site_model->get_by_id($site_id)
        ];
        
        $this->render('over-time/over_time_site_report_exporttoexcel_view', $data);
    }

}
        
    /* End of file  Over_time_site_report.php */
        
                            