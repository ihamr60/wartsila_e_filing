<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Shift_group extends MY_Controller {

    public function index()
    {   
        $this->load->model('Site_model');

        $data = [
            'scripts' => ['my/js/shift_group'],
            'sites' => $this->Site_model->get_by_persmission()
        ];

        $this->render('shift-group/shift_group_view', $data);
    }

    public function new_shift_group($site_id = null,$group_id = null)
    {
        $this->load->model('Site_model');
        $this->load->model('Shift_group_model');
        $data = [
            'scripts' => ['my/js/shift_group_new'],
            'sites' => $this->Site_model->get_by_persmission(),
            'site_id' => $site_id,
            'group_name' => $this->Shift_group_model->get_group_name($group_id),
            'group_members' => array()
        ];

        // 'group_members' => $this->Shift_group_model->get_members($group_id)

        $this->render('shift-group/shift_group_new_view', $data);
    }
    
    public function edit($group_id)
    {
        $this->load->model('Shift_group_model');

        $data = [
            'scripts' => ['my/js/shift_group_edit'],
            'site' => $this->Shift_group_model->get_site($group_id),
            'group_id' => $group_id,
            'group_name' => $this->Shift_group_model->get_group_name($group_id),
            'group_members' => $this->Shift_group_model->get_members($group_id)
        ];

        $this->render('shift-group/shift_group_edit_view', $data);
    }
}
        
    /* End of file  Shift_group.php */
        
                            