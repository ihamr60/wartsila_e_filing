<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends MY_Controller {
    
    public function index()
    {  
        $this->load->model('Site_model'); 
        $data = [
            'scripts' => ['my/js/schedule'],
            'sites' => $this->Site_model->get_by_persmission()
        ];
        $this->render('schedule/schedule_view', $data);
    }

    public function preview($site_id,$date)
    {
        $this->load->model(array('Schedule_model', 'Site_model')); 

        $month = date('m',strtotime($date));
        $year = date('Y',strtotime($date));

        $data = [
            'scripts' => ['fullcalendar/core/main.min', 'fullcalendar/daygrid/main.min', 'fullcalendar/list/main.min', 'my/js/schedule_preview'],
            'csses' => ['fullcalendar/core/main.min', 'fullcalendar/daygrid/main.min', 'fullcalendar/list/main.min'],
            'date' => $date,
            'events' => $this->Schedule_model->get_schedule_list($site_id,$month,$year),
            'site_name' => $this->Site_model->get_by_id($site_id)->site_name
        ];
        $this->render('schedule/preview_view',$data);
    }

}