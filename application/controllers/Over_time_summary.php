<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');

require('./exportexcel/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;  
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Supervisor;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnDimension;
use PhpOffice\PhpSpreadsheet\Worksheet\RowDimension;
use PhpOffice\PhpSpreadsheet\Worksheet\Dimension;
class Over_time_summary extends MY_Controller {

    public function index($year)
    {
        $this->load->model('Overtime_model');

        $data = [
            'scripts' => ['my/js/over_time_summary'],
            'ot_sum_dept' => $this->Overtime_model->overtime_report_summary_per_dept_per_month($year),
            'ot_sum_month' => $this->Overtime_model->overtime_report_summary_per_month($year),
            'year' => $year
        ];
        $this->render('over-time/over_time_summary_view', $data);
    }

    public function select()
    {
        $data = [
            'scripts' => ['my/js/over_time_summary_select']
        ];
        $this->render('over-time/over_time_summary_select_view',$data);
    }

    public function exportToExcel($year)
    {
        $spreadsheet = new Spreadsheet();
        $this->load->model('Overtime_model');
         /* start of sheet summary overtime per dept */
        $ot_sum_dept = $this->Overtime_model->overtime_report_summary_per_dept_per_month($year);
        if((sizeof($ot_sum_dept) > 0) ) {
            $header = array_keys(get_object_vars($ot_sum_dept[0]));
        }
        // summaryovertimepersite
        $summaryovertimepersite = $spreadsheet->setActiveSheetIndex(0);
        $summaryovertimepersite->setTitle('summaryovertimepersite');
        $summaryovertimepersite->setCellValue( 'A9', 'No.');
        $summaryovertimepersite->setCellValue( 'B9', 'Date.');
        $column = 'C';
        $count = 1;
        if (sizeof($ot_sum_dept) > 0) {
            foreach ($header as $value) {
                if($value == 'date') {
                    continue;
                }
                $summaryovertimepersite->setCellValue( $column.'9', $value);
                $cell_target_for_x_axis = 'summaryovertimepersite!$C$9:$' . $column .'$9' ;
                $xAxisTickValues = [
                    new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING,$cell_target_for_x_axis , null, $count)
                ]; //X-Axis
                $column++;
                $count++;
            }
            $row = 10;
            $column = 'A';
            $dataSeriesLabels = array();
            $dataSeriesValues = array();
            $count = 1;
            $counter = 1;
            foreach ($ot_sum_dept as $value ) {
                $summaryovertimepersite->setCellValue( $column.$row, $counter);
                $column++;
                $cell_target = 'summaryovertimepersite!$'.$column.'$' . $row ;
                array_push($dataSeriesLabels, new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $cell_target , null, 1)); //Bar Label
                for($i = 0; $i < sizeof($header) ; $i++) {
                    $obj_name = strval($header[$i]);
                    $cell_value = $value->$obj_name;
                    $summaryovertimepersite->setCellValue( $column.$row, $cell_value);
                    $column++;
                    $count++;
                }
                $column_ascii = ord($column) - 1;
                $column = chr($column_ascii);
                $lastcolumn = $column;
                $cell_target_for_data_values = 'summaryovertimepersite!$C$' . $row . ':$' . $column . '$' . $row;
                array_push($dataSeriesValues, new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $cell_target_for_data_values, null, ($count-1) ));
                $count = 1;
                $counter++;
                $column = 'A';
                $row++;

            }
            $series = new DataSeries(
                DataSeries::TYPE_BARCHART, // plotType
                DataSeries::GROUPING_STANDARD, // plotGrouping
                range(0, count($dataSeriesValues) - 1), // plotOrder
                $dataSeriesLabels, // plotLabel
                $xAxisTickValues, // plotCategory
                $dataSeriesValues        // plotValues
            );
            $series->setPlotDirection(DataSeries::DIRECTION_COL);
            $plotArea = new PlotArea(null, [$series]);
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            $chart_title = new Title('Summary Report');
            $yAxisLabel = new Title('');
            $chart = new Chart(
                'Summary Overtime', // name
                $chart_title, // title
                $legend, // legend
                $plotArea, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                $yAxisLabel  // yAxisLabel
            );
            $row += 5;
            $endcharty = $row + 30;
            $endchartx = $lastcolumn++;
            for ($i = 0;$i < 6; $i++)
            {
                $endchartx++;
            }

            /*                  Start Style               */
            $endrow = $row-5;
            $column_ascii = ord($lastcolumn) - 1;
            $column = chr($column_ascii);
            $lastcolumn = $column;
            $summaryovertimepersite->setCellValue( 'A4', 'Summary Overtime per Site');
            $summaryovertimepersite->mergeCells("A4:{$lastcolumn}4");
            $summaryovertimepersite->getStyle("A4:{$lastcolumn}4")->getFont()->applyFromArray(array( 'bold' => TRUE, 'size' => 14 ));
            $summaryovertimepersite->getStyle("A9:L{$endrow}")->getAlignment()->setHorizontal('center');
            $summaryovertimepersite->getStyle("A4:{$lastcolumn}4")->getAlignment()->setHorizontal('center');
            $summaryovertimepersite->setCellValue( "{$lastcolumn}6", "Period : $year");
            $summaryovertimepersite->getStyle("{$lastcolumn}6")->getAlignment()->setWrapText(true);
            $chart->setTopLeftPosition("A$row");
            $chart->setBottomRightPosition("{$endchartx}{$endcharty}");
            $summaryovertimepersite->getColumnDimension("A")->setWidth(floatval(4));
            $summaryovertimepersite->getColumnDimension("B")->setWidth(floatval(18));
            $column = 'C';
            while ( $column <= $lastcolumn) {
                $summaryovertimepersite->getColumnDimension($column)->setWidth(floatval(15));
                

                $column++;
                
            }
            $row_looping=9;
            $column = 'A';
            $border_style_header_a= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_DOUBLE,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ), 
                                    'top' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'left' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            $border_style_header= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_DOUBLE,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ), 
                                    'top' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'left' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );

            $border_style_data_a= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_THIN,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ),
                                    'left' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            $border_style_data= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_THIN,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            while($row_looping != $endrow){
                while($column <= $lastcolumn){
                    if ($row_looping == 9) {
                        if($column == 'A') {
                            $style = $border_style_header_a;
                        }
                        else{
                            $style = $border_style_header;
                        }
                    }
                    else{
                        if($column == 'A') {
                            $style = $border_style_data_a;
                        }
                        else{
                            $style = $border_style_data;
                        }
                    }
                    $summaryovertimepersite->getStyle("{$column}{$row_looping}")->getBorders()->applyFromArray($style);
                    $column++;
                }
                $column = 'A';
                $row_looping++;
            }
            
            /*                  End Style               */
            $summaryovertimepersite->addChart($chart);
            /* end of sheet summary overtime per dept */

            /* start of sheet summary overtime per month */
            $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'summaryovertimepermonth');
            $spreadsheet->addSheet($myWorkSheet, 1);
            $worksheet = $spreadsheet->setActiveSheetIndex(1);
            $ot_sum_month = $this->Overtime_model->overtime_report_summary_per_month($year);

            $worksheet->setCellValue( 'A9', 'Date');
            $worksheet->setCellValue( 'B9', 'Total Works');
            
            $row = 10;
            $count = 1;
            $dataSeriesLabels = array();
            $dataSeriesValues = array();
            $xAxisTickValues = array();
            foreach ($ot_sum_month as $value ) {
                $worksheet->setCellValue( "A{$row}", $value->date);
                $worksheet->setCellValue( "B{$row}", $value->total_works);
                
                $cell_target = 'summaryovertimepermonth!$A$' . $row ;
                array_push($dataSeriesLabels, new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'Month' , null, 1)); //Bar Label
                
                
                $row++;
                $count++;
            }
            $cell_target_for_x_axis = 'summaryovertimepermonth!$A$10:$A$' . ($row-1) ;
            array_push($xAxisTickValues, new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, $cell_target_for_x_axis, null, 12 ));
            $cell_target_for_data_values = 'summaryovertimepermonth!$B$10:$B$' . ($row-1) ;
            array_push($dataSeriesValues, new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, $cell_target_for_data_values, null, 12 ));
            $series = array();
            $lastcolumn = 'B';
            $series = new DataSeries(
                DataSeries::TYPE_BARCHART, // plotType
                DataSeries::GROUPING_STANDARD, // plotGrouping
                range(0, count($dataSeriesValues) - 1), // plotOrder
                $dataSeriesLabels, // plotLabel
                $xAxisTickValues, // plotCategory
                $dataSeriesValues        // plotValues
            );
            $series->setPlotDirection(DataSeries::DIRECTION_COL);
            $plotArea = new PlotArea(null, [$series]);
            $legend = new Legend(Legend::POSITION_RIGHT, null, false);
            $chart_title = new Title('Summary Report');
            $yAxisLabel = new Title('');
            $chart = new Chart(
                'Summary Overtime', // name
                $chart_title, // title
                null, // legend
                $plotArea, // plotArea
                true, // plotVisibleOnly
                0, // displayBlanksAs
                null, // xAxisLabel
                $yAxisLabel  // yAxisLabel
            );
            $endcharty = $row + 30;
            $endchartx = $lastcolumn;

            for ($i = 0;$i < 6; $i++)
            {
                $endchartx++;
            }
            $xstart = $row + 5;
            $chart->setTopLeftPosition("A$xstart");
            $chart->setBottomRightPosition("{$endchartx}{$endcharty}");

            $worksheet->getColumnDimension("A")->setWidth(floatval(32));
            $worksheet->getColumnDimension("B")->setWidth(floatval(32));
            $endrow = $row;
            $worksheet->setCellValue( 'A4', 'Summary Overtime per Month');
            $worksheet->mergeCells('A4:C4');
            $worksheet->getStyle("A4")->getAlignment()->setHorizontal('center');
            $worksheet->getStyle("A4:{$lastcolumn}4")->getFont()->applyFromArray(array( 'bold' => TRUE, 'size' => 14 ));
            $worksheet->getStyle("A9:L{$endrow}")->getAlignment()->setHorizontal('center');
            $worksheet->getStyle("A4:{$lastcolumn}4")->getAlignment()->setHorizontal('center');
            $worksheet->setCellValue( "A6", "Period : $year");
            $worksheet->getStyle("A6")->getAlignment()->setWrapText(true);

            
            $row_looping=9;
            $column = 'A';
            $border_style_header_a= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_DOUBLE,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ), 
                                    'top' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'left' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            $border_style_header= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_DOUBLE,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ), 
                                    'top' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_DOUBLE, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );

            $border_style_data_a= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_THIN,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ),
                                    'left' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            $border_style_data= array(
                                    'bottom' => 
                                    array( 
                                        'borderStyle' => Border::BORDER_THIN,
                                        'color' => array( 
                                            'rgb' => '000000'
                                        ) 
                                    ),
                                    'right' => array( 
                                        'borderStyle' => Border::BORDER_THIN, 
                                        'color' => array( 
                                            'rgb' => '000000' 
                                        ) 
                                    ) 
                                );
            while($row_looping != $endrow){
                while($column <= $lastcolumn){
                    if ($row_looping == 9) {
                        if($column == 'A') {
                            $style = $border_style_header_a;
                        }
                        else{
                            $style = $border_style_header;
                        }
                    }
                    else{
                        if($column == 'A') {
                            $style = $border_style_data_a;
                        }
                        else{
                            $style = $border_style_data;
                        }
                    }
                    $worksheet->getStyle("{$column}{$row_looping}")->getBorders()->applyFromArray($style);
                    $column++;
                }
                $column = 'A';
                $row_looping++;
            }

            $worksheet->addChart($chart);

            /* end of sheet summary overtime per month */
            $title = 'Overtime Summary ' . $year;
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=\"$title.xlsx\"");
            header("Pragma: no-cache");
            header("Expires: 0");

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->setIncludeCharts(true);
            $callStartTime = microtime(true);
            $writer->save('php://output');
            exit;
        }
        
    }
        
}
        
    /* End of file  overtime_summary.php */
        
                            