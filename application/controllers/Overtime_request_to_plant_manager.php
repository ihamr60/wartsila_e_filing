<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Overtime_request_to_plant_manager extends MY_Controller {

    public function index() {
        $this->load->model('Employee_model');

        $data = [
            'scripts' => ['my/js/over_time_request_to_plant_manager'],
            'employees' => $this->Employee_model->get_employees_by_superior($this->session->user_id),
            'plantManagers' => $this->Employee_model->get_plant_manager()
        ];
        $this->render('over-time/over_time_request_to_plant_manager_view', $data);
    }
        
}
        
    /* End of file  Overtime_request_to_plant_manager.php */
        
                            