<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hr extends MY_Controller {

	public function bg_title()
	{   	
		$this->load->model('web_app_model');
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		$bc['data_parent_id']	= $this->web_app_model->getWhere('','parent_id','title');
		$this->render('e_cp/hr/bg_title', $bc);	
	}

	public function bg_content()
	{   	
		function limit_words($string, $word_limit)
		{
		    $words = explode(" ",$string);
		    return implode(" ",array_splice($words,0,$word_limit));
		}
		$this->load->model('web_app_model');
		$bc['data_content']		= $this->web_app_model->getJoin('title_id','title_id','title','content');
		$this->render('e_cp/hr/bg_content', $bc);	
	}

	public function bg_add_content()
	{   	
		$this->load->model('web_app_model');
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		$this->render('e_cp/hr/bg_add_content', $bc);	
	}

	public function bg_edit_content()
	{   	
		$this->load->model('web_app_model');
		$content_id 			= $this->uri->segment(4);
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		$bc['data_content'] 	= $this->web_app_model->getWhereOneItem($content_id,'content_id','content');
		$this->render('e_cp/hr/bg_edit_content', $bc);
	}

	public function add_content()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$content_id				= $this->get_id();	
		$content_ind			= $this->input->post('content_ind');
		$content_eng			= $this->input->post('content_eng');
		$created_at				= date('Y-m-d H:i:s');
		
		$data = array(	
			'content_id' 	  	 => $content_id,
			'title_id' 	  	 	 => $title_id,
			'content_ind'		 => $content_ind,
			'content_eng'		 => $content_eng,
			'created_at'		 => $created_at,
			);

		$this->web_app_model->insertData($data,'content');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Content data added successfully!
									</div>");
	}

	public function edit_content()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$content_id				= $this->input->post('content_id');	
		$content_ind			= $this->input->post('content_ind');
		$content_eng			= $this->input->post('content_eng');
		$updated_at				= date('Y-m-d H:i:s');
		
		$data = array(	
			'title_id' 	  	 	 => $title_id,
			'content_ind'		 => $content_ind,
			'content_eng'		 => $content_eng,
			'updated_at'		 => $updated_at,
			);

		$where = array(	
			'content_id' 	  	 => $content_id,
			);

		$this->web_app_model->updateDataWhere($data,$where,'content');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Content data has been updated!
									</div>");
	}

	public function deleteContent()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('content_id'=>$id);
		
		$this->web_app_model->deleteData('content',$hapus);
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Content Data has been deleted!
								</div>");
	}

	public function add_title()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->get_id();
		$title_ind				= $this->input->post('title_ind');
		$title_eng				= $this->input->post('title_eng');
		$parent_id				= $this->input->post('parent_id');
		$created_at				= date('Y-m-d H:i:s');
		$level_max				= $this->web_app_model->getOneItemWhereOrderByLimit($parent_id,'parent_id','level','DESC','1','title');
		$level					= $level_max['level'] + 1;	

		$data = array(	
			'title_id' 	  		 => $title_id,
			'title_ind' 	  	 => $title_ind,
			'title_eng'		 	 => $title_eng,
			'parent_id'			 => $parent_id,
			'created_at'		 => $created_at,
			'level'			 	 => $level,
			);

		$this->web_app_model->insertData($data,'title');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Title data added successfully!
									</div>");
	}

	public function edit_title()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$title_ind				= $this->input->post('title_ind');
		$title_eng				= $this->input->post('title_eng');
		$parent_id_update		= $this->input->post('parent_id');
		$parent_id_ori			= $this->input->post('parent_id_ori');
		$updated_at				= date('Y-m-d H:i:s');

		if($parent_id_ori==$parent_id_update)
		{
			$level					= $this->input->post('level');
		}
		else
		{
			$level_max				= $this->web_app_model->getOneItemWhereOrderByLimit($parent_id_update,'parent_id','level','DESC','1','title');
			$level					= $level_max['level'] + 1;	
		}

		$data = array(	
			'title_ind' 	  	 => $title_ind,
			'title_eng'		 	 => $title_eng,
			'parent_id'			 => $parent_id_update,
			'updated_at'		 => $updated_at,
			'level'			 	 => $level,
			);

		$where = array(	
			'title_id' 	  		 => $title_id,
			);

		$this->web_app_model->updateDataWhere($data,$where,'title');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Title data has been updated!
									</div>");
	}

	public function deleteTitle()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('title_id'=>$id);
		
		$cek        = $this->web_app_model->getwhere($id,'title_id','content');
		if (count($cek)>0)
		{
			header('location:'.base_url().'index.php/e_cp/hr/bg_title');
			$this->session->set_flashdata("info","<div class='alert alert-warning'>
										<strong>
											<i class='icon-ok'></i>
											Warning!
										</strong>
										Title data is being used from the content table!
								</div>");
		}
		else
		{
		$this->web_app_model->deleteData('title',$hapus);
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Title Data has been deleted!
								</div>");
		}
	}
}