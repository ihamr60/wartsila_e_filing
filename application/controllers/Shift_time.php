<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shift_time extends MY_Controller {
    
    public function index()
    {   
        $this->load->model('Site_model');
        $data = [
            'scripts' => ['my/js/shift_time'],
            'sites' => $this->Site_model->get_by_persmission()
        ];
        $this->render('shift-time/shift_time_view', $data);
    }

}