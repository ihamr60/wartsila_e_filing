<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Over_time_history extends MY_Controller {

public function index()
{
        $data = [
            'scripts' => ['my/js/over_time_history']
        ];
        $this->render('over-time/over_time_history_view', $data);
}
        
}
        
    /* End of file  Over_time_history.php */
        
                            