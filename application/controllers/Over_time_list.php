<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Over_time_list extends MY_Controller {

public function index()
{
        $data = [
            'scripts' => ['my/js/over_time_list']
        ];
        $this->render('over-time/over_time_list_view', $data);
}
        
}
        
    /* End of file  Overtime list.php */
        
                            