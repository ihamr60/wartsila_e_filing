<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adm_permiss extends MY_Controller {

	public function index()
	{   	
		$this->load->model('web_app_model');
		$bc['data_adm_permiss'] 		= $this->web_app_model->getJoinGroup('employee_id','employee_id','PersNo','overtime_site_report_permission','employee');
		//$bc['data_parent_id']	= $this->web_app_model->getWhere('','parent_id','title');
		$this->render('adm_permiss/bg_adm_permiss', $bc);	
	}

	public function bg_add_permiss()
	{   	
		$this->load->model('web_app_model');
		$bc['data_site']				= $this->web_app_model->getAllData('site');
		$bc['data_employee']			= $this->web_app_model->getAllData('employee');
		$bc['data_adm_permiss'] 		= $this->web_app_model->getJoinWhere('site_id','site_id','overtime_site_report_permission','site','employee_id',$this->uri->segment(3));
		$this->render('adm_permiss/bg_add_permiss', $bc);	
	}

	public function bg_edit_permiss()
	{   	
		$this->load->model('web_app_model');
		$bc['data_employee'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'PersNo','employee');
		$bc['data_adm_permiss'] 		= $this->web_app_model->getJoinWhere('site_id','site_id','overtime_site_report_permission','site','employee_id',$this->uri->segment(3));
		$bc['data_site']				= $this->web_app_model->getAllData('site');
		$this->render('adm_permiss/bg_edit_permiss', $bc);	
	}

	public function deletePermiss()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(3);
		$PersNo 	= $this->uri->segment(4);
		$hapus 		= array('overtime_site_report_permission_id'=>$id);
		
		$this->web_app_model->deleteData('overtime_site_report_permission',$hapus);
		header('location:'.base_url().'index.php/adm_permiss/bg_edit_permiss/'.$PersNo.'');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Permission Data has been deleted!
								</div>");
	}

	public function addPermiss()
	 {
		$this->load->model('web_app_model'); 
		$site_id							= $this->input->post('site_id');
		$overtime_site_report_permission_id	= $this->get_id();
		$employee_id 						= $this->input->post('employee_id');
		
		$data = array(	
			'site_id' 	  	 					=> $site_id,
			'employee_id' 	  	 				=> $employee_id,
			'overtime_site_report_permission_id'=> $overtime_site_report_permission_id,
			);

		$this->web_app_model->insertData($data,'overtime_site_report_permission');	
		header('location:'.base_url().'index.php/adm_permiss/bg_edit_permiss/'.$employee_id.'');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Permission data added successfully!
									</div>");
	}

	public function deleteAdminPermiss()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(3);
		$hapus 		= array('employee_id'=>$id);
		
		$this->web_app_model->deleteData('overtime_site_report_permission',$hapus);
		header('location:'.base_url().'index.php/adm_permiss/');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Admin Data has been deleted!
								</div>");
	}









//=============================================================== OLD
	public function bg_title()
	{   	
		$this->load->model('web_app_model');
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		//$bc['data_parent_id']	= $this->web_app_model->getWhere('','parent_id','title');
		$this->render('e_cp/hr/bg_title', $bc);	
	}

	public function bg_content()
	{   	
		function limit_words($string, $word_limit)
		{
		    $words = explode(" ",$string);
		    return implode(" ",array_splice($words,0,$word_limit));
		}
		$this->load->model('web_app_model');
		$bc['data_content']		= $this->web_app_model->getJoin('title_id','title_id','title','content');
		$this->render('e_cp/hr/bg_content', $bc);	
	}

	public function bg_add_content()
	{   	
		$this->load->model('web_app_model');
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		$this->render('e_cp/hr/bg_add_content', $bc);	
	}

	public function bg_edit_content()
	{   	
		$this->load->model('web_app_model');
		$content_id 			= $this->uri->segment(4);
		$bc['data_title'] 		= $this->web_app_model->getAllData('title');
		$bc['data_content'] 	= $this->web_app_model->getWhereOneItem($content_id,'content_id','content');
		$this->render('e_cp/hr/bg_edit_content', $bc);
	}

	public function add_content()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$content_id				= $this->get_id();	
		$content_ind			= $this->input->post('content_ind');
		$content_eng			= $this->input->post('content_eng');
		$created_at				= date('Y-m-d H:i:s');
		
		$data = array(	
			'content_id' 	  	 => $content_id,
			'title_id' 	  	 	 => $title_id,
			'content_ind'		 => $content_ind,
			'content_eng'		 => $content_eng,
			'created_at'		 => $created_at,
			);

		$this->web_app_model->insertData($data,'content');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Content data added successfully!
									</div>");
	}

	public function edit_content()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$content_id				= $this->input->post('content_id');	
		$content_ind			= $this->input->post('content_ind');
		$content_eng			= $this->input->post('content_eng');
		$updated_at				= date('Y-m-d H:i:s');
		
		$data = array(	
			'title_id' 	  	 	 => $title_id,
			'content_ind'		 => $content_ind,
			'content_eng'		 => $content_eng,
			'updated_at'		 => $updated_at,
			);

		$where = array(	
			'content_id' 	  	 => $content_id,
			);

		$this->web_app_model->updateDataWhere($data,$where,'content');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Content data has been updated!
									</div>");
	}

	public function deleteContent()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('content_id'=>$id);
		
		$this->web_app_model->deleteData('content',$hapus);
		header('location:'.base_url().'index.php/e_cp/hr/bg_content');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Content Data has been deleted!
								</div>");
	}

	public function add_title()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->get_id();
		$title_ind				= $this->input->post('title_ind');
		$title_eng				= $this->input->post('title_eng');
		$parent_id				= $this->input->post('parent_id');
		$created_at				= date('Y-m-d H:i:s');
		$level_max				= $this->web_app_model->getOneItemWhereOrderByLimit($parent_id,'parent_id','level','DESC','1','title');
		$level					= $level_max['level'] + 1;	

		$data = array(	
			'title_id' 	  		 => $title_id,
			'title_ind' 	  	 => $title_ind,
			'title_eng'		 	 => $title_eng,
			'parent_id'			 => $parent_id,
			'created_at'		 => $created_at,
			'level'			 	 => $level,
			);

		$this->web_app_model->insertData($data,'title');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success!
											</strong>
											Title data added successfully!
									</div>");
	}

	public function edit_title()
	 {
	 	date_default_timezone_set('Asia/Jakarta');
		$this->load->model('web_app_model'); 
		$title_id				= $this->input->post('title_id');
		$title_ind				= $this->input->post('title_ind');
		$title_eng				= $this->input->post('title_eng');
		$parent_id_update		= $this->input->post('parent_id');
		$parent_id_ori			= $this->input->post('parent_id_ori');
		$updated_at				= date('Y-m-d H:i:s');

		if($parent_id_ori==$parent_id_update)
		{
			$level					= $this->input->post('level');
		}
		else
		{
			$level_max				= $this->web_app_model->getOneItemWhereOrderByLimit($parent_id_update,'parent_id','level','DESC','1','title');
			$level					= $level_max['level'] + 1;	
		}

		$data = array(	
			'title_ind' 	  	 => $title_ind,
			'title_eng'		 	 => $title_eng,
			'parent_id'			 => $parent_id_update,
			'updated_at'		 => $updated_at,
			'level'			 	 => $level,
			);

		$where = array(	
			'title_id' 	  		 => $title_id,
			);

		$this->web_app_model->updateDataWhere($data,$where,'title');	
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","
									<div class='alert alert-success'>
											<strong>
												<i class='icon-ok'></i>
												Success! - 
											</strong>
											Title data has been updated!
									</div>");
	}

	public function deleteTitle()
	{
		$this->load->model('web_app_model'); 
		$id 		= $this->uri->segment(4);
		$hapus 		= array('title_id'=>$id);
		
		$cek        = $this->web_app_model->getwhere($id,'title_id','content');
		if (count($cek)>0)
		{
			header('location:'.base_url().'index.php/e_cp/hr/bg_title');
			$this->session->set_flashdata("info","<div class='alert alert-warning'>
										<strong>
											<i class='icon-ok'></i>
											Warning!
										</strong>
										Title data is being used from the content table!
								</div>");
		}
		else
		{
		$this->web_app_model->deleteData('title',$hapus);
		header('location:'.base_url().'index.php/e_cp/hr/bg_title');
		$this->session->set_flashdata("info","<div class='alert alert-success'>
										<strong>
											<i class='icon-ok'></i>
											Okay!
										</strong>
										Title Data has been deleted!
								</div>");
		}
	}
}