<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function get_id()
	{
		$result = $this->db->query("Select UPPER(REPLACE(UUID(), '-', '')) as Id");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
	}
	
	public function __construct(){
		parent::__construct();
		
		// DEV
// 		$this->load->model('Employee_model');
// 		//$this->session->user_id = '10029402';
// 		$this->session->user_id = $_GET['user_id'];
// 		$this->session->isHasSuperior = $this->Employee_model->getSuperior($this->session->user_id);

// 		$tmp_menu = $this->Employee_model->get_menu($this->session->user_id);

// 		$menu = [];

// 		foreach ($tmp_menu as $key => $value) {
// 			$menu[] = $value['menu_id'];
// 		}

// 		$this->session->userMenu = $menu;
// 		$this->session->user = $this->Employee_model->get_by_id($this->session->user_id);
// 		$this->session->countryCode = $this->Employee_model->get_country($this->session->user_id);

		// adjust to IMS
		if(isset($_GET['user_id'])){
			$this->load->model('Employee_model');
			$this->session->user_id = $_GET['user_id'];
			$this->session->isHasSuperior = $this->Employee_model->getSuperior($this->session->user_id);

			$tmp_menu = $this->Employee_model->get_menu($this->session->user_id);

			$menu = [];

			foreach ($tmp_menu as $key => $value) {
				$menu[] = $value['menu_id'];
			}

			$this->session->userMenu = $menu;
			$this->session->user = $this->Employee_model->get_by_id($this->session->user_id);
			$this->session->countryCode = $this->Employee_model->get_country($this->session->user_id);
		}else if(!isset($_GET['user_id']) && !isset($_SESSION['user_id'])){
			$this->load->model('Employee_model');
			$this->session->user_id = '10057751';
			$this->session->isHasSuperior = $this->Employee_model->getSuperior($this->session->user_id);

			$tmp_menu = $this->Employee_model->get_menu($this->session->user_id);

			$menu = [];

			foreach ($tmp_menu as $key => $value) {
				$menu[] = $value['menu_id'];
			}

			$this->session->userMenu = $menu;
			$this->session->user = $this->Employee_model->get_by_id($this->session->user_id);
			$this->session->countryCode = $this->Employee_model->get_country($this->session->user_id);
		}
	}

	public function isLogin()
	{
		return isset($this->session->user_id);
	}

	public function render($view, $data = array()){
		$this->load->model('web_app_model');
		$data['jumlah_belumBaca']		= $this->web_app_model->hitungJumlahBelumBaca();
		$this->load->view('/template/header', $data);
		$this->load->view($view, $data);
		$this->load->view('/template/footer', $data);
	}

	public function renderJson($data)
	{
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode($data));
	}

	public function sendEmail($values, $data = array())
	{
		if(ENVIRONMENT == 'production')
			$to = $values['to'];
		else
			$to = 'thilmanrevanda@gmail.com';

		$config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.suaaleave.com',
            'smtp_port' => 587,
            'smtp_user' => 'no-reply@suaaleave.com',
			'smtp_pass' => 'VV0G74GGD6NX',
			'mailtype'  => 'html',
			'wordwrap' => true,
			'smtp_crypto' => 'tls'
		);
		
		$this->load->library('email', $config);

		$this->email
		->set_newline("\r\n")
		->from('no-reply@suaaleave.com', 'Wartsila Application')
		->to($to)
		->subject($values['subject'])
		->message($this->load->view($values['view'], $data,true));

		return $this->email->send();
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */