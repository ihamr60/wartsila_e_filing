<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	public function get_id()
	{
		$result = $this->db->query("Select UPPER(REPLACE(UUID(), '-', '')) as Id");
		return ($result->num_rows() == 1) ? $result->result()[0]->Id : FALSE;
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */