<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shift_time_model extends MY_Model {

    private $table = 'shift_time';

    public function create($site_id, $shift_name, $time_in, $time_out, $return = FALSE) {
        $variable = array(
            'shift_time_id' => $this->get_id(),
			'shift_time_name' => $shift_name,
			'site_id' => $site_id,
			'start_time' => $time_in,
			'finish_time' => $time_out
        );
        
        $this->db->insert($this->table, $variable);

        if($this->db->affected_rows() > 0)
			return ($return) ? $variable : TRUE;
		else 
			return FALSE;  
	}
	
	public function update($site_id, $shift_time_id, $shift_name, $time_in, $time_out, $return = FALSE) {
        $variable = array(
            'shift_time_id' => $shift_time_id,
			'shift_time_name' => $shift_name,
			'site_id' => $site_id,
			'start_time' => $time_in,
			'finish_time' => $time_out
        );
        $this->db->where('shift_time_id', $shift_time_id)->update($this->table, $variable);

        if($this->db->affected_rows() > 0)
			return ($return) ? $variable : TRUE;
		else 
			return FALSE;  
	}


    public function get($site_id, $start = -1, $end = -1, $searchValue = '') {
		$this->db->select('shift_time_id, shift_time_name, site_id, TIME_FORMAT(start_time, "%H:%i") as start_time, TIME_FORMAT(finish_time, "%H:%i") as finish_time')
					->from($this->table)
					->where('site_id', $site_id);

		if($searchValue !== ''){
			$this->db
			->like('shift_time_name', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
		}

        $this->db->order_by('shift_time_name', 'ASC');

		return $this->db->get()->result_array();
    }
    
    public function total()
	{
		$result = $this->db->select('count(*) as Total')
							->from($this->table)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
	} 

	public function get_all_shifts_by_site($site_id)
    {
        return $this->db->select('shift_time_id, shift_time_name, site_id, TIME_FORMAT(start_time, "%H:%i") as start_time, TIME_FORMAT(finish_time, "%H:%i") as finish_time')
                            ->from($this->table)
                            ->where('site_id', $site_id)
                            ->get()->result_array();
    }

	public function delete($shift_time_id)
	{
		$this->db->where('shift_time_id', $shift_time_id)->delete($this->table);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function get_employee_shift_time($employee_id, $date)
	{
		$result = $this->db->select('IF(st2.shift_time_name, st2.shift_time_name, st.shift_time_name) AS name, IF(st2.start_time,st2.start_time,st.start_time) AS start, IF(st2.finish_time,st2.finish_time,st.finish_time) AS finish, IF(st2.finish_time,(st2.start_time > st2.finish_time),(st.start_time > st.finish_time)) AS is_day_plus')
							->from('schedule s')
							->join('shift_group sg', 'sg.shift_group_id = s.shift_group_id')
							->join('shift_time st', 'st.shift_time_id = s.shift_time_id')
							->join('shift_group_detail sgd', 'sgd.shift_group_id = sg.shift_group_id')
							->join('schedule_advanced sa', 'sa.schedule_id = s.schedule_id', 'left')
							->join('shift_time st2', 'st2.shift_time_id = sa.shift_time_id', 'left')
							->where(array('sgd.employee_id' => $employee_id, 's.schedule_date' => $date))
							->get()->result_array();

		return (sizeof($result) == 1) ? $result[0] : FALSE;
	}
}

?>