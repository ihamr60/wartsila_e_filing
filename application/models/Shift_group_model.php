<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Shift_group_model extends MY_Model {

    private $table = 'shift_group';
    private $table_details = 'shift_group_detail';

    public function create($name, $site_id, $employees_id, $return = FALSE)
    {
		$variable = array(
            'shift_group_id' => $this->get_id(),
            'shift_group_name' => $name,
            'site_id' => $site_id
        );
        
        $this->db->insert($this->table, $variable);

		if($this->db->affected_rows() > 0){
            $this->create_batch_detail($variable['shift_group_id'],$employees_id);
            return ($return) ? $variable : TRUE;
        }else 
			return FALSE;  
    }
    
    public function create_batch_detail($group_id,$employees_id)
    {
        $data = array();

        foreach ($employees_id as $employee_id){
            $data[] = array(
                'shift_group_detail_id' => $this->get_id(),
                'shift_group_id' => $group_id,
                'employee_id' => $employee_id
            );
        }

        $this->db->insert_batch($this->table_details, $data);

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

	public function get($site_id, $start = -1, $end = -1, $searchValue = '')
	{
		$this->db->select("sg.*")
                    ->from($this->table . ' sg')
                    ->join('site o', 'o.site_id=sg.site_id')
                    ->where('o.site_id', $site_id);

		if($searchValue !== ''){
			$this->db
			->like('shift_group_name ', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
        }
        
        $this->db->order_by('shift_group_name', 'ASC');

		return $this->db->get()->result_array();
    }

    public function get_members($group_id)
    {
		return $this->db->select("e.PersNo as employee_id, e.first_name, e.last_name")
                            ->from($this->table_details . ' sgd')
                            ->join('employee e', 'e.PersNo=sgd.employee_id')
                            ->where('shift_group_id', $group_id)
                            ->get()
                            ->result_array();
    }
    
    public function total()
	{
		$result = $this->db->select('count(*) as Total')
							->from($this->table)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
    }

    public function get_site($group_id)
    {
        $result = $this->db->select('o.site_id as site_id, o.site_name as site_name')
                            ->from($this->table. ' sg')
                            ->join('site o', 'o.site_id=sg.site_id')
                            ->where('sg.shift_group_id', $group_id)
                            ->get()->result_array();

        return (sizeof($result) == 1) ? $result[0] : array();
    }
    
    public function get_group_name($group_id) {
        $result = $this->db->select('shift_group_name')
                            ->from($this->table)
                            ->where('shift_group_id', $group_id)
                            ->get();
        return ($result->num_rows() == 1) ? $result->result()[0]->shift_group_name : FALSE;
    }

    public function update_shift_group($shift_group_id, $shift_group_name)
    {
        $this->db->where('shift_group_id', $shift_group_id)
                    ->update($this->table, array('shift_group_name' => $shift_group_name));

        return ($this->db->affected_rows() > 0);
    }

    public function get_site_id_by_employee_id($employee_id)
    {
        $result =  $this->db->select('sg.site_id')
                            ->from('shift_group_detail sgd')
                            ->join('shift_group sg', 'sg.shift_group_id = sgd.shift_group_id', 'left')
                            ->where('sgd.employee_id', $employee_id)
                            ->get();
        return ($result->num_rows() == 1) ? $result->result()[0]->site_id : FALSE;
    }

    function delete_shift_members($shift_group_id, $delete_members)
    {
        $this->db->where('shift_group_id', $shift_group_id)
                    ->where_in('employee_id', $delete_members)
                    ->delete($this->table_details);
        return ($this->db->affected_rows() > 0);
    }

    function add_shift_members($shift_group_id, $add_members)
    {
        $data = array();

        foreach ($add_members as $employee_id) {
            $data[] = array(
                'shift_group_detail_id' => $this->get_id(),
                'shift_group_id' => $shift_group_id,
                'employee_id' => $employee_id
            );
        }

        $this->db->insert_batch($this->table_details, $data);

        return ($this->db->affected_rows() > 0);
    }

    public function get_all_groups_by_site($site_id)
    {
        return $this->db->select('*')
                            ->from($this->table)
                            ->where('site_id', $site_id)
                            ->order_by('shift_group_name', 'ASC')
                            ->get()->result_array();
    }

    public function getList()
    {
        $result = $this->db->select('*')
                            ->from($this->table);
        return $this->db->get()->result_array();
    }

    public function get_all_employee_by_site($site_id)
    {
        $result = $this->db->select("sg.*, sgd.*, CONCAT(e.First_name, \" \", e.Last_name) AS full_name")
                            ->from("$this->table sg")
                            ->join("$this->table_details sgd", 'sg.shift_group_id=sgd.shift_group_id')
                            ->join("employee e", "e.PersNo = sgd.employee_id", 'left')
                            ->where('sg.site_id', $site_id);
        return $this->db->get()->result_array();
    }

	public function delete($shift_group_id)
	{
		$this->db->where('shift_group_id', $shift_group_id)->delete($this->table);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
}
                        
/* End of file Shift_group_model.php */
    
                        