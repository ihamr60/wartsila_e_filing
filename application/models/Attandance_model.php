<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Attandance_model extends CI_Model {
    
    public function get_attendance($employee_id,$date, $plus = FALSE)
    {
		$this->db->select('coming, leaving, DATE_FORMAT(tr_date, "%d %M %Y") as date')->from('attendance');

        if($plus)
            $this->db->where('tr_date', $date);
        else
            $this->db->where('tr_date >=', $date)->where('tr_date <=', $date);
        
        $result = $this->db->get()->result_array();

		return (sizeof($result)) ? $result : array();
    }
                        
}
                        
/* End of file Attandance_model.php */