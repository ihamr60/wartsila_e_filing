<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Employee_model extends CI_Model {
                
    private $table = 'employee';

    // public function datalist_employee($keyword){
    //     $result = $this->db->select('*')
    //                 ->from($this->table)
    //                 ->like('first_name', $keyword)
    //                 ->or_like('last_name', $keyword)
    //                 ->or_like('employee_id', $keyword)
    //                 ->limit(2)
    //                 ->get();

    //     return  ($result->num_rows() > 0) ? $result->result() : FALSE;     
    // }
    
	public function get($start = 0, $end = 0, $searchValue = '')
	{
		$this->db->select("e.PersNo as employee_id, e.First_name as first_name, e.Last_name as last_name")
					->from($this->table . ' e');

		if($searchValue !== ''){
			$this->db
			->like('e.PersNo', $searchValue)
			->or_like('e.First_name', $searchValue)
			->or_like('e.Last_name', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
		}

		return $this->db->get()->result_array();
	}

	public function total()
	{
		$result = $this->db->select('count(*) as Total')
							->from($this->table)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
	}

	public function isSuperior($user_id)
	{
		$result = $this->db->distinct()->select('FIELD26')
							->from('employee')
							->where('FIELD26', $user_id)
							->get();

		return ($result->num_rows() == 1) ? TRUE : FALSE;
	}

	public function getSuperior($employee_id)
	{
		$result = $this->db->select('if(LENGTH(FIELD26) < 8, 0, IF(ISNULL(FIELD26), 0, FIELD26)) AS superior_id')
							->from('employee')
							->where('PersNo', $employee_id)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->superior_id : FALSE;
	}

	public function get_by_id($employee_id)
	{
		$result = $this->db->select('*')
							->from('employee')
							->where('PersNo', $employee_id)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0] : FALSE;
	}

	
	function get_menu($employee_id)
	{
		$result = $this->db->select('menu_id')
							->from('user_menu')
							->where('user_id', $employee_id)
							->get()->result_array();

		return (sizeof($result) > 0) ? $result : FALSE;
	}

	public function get_employees_by_superior($superior_id)
	{
		// return $this->db->select('PersNo, Known_As')
		// 					->from($this->table)
		// 					->where('Field26', $superior_id)
		// 					->where_in('PersNo', "
		// 						(
		// 							Select sgd.employee_id 
		// 							from shift_group sg
		// 							join shift_group_detail sgd on sg.shift_group_id=sgd.shift_group_id
		// 							join employee e on e.PersNo=sgd.employee_id
		// 							where e.Field26
		// 						)
		// 					")
		// 					->get()->result_array();

		return $this->db->distinct()->select('sgd.employee_id, e.PersNo, e.Known_As')
						->from('shift_group sg')
						->join('shift_group_detail sgd', 'sg.shift_group_id=sgd.shift_group_id')
						->join('employee e','e.PersNo=sgd.employee_id')
						->where('e.Field26', $superior_id)
						->get()->result_array();
	}

	public function get_plant_manager()
	{
		return $this->db->select('PersNo, Known_As, dept')
							->from($this->table)
							->where('Field17', 'PLANT MANAGER')->get()->result_array();
	}

	public function get_country($employee_id)
	{
		$result = $this->db->select('s.country_code')
							->from("$this->table e")
							->join('site s', 's.site_name = e.dept')
							->where('PersNo', $employee_id)
							->get()
							->result_array();

		return (sizeof($result) == 1) ? $result[0]['country_code'] : FALSE;
	}
}
                        
/* End of file Employee.php */
    
                        