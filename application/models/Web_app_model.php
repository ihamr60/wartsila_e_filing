<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_App_Model extends MY_Model 
{	
	
	//FUNGSI SELECT ALL DATA (READ)
	public function getAllData($table)
	{
		return $this->db->get($table)->result();
	}

	public function hitungJumlahBelumBaca()
	{
		$query = $this->db->query('SELECT * FROM tbl_notif WHERE stts="0"');
		return $query->num_rows();
	}

	// FUNGSI SELECT BY WHERE (READ BY WHERE)
	public function getWhere($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table)->result();
	}

	// FUNGSI SELECT BY WHERE ORDER BY (PENGURUTAN DESC)
	public function getOneItemWhereOrderByLimit($where,$field,$field_order,$descOrAsc,$limit,$table)
	{
		$this->db->where($field,$where);
		$this->db->order_by($field_order,$descOrAsc);
		$this->db->limit($limit);
		$data = array();
  		$Q = $this->db->get($table);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	// FUNGSI SELECT BY  2 WHERE (READ BY WHERE)
	public function getWhere2($where1,$field1,$where2,$field2,$table)
	{
		$this->db->where($field1,$where1);
		$this->db->where($field2,$where2);
		return $this->db->get($table)->result();
	}
	
	//FUNGSI INSERT DATA (CREATE)
	public function insertData($data,$table)
	{
		$this->db->insert($table,$data);
	}

	public function updateData($data,$table)
	{
		$this->db->update($table,$data);
	}

	//FUNGSI DELETE DATA (DELETE)
	function deleteData($table,$data)
	{
		$this->db->delete($table, $data);
	}

	//AUTO DELETE NOTIF DALAM 30 HARI
	function deleteNotif($table,$kolom_date,$jumlah_hari)
	{
		$this->db->query("DELETE FROM ".$table." WHERE DATEDIFF(CURDATE(), ".$kolom_date.") > ".$jumlah_hari."");
	}


	// FUNGSI UPDATE DATA BY WHERE (UPDATE BY WHERE)
	public function updateDataWhere($data,$where,$table)
	{
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
	// FUNGSI SELECT BY WHERE JOIN (READ BY WHERE JOIN)
	public function getJoin($idTabel1,$idTabel2,$table1,$table2)
	{
		  $this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		  $this->db->from($table1);
		  $query = $this->db->get();
		  return $query->result();
	}

	public function getJoinGroup($group,$idTabel1,$idTabel2,$table1,$table2)
	{
		  $this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
			$this->db->group_by($group);
		  $this->db->from($table1);
		  $query = $this->db->get();
		  return $query->result();
	}
	
	public function getJoinWhere($idTabel1,$idTabel2,$table1,$table2,$field,$where)
	{
		  $this->db->join($table2, ''.$table1.'.'.$idTabel1.' = '.$table2.'.'.$idTabel2.'');
		  $this->db->from($table1);
		  $this->db->where($field,$where);
		  $query = $this->db->get();
		  return $query->result();
	}

	public function getJoinFour()
	{
		 
		 $this->db->join('tbl_province','tbl_holiday.province_id=tbl_province.province_id');
		 $this->db->join('tbl_country','tbl_holiday.country_id=tbl_country.country_id');
		 $this->db->join('tbl_city','tbl_holiday.city_id=tbl_city.city_id');
		 $this->db->from('tbl_holiday');
		
		 $query = $this->db->get();
		 return $query->result();
	}

	public function getJoinFourWhere($data)
	{
		 
		 $this->db->join('tbl_province','tbl_holiday.province_id=tbl_province.province_id');
		 $this->db->join('tbl_country','tbl_holiday.country_id=tbl_country.country_id');
		 $this->db->join('tbl_city','tbl_holiday.city_id=tbl_city.city_id');
		 $this->db->from('tbl_holiday');
		 $this->db->where('holiday_id',$data);
		 $query = $this->db->get();
		 return $query->result();
	}

	public function getWhereOneItem($where,$field,$table)
	{
		$data = array();
  		$options = array($field => $where);
  		$Q = $this->db->get_where($table,$options);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getOneItem($table)
	{
		$data = array();
  		$Q = $this->db->get($table);
    		if ($Q->num_rows() > 0)
				{
      				$data = $Q->row_array();
    			}
  		$Q->free_result();
  		return $data;
	}

	public function getWhereAllItem($where,$field,$table)
	{
		$this->db->where($field,$where);
		return $this->db->get($table)->result();
	}
}
