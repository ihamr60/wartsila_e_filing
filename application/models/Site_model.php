<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Site_model extends MY_Model {

    private $table = 'site';
                        
	public function create($site_name, $site_address, $return = FALSE)
	{
		$variable = array(
            'site_id' => $this->get_id(),
			'site_name' => $site_name,
			'site_address' => $site_address
		);
	
		$this->db->insert($this->table, $variable);

		if($this->db->affected_rows() > 0)
			return ($return) ? $variable : TRUE;
		else 
			return FALSE;     
	}
	
	public function update($site_id, $site_name, $site_address, $return = FALSE)
	{
		$variable = array(
            'site_id' => $site_id,
			'site_name' => $site_name,
			'site_address' => $site_address
		);
	
		$this->db->where('site_id', $site_id)->update($this->table, $variable);

		if($this->db->affected_rows() > 0)
			return ($return) ? $variable : TRUE;
		else 
			return FALSE;     
	}
	   
	public function delete($site_id)
	{
		$this->db->where('site_id', $site_id)->delete($this->table);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
	
	public function get($start = -1, $end = -1, $searchValue = '')
	{
		$this->db->select("*")
					->from($this->table);

		if($searchValue !== ''){
			$this->db
			->like('site_name', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
		}

        $this->db->order_by('site_name', 'ASC');

		return $this->db->get()->result_array();
	}

	public function get_all()
	{
		return $this->db->select('*')
					->from($this->table)
					->order_by('site_name', 'ASC')
					->get()
					->result_array();
	}

	public function get_by_persmission()
	{
		return $this->db->select('s.*')
					->from('overtime_site_report_permission op')
					->where('employee_id', $_SESSION['user_id'])
					->join($this->table . ' s', 's.site_id=op.site_id')
					->order_by('site_name', 'ASC')
					->get()
					->result_array();
	}

	public function get_by_id($id)
	{
		$result = $this->db->select('*')
							->from($this->table)
							->where('site_id', $id)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0] : FALSE;
	}
	
	// public function get_by_id($site_id)
	// {
	// 	$this->db->select("*")
	// 				->from($this->table)
	// 				->where('site_id', $site_id);

	// 	return ($result->num_rows() == 1) ? $result->result()[0] : FALSE;
	// }

	public function total()
	{
		$result = $this->db->select('count(*) as Total')
							->from($this->table)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
	} 
}
                        
/* End of file Site_model.php */
    
                        