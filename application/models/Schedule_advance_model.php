<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Schedule_advance_model extends MY_Model {
    
    private $table = 'schedule_advanced';

    public function create($schedule_id, $employee_id, $shift_time_id = NULL)
    {
        $variables = array(
            'schedule_advanced_id' => $this->get_id(),
            'schedule_id' => $schedule_id,
            'employee_id' => $employee_id
        );

        if($shift_time_id){
            $variables['shift_time_id'] = $shift_time_id;
        }

        $this->db->insert($this->table, $variables);

        return ($this->db->affected_rows() > 0);
    }

    public function get_by_site_and_date($site_id,$date)
    {
        return $this->db->select('sa.*, e.First_name as first_name, e.Last_name as last_name, (SELECT shift_time_name FROM shift_time WHERE shift_time_id=sa.shift_time_id) AS shift_time_name')
                    ->from('schedule_advanced sa')
                    ->join('schedule s', 's.schedule_id=sa.schedule_id')
                    ->join('employee e', 'sa.employee_id=e.PersNo')
                    ->where(array('s.site_id' => $site_id, 's.schedule_date' => $date))
                    ->get()
                    ->result_array();
    }

	public function delete($schedule_advanced_id)
	{
		$this->db->where('schedule_advanced_id', $schedule_advanced_id)->delete($this->table);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}
}
                        
/* End of file Schedule_advance_model.php */
    
                        