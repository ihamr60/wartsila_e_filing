<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Overtime_model extends MY_Model {

private $table = 'over_time';
private $table_extend = 'over_time_extend';

    public function add_over_time($site_id, $employee_id, $superior_id, $overtime_date , $time, $purpose, $day_status, $return = FALSE) {    
        $data = array(
            'over_time_id' => $this->get_id(),
            'site_id' => $site_id,
            'employee_id' => $employee_id,
            'superior_id' => $superior_id,
            'overtime_date' => $overtime_date,
            'time' => $time,
            'purpose' => $purpose,
            'day_status' => $day_status,
            'status' => 'ON REQUEST'
        );

        $this->db->insert($this->table, $data);

        if($this->db->affected_rows() > 0)
                return ($return) ? $data : TRUE;
            else 
                return FALSE; 
                                    
    }

    public function add_over_time_by_superior($site_id, $requestor, $to_employee, $plant_manager, $overtime_date , $time, $purpose, $day_status, $return = FALSE) {    
        $data = array(
            'over_time_id' => $this->get_id(),
            'site_id' => $site_id,
            'employee_id' => $to_employee,
            'superior_id' => $plant_manager,
            'overtime_date' => $overtime_date,
            'time' => $time,
            'purpose' => $purpose,
            'day_status' => $day_status,
            'status' => 'ON REQUEST',
            'requestor' => $requestor
        );

        $this->db->insert($this->table, $data);

        if($this->db->affected_rows() > 0)
                return ($return) ? $data : TRUE;
            else 
                return FALSE; 
                                    
    }

    public function get_by_superior_id($superior_id, $start = -1, $end = -1, $searchValue = '')
    {
        $this->db->select('ot.*, DATE_FORMAT(ot.overtime_date, "%d-%M-%Y") as overtime_date_formated, TIME_FORMAT(ot.time, "%H Hour(s) %i Minute(s)") as time_formated, (SELECT e.Known_As FROM employee e WHERE e.PersNo = ot.employee_id) AS employee_name, (SELECT e.Field17 FROM employee e WHERE e.PersNo = ot.employee_id) AS employee_position, ote.over_time_extend_id AS extend_id, IFNULL(ote.status, "-") AS extend_status, IFNULL(ote.purpose, "-") AS extend_purpose, IFNULL(ote.over_time_extend_requested_date, "-") AS extend_request, IFNULL(ote.over_time_extend_approved_date, "-") AS extend_approved, IFNULL(TIME_FORMAT(ote.time, "%H Hour(s) %i Minute(s)"), "-") AS extend_time, IFNULL(ote.time, "-") AS extend_time_full, SEC_TO_TIME(TIME_TO_SEC(ot.TIME) + TIME_TO_SEC(ote.TIME)) AS total_overtime, IF(ot.status = "ON REQUEST" || ote.status = "ON REQUEST", 1, 0) as priority')
                 ->from($this->table . ' ot')
                 ->join($this->table_extend .' ote', 'ot.over_time_id = ote.over_time_id', 'left')
                 ->where('superior_id', $superior_id)
                 ->order_by('priority', 'DESC')
                 ->order_by('ot.overtime_date', 'DESC');
		if($searchValue !== ''){
			$this->db
            ->like('ot.employee_id', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
		}

		return $this->db->get()->result_array();
    }
    
    public function total_by_superior_id($superior_id)
    {
        $result = $this->db->select('count(*) as Total')
                            ->from($this->table)
                            ->where('superior_id', $superior_id)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
    }

    public function get_by_employee_id($employee_id, $start = -1, $end = -1, $searchValue = '')
    {
        $this->db->select('ot.*,er.Known_As as requestor_name, er.PersNo as requestor_id, DATE_FORMAT(ot.overtime_date, "%d-%M-%Y") as overtime_date_formated, TIME_FORMAT(ot.time, "%H Hour(s) %i Minute(s)") as time_formated, (SELECT e.Known_As FROM employee e WHERE e.PersNo = ot.employee_id) AS employee_name, (SELECT e.Field17 FROM employee e WHERE e.PersNo = ot.employee_id) AS employee_position, ote.over_time_extend_id AS extend_id, IFNULL(ote.status, "-") AS extend_status, IFNULL(ote.purpose, "-") AS extend_purpose, IFNULL(ote.over_time_extend_requested_date, "-") AS extend_request, IFNULL(ote.over_time_extend_approved_date, "-") AS extend_approved, IFNULL(TIME_FORMAT(ote.time, "%H Hour(s) %i Minute(s)"), "-") AS extend_time, SEC_TO_TIME(TIME_TO_SEC(ot.TIME) + TIME_TO_SEC(ote.TIME)) AS total_overtime, a.coming, a.leaving, if(CURDATE() > ot.overtime_date,0,1) as allow_request')
                    ->from($this->table . ' ot')
                    ->join($this->table_extend .' ote', 'ot.over_time_id = ote.over_time_id', 'left')
                    ->join('attendance a', 'ot.overtime_date = a.tr_date AND a.user_id = ot.employee_id', 'left')
                    ->join('employee er', 'er.PersNo=ot.requestor', 'left')
                    ->where('employee_id', $employee_id)
                    ->order_by('ot.created_date', 'DESC');

		if($searchValue !== ''){
			$this->db
            ->like('ot.employee_id', $searchValue);
		}

		if($start >= 0 && $end >= 0){
			$this->db->limit($end, $start);
		}

		return $this->db->get()->result_array();
    }

    public function total_by_employee_id($employee_id)
    {
        $result = $this->db->select('count(*) as Total')
                            ->from($this->table)
                            ->where('employee_id', $employee_id)
							->get();

		return ($result->num_rows() == 1) ? $result->result()[0]->Total : FALSE;
    }

    public function update_status($overtime_id, $status)
    {
        $data = array(
        'status' => $status
        );

        $result = $this->db->where('over_time_id', $overtime_id)
                ->update($this->table, $data);
        if($result)
			return TRUE;
		else 
			return FALSE; 
    }

    public function update_extend_status($extend_id, $status)
    {
        $data = array(
        'status' => $status
        );

        $result = $this->db->where('over_time_extend_id', $extend_id)
                ->update($this->table_extend, $data);
        if($result)
			return TRUE;
		else 
			return FALSE; 
    }

    public function add_extend($overtime_id, $time, $purpose, $return = FALSE)
    {
        $data = array(
            'over_time_extend_id' => $this->get_id(),
            'over_time_id'        => $overtime_id,
            'time'                => $time,
            'status'              => "ON REQUEST",
            'purpose'             => $purpose,
            'over_time_extend_approved_date' => NULL
        );

        $this->db->insert($this->table_extend, $data);

        if($this->db->affected_rows() > 0)
                return ($return) ? $variable : TRUE;
            else 
                return FALSE; 
    }

    public function is_holiday($overtime_date, $employee_id, $return = FALSE)
    {
        $result =  $this->db->select('st.start_time, st.finish_time, s.schedule_id, s.shift_time_id, s.shift_group_id')
                            ->from('shift_group_detail sgd')
                            ->join('schedule s','s.shift_group_id=sgd.shift_group_id', 'left')
                            ->join('shift_time st', 'st.shift_time_id=s.shift_time_id', 'left')
                            ->where('sgd.employee_id', $employee_id)
                            ->where('s.schedule_date', $overtime_date)
                            ->get();
        return $result->num_rows() == 0;
    }

    public function overtime_report_individual($employee_id, $year, $month, $return = FALSE)
    {
        $this->db->query("
            SET @sql_basedata = ( 'SELECT  DATE_FORMAT(ot.overtime_date, \"%Y-%b-%d\") as overtime_date,DATE_FORMAT(ot.overtime_date, \"%a\") as day, ot.employee_id, ot.time, ote.time AS extend_time, IF((day_status IS TRUE ), 	@day_status := \"HOLIDAY\", 	@day_status :=\"NORMAL\")	AS day_status, FORMAT((@total_works := HOUR(ot.time) + (MINUTE(ot.time)/60) + (IF(( ote.time <> \"NULL\"), 	(HOUR(ote.time) + (MINUTE(ote.time)/60))  , 0 )  )),2) AS total_works, IF(@day_status = \"HOLIDAY\", 	@time_factor_one_and_half := 0.00, 	IF(@total_works < 1, 		@time_factor_one_and_half := FORMAT((@total_works),2), 		@time_factor_one_and_half := 1.00)) AS time_factor_one_and_half, IF(@day_status = \"HOLIDAY\",	(IF((s.country_code = \"ID\"), 	 	IF(@total_works > 8, 			@time_factor_two := 8.00, @time_factor_two := FORMAT((@total_works),2)),		@time_factor_two := FORMAT(@total_works,2))),	IF((s.country_code = \"ID\"),		IF((IF((@total_works - @time_factor_one_and_half) > 0, @total_works := FORMAT((@total_works - @time_factor_one_and_half),2), @total_works := 0.00)) > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT(@total_works,2)), IF(((@total_works := @total_works - @time_factor_one_and_half) > 0), FORMAT((@time_factor_two := @total_works),2), @time_factor_two := 0  )   )) AS time_factor_two, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00)) > 1, @time_factor_three := 1.00, @time_factor_three := FORMAT(@total_works,2)), IF(IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00) > 0, @time_factor_three := FORMAT(@total_works,2), @time_factor_three := 0.00)) AS time_factor_three, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_three) > 0, @total_works := FORMAT((@total_works - @time_factor_three),2), @total_works := 0.00)) > 0, @time_factor_four := FORMAT(@total_works,2), @time_factor_four := 0.00), @time_factor_four := 0.00) AS time_factor_four
            FROM over_time ot
            LEFT JOIN over_time_extend ote
            ON ot.over_time_id = ote.over_time_id
            LEFT JOIN leave_pub_holidays lph
            ON ot.overtime_date = lph.dt_holiday
            LEFT JOIN attendance a
            ON ot.overtime_date = a.tr_date AND a.user_id = ot.employee_id
            LEFT JOIN site s
            ON s.site_id = ot.site_id
            WHERE ot.employee_id = $employee_id AND ot.status = \"APPROVED\" AND YEAR(overtime_date) = $year AND MONTH(overtime_date) = $month
            ORDER BY overtime_date ASC');
        ");

        $this->db->query("PREPARE stmt FROM @sql_basedata;");
        $query = $this->db->query("EXECUTE stmt;");
        $this->db->query("DEALLOCATE PREPARE stmt;");
        return ($query->num_rows() > 0) ? $query->result_object() : array();

    }

    public function overtime_report_summary_individual($employee_id, $year, $month)
    {
        $this->db->query("
            SET @sql_basedata = ( 'SELECT  DATE_FORMAT(ot.overtime_date, \"%Y-%b-%d\") as overtime_date,DATE_FORMAT(ot.overtime_date, \"%a\") as day, ot.employee_id, ot.time, ote.time AS extend_time, IF((day_status IS TRUE ), 	@day_status := \"HOLIDAY\", 	@day_status :=\"NORMAL\")	AS day_status, FORMAT((@total_works := HOUR(ot.time) + (MINUTE(ot.time)/60) + (IF(( ote.time <> \"NULL\"), 	(HOUR(ote.time) + (MINUTE(ote.time)/60))  , 0 )  )),2) AS total_works, IF(@day_status = \"HOLIDAY\", 	@time_factor_one_and_half := 0.00, 	IF(@total_works < 1, 		@time_factor_one_and_half := FORMAT((@total_works),2), 		@time_factor_one_and_half := 1.00)) AS time_factor_one_and_half, IF(@day_status = \"HOLIDAY\",	(IF((s.country_code = \"ID\"), 	 	IF(@total_works > 8, 			@time_factor_two := 8.00, @time_factor_two := FORMAT((@total_works),2)),		@time_factor_two := FORMAT(@total_works,2))),	IF((s.country_code = \"ID\"),		IF((IF((@total_works - @time_factor_one_and_half) > 0, @total_works := FORMAT((@total_works - @time_factor_one_and_half),2), @total_works := 0.00)) > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT(@total_works,2)), IF(((@total_works := @total_works - @time_factor_one_and_half) > 0), FORMAT((@time_factor_two := @total_works),2), @time_factor_two := 0  )   )) AS time_factor_two, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00)) > 1, @time_factor_three := 1.00, @time_factor_three := FORMAT(@total_works,2)), IF(IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00) > 0, @time_factor_three := FORMAT(@total_works,2), @time_factor_three := 0.00)) AS time_factor_three, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_three) > 0, @total_works := FORMAT((@total_works - @time_factor_three),2), @total_works := 0.00)) > 0, @time_factor_four := FORMAT(@total_works,2), @time_factor_four := 0.00), @time_factor_four := 0.00) AS time_factor_four
            FROM over_time ot
            LEFT JOIN over_time_extend ote
            ON ot.over_time_id = ote.over_time_id
            LEFT JOIN leave_pub_holidays lph
            ON ot.overtime_date = lph.dt_holiday
            LEFT JOIN attendance a
            ON ot.overtime_date = a.tr_date AND a.user_id = ot.employee_id
            LEFT JOIN site s
            ON s.site_id = ot.site_id
            WHERE ot.employee_id = $employee_id AND ot.status = \"APPROVED\" AND YEAR(overtime_date) = $year AND MONTH(overtime_date) = $month
            ORDER BY overtime_date ASC');
        ");

        $this->db->query("
            SET @sql_generate = ( CONCAT('
                SELECT employee_id, FORMAT((SUM(total_works)),2) AS total_works, FORMAT((SUM(time_factor_one_and_half)),2) AS oneofive, FORMAT((SUM(time_factor_two)),2) AS two, FORMAT((SUM(time_factor_three)),2) AS three, FORMAT((SUM(time_factor_four)),2) AS four
                FROM (', @sql_basedata, ') AS t')
            );
        ");

        $this->db->query("PREPARE stmt FROM @sql_generate;");
        $query = $this->db->query("EXECUTE stmt;");
        $this->db->query("DEALLOCATE PREPARE stmt;");
        return ($query->num_rows() > 0) ? $query->result_object()[0] : FALSE;
    }

    public function overtime_report_summary_per_dept_per_month($year)
    {
        $this->db->query("
            SET @sql_dinamis = (
                SELECT
                    GROUP_CONCAT( DISTINCT
                        CONCAT('(FORMAT((SUM(IF(site_name = \"', site_name ,'\",total_works,0))/total_employee),3)) AS \"', site_name, '\"'
                        )
                    )
                FROM site
                WHERE site_id IN (
                    SELECT osrm.site_id
                    FROM overtime_site_report_permission osrm
                    WHERE osrm.employee_id = '{$_SESSION['user_id']}'
                )
            );
        ");

        $this->db->query("
            SET @sql_total_employee_overtime_per_site = (
                'SELECT  ot.site_id, s.site_name, COUNT(DISTINCT employee_id) AS total_employee, MONTH(ot.overtime_date) as month, YEAR(ot.overtime_date) as year
                FROM over_time ot
                LEFT JOIN site s
                ON ot.site_id = s.site_id
                WHERE STATUS = \"APPROVED\"
                GROUP BY site_id, MONTH(ot.overtime_date), YEAR(ot.overtime_date)
                ORDER BY ot.site_id, YEAR(ot.overtime_date), MONTH(ot.overtime_date)
                '
            );
        ");
        $this->db->query("
            SET @sql_basedata = (
                CONCAT(
                'SELECT ot.site_id, s.site_name, teps.total_employee, ot.overtime_date, ot.employee_id,IF(( day_status IS TRUE ), @day_status := \"HOLIDAY\", @day_status :=\"NORMAL\") AS day_status, FORMAT((@total_works := HOUR(ot.time) + (MINUTE(ot.time)/60) + (IF(( ote.time <> \"NULL\"), (HOUR(ote.time) + (MINUTE(ote.time)/60))  , 0 )  ) ),2) AS total_works, IF(@day_status = \"HOLIDAY\", @time_factor_one_and_half := 0.00, IF(@total_works < 1, @time_factor_one_and_half := FORMAT((@total_works),2), @time_factor_one_and_half := 1.00)) AS time_factor_one_and_half, IF(@day_status = \"HOLIDAY\",(IF((s.country_code = \"ID\"), IF(@total_works > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT((@total_works),2)), FORMAT(@total_works,2))),IF((s.country_code = \"ID\"), IF((IF((@total_works - @time_factor_one_and_half) > 0, @total_works := FORMAT((@total_works - @time_factor_one_and_half),2), @total_works := 0.00)) > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT(@total_works,2)), IF(((@total_works := @total_works - @time_factor_one_and_half) > 0), FORMAT((@time_factor_two := @total_works),2), @time_factor_two := 0  )   )) AS time_factor_two, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00)) > 1, @time_factor_three := 1.00, @time_factor_three := FORMAT(@total_works,2)), IF(IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00) > 0, @time_factor_three := FORMAT(@total_works,2), @time_factor_three := 0.00)) AS time_factor_three, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_three) > 0, @total_works := FORMAT((@total_works - @time_factor_three),2), @total_works := 0.00)) > 0, @time_factor_four := FORMAT(@total_works,2), @time_factor_four := 0.00), @time_factor_four := 0.00) AS time_factor_four
                FROM over_time ot
                LEFT JOIN over_time_extend ote
                ON ot.over_time_id = ote.over_time_id
                LEFT JOIN ( ', @sql_total_employee_overtime_per_site, ' ) AS teps
                ON ot.site_id = teps.site_id AND YEAR(ot.overtime_date) = teps.year AND MONTH(ot.overtime_date) = teps.month
                LEFT JOIN leave_pub_holidays lph
                ON ot.overtime_date = lph.dt_holiday
                LEFT JOIN site s
                ON ot.site_id = s.site_id
                WHERE ot.status = \"APPROVED\" AND YEAR(ot.overtime_date) = $year
                ORDER BY overtime_date ASC')
            );
        ");
        $this->db->query("
            SET @sql_generate = (
                CONCAT('SELECT CONCAT(MONTHNAME(overtime_date),\",\", YEAR(overtime_date)) AS date, ', @sql_dinamis, ' FROM ( ', @sql_basedata, ' ) as t GROUP BY YEAR(overtime_date), MONTH(overtime_date) ORDER BY YEAR(overtime_date), MONTH(overtime_date) ASC' )
            );
        ");
        $this->db->query("PREPARE stmt FROM @sql_generate;");
        $query = $this->db->query("EXECUTE stmt;");
        $this->db->query("DEALLOCATE PREPARE stmt;");
        
        return ($query->num_rows() > 0) ? $query->result_object() : array();
        
    }

    public function overtime_report_summary_per_month($year)
    {

        $this->db->query("
        SET @sql_dinamis_result = (
            SELECT
                GROUP_CONCAT( DISTINCT
                    (CONCAT('(IFNULL(`', site_name ,'`,0)) ')) SEPARATOR '+ ' )
            FROM site
            WHERE site_id IN (
                    SELECT osrm.site_id
                    FROM overtime_site_report_permission osrm
                    WHERE osrm.employee_id = '{$_SESSION['user_id']}'
                )
        );
        ");
        $this->db->query("
        SET @sql_dinamis = (
                SELECT
                    GROUP_CONCAT( DISTINCT
                        CONCAT('(FORMAT((SUM(IF(site_name = \"', site_name ,'\",total_works,0))/total_employee),3)) AS \"', site_name, '\"'
                        )
                    )
                FROM site
                WHERE site_id IN (
                    SELECT osrm.site_id
                    FROM overtime_site_report_permission osrm
                    WHERE osrm.employee_id = '{$_SESSION['user_id']}'
                )
            );
        ");
        $this->db->query("
        SET @sql_total_employee_overtime_per_site = (
            'SELECT  ot.site_id, s.site_name, COUNT(DISTINCT employee_id) AS total_employee, MONTH(ot.overtime_date) as month, YEAR(ot.overtime_date) as year
            FROM over_time ot
            LEFT JOIN site s
            ON ot.site_id = s.site_id
            WHERE STATUS = \"APPROVED\"
            GROUP BY site_id, MONTH(ot.overtime_date), YEAR(ot.overtime_date)
            ORDER BY ot.site_id, YEAR(ot.overtime_date), MONTH(ot.overtime_date)
            '
        );
        ");
        $this->db->query("
            SET @sql_basedata = (
                CONCAT(
                'SELECT ot.site_id, s.site_name, teps.total_employee, ot.overtime_date, ot.employee_id,IF(( day_status IS TRUE ), @day_status := \"HOLIDAY\", @day_status :=\"NORMAL\") AS day_status, FORMAT((@total_works := HOUR(ot.time) + (MINUTE(ot.time)/60) + (IF(( ote.time <> \"NULL\"), (HOUR(ote.time) + (MINUTE(ote.time)/60))  , 0 )  ) ),2) AS total_works, IF(@day_status = \"HOLIDAY\", @time_factor_one_and_half := 0.00, IF(@total_works < 1, @time_factor_one_and_half := FORMAT((@total_works),2), @time_factor_one_and_half := 1.00)) AS time_factor_one_and_half, IF(@day_status = \"HOLIDAY\",(IF((s.country_code = \"ID\"), IF(@total_works > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT((@total_works),2)), FORMAT(@total_works,2))),IF((s.country_code = \"ID\"), IF((IF((@total_works - @time_factor_one_and_half) > 0, @total_works := FORMAT((@total_works - @time_factor_one_and_half),2), @total_works := 0.00)) > 8, @time_factor_two := 8.00, @time_factor_two := FORMAT(@total_works,2)), IF(((@total_works := @total_works - @time_factor_one_and_half) > 0), FORMAT((@time_factor_two := @total_works),2), @time_factor_two := 0  )   )) AS time_factor_two, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00)) > 1, @time_factor_three := 1.00, @time_factor_three := FORMAT(@total_works,2)), IF(IF((@total_works - @time_factor_two) > 0, @total_works := FORMAT((@total_works - @time_factor_two),2), @total_works := 0.00) > 0, @time_factor_three := FORMAT(@total_works,2), @time_factor_three := 0.00)) AS time_factor_three, IF(@day_status = \"HOLIDAY\", IF((IF((@total_works - @time_factor_three) > 0, @total_works := FORMAT((@total_works - @time_factor_three),2), @total_works := 0.00)) > 0, @time_factor_four := FORMAT(@total_works,2), @time_factor_four := 0.00), @time_factor_four := 0.00) AS time_factor_four
                FROM over_time ot
                LEFT JOIN over_time_extend ote
                ON ot.over_time_id = ote.over_time_id
                LEFT JOIN ( ', @sql_total_employee_overtime_per_site, ' ) AS teps
                ON ot.site_id = teps.site_id AND YEAR(ot.overtime_date) = teps.year AND MONTH(ot.overtime_date) = teps.month
                LEFT JOIN leave_pub_holidays lph
                ON ot.overtime_date = lph.dt_holiday
                LEFT JOIN site s
                ON ot.site_id = s.site_id
                WHERE ot.status = \"APPROVED\" AND YEAR(ot.overtime_date) = $year
                ORDER BY overtime_date ASC')
            );
        ");
        $this->db->query("
            SET @sql_summary_per_site = (
                CONCAT('SELECT CONCAT(MONTHNAME(overtime_date),\",\", YEAR(overtime_date)) AS date, overtime_date, ', @sql_dinamis, ' FROM ( ', @sql_basedata, ' ) as t GROUP BY YEAR(overtime_date), MONTH(overtime_date) ORDER BY date DESC' )
            );
        ");
        $this->db->query("
            SET @sql_generate = (
                CONCAT('SELECT date, FORMAT((', @sql_dinamis_result, ' ),3) AS total_works FROM ( ', @sql_summary_per_site, ' ) as t GROUP BY date ORDER BY YEAR(overtime_date), MONTH(overtime_date) ASC' )
            );
        ");
        $this->db->query("PREPARE stmt FROM @sql_generate;");
        $query = $this->db->query("EXECUTE stmt;");
        $this->db->query("DEALLOCATE PREPARE stmt;");
        
        return ($query->num_rows() > 0) ? $query->result_object() : array();

    }

    public function update_overtime_time($overtime_id, $time)
    {
        $data = array(
        'time' => $time
        );

        $result = $this->db->where('over_time_id', $overtime_id)
                ->update($this->table, $data);
        if($result)
			return TRUE;
		else 
			return FALSE; 
    }

    public function update_overtime_extend_time($extend_id, $time)
    {
        $data = array(
        'time' => $time
        );

        $result = $this->db->where('over_time_extend_id', $extend_id)
                ->update($this->table_extend, $data);
        if($result)
			return TRUE;
		else 
			return FALSE; 
    }

    public function get_existing_month_by_site_and_year($site_id,$year)
    {
        $this->db->select("DISTINCT DATE_FORMAT(overtime_date, '%M') AS formated_month, DATE_FORMAT(overtime_date, '%m') AS month")
                ->from($this->table)
                ->where(array(
                    'YEAR(overtime_date)' => $year,
                    'site_id' => $site_id
                ))
                ->order_by('overtime_date ASC');

        return $this->db->get()->result_array();
    }

    public function get_existing_year_by_site($site_id)
    {
        $this->db->select("DISTINCT DATE_FORMAT(overtime_date, '%Y') AS year")
                ->from($this->table)
                ->where('site_id', $site_id)
                ->order_by('overtime_date ASC');

        return $this->db->get()->result_array();
    }
}



/* End of file overtime.php */
    
                        