<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
                        
class Schedule_model extends MY_Model {
    
    private $table = 'schedule';

    public function create($schedule_date, $site_id, $shift_group_id, $shift_time_id)
    {
        $variables = array(
            'schedule_id' => $this->get_id(),
            'schedule_date' => $schedule_date,
            'site_id' => $site_id,
            'shift_group_id' => $shift_group_id,
            'shift_time_id' => $shift_time_id
        );

        $this->db->insert($this->table, $variables);

        return ($this->db->affected_rows() > 0);
    }

    public function update($schedule_id, $schedule_date, $site_id, $shift_group_id, $shift_time_id)
    {
        $variables = array(
            'schedule_date' => $schedule_date,
            'site_id' => $site_id,
            'shift_group_id' => $shift_group_id,
            'shift_time_id' => $shift_time_id
        );

        $this->db->where('schedule_id' , $schedule_id)->update($this->table, $variables);

        return ($this->db->affected_rows() > 0);
    }

    public function delete($schedule_id)
    {
        $this->db->delete($this->table, array('schedule_id' => $schedule_id));

        return ($this->db->affected_rows() > 0);
    }

    public function get_group_shift_time_schedule($site_id, $date)
    {
        return $this->db->select("sg.*, (SELECT st.shift_time_id FROM schedule s 
                                JOIN shift_time st ON st.shift_time_id = s.shift_time_id
                                WHERE s.shift_group_id = sg.shift_group_id
                                AND s.site_id = '".$site_id."'
                                AND schedule_date = '".$date."') as shift_time_id,
                                (SELECT s.schedule_id FROM schedule s 
                                JOIN shift_time st ON st.shift_time_id = s.shift_time_id
                                WHERE s.shift_group_id = sg.shift_group_id
                                AND s.site_id = '".$site_id."'
                                AND schedule_date = '".$date."') as schedule_id")
                                ->from('shift_group sg')
                                ->where('sg.site_id', $site_id)
                                ->order_by('sg.shift_group_name', 'ASC')
                                ->get()
                                ->result_array();
    }

    public function add_bulk_schedule($schedule_date, $site_id, $shift_group_id, $shift_time_id)
    {
        $data = array();
        $schedule_date_list = explode(", ",$schedule_date);
        for ($i=0; $i < sizeof($schedule_date_list) ; $i++) {
            if(!$this->has_set_schedule($schedule_date_list[$i], $shift_group_id)){
                $current_data = array(
                    'schedule_id' => $this->get_id(),
                    'schedule_date' => $schedule_date_list[$i],
                    'site_id' => $site_id,
                    'shift_group_id' => $shift_group_id,
                    'shift_time_id' => $shift_time_id
                );
                array_push($data,$current_data);
            }
        }

        if(sizeof($data) > 0){
            $this->db->insert_batch($this->table, $data);
            return ($this->db->affected_rows() > 0);
        }else{
            return FALSE;
        }
   }

   public function has_set_schedule($schedule_date, $shift_group_id)
   {
		$result = $this->db->select('COUNT(*) as total')
							->from($this->table)
							->where(array('schedule_date' => $schedule_date, 'shift_group_id' => $shift_group_id))
							->get();
		return ($result->result()[0]->total > 0);
   }

//    public function get_schedule_list($site_id,$month,$year)
//    {
//         return $this->db->query("SELECT CONCAT(s.schedule_date, 'T' ,st.start_time) AS 'start',
//                                 CONCAT(IF(st.start_time > st.finish_time, s.schedule_date + INTERVAL 1 DAY, s.schedule_date), 'T' ,st.finish_time) AS 'end', 
//                                 CONCAT(sg.shift_group_name, ' ' ,st.shift_time_name) AS title FROM schedule s 
//                                 JOIN shift_group sg ON sg.shift_group_id=s.shift_group_id 
//                                 JOIN shift_time st ON st.shift_time_id=s.shift_time_id
//                                 WHERE s.site_id = '$site_id'
//                                 AND YEAR(s.schedule_date) = $year
//                                 AND MONTH(s.schedule_date) = $month
//                                 ORDER BY shift_time_name ASC")
//                         ->result_array();
//    }

   public function get_schedule_list($site_id,$month,$year)
   {
        return $this->db->query("SELECT s.schedule_date AS 'start', 
                                CONCAT(sg.shift_group_name, ' [ ' ,st.shift_time_name, ' ] ') AS title FROM schedule s 
                                JOIN shift_group sg ON sg.shift_group_id=s.shift_group_id 
                                JOIN shift_time st ON st.shift_time_id=s.shift_time_id
                                WHERE s.site_id = '$site_id'
                                AND YEAR(s.schedule_date) = $year
                                AND MONTH(s.schedule_date) = $month
                                ORDER BY shift_group_name ASC")
                        ->result_array();
   }

//    public function test_disabled_date($site_id, $shift_group_id, $month, $year)
//    {
//        return $this->db->select('CAST(DATE_FORMAT(schedule_date, "%d") AS INT) as day')
//                 ->from($this->table)
//                 ->where(array(
//                     'site_id'=> $site_id,
//                     'shift_group_id'=> $shift_group_id,
//                     'MONTH(schedule_date)'=> $month,
//                     'YEAR(schedule_date)' => $year
//                     ))
//                 ->get()
//                 ->result_array();
//    }
                        
}
                        
/* End of file schedule_model.php */
    
                        