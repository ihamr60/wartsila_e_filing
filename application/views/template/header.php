<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Wartsila Application</title>

    <!-- Bootstrap -->
        
        <link href="<?php echo base_url('assets/bootstrap/css/bootstrap-cv.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
		<!-- DataTables -->
		<link href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">
		<!-- Select2 -->
		<link href="<?php echo base_url('assets/select2/css/select2.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo base_url('assets/select2/css/select2-bootstrap4.min.css'); ?>" rel="stylesheet">
		<!-- Air Date Picker -->
		<link href="<?php echo base_url('assets/air-datepicker/css/datepicker.min.css'); ?>" rel="stylesheet" type="text/css">
        <!-- Chartjs -->
        <link href="<?php echo base_url('assets/Chart.js-2.8.0/dist/Chart.min.css'); ?>" rel="stylesheet">
		<!-- Load CSS Form Controller -->
		<?php if(isset($csses)){
		foreach ($csses as $css) { ?>
				<link href="<?php echo base_url('assets/' . $css . '.css'); ?>" rel="stylesheet"/>
		<?php } 
		} ?>

		<!-- Our Custom CSS -->
		<link href="<?php echo base_url('assets/my/css/style.css'); ?>" rel="stylesheet">

		<!-- Scrollbar Custom CSS -->
		<link href="<?php echo base_url('assets/mCustomScrollbar/css//jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">		

        <!-- jQuery -->
        <!--<script src="<?php echo base_url('assets/jquery/js/jquery-3.3.1.min.js');?>"></script> OLD -->
        <script src="<?php echo base_url('assets/jquery/js/jquery-3.4.1.min.js');?>"></script>
        
        <!-- Font Awesome JS -->
        <script src="<?php echo base_url('assets/fontawesome/js/all.min.js');?>"></script>

        <!-- CKEditor by ilham -->
        <script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>"></script>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" alt="Wartsila Logo" width="100">
            </div>
            <ul class="list-unstyled components">
                <button type="button" id="closeSidebarCollapse" class="btn d-md-block d-lg-none">
                    <i class="fas fa-align-left"></i>
                </button>
                
                <li class="menu-dashboard">
                    <a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i> Dashboard</a>
                </li>
                
                <li>
                    <a href="<?php echo base_url();?>index.php/e_filing/employee?tabPersonData=1"><i class="fas fa-users"></i> My Profile (For Employee)</a>
                </li>
                <li>
                    <a href="#e_filing" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-file-invoice"></i> E-Filing (For HR)&#8195;
                        <?php 
                            if($jumlah_belumBaca!="0")
                            {
                                echo '<span class="badge badge-warning blinktext"><font size="3">'.$jumlah_belumBaca.'</font></span>';
                            } 
                        ?>
                    </a>
                    <ul class="collapse list-unstyled" id="e_filing">
                        <li>
                            <a href="#notif" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Activity Notification&#8195;
                                <?php 
                                    if($jumlah_belumBaca!="0")
                                    {
                                        echo '<span class="badge badge-warning blinktext"><font size="3">'.$jumlah_belumBaca.'</font></span>';
                                    } 
                                ?>
                            </a>
                            <ul class="collapse list-unstyled" id="notif">
                                <li>
                                    <a href="<?php echo base_url('index.php/e_filing/hr/bg_notif_unread'); ?>">&#8195;&#8195;Unread
                                        <?php 
                                            if($jumlah_belumBaca!="0")
                                            {
                                                echo '<span class="badge badge-warning blinktext"><font size="3">'.$jumlah_belumBaca.'</font></span>';
                                            } 
                                        ?>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url('index.php/e_filing/hr/bg_notif_read'); ?>">&#8195;&#8195;Read</a>
                                </li>
                              
                                <li>
                                    <a href="<?php echo base_url('index.php/e_filing/hr/bg_notif'); ?>">&#8195;&#8195;All</a>
                                </li>
                            </ul>
                        </li>
                      
                        <li>
                            <a href="<?php echo base_url('index.php/e_filing/hr'); ?>">Employee Profile</a>
                        </li>
                        
                        <li>
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_filing_category'); ?>">Filing Category</a>
                        </li>
                      
                        <li>
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_required_file'); ?>">Required File</a>
                        </li>
                    </ul>
                </li>
		<!--<li>-->
  <!--                  <a href="<?php echo base_url(); ?>index.php/adm_permiss"><i class="fas fa-user"></i> Admin Permission</a>-->
  <!--              </li>-->
                <li class="menu-holiday">
                    <a href="#holidays" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pub Holidays</a>
                    <ul class="collapse list-unstyled" id="holidays">
                        
                        <li class="menu-holiday-sub-add-holiday">
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_holiday'); ?>">Holidays</a>
                        </li>
                      
                        <li class="menu-holiday-sub-add-country">
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_list_country'); ?>">Country List</a>
                        </li>
                        
                        <li class="menu-holiday-sub-add-province">
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_list_province'); ?>">Province List</a>
                        </li>
                      
                        <li class="menu-holiday-sub-add-city">
                            <a href="<?php echo base_url('index.php/e_filing/hr/bg_list_city'); ?>">City List</a>
                        </li>
                       
                    </ul>
                </li>
	      	                
		<!-- <li>
                    <a href="#">About</a>
                </li> -->
            </ul>
        </nav>