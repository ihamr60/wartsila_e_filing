                <!-- END Content -->
                </div>
    </div>
    
    <!-- Popper.JS -->
    <script src="<?php echo base_url('assets/popper/js/popper.min.js');?>"></script>
    <!-- Bootstrap JS -->
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
    <!-- jQuery Custom Scroller -->
    <script src="<?php echo base_url('assets/mCustomScrollbar/js/jquery.mCustomScrollbar.concat.min.js');?>"></script>
    <!-- Data Tables -->
    <script src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap4.min.js');?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/select2/js/select2.min.js'); ?>"></script>
    <!-- Air Date Picker -->
    <script src="<?php echo base_url('assets/air-datepicker/js/datepicker.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/air-datepicker/js/i18n/datepicker.en.js'); ?>"></script>
    <!-- Sweet Alert -->
    <script src="<?php echo base_url('assets/sweetalert/js/sweetalert2.min.js');?>"></script>
    <!-- Chartjs -->
    <script src="<?php echo base_url('assets/Chart.js-2.8.0/dist/Chart.bundle.min.js');?>"></script>

    <!-- Init -->
    <script type="text/javascript">
        var host = "<?php echo site_url(); ?>/";
        
        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000
                        });

        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });

            $('#closeSidebarCollapse').on('click', function () {
                $(this).toggleClass('active');
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>

    <!-- Load JS Form Controller -->
    <?php if(isset($scripts)){
    foreach ($scripts as $script) { ?>
        <script src="<?php echo base_url('assets/' . $script . '.js');?>"></script>
    <?php } 
    } ?>
</body>

</html>