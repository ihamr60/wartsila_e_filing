    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Shift Group</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">shift group</li>
        </ol>
        </nav>
    </div>


    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <a href="./shift_group/new_shift_group" class="btn btn-sm btn-success text-white btn-new-shift-group" target="_blank">
                <i class="fas fa-plus"></i> Add a new shift group
            </a>
        </div>
    </div>

    <!-- Choose site Card -->
    <div class="card card-header-custom">
        <div class="card-header">Choose Site</div>
        <div class="card-body">
            <select class="form-control" id="siteSelect" name="siteSelect" style="width: 100%">
                <?php foreach ($sites as $site): ?>
                    <option value="<?php echo $site['site_id'] ?>"><?php echo $site['site_name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <!-- Data Table Card -->
    <div class="card card-header-custom">
        <h5 class="card-header"><i class="fas fa-list"></i></h5>
        <div class="card-body">
            <table id="shiftDataTable" class="table table-hover w-100">
                <thead>
                    <tr>
                        <th></th>
                        <th>Shift Group Name</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>