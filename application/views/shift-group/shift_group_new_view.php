    <!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Shift Group New</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/shift_group'); ?>">shift group</a></li>
            <li class="breadcrumb-item">shift group new</li>
        </ol>
        </nav>
    </div>

    <!-- Card -->
    <div class="card card-header-custom">
        <div class="card-header">New Shift Group Form</div>
        <div class="card-body">
            <form id="add-new-shift-group" method="POST"  autocomplete="off">
                <div class="form-group">
                    <label for="input-name">Shift Name</label>
                    <input type="text" class="form-control" id="input-name" name="inputName" value="<?php echo $group_name ?>" placeholder="Ex : Group A" required>
                </div>
                
                <div class="form-group">
                    <label for="siteSelect">Select Site</label>
                    <select class="form-control" id="siteSelect" name="siteSelect" disabled>
                        <?php foreach ($sites as $site): ?>
                            <option value="<?php echo $site['site_id'] ?>" <?php if($site_id && ($site_id == $site['site_id'])) echo 'selected'; ?>><?php echo $site['site_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="input-employees">Selected Employee</label>
                    <select id="input-employees" name="input-employees" class="custom-select" size="5">
                    <?php foreach ($group_members as $member): ?>
                            <option value="<?php echo $member['employee_id'] ?>"> <?php echo $member['employee_id'] . ' - ' . $member['first_name'] . ' ' . $member['last_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

    <!-- Select Employee -->
    <div class="modal fade" id="selectEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="selectEmployeeModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="selectEmployeeModalTitle">Select Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span class="btn btn-primary mb-3">
                        Selected Employee <span class="badge badge-light">0</span>
                    </span>
                    <table id="employeeDataTable" class="table table-striped table-bordered w-100">
                        <thead>
                            <tr>
                                <th>Pers No</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>