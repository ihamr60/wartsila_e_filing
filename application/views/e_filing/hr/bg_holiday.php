
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Holidays</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">Holidays</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewHolidayModal">
            <i class="fas fa-plus"></i> Create a new holiday
            </button>
        </div>
    </div> 

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Holiday List </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th>COUNTRY</th>
                        <th>PROVINCE</th>
                        <th>CITY</th>
                        <th>DATE</th>
                        <th>DESCRIPTION</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                                      <?php
											foreach ($dataHoliday as $d)
											{
									  ?>
                                      <tr class="gradeA">
                                          <td><?php echo $d->country_name ?></td>
                                          <td><?php echo $d->province_name ?></td>
                                          <td><?php echo $d->city_name ?></td>
                                          <td><?php echo $d->date_holiday ?></td>
									      <td><?php echo $d->description ?></td>
                                          <td>
                                              
                                              <button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_editHoliday/<?php echo $d->holiday_id ?>'" class="btn btn-primary btn-sm">Edit</button>
                                               <a href="<?php echo base_url();?>index.php/e_filing/hr/deleteHoliday/<?php echo $d->holiday_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</a>
                                  		  </td>
                                      </tr>
                                      
                                      <?php } ?>
                                      </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Site START -->
    <div class="modal fade" id="addNewHolidayModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/add_holiday">
                        <div class="form-group">
                            <label for="input-address">Country</label>
                            <select name="country_id" class="form-control" required>   
                            <?php
								foreach($dataCountry as $a) 
								{
							?>                        	
                                 <option selected value="<?php echo $a->country_id ?>"><?php echo $a->country_name ?></option>
                            <?php } ?>
                                         
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Province</label>
                            <select name="province_id" class="form-control" required>   
                            <?php
								foreach($dataProvince as $b) 
								{
							?>                        	
                                 <option selected value="<?php echo $b->province_id ?>"><?php echo $b->province_name ?></option>
                            <?php } ?>
                                         
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="input-address">City</label>
                            <select name="city_id" class="form-control" required>   
                            <?php
								foreach($dataCity as $c) 
								{
							?>                        	
                                 <option selected value="<?php echo $c->city_id ?>"><?php echo $c->city_name ?></option>
                            <?php } ?>
                                         
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="schedule-date">Holiday Date</label>
                            <input type="date" class="form-control" name="holiday_date" required>
                        </div>
                        <div class="form-group">
                            <label for="schedule-date">Description</label>
                            <div>
                            <textarea cols="50" rows="10" name="description"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add New Site END -->

    <!-- MODAL Edit Site START -->
    <div class="modal fade" id="editSiteModal" tabindex="-1" role="dialog" aria-labelledby="editSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-site" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Site END -->
    
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    