
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">City List</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">City List</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewCityModal">
            <i class="fas fa-plus"></i> Add a new city
            </button>
        </div>
    </div> 

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; City List </div>
        <div class="card-body">
         <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th>CITY CODE</th>
                        <th>CITY NAME</th>
                        <th>PROVINCE NAME</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                                      <?php
											foreach ($dataCity as $d)
											{
									  ?>
                                      <tr class="gradeA">
                                          <td><?php echo $d->city_code ?></td>
                                          <td><?php echo $d->city_name ?></td>
                                          <td><?php echo $d->province_name ?></td>
                                          <td>
                                              
                                              <button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_editCity/<?php echo $d->city_id ?>'" class="btn btn-primary btn-sm">Edit</button>
                                              <a href="<?php echo base_url();?>index.php/e_filing/hr/deleteCity/<?php echo $d->city_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</a>
                                  		  </td>
                                      </tr>
                                      
                                      <?php } ?>
                                      </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Site START -->
    <div class="modal fade" id="addNewCityModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add New City</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/add_city">
                        <div class="form-group">
                            <label for="input-name">City Code</label>

                            <input type="text" class="form-control" name="city_code" placeholder="Ex : SAB" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">City Name</label>
                            <input type="text" class="form-control" name="city_name" placeholder="Ex : SABANG" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Province</label>
                            <select name="province_id" class="form-control" required>   
                            <?php
								foreach($dataProvince as $a) 
								{
							?>                        	
                                 <option selected value="<?php echo $a->province_id ?>"><?php echo $a->province_name ?></option>
                            <?php } ?>
                                         
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add New Site END -->

    <!-- MODAL Edit Site START -->
    <div class="modal fade" id="editCityModal" tabindex="-1" role="dialog" aria-labelledby="editSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-site" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Site END -->
    
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    