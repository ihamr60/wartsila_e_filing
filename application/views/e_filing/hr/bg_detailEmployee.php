<!-- Page Content  -->
<div id="content">
    <div class="page-head">
        <h2 class="page-head-title">LIVE CV - <?php echo $employee['First_name']." ".$employee['Last_name'] ?></h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/e_filing/hr">Employee Data</a></li>
                <li class="breadcrumb-item">Live CV</li>
            </ol>
        </nav>
    </div>

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal -->
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li style="<?php echo $this->session->flashdata('person2'); ?>">
                    <a data-toggle="tab" href="#panel_overview">
                        Personal Data <?php echo $this->session->flashdata('person'); ?>
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_organisasi">
                        Organization <?php echo $this->session->flashdata('org'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_kerja">
                        Work Experience <?php echo $this->session->flashdata('work'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_pendidikan">
                        Education <?php echo $this->session->flashdata('edu'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_kompetensi">
                        Certification <?php echo $this->session->flashdata('cert'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_bahasa">
                        Language <?php echo $this->session->flashdata('lang'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_keluarga">
                        Family <?php echo $this->session->flashdata('fam'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_kursus">
                        Training <?php echo $this->session->flashdata('train'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_prestasi">
                        Achievement <?php echo $this->session->flashdata('achi'); ?>
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_referensi">
                        Reference <?php echo $this->session->flashdata('ref'); ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- start: PAGE CONTENT -->
    <div class="row">
        <div class="col-sm-12">
            <div class="tabbable">
                <div class="tab-content">       
                    <div id="panel_overview" class="tab-pane in active">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;PERSONAL DATA <?php echo $this->session->flashdata('person'); ?></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div>
                                            <div class="card card-header-custom"> 
                                                <div class="pt-4" style="<?php echo $this->session->flashdata('photo'); ?>"> 
                                                    <p align="center">
                                                        <img style="width: 150; height: 250px; border: 0px #000 solid; border-radius: 6%;" src="<?php echo base_url();?>upload/photo/<?php echo $employee['photo'] ?>">
                                                    </p>
                                                    <p align="center"><?php echo $this->session->flashdata('photo2'); ?></p>
                                                    <h4 align="center"><?php echo $employee['First_name']." ".$employee['Last_name' ] ?></h4>
                                                    <p  align="center"><font size="2"><?php echo $employee['mail_addr'] ?></font></p>		
                                                    <hr>
                                                </div>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">PERSONAL INFORMATION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="<?php echo $this->session->flashdata('person_inf'); ?>">
                                                        <tr>
                                                            <td>PersNo</td>
                                                            <td><span class="label label-sm label-default"><?php echo $employee['PersNo']; ?></span></a>
                                                            </td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Sex</td>
                                                            <td><?php echo $employee['gender'] ?> </td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Birtdate</td>
                                                            <td><?php echo $employee['Birthdate'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Religion</td>
                                                            <td><?php echo $employee['religion'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tribe</td>
                                                            <td><?php echo $employee['tribe'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Height</td>
                                                            <td><?php echo $employee['height'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Weight</td>
                                                            <td><?php echo $employee['weight'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Blood Type</td>
                                                            <td><?php echo $employee['blood_type'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Passion</td>
                                                            <td><?php echo $employee['passion'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Marital Stats</td>
                                                            <td><?php echo $employee['marital_stats'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nationality</td>
                                                            <td><?php echo $employee['Nationality'] ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">GENERAL INFORMATION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="<?php echo $this->session->flashdata('person_inf'); ?>">

                                                        <tr>
                                                            <td>Position</td>
                                                            <td><?php echo $employee['Field17'] ?></td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Superior</td>
                                                            <td><?php echo $employee['Name_of_superior'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Departement</td>
                                                            <td><?php echo $employee['dept'] ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">CONTRACT INFORMATION</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="<?php echo $this->session->flashdata('person_inf'); ?>">
                                                        <tr>
                                                            <td>Status Employment</td>
                                                            <td><?php echo $employee['Employment_Status'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Join Date</td>
                                                            <td><?php echo $employee['join_date'] ?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td>Contract Type</td>
                                                            <td><?php echo $employee['Contract_Type'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Contract End</td>
                                                            <td>
                                                                <?php 
                                                                if($employee['Contract_Type'] == "PERMANENT CONTRACT")
                                                                {
                                                                   echo "PERMANENT";	
                                                               }
                                                               else
                                                               {
                                                                   echo $employee['Cont_End'];
                                                               }
                                                               ?>
                                                           </td>
                                                       </tr>
                                                   </tbody>
                                               </table>
                                               <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">CURRENT ADDRESS INFORMATION <?php echo $this->session->flashdata('curad2'); ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="<?php echo $this->session->flashdata('person_inf'); ?>">
                                                        <tr>
                                                            <td>Address</td>
                                                            <td><?php echo $employee['address_current'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>City</td>
                                                            <td><?php echo $employee['city_current'] ?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td>Province</td>
                                                            <td><?php echo $employee['province_current'] ?></td>
                                                        </tr>
                                                   </tbody>
                                               </table>
                                               <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">PERMANENT ADDRESS INFORMATION <?php echo $this->session->flashdata('perad2'); ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="<?php echo $this->session->flashdata('person_inf'); ?>">
                                                        <tr>
                                                            <td>Address</td>
                                                            <td><?php echo $employee['address_permanent'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>City</td>
                                                            <td><?php echo $employee['city_permanent'] ?></td> 
                                                        </tr>
                                                        <tr>
                                                            <td>Province</td>
                                                            <td><?php echo $employee['province_permanent'] ?></td>
                                                        </tr>
                                                   </tbody>
                                               </table>
                                           </div>
                                       </div>
                                   </div>

                                   <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card card-header-custom"> 
                                                    <?php echo $this->session->flashdata('info'); ?>
                                                    <div class="col-md-12">
                                                         <h4><img src='<?php echo base_url()?>/assets/images/card.png'> <b>ID CARD NUMBER INFORMATION <?php echo $this->session->flashdata('person_inf2'); ?> </b></h4>
                                                         <hr>
                                                    </div>
                                                    <div class="col-md-12 pt-3 pb-3" style="<?php echo $this->session->flashdata('person_inf'); ?>">
                                                        <div class="col-md-6">
                                                             <div class="form-group">
                                                                 <label class="control-label">
                                                                     KTP
                                                                 </label>   			
                                                                 <input readonly class="form-control" type="text" value="<?php echo $employee['no_ktp'] ?>">   
                                                             </div>
                                                             <div class="form-group">
                                                                 <label class="control-label">
                                                                     NPWP
                                                                 </label>         		
                                                                 <input readonly class="form-control" type="text" value="<?php echo $employee['npwp'] ?>">    
                                                             </div>
                                                             <div class="form-group">
                                                                 <label class="control-label">
                                                                     BPJS Ketenagakerjaan
                                                                 </label>
                                                                 <input readonly class="form-control" type="text" value="<?php echo $employee['bpjs_naker'] ?>">
                                                             </div>
                                                             <div class="form-group">
                                                                 <label class="control-label">
                                                                     BANK
                                                                 </label>
                                                                 <input readonly class="form-control" type="text" value="<?php echo $employee['Bank_Account']." - ".$employee['Field34'] ?>">
                                                             </div>
                                                             <div class="form-group">
                                                                <label class="control-label">
                                                                    JAMSOSTEK
                                                                </label>
                                                                <input readonly class="form-control" type="text" value="<?php echo $employee['jamsostek'] ?>">
                                                            </div>
                                                        </div>
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    BPJS Kesehatan
                                                                </label>
                                                                <input readonly class="form-control" type="text" value="<?php echo $employee['bpjs_kes'] ?>">    
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    Pasport
                                                                </label>
                                                                <input readonly class="form-control" type="text" value="<?php echo $employee['passport'] ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    KK
                                                                </label>
                                                                <input readonly class="form-control" type="text" value="<?php echo $employee['no_kk'] ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    DPLK
                                                                </label>
                                                                <input readonly class="form-control" type="text" value="<?php echo $employee['dplk'] ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 pt-3">

                                                <div class="card card-header-custom">
                                                    <div class="col-md-12 pt-3">
                                                        <h4><img src='<?php echo base_url()?>/assets/images/attach.png'> <b>ATTACHMENT SECTION</b> <?php echo $this->session->flashdata('attach2'); ?></h4>
                                                        <hr>
                                                    </div>

                                                    <div class="card-body">
                                                        <!-- Button trigger modal  -->
                                                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewFilingModal<?php echo $employee['PersNo'] ?>">
                                                            <i class="fas fa-plus"></i> Add a new E-Filing
                                                        </button>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class='alert alert-warning'>
                                                            <strong>
                                                               WARNING! -
                                                            </strong>
                                                            <font color="red">*</font>Please upload all required files
                                                        </div>
                                                    </div>

                                                    <div class="card-body">
                                                        <table style="<?php echo $this->session->flashdata('attach'); ?>"  class="table table-striped table-bordered table-hover" id="example">
                                                            <thead>
                                                                <tr>
                                                                    <th width="10">NO</th>
                                                                    <th>FILE NAME</th>
                                                                    <th width="180px" >ATTACHMENT</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $no=0;
                                                                    foreach($data_file_req as $d)
                                                                    {
                                                                        $no++;
                                                                ?>
                                                                <tr>
                                                                    <td align="center"><?php echo $no; ?></td>
                                                                    <td>
                                                                        <?php 
                                                                            echo $d->required_file_name.'<font color="red">*</font> <span class="badge badge-warning">'.$d->category_name.'</span>'; 

                                                                            if(empty($filing_req))
                                                                            {
                                                                                echo '&#8195;<img src="'.base_url().'/assets/images/false.png">';   
                                                                            }
                                                                            else
                                                                            {
                                                                                foreach($filing_req as $a)
                                                                                {
                                                                                    $cek   = $this->web_app_model->getWhere2($this->uri->segment(4),'PersNo_req',$d->uuid_employee_required_file,'uuid_employee_required_file','tbl_filing_required');

                                                                                    if (count($cek) > 0) {
                                                                                        echo '&#8195;<img src="'.base_url().'/assets/images/checklist.png">';
                                                                                        break;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        echo '&#8195;<img src="'.base_url().'/assets/images/false.png">';
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                    <td align="center">
                                                                    <?php 
                                                                        if(empty($filing_req))
                                                                            {
                                                                                echo'<a class="btn btn-warning btn-sm">Not&#8195;Found</a>
                                                                                             <a 
                                                                                              href="javascript:;" 
                                                                                              class="btn btn-sm btn-primary"
                                                                                              data-file_name_req="'.$d->required_file_name.'"
                                                                                              data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                                                              data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                                                              data-toggle="modal" data-target="#uploadFile_add">
                                                                                             <i class="fa fa-upload"></i> Upload </a>
                                                                                             ';
                                                                            }
                                                                            else
                                                                            {
                                                                                foreach($filing_req as $a)
                                                                                { 
                                                                                    $cek   = $this->web_app_model->getWhere2($this->uri->segment(4),'PersNo_req',$d->uuid_employee_required_file,'uuid_employee_required_file','tbl_filing_required');

                                                                                    if (count($cek) > 0) 
                                                                                    {
                                                                                        if($a->PersNo_req==$this->uri->segment(4) && $a->uuid_employee_required_file==$d->uuid_employee_required_file)
                                                                                        {
                                                                                            echo'<a href="'.base_url().'upload/others/'.$a->attach_req.'" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> View Doc</a>
                                                                                                 <a 
                                                                                                  href="javascript:;" 
                                                                                                  class="btn btn-sm btn-info"
                                                                                                  data-uuid_filing_req="'.$a->uuid_filing_req.'"
                                                                                                  data-file_name_req="'.$d->required_file_name.'"
                                                                                                  data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                                                                  data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                                                                  data-toggle="modal" data-target="#uploadFile_update">
                                                                                                 <i class="fas fa-upload"></i> Change </a>';
                                                                                            break;
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        echo'<a class="btn btn-warning btn-sm">Not&#8195;Found</a>
                                                                                             <a 
                                                                                              href="javascript:;" 
                                                                                              class="btn btn-sm btn-primary"
                                                                                              data-uuid_filing_req="'.$a->uuid_filing_req.'"
                                                                                              data-file_name_req="'.$d->required_file_name.'"
                                                                                              data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                                                              data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                                                              data-toggle="modal" data-target="#uploadFile_add">
                                                                                             <i class="fa fa-upload"></i> Upload </a>
                                                                                             ';
                                                                                        break;
                                                                                    }
                                                                                }
                                                                             }
                                                                        ?>
                                                                    </td>    
                                                                </tr>
                                                                <?php } ?>
                                                                <?php 
                                                                    foreach($data_file as $d)
                                                                    {
                                                                        $no++;
                                                                ?>
                                                                <tr>
                                                                    <td align="center"><?php echo $no; ?></td>
                                                                    <td><?php echo $d->file_name.' <span class="badge badge-warning">'.$d->category.'</span>' ?></td>
                                                                    <td align="center">
                                                                       <a href="<?php echo base_url();?>upload/others/<?php echo $d->attachment ?>" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> View Doc</a>
                                                                       <a onclick="return confirm('Anda yakin akan menghapus dokumen ini?')" href="<?php echo base_url();?>index.php/e_filing/upload/HRdeleteDoc/<?php echo $d->uuid_filing ?>/<?php echo $d->attachment ?>/<?php echo $d->PersNo ?>" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                                                    </td>    
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>          
                                        </div> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>  

                    <!-- Modal Upload add File START -->
                        <div class="modal fade" id="uploadFile_add" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fa fa-upload"></i></i>&#8195;Upload E-Filing</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/HRUploadDoc_req_add" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="file_name_req">File Name Required</label>
                                                <input type="hidden" value="<?php echo $employee['First_name'] ?>" name="First_name">
                                                <input type="hidden" value="<?php echo $employee['Last_name'] ?>" name="Last_name">
                                                <input type="hidden" value="<?php echo $employee['PersNo'] ?>" name="PersNo">
                                                <input type="hidden" id="uuid_employee_required_file" name="uuid_employee_required_file">
                                                <input type="hidden" id="uuid_filing_category" name="uuid_filing_category">
                                                <input 
                                                 readonly 
                                                 id="file_name_req"
                                                 name="file_name_req"
                                                 class="form-control post_variable"
                                                 type="text" 
                                                 required>   
                                                 <div class="invalid-feedback"></div>
                                                 <div class="valid-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="attach_req">Choose File</label>
                                                <input 
                                                 id="attach_req"
                                                 name="attach_req"
                                                 class="form-control post_variable"
                                                 type="file" 
                                                 required>   
                                                 <div class="invalid-feedback"></div>
                                                 <div class="valid-feedback"></div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Upload add END -->

                        <!-- Modal Upload_update File START -->
                        <div class="modal fade" id="uploadFile_update" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fa fa-upload"></i></i>&#8195;Upload E-Filing</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/HRUploadDoc_req_update" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label for="file_name_req">File Name Required</label>
                                                <input type="hidden" id="uuid_filing_req" name="uuid_filing_req">
                                                <input type="hidden" value="<?php echo $employee['First_name'] ?>" name="First_name">
                                                <input type="hidden" value="<?php echo $employee['Last_name'] ?>" name="Last_name">
                                                <input type="hidden" value="<?php echo $employee['PersNo'] ?>" name="PersNo">
                                                <input type="hidden" id="uuid_employee_required_file" name="uuid_employee_required_file">
                                                <input type="hidden" id="uuid_filing_category" name="uuid_filing_category">
                                                <input 
                                                 readonly 
                                                 id="file_name_req"
                                                 name="file_name_req"
                                                 class="form-control post_variable"
                                                 type="text" 
                                                 required>   
                                                 <div class="invalid-feedback"></div>
                                                 <div class="valid-feedback"></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="attach_req">Choose File</label>
                                                <input 
                                                 id="attach_req"
                                                 name="attach_req"
                                                 class="form-control post_variable"
                                                 type="file" 
                                                 required>   
                                                 <div class="invalid-feedback"></div>
                                                 <div class="valid-feedback"></div>
                                            </div>
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload & Change</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal Upload_update END -->

                    <!-- PANEL PENGALAMAN ORGANISASI -->
                    <div id="panel_pengalaman_organisasi" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;HISTORY OF THE ORGANIZATION EXPERIENCE <?php echo $this->session->flashdata('org'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('org1'); ?>" class="table table-striped table-bordered table-hover" id="example2">
                                    <thead>
                                        <tr>
                                            <th>ORGANIZATION</th>
                                            <th>POSITION</th>
                                            <th>START YEAR</th>
                                            <th>STOP YEAR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                           foreach ($organisasi as $b )
                                           {
                                             ?>
                                             <tr>
                                                <td><?php echo $b->organisasi ?></td>
                                                <td><?php echo $b->jabatan ?></td>
                                                <td><?php echo $b->thn_mulai ?></td>
                                                <td><?php echo $b->thn_berakhir ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL PENGALAMAN ORGANISASI -->

                    <!-- START PANEL PENGALAMAN PENDIDIKAN -->
                    <div id="panel_pengalaman_pendidikan" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;HISTORY OF EDUCATION <?php echo $this->session->flashdata('edu'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('edu1'); ?>" class="table table-striped table-bordered table-hover" id="example3">
                                    <thead>
                                        <tr>     
                                            <th>INSTITUTION</th>
                                            <th>MAJORS</th>
                                            <th>IPK/GPA</th>
                                            <th>STAR YEAR</th>
                                            <th>GRADUATION YEAR</th>
                                            <th>DIPLOMA NUMBER</th>           
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php 
                                          foreach ($pendidikan as $e )
                                          {
                                             ?>
                                             <tr>    
                                                <td><?php echo $e->institusi ?></td>
                                                <td><?php echo $e->jurusan ?></td>
                                                <td><?php echo $e->ipk ?></td>
                                                <td><?php echo $e->thn_masuk ?></td>
                                                <td><?php echo $e->thn_lulus ?></td>
                                                <td><?php echo $e->no_ijazah ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PENDIDIKAN -->

                    <!-- START PANEL PENGALAMAN Kerja -->
                    <div id="panel_pengalaman_kerja" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;HISTORY OF WORK EXPERIENCE <?php echo $this->session->flashdata('work'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('work1'); ?>" class="table table-striped table-bordered table-hover" id="example4">
                                    <thead>
                                        <tr>               
                                            <th>COMPANY</th>
                                            <th>POSITION</th>
                                            <th>START WORKING</th>
                                            <th>STOP WORKING</th>      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($job_ex as $a)
                                            {
                                              ?>
                                              <tr>
                                                <td><?php echo $a->perusahaan ?></td>
                                                <td><?php echo $a->jabatan ?></td>
                                                <td><?php echo $a->thn_mulai ?></td>
                                                <td><?php echo $a->thn_berakhir ?></td>
                                            </tr> 
                                        <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Pengalaman Kerja -->

                    <!-- START PANEL Kompetensi -->
                    <div id="panel_kompetensi" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;LIST OF COMPETENCE / CERTIFICATION <?php echo $this->session->flashdata('cert'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('cert1'); ?>" class="table table-striped table-bordered table-hover" id="example5">
                                    <thead>
                                        <tr>                  
                                            <th>COMPETENCE</th>
                                            <th>LEVEL</th>                                     
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            foreach($kompetensi as $c)
                                            {
                                              ?>
                                              <tr>
                                                <td><?php echo $c->kompetensi?></td> 
                                                <td><?php echo $c->level ?></td> 
                                            </tr>  
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Kompetensi -->

                    <!-- PANEL BAHASA -->
                    <div id="panel_bahasa" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;LIST OF LANGUAGE MASTERED <?php echo $this->session->flashdata('lang'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('lang1'); ?>" class="table table-striped table-bordered table-hover" id="example6">
                                    <thead>
                                        <tr>
                                            <th>LANGUAGE</th>
                                            <th>LEVEL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($language as $f)
                                            {
                                                ?>
                                                <tr> 
                                                    <td><?php echo $f->bahasa ?></td>      
                                                    <td><?php echo $f->level ?></td>
                                                </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL BAHASA -->

                    <!-- PANEL KELUARGA -->
                    <div id="panel_keluarga" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;LIST OF FAMILY <?php echo $this->session->flashdata('fam'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('fam1'); ?>" class="table table-striped table-bordered table-hover" id="example7">
                                    <thead>
                                        <tr>
                                            <th>FULLNAME</th>
                                            <th>CONTACT</th>
                                            <th>BIRTHDATE</th>
                                            <th>RELATION</th>
                                            <th>JOB</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                           foreach ($family as $g)
                                           {
                                               ?>
                                               <tr>   
                                                <td><?php echo $g->nama_sdr ?></td>                
                                                <td><?php echo $g->no_hp ?></td>
                                                <td><?php echo $g->tgl_lhr_sdr ?></td>
                                                <td><?php echo $g->hubungan ?></td>
                                                <td><?php echo $g->pekerjaan ?></td>        
                                            </tr>  
                                        <?php } ?>                           
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL KELUARGA -->

                    <!-- PANEL KURSUS -->
                    <div id="panel_kursus" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;HISTORY OF TRAINING <?php echo $this->session->flashdata('train'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('train1'); ?>" class="table table-striped table-bordered table-hover" id="example8">
                                    <thead>
                                        <tr> 
                                            <th>COURSE NAME</th>
                                            <th>COURSE SITES</th>
                                            <th>START COURSE</th>
                                            <th>FINISH COURSE</th>
                                            <th>ORGANIZER</th>        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach($training as $h)
                                        {
                                            ?>
                                            <tr> 
                                                <td><?php echo $h->training ?></td>
                                                <td><?php echo $h->tmpt_training ?></td>
                                                <td><?php echo $h->mulai_training ?></td>
                                                <td><?php echo $h->selesai_training ?></td>
                                                <td><?php echo $h->penyelenggara ?></td>
                                            </tr>
                                        <?php } ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL KURSUS -->

                    <!-- PANEL PRESTASI -->
                    <div id="panel_prestasi" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;LIST OF ACHIEVEMENT <?php echo $this->session->flashdata('achi'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('achi1'); ?>" class="table table-striped table-bordered table-hover" id="example9">
                                    <thead>
                                        <tr>      
                                            <th>ACHIEVEMENT</th>
                                            <th>YEAR</th>
                                            <th>ORGANIZER/APPRECIATOR</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach($achievement as $i)
                                            {
                                              ?>
                                              <tr>
                                                <td><?php echo $i->achievement ?></td>
                                                <td><?php echo $i->year ?></td>
                                                <td><?php echo $i->organizer ?></td>
                                            </tr>
                                        <?php } ?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL PRESTASIs -->

                    <!-- PANEL REFERENSI -->
                    <div id="panel_referensi" class="tab-pane">
                        <div class="card card-header-custom">
                            <div class="card-header"><i class="fas fa-list"></i> &#8195;LIST OF REFFERENCE <?php echo $this->session->flashdata('ref'); ?></div>
                            <div class="card-body">
                                <table style="<?php echo $this->session->flashdata('ref1'); ?>" class="table table-striped table-bordered table-hover" id="example10">
                                    <thead>
                                        <tr>
                                            <th>FULLNAME</th>
                                            <th>RELATION</th>
                                            <th>NUMBER HANDPHONE</th>
                                            <th>E-MAIL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                           foreach($refference as $j)
                                           {
                                             ?>
                                             <tr>  
                                                <td><?php echo $j->fullname ?></td>
                                                <td><?php echo $j->relation ?></td>
                                                <td><?php echo $j->no_hp ?></td>
                                                <td><?php echo $j->email ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL REFERENSI --> 
                </div>
            </div>
        </div>
    </div>
    <!-- end: PAGE CONTENT-->

    <!-- Modal Refference -->
    <?php 
        foreach($ALLemployee as $d)
        {
    ?>
        <div class="modal fade" id="addNewFilingModal<?php echo $d->PersNo ?>" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add E-Filing</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/HRUploadDoc" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="file_name">File Name</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" value="<?php echo $d->PersNo ?>" name="PersNo">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 pattern="[A-Za-z0-9 -]+"
                                 id="file_name"
                                 name="file_name"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: File Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="control-label">
                                   Category 
                                </label> 
                                <select
                                id="category"
                                name="category"
                                class="form-control post_variable"
                                required>
                                <?php 
                                  foreach($category as $d)
                                  {                                      
                                      echo '<option value="'.$d->category_name.'">'.$d->category_name.'</option>';
                                  } 
                                ?>
                                </select>            
                                <div class="invalid-feedback"></div>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="attachment">Choose File</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="attachment"
                                 name="attachment"
                                 class="form-control post_variable"
                                 type="file" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
        <!-- Modal Refference END -->
</div>

<script>
 $(document).ready(function() {
  $('#example').DataTable();
  $('#example2').DataTable();
  $('#example3').DataTable();
  $('#example4').DataTable();
  $('#example5').DataTable();
  $('#example6').DataTable();
  $('#example7').DataTable();
  $('#example8').DataTable();
  $('#example9').DataTable();
  $('#example10').DataTable();
} );

 $('#uploadFile_add').on('show.bs.modal', function (event) {
          var div = $(event.relatedTarget)
          var modal          = $(this)
          modal.find('#uuid_filing_req').attr("value",div.data('uuid_filing_req'));
          modal.find('#file_name_req').attr("value",div.data('file_name_req'));
          modal.find('#uuid_filing_category').attr("value",div.data('uuid_filing_category'));
          modal.find('#uuid_employee_required_file').attr("value",div.data('uuid_employee_required_file'));
      });

 $('#uploadFile_update').on('show.bs.modal', function (event) {
          var div = $(event.relatedTarget)
          var modal          = $(this)
          modal.find('#uuid_filing_req').attr("value",div.data('uuid_filing_req'));
          modal.find('#file_name_req').attr("value",div.data('file_name_req'));
          modal.find('#uuid_filing_category').attr("value",div.data('uuid_filing_category'));
          modal.find('#uuid_employee_required_file').attr("value",div.data('uuid_employee_required_file'));
      });

</script>