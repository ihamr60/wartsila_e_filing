
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Edit Province</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/e_filing/hr/bg_list_province">Province List</a></li>
        	<li class="breadcrumb-item">Edit Province</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Data Tables Card -->
    <div class="col-md-12">
       <div class="card card-header-custom"> 
        <br>
       <div class="col-md-12">
       		<h4>Edit Province</h4>
       		<hr>
       </div>
       <?php
	   		foreach($dataProvince as $d)
			{ 
	   ?>
       <div class="col-md-8">
        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/editProvince">
           <div class="form-group">
               <label class="control-label">
                   Province Code
               </label>   			
               <input class="form-control" name="province_id" type="hidden" value="<?php echo $d->province_id ?>">
               <input readonly class="form-control" name="province_code" type="text" value="<?php echo $d->province_code ?>">   
           </div>
           <div class="form-group">
               <label class="control-label">
                   Province Name
               </label>         		
               <input class="form-control" name="province_name" type="text" value="<?php echo $d->province_name ?>">    
           </div>
            <div class="form-group">
               <label class="control-label">
                   Country
               </label>         		
              <select name="country_id" class="form-control">
             <?php 
				if($d->country_id==$d->country_id)
					{
						echo'<option selected value="'.$d->country_id.'">'.$d->country_name.'</option>';
						
						foreach ($dataCountry as $e)
						{
							echo'<option value="'.$e->country_id.'">'.$e->country_name.'</option>';
						}
					}
			?>
                                                         
           </select>
           </div>
           
           <br>
           <button type="submit" class="btn btn-primary">Edit</button>
         </form>
           <br>                         
       </div>
       <?php } ?>
      </div>
    </div>

    <!-- Modal Add New Site START -->
    <div class="modal fade" id="addNewCityModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add New City</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/add_city">
                        <div class="form-group">
                            <label for="input-name">City Code</label>

                            <input type="text" class="form-control" name="city_code" placeholder="Ex : ID" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">City Name</label>
                            <input type="text" class="form-control" name="city_name" placeholder="Ex : INDONESIA" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add New Site END -->

    <!-- MODAL Edit Site START -->
    <div class="modal fade" id="editCityModal" tabindex="-1" role="dialog" aria-labelledby="editSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-site" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Site END -->
    
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    