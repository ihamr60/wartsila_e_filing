<!-- Page Content  -->
<div id="content">
    <div class="page-head">
        <h2 class="page-head-title">Employee Data</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
                <li class="breadcrumb-item">Employee Data</li>
            </ol>
        </nav>
    </div>

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Employee Database </div>
        <div class="card-body">
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Name</th>
                        <th>Birth Date</th>
                        <th>Department</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($dataKaryawan as $d)
                        {
                         ?>
                            <tr class="gradeA">
                              <td><?php echo $d->PersNo ?></td>
                              <td><?php echo $d->Known_As ?></td>
                              <td><?php echo $d->Birthdate ?></td>
                              <td><?php echo $d->dept ?></td>
                              <td>
                                  <a href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/<?php echo $d->PersNo ?>' class="btn btn-success btn-sm"><i class="fas fa-file"></i>&#8195;Live CV</a>
                                  
                              </td>
                            </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Site START -->
    <div class="modal fade" id="addNewSiteModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-new-site" method="POST"  autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add New Site END -->

    <!-- MODAL Edit Site START -->
    <div class="modal fade" id="editSiteModal" tabindex="-1" role="dialog" aria-labelledby="editSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-site" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Site END -->
</div>

<script>
 $(document).ready(function() {
  $('#example').DataTable();
} );
</script>
