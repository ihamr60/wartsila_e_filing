<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Employee Required File</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">Required File</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addRequiredFileModal">
                <i class="fas fa-plus"></i> Create a new required file
            </button>
        </div>
    </div> 

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Required File </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>Required File Name</th>
                        <th>Category Name</th>
                        <th width="140px">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
          					foreach ($dataRequiredFile as $d)
          					{     
                      $no++;
              		?>
                  <tr>
                      <td align="center"><?php echo $no; ?></td>
                      <td><?php echo $d->required_file_name ?></td>
                      <td><?php echo $d->category_name ?></td>
                      <td>
                          <button
                              class="btn btn-sm btn-primary"
                              data-toggle="modal" data-target="#editRequiredFileModal<?php echo $d->uuid_employee_required_file ?>">
                              <i class="fas fa-edit"></i> Edit
                          </button>
                          <a href="<?php echo base_url();?>index.php/e_filing/hr/deleteRequiredFile/<?php echo $d->uuid_employee_required_file ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
              		  </td>
                  </tr>
                  <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Required File START -->
    <div class="modal fade" id="addRequiredFileModal" tabindex="-1" role="dialog" aria-labelledby="addNewRequiredFileTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewRequiredFileTitle"><i class="fas fa-file-signature"></i>&#8195;Add New Required File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/add_requiredFile">
                        <div class="form-group">
                            <label for="required_file_name">Required File Name</label>
                            <input 
                              id="required_file_name" 
                              onkeyup="this.value = this.value.toUpperCase()"
                              name="required_file_name"
                              type="text" 
                              class="form-control post_variable"  
                              placeholder="Required File Name" 
                              required>
                        </div>
                        <div class="form-group">
                            <label for="uuid_filing_category">Category</label>
                            <select 
                             id="uuid_filing_category"
                             name="uuid_filing_category"
                             class="form-control post_variable"
                             required>
                             <?php 
                              foreach($dataFilingCategory as $d)
                              {
                             ?>
                                <option value="<?php echo $d->uuid_filing_category ?>"><?php echo $d->category_name; ?></option>
                             <?php } ?>
                            </select>
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class='alert alert-warning'>
                                                            <strong>
                                                               WARNING! -
                                                            </strong>
                                                            Please first fill in the "Filing Category" section on the menu for this category option
                                                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add Required File END -->

    <?php
      foreach($dataRequiredFile as $d)
      {
    ?>
    <!-- MODAL Edit Required File START -->
    <div class="modal fade" id="editRequiredFileModal<?php echo $d->uuid_employee_required_file ?>" tabindex="-1" role="dialog" aria-labelledby="editRequiredFileModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editRequiredFileModalTitle"><i class="fas fa-file-signature"></i>Edit Required File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/edit_requiredFile">
                        <div class="form-group">
                          <label for="required_file_name">Required File Name</label>
                          <input type="hidden" name="uuid_employee_required_file" value="<?php echo $d->uuid_employee_required_file ?>">
                          <input 
                              id="required_file_name" 
                              name="required_file_name" 
                              type="text" 
                              onkeyup="this.value = this.value.toUpperCase()"
                              class="form-control"
                              value="<?php echo $d->required_file_name ?>" 
                              required>
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="uuid_filing_category">Category</label>
                            <select 
                             id="uuid_filing_category"
                             name="uuid_filing_category"
                             class="form-control post_variable"
                             required>
                             <option value="<?php echo $d->uuid_filing_category ?>"><?php echo $d->category_name ?></option>
                             <?php 
                              foreach($dataFilingCategory as $e)
                              {
                                if($d->uuid_filing_category!=$e->uuid_filing_category)
                                {
                                  echo '<option value="'.$e->uuid_filing_category.'">'.$e->category_name.'</option>';
                                }
                              }
                             ?>
                            </select>
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Required File END -->
  <?php } ?>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    