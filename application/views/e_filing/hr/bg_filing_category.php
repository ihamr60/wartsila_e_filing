<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Filing Category</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">Filing Category</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addFilingCategoryModal">
                <i class="fas fa-plus"></i> Create a new filing category
            </button>
        </div>
    </div> 

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Filing Category </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>Category Name</th>
                        <th width="140">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                        $no=0;
						foreach ($dataFilingCategory as $d)
						{     
                        $no++;
				  ?>
                  <tr>
                      <td align="center"><?php echo $no; ?></td>
                      <td><?php echo $d->category_name ?></td>
                      <td>
                          <button
                              href="javascript:;" 
                              type="button" 
                              class="btn btn-sm btn-primary"
                              data-uuid_filing_category="<?php echo $d->uuid_filing_category ?>"
                              data-category_name="<?php echo $d->category_name ?>"
                              data-toggle="modal" data-target="#editFilingCategoryModal">
                              <i class="fas fa-edit"></i> Edit
                          </button>
                          <a href="<?php echo base_url();?>index.php/e_filing/hr/deleteFilingCategory/<?php echo $d->uuid_filing_category ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
              		  </td>
                  </tr>
                  <?php } ?>
                  </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Filing Category START -->
    <div class="modal fade" id="addFilingCategoryModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add New Filing Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/add_filingCategory">
                        <div class="form-group">
                            <label for="category_name">Filing Category</label>
                            <input 
                                onkeyup="this.value = this.value.toUpperCase()"
                                id="category_name" 
                                name="category_name"
                                type="text" 
                                class="form-control post_variable"  
                                placeholder="Category Name" 
                                required>
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add Filing Category END -->

    <!-- MODAL Filing category START -->
    <div class="modal fade" id="editFilingCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editFilingCategoryModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editFilingCategoryModalTitle"><i class="fas fa-file-signature"></i>Edit Filing Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_filing/hr/edit_filingCategory">
                        <div class="form-group">
                            <label for="category_name">Filing Category</label>
                            <input type="hidden" name="uuid_filing_category" id="uuid_filing_category">
                            <input 
                                onkeyup="this.value = this.value.toUpperCase()"
                                id="category_name" 
                                name="category_name" 
                                type="text" 
                                class="form-control post_variable"
                                required>
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Filing Category END -->
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );

       $('#editFilingCategoryModal').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)
              // Isi nilai pada field
              modal.find('#uuid_filing_category').attr("value",div.data('uuid_filing_category'));
              modal.find('#category_name').attr("value",div.data('category_name'));
          });
    </script>
    