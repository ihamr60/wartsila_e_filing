<!-- Page Content  -->
<div id="content">

    <div class="page-head">
        <h2 class="page-head-title">Live CV (Curriculum Vitae)</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/e_filing/hr">Employee Data</a></li>
                <li class="breadcrumb-item">Live CV</li>
            </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal -->
            <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                <li>
                    <a data-toggle="tab" href="#panel_overview">
                        Personal Data
                    </a>
                </li>

                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_organisasi">
                        Organization
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_kerja">
                        Work Experience
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_pengalaman_pendidikan">
                        Education
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_kompetensi">
                        Certification
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_bahasa">
                        Language
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_keluarga">
                        Family
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_kursus">
                        Training
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_prestasi">
                        Achievement
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_referensi">
                        Reference
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#panel_e-filing">
                        E-Filing & Others
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <?php echo $this->session->flashdata('info'); ?>
    <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-sm-12">
                <div class="tabbable">
                    <div class="tab-content">
                        <div id="panel_overview" class="tab-pane <?php echo $this->session->flashdata('person-tab-active') ?>">
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;GENERAL INFORMATION</div>
                                <div class="card-body">
                                    <div class="row">
                                          
                                        <div class="col-md-4">
                                          <form action="<?php echo base_url();?>index.php/e_filing/employee/updatePhotoPersonal" method="post" enctype="multipart/form-data">
                                              <div class="card card-header-custom"> 
                                                  <div align="center" class="pt-3"> 
                                                      <div class="pt-3">
                                                              <img style="width: 150; height: 250px; border: 0px #000 solid; border-radius: 6%;" 	src="<?php echo base_url();?>upload/photo/<?php echo $employee['photo'] ?>">
                                                          <input class="pt-2" align="center" type="file" name="photo">
                                                          <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                                          <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                                          <input type="hidden" name="PersNo" value="<?php echo $employee['PersNo'] ?>">
                                                          <button type="submit" class="btn btn-success"><i class="fas fa-plus"></i> Add Photo</button>
                                                          <hr>
                                                      </div>
                                                  </div>
                                              </div>
                                            </form>
                                        </div>

                                            <!-- Personal Information -->
                                            <div class="col-md-8">
                                        <form action="<?php echo base_url();?>index.php/e_filing/employee/updateDataPersonal" method="post" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card card-header-custom"> 
                                                            <div class="col-md-12">
                                                               <h4><img src='<?php echo base_url()?>/assets/images/person.png'> <b>Personal Information</b></h4>
                                                               <hr>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="First_name" class="control-label">
                                                                       First Name 
                                                                    </label>   			
                                                                    <input
                                                                    onkeyup="this.value = this.value.toUpperCase()" 
                                                                    readonly
                                                                    id="First_name"
                                                                    name="First_name"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="First Name" 
                                                                    value="<?php echo $employee['First_name'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="PersNo" class="control-label">
                                                                       PersNo 
                                                                    </label>             
                                                                    <input 
                                                                    onkeyup="this.value = this.value.toUpperCase()"
                                                                    readonly
                                                                    id="PersNo"
                                                                    name="PersNo"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="Personal Number" 
                                                                    value="<?php echo $employee['PersNo'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="gender" class="control-label">
                                                                       Sex 
                                                                    </label>             
                                                                    <select
                                                                        id="gender"
                                                                        name="gender"
                                                                        class="form-control post_variable"
                                                                        type="text" 
                                                                        placeholder="Sex" 
                                                                        required>
                                                                        <?php 
                                                                           if($employee['gender']=='MALE')
                                                                           {   
                                                                            echo 
                                                                            '<option selected value="MALE">Male</option>
                                                                            <option value="FEMALE">Female</option>';
                                                                            }
                                                                            else if($employee['gender']=='FEMALE')
                                                                            {
                                                                                echo 
                                                                                '<option value="MALE">Male</option>
                                                                                <option selected value="FEMALE">Female</option>';
                                                                            }
                                                                            else
                                                                                echo
                                                                            '<option value="MALE">Male</option>
                                                                            <option value="FEMALE">Female</option>'
                                                                        ?>
                                                                    </select>
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="Last_name" class="control-label">
                                                                       Last Name 
                                                                   </label>             
                                                                   <input 
                                                                  onkeyup="this.value = this.value.toUpperCase()"
                                                                   readonly 
                                                                   id="Last_name"
                                                                   name="Last_name"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   placeholder="Last Name" 
                                                                   value="<?php echo $employee['Last_name'] ?>"
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="Birthdate" class="control-label">
                                                                       Birthdate 
                                                                   </label>             
                                                                   <input
                                                                   id="Birthdate"
                                                                   name="Birthdate"
                                                                   class="form-control post_variable input-tanggal"
                                                                   type="text" 
                                                                   placeholder="Birthdate" 
                                                                   data-date-format="yyyy-mm-dd"
                                                                   value="<?php echo $employee['Birthdate'] ?>"
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="religion" class="control-label">
                                                                       Religion 
                                                                   </label>             
                                                                   <input 
                                                                   onkeyup="this.value = this.value.toUpperCase()"
                                                                   id="religion"
                                                                   name="religion"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   value="<?php echo $employee['religion'] ?>" 
                                                                   placeholder="Your Religion" 
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div> 

                                            <div class="col-md-12 pt-2">
                                                <div class="card card-header-custom"> 
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="height" class="control-label">
                                                                Height 
                                                            </label>             
                                                            <input 
                                                            onkeyup="this.value = this.value.toUpperCase()"
                                                            id="height"
                                                            name="height"
                                                            class="form-control post_variable"
                                                            type="text" 
                                                            placeholder="Ex: 168 CM" 
                                                            value="<?php echo $employee['height'] ?>" 
                                                            required>   
                                                            <div class="invalid-feedback"></div>
                                                            <div class="valid-feedback"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="tribe" class="control-label">
                                                                Tribe 
                                                            </label>             
                                                            <input 
                                                            onkeyup="this.value = this.value.toUpperCase()"
                                                            id="tribe"
                                                            name="tribe"
                                                            class="form-control post_variable"
                                                            type="text" 
                                                            placeholder="Ex: JAWA" 
                                                            value="<?php echo $employee['tribe'] ?>" 
                                                            required>   
                                                            <div class="invalid-feedback"></div>
                                                            <div class="valid-feedback"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="passion" class="control-label">
                                                               Your Passion
                                                           </label>
                                                           <input 
                                                            onkeyup="this.value = this.value.toUpperCase()"
                                                            id="passion"
                                                            name="passion"
                                                            class="form-control post_variable"
                                                            type="text" 
                                                            placeholder="Ex: Web Delopment" 
                                                            value="<?php echo $employee['passion'] ?>" 
                                                            required>   
                                                            <div class="invalid-feedback"></div>
                                                            <div class="valid-feedback"></div>
                                                       </div>
                                                       <div class="form-group">
                                                           <label for="marital_stats" class="control-label">
                                                               Marital Status
                                                           </label>
                                                            <select
                                                              id="marital_stats"
                                                              name="marital_stats"
                                                              class="form-control post_variable"
                                                              type="text"  
                                                              required>
                                                              <?php 
                                                                 if($employee['marital_stats']=='SINGLE PARENT')
                                                                 {   
                                                                  echo 
                                                                  '<option value="MARRIED">MARRIED</option>
                                                                  <option value="SINGLE">SINGLE</option>
                                                                  <option selected value="SINGLE PARENT">SINGLE PARENT</option>';
                                                                  }
                                                                  else if($employee['marital_stats']=='SINGLE')
                                                                  {
                                                                      echo 
                                                                      '<option value="MARRIED">MARRIED</option>
                                                                      <option selected value="SINGLE">SINGLE</option>
                                                                      <option value="SINGLE PARENT">SINGLE PARENT</option>';
                                                                  }
                                                                  else if($employee['marital_stats']=='MARRIED')
                                                                  {
                                                                      echo 
                                                                      '<option selected value="MARRIED">MARRIED</option>
                                                                      <option value="SINGLE">SINGLE</option>
                                                                      <option value="SINGLE PARENT">SINGLE PARENT</option>';
                                                                  }
                                                                  else
                                                                      echo
                                                                      '<option value="MARRIED">MARRIED</option>
                                                                      <option value="SINGLE">SINGLE</option>
                                                                      <option value="SINGLE PARENT">SINGLE PARENT</option>'
                                                              ?>
                                                            </select>
                                                            <div class="invalid-feedback"></div>
                                                            <div class="valid-feedback"></div>
                                                       </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                           <label for="weight" class="control-label">
                                                               Weight 
                                                           </label>             
                                                           <input 
                                                           onkeyup="this.value = this.value.toUpperCase()"
                                                           id="weight"
                                                           name="weight"
                                                           class="form-control post_variable"
                                                           type="text" 
                                                           placeholder="Ex: 55 KG" 
                                                           value="<?php echo $employee['weight'] ?>" 
                                                           required>   
                                                           <div class="invalid-feedback"></div>
                                                           <div class="valid-feedback"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="blood_type" class="control-label">
                                                                Blood Type 
                                                            </label>             
                                                            <select
                                                            id="blood_type"
                                                            name="blood_type"
                                                            class="form-control post_variable"
                                                            type="text" 
                                                            required> 
                                                            <option selected value="-">-</option>
                                                            <?php 
                                                                if($employee['blood_type']=="O")
                                                                {
                                                                    echo  
                                                                    '<option selected value="O">O</option>
                                                                    <option value="A">A</option>
                                                                    <option value="B">B</option>
                                                                    <option value="AB">AB</option>';
                                                                }
                                                                else if ($employee['blood_type']=="A") {
                                                                    echo  
                                                                    '<option value="O">O</option>
                                                                    <option selected value="A">A</option>
                                                                    <option value="B">B</option>
                                                                    <option value="AB">AB</option>';
                                                                }
                                                                else if ($employee['blood_type']=="B") {
                                                                    echo  
                                                                    '<option value="O">O</option>
                                                                    <option value="A">A</option>
                                                                    <option selected value="B">B</option>
                                                                    <option value="AB">AB</option>';
                                                                }
                                                                else if ($employee['blood_type']=="AB") {
                                                                    echo  
                                                                    '<option value="O">O</option>
                                                                    <option value="A">A</option>
                                                                    <option value="B">B</option>
                                                                    <option selected value="AB">AB</option>';
                                                                }
                                                                else
                                                                {
                                                                    echo 
                                                                    '<option value="O">O</option>
                                                                    <option value="A">A</option>
                                                                    <option value="B">B</option>
                                                                    <option value="AB">AB</option>';
                                                                }
                                                            ?>
                                                            </select>   
                                                            <div class="invalid-feedback"></div>
                                                            <div class="valid-feedback"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="Nationality" class="control-label">
                                                                Nationality
                                                            </label>
                                                            <input 
                                                             onkeyup="this.value = this.value.toUpperCase()"
                                                             id="Nationality"
                                                             name="Nationality"
                                                             class="form-control post_variable"
                                                             type="text" 
                                                             placeholder="Ex: INDONESIAN" 
                                                             value="<?php echo $employee['Nationality'] ?>" 
                                                             required>   
                                                             <div class="invalid-feedback"></div>
                                                             <div class="valid-feedback"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Address information -->
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card card-header-custom"> 
                                                            <div class="col-md-12 pt-4">
                                                               <h4><img src='<?php echo base_url()?>/assets/images/loc.png'> <b>Address Information</b></h4>
                                                               <hr>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="address_current" class="control-label">
                                                                       Current Address 
                                                                    </label>        
                                                                    <input 
                                                                    onkeyup="this.value = this.value.toUpperCase()"
                                                                    id="address_current"
                                                                    name="address_current"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="Current Address" 
                                                                    value="<?php echo $employee['address_current'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="city_current" class="control-label">
                                                                       Current City 
                                                                    </label> 
                                                                    <select
                                                                    id="city_current"
                                                                    name="city_current"
                                                                    class="form-control post_variable"
                                                                    required>
                                                                    <option selected value="-">-</option>
                                                                    <?php 
                                                                        if($employee['city_current']==$employee['city_current'])
                                                                        { 
                                                                          echo '<option selected value="'.$employee['city_current'].'">'.$employee['city_current'].'</option>';
                                                                          
                                                                          foreach($data_city as $d)
                                                                          {
                                                                            if($d->city_name!=$employee['city_current'])
                                                                            {
                                                                              echo '<option value="'.$d->city_name.'">'.$d->city_name.'</option>';
                                                                            }
                                                                          }
                                                                        } 
                                                                    ?>
                                                                    </select>            
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="province_current" class="control-label">
                                                                       Current Province 
                                                                    </label>             
                                                                    <select
                                                                    id="province_current"
                                                                    name="province_current"
                                                                    class="form-control post_variable"
                                                                    required>
                                                                    <option selected value="-">-</option>
                                                                    <?php 
                                                                        if($employee['province_current']==$employee['province_current'])
                                                                        { 
                                                                          echo '<option selected value="'.$employee['province_current'].'">'.$employee['province_current'].'</option>';
                                                                          
                                                                          foreach($data_province as $d)
                                                                          {
                                                                            if($d->province_name!=$employee['province_current'])
                                                                            {
                                                                              echo '<option value="'.$d->province_name.'">'.$d->province_name.'</option>';
                                                                            }
                                                                          }
                                                                        } 
                                                                    ?>
                                                                    </select>
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="address_permanent" class="control-label">
                                                                       Permanent Address 
                                                                   </label>             
                                                                   <input
                                                                   onkeyup="this.value = this.value.toUpperCase()" 
                                                                   id="address_permanent"
                                                                   name="address_permanent"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   placeholder="Permanent Address" 
                                                                   value="<?php echo $employee['address_permanent'] ?>"
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="city_permanent" class="control-label">
                                                                       Permanent City 
                                                                   </label>             
                                                                   <select
                                                                    id="city_permanent"
                                                                    name="city_permanent"
                                                                    class="form-control post_variable"
                                                                    required>
                                                                    <option selected value="-">-</option>
                                                                    <?php 
                                                                        if($employee['city_permanent']==$employee['city_permanent'])
                                                                        { 
                                                                          echo '<option selected value="'.$employee['city_permanent'].'">'.$employee['city_permanent'].'</option>';

                                                                          foreach($data_city as $d)
                                                                          {
                                                                            if($d->city_name!=$employee['city_permanent'])
                                                                            {
                                                                              echo '<option value="'.$d->city_name.'">'.$d->city_name.'</option>';
                                                                            }
                                                                          }
                                                                        } 
                                                                    ?>
                                                                    </select>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="province_permanent" class="control-label">
                                                                       Permanent Province 
                                                                   </label>             
                                                                   <select
                                                                    id="province_permanent"
                                                                    name="province_permanent"
                                                                    class="form-control post_variable"
                                                                    required>
                                                                    <option selected value="-">-</option>
                                                                    <?php 
                                                                        if($employee['province_permanent']==$employee['province_permanent'])
                                                                        { 
                                                                          echo '<option selected value="'.$employee['province_permanent'].'">'.$employee['province_permanent'].'</option>';
                                                                          
                                                                          foreach($data_province as $d)
                                                                          {
                                                                            if($d->province_name!=$employee['province_permanent'])
                                                                            {
                                                                              echo '<option value="'.$d->province_name.'">'.$d->province_name.'</option>';
                                                                            }
                                                                          }
                                                                        } 
                                                                    ?>
                                                                    </select>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>

                                            <!-- card number -->
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card card-header-custom"> 
                                                            <div class="col-md-12 pt-4">
                                                               <h4><img src='<?php echo base_url()?>/assets/images/card.png'> <b>Card Number Information</b></h4>
                                                               <hr>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="no_ktp" class="control-label">
                                                                       KTP 
                                                                    </label>        
                                                                    <input 
                                                                    id="no_ktp"
                                                                    name="no_ktp"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="KTP Card Number" 
                                                                    value="<?php echo $employee['no_ktp'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="npwp" class="control-label">
                                                                        NPWP 
                                                                    </label>             
                                                                    <input 
                                                                    id="npwp"
                                                                    name="npwp"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="NPWP Card Number" 
                                                                    value="<?php echo $employee['npwp'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="bpjs_kes" class="control-label">
                                                                       BPJS Kesehatan 
                                                                    </label>             
                                                                    <input
                                                                    id="bpjs_kes"
                                                                    name="bpjs_kes"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="BPJS Kesehatan Card Number" 
                                                                    value="<?php echo $employee['bpjs_kes'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="bpjs_naker" class="control-label">
                                                                       BPJS Ketenagakerjaan 
                                                                    </label>             
                                                                    <input
                                                                    id="bpjs_naker"
                                                                    name="bpjs_naker"
                                                                    class="form-control post_variable"
                                                                    type="text" 
                                                                    placeholder="BPJS Ketenagakerjaan Card Number" 
                                                                    value="<?php echo $employee['bpjs_naker'] ?>"
                                                                    required>   
                                                                    <div class="invalid-feedback"></div>
                                                                    <div class="valid-feedback"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                   <label for="passport" class="control-label">
                                                                       Passport 
                                                                   </label>             
                                                                   <input 
                                                                   id="passport"
                                                                   name="passport"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   placeholder="Passport Card Number" 
                                                                   value="<?php echo $employee['passport'] ?>"
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="no_kk" class="control-label">
                                                                       KK 
                                                                   </label>             
                                                                   <input
                                                                   id="no_kk"
                                                                   name="no_kk"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   placeholder="KK Number" 
                                                                   value="<?php echo $employee['no_kk'] ?>"
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="dplk" class="control-label">
                                                                       DPLK 
                                                                   </label>             
                                                                   <input 
                                                                   id="dplk"
                                                                   name="dplk"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   value="<?php echo $employee['dplk'] ?>" 
                                                                   placeholder="DPLK Card Number" 
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                   <label for="jamsostek" class="control-label">
                                                                       Jamsostek 
                                                                   </label>             
                                                                   <input 
                                                                   id="jamsostek"
                                                                   name="jamsostek"
                                                                   class="form-control post_variable"
                                                                   type="text" 
                                                                   value="<?php echo $employee['jamsostek'] ?>" 
                                                                   placeholder="Jamsostek Card Number" 
                                                                   required>   
                                                                   <div class="invalid-feedback"></div>
                                                                   <div class="valid-feedback"></div>
                                                                </div>
                                                           </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="col-md-12 pt-4">
                                              <hr>
                                              <button type="submit" class="btn btn-success btn-md col-md-12"><i class="fas fa-file-signature"></i> SUBMIT</button>
                                            </div>
                                        </form>                          
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PANEL PENGALAMAN ORGANISASI -->
                        <div id="panel_pengalaman_organisasi" class="tab-pane <?php echo $this->session->flashdata('organiz-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewOrganizationModal">
                                        <i class="fas fa-plus"></i> Create a new Organization
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF ORGANIZATION EXPERIENCE</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example10">
                                        <thead>
                                            <tr> 
                                                <th>ORGANIZATION</th>
                                                <th>POSITION</th>
                                                <th>START YEAR</th>
                                                <th>STOP YEAR</th>
                                                <th width="150px">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($organization as $d) { ?>   
                                            <tr> 
                                                <td><?php echo $d->organisasi ?></td>
                                                <td><?php echo $d->jabatan ?></td>
                                                <td><?php echo $d->thn_mulai ?></td>
                                                <td><?php echo $d->thn_berakhir ?></td>
                                                <td align="center">
                                                  <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_organisasi="<?php echo $d->id_organisasi ?>"
                                                      data-organisasi="<?php echo $d->organisasi ?>"
                                                      data-jabatan="<?php echo $d->jabatan ?>"
                                                      data-thn_mulai="<?php echo $d->thn_mulai ?>"
                                                      data-thn_berakhir="<?php echo $d->thn_berakhir ?>"
                                                      data-toggle="modal" data-target="#editOrganization">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusOrganization/<?php echo $d->id_organisasi ?>" onclick="return confirm('Anda yakin akan menghapus data organisasi ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL PENGALAMAN ORGANISASI -->

                        <!-- START PANEL PENGALAMAN PENDIDIKAN -->
                        <div id="panel_pengalaman_pendidikan" class="tab-pane <?php echo $this->session->flashdata('edu-tab-active')?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewEducationModal">
                                        <i class="fas fa-plus"></i> Create a new education
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF EDUCATION HISTORY</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example2">
                                        <thead>
                                            <tr>     
                                                <th>INSTITUTION</th>
                                                <th>MAJORS</th>
                                                <th>IPK/GPA</th>
                                                <th>STAR YEAR</th>
                                                <th>GRADUATION YEAR</th>
                                                <th>DIPLOMA NUMBER</th>
                                                <th width="150px">ACTION</th>            
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($education as $d){ ?>
                                            <tr>    
                                                <td><?php echo $d->institusi ?></td>
                                                <td><?php echo $d->jurusan ?></td>
                                                <td><?php echo $d->ipk ?></td>
                                                <td><?php echo $d->thn_masuk ?></td>
                                                <td><?php echo $d->thn_lulus ?></td>
                                                <td><?php echo $d->no_ijazah ?></td>
                                                <td align="center">
                                                  <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_education="<?php echo $d->id_education ?>"
                                                      data-institusi="<?php echo $d->institusi ?>"
                                                      data-no_ijazah="<?php echo $d->no_ijazah ?>"
                                                      data-jurusan="<?php echo $d->jurusan ?>"
                                                      data-ipk="<?php echo $d->ipk ?>"
                                                      data-thn_masuk="<?php echo $d->thn_masuk ?>"
                                                      data-thn_lulus="<?php echo $d->thn_lulus ?>"
                                                      data-toggle="modal" data-target="#editEducation">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusEducation/<?php echo $d->id_education ?>" onclick="return confirm('Anda yakin akan menghapus data Pendidikan ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td>    
                                            </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PENDIDIKAN -->

                        <!-- START PANEL PENGALAMAN Kerja -->
                        <div id="panel_pengalaman_kerja" class="tab-pane <?php echo $this->session->flashdata('work-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewWorkExModal">
                                        <i class="fas fa-plus"></i> Create a new job experience
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF WORK EXPERIENCE</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example3">
                                        <thead>
                                            <tr>               
                                                <th>COMPANY</th>
                                                <th>POSITION</th>
                                                <th>START WORKING</th>
                                                <th>STOP WORKING</th>
                                                <th width="150px">ACTION</th>       
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach($job_ex as $d){ ?>
                                            <tr>
                                                <td><?php echo $d->perusahaan ?></td>
                                                <td><?php echo $d->jabatan ?></td>
                                                <td><?php echo $d->thn_mulai ?></td>
                                                <td><?php echo $d->thn_berakhir ?></td>
                                                <td align="center">
                                                  <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_job="<?php echo $d->id_job ?>"
                                                      data-perusahaan="<?php echo $d->perusahaan ?>"
                                                      data-jabatan="<?php echo $d->jabatan ?>"
                                                      data-thn_mulai="<?php echo $d->thn_mulai ?>"
                                                      data-thn_berakhir="<?php echo $d->thn_berakhir ?>"
                                                      data-toggle="modal" data-target="#editWorkEx">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusWorkEx/<?php echo $d->id_job ?>" onclick="return confirm('Anda yakin akan menghapus data pekerjaan ini?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr> 
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END Pengalaman Kerja -->

                        <!-- START PANEL Kompetensi -->
                        <div id="panel_kompetensi" class="tab-pane <?php echo $this->session->flashdata('cert-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewCertificationModal">
                                        <i class="fas fa-plus"></i> Create a new certification
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF COMPETENCE / CERTIFICATION</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example4">
                                        <thead>
                                            <tr>                  
                                                <th>COMPETENCE</th>
                                                <th>LEVEL</th>
                                                <th width="150px">ACTION</th>                                     
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach($competence as$d){ ?>
                                            <tr>
                                                <td><?php echo $d->kompetensi ?></td> 
                                                <td><?php echo $d->level ?></td> 
                                                <td align="center">
                                                <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_competence="<?php echo $d->id_competence ?>"
                                                      data-kompetensi="<?php echo $d->kompetensi ?>"
                                                      data-level="<?php echo $d->level ?>"
                                                      data-toggle="modal" data-target="#editCertification">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusCompetence/<?php echo $d->id_competence ?>" onclick="return confirm('Anda yakin akan menghapus data Kompetensi/Sertifikasi ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td>  
                                            </tr>  
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END Kompetensi -->

                        <!-- PANEL BAHASA -->
                        <div id="panel_bahasa" class="tab-pane <?php echo $this->session->flashdata('lang-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewLanguageModal">
                                        <i class="fas fa-plus"></i> Create a new language
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF LANGUAGE MASTERED</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example5">
                                        <thead>
                                            <tr>
                                                <th>LANGUAGE</th>
                                                <th>LEVEL</th>
                                                <th width="150px">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                              foreach($language as $d)
                                              {
                                          ?>
                                            <tr> 
                                                <td><?php echo $d->bahasa ?></td>      
                                                <td><?php echo $d->level ?></td> 
                                                <td align="center">
                                                    <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_language="<?php echo $d->id_language ?>"
                                                      data-bahasa="<?php echo $d->bahasa ?>"
                                                      data-level="<?php echo $d->level ?>"
                                                      data-toggle="modal" data-target="#editLanguage">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusLanguage/<?php echo $d->id_language ?>" onclick="return confirm('Anda yakin akan menghapus data Bahasa ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td> 
                                            </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL BAHASA -->

                        <!-- PANEL KELUARGA -->
                        <div id="panel_keluarga" class="tab-pane <?php echo $this->session->flashdata('fam-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewFamilyModal">
                                        <i class="fas fa-plus"></i> Create a new family
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF MY FAMILY</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example6">
                                        <thead>
                                            <tr>
                                                <th>FULLNAME</th>
                                                <th>CONTACT</th>
                                                <th>BIRTHDATE</th>
                                                <th>RELATION</th>
                                                <th>JOB</th>  
                                                <th width="130px">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                              foreach($family as $d)
                                              {
                                          ?>
                                            <tr>   
                                                <td><?php echo $d->nama_sdr ?></td>                
                                                <td><?php echo $d->no_hp ?></td>
                                                <td><?php echo date('d F Y', strtotime($d->tgl_lhr_sdr)) ?></td>
                                                <td><?php echo $d->hubungan ?></td>
                                                <td><?php echo $d->pekerjaan ?></td>  
                                                <td>
                                                    <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_family="<?php echo $d->id_family ?>"
                                                      data-nama_sdr="<?php echo $d->nama_sdr ?>"
                                                      data-tgl_lhr_sdr="<?php echo $d->tgl_lhr_sdr ?>"
                                                      data-hubungan="<?php echo $d->hubungan ?>"
                                                      data-pekerjaan="<?php echo $d->pekerjaan ?>"
                                                      data-no_hp="<?php echo $d->no_hp ?>"
                                                      data-toggle="modal" data-target="#editFamily">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusFamily/<?php echo $d->id_family ?>" onclick="return confirm('Anda yakin akan menghapus data Keluarga ini?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                                </td>      
                                            </tr>              
                                            <?php } ?>        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 
                        <!-- END PANEL KELUARGA -->

                        <!-- PANEL KURSUS -->
                        <div id="panel_kursus" class="tab-pane <?php echo $this->session->flashdata('training-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewTrainingModal">
                                        <i class="fas fa-plus"></i> Create a new training
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF TRAINING</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example7">
                                        <thead>
                                            <tr> 
                                                <th>COURSE NAME</th>
                                                <th>COURSE SITES</th>
                                                <th>START COURSE</th>
                                                <th>FINISH COURSE</th>
                                                <th>ORGANIZER</th>
                                                <th width="150px">ACTION</th>         
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                              foreach($training as $d)
                                              {
                                          ?>
                                            <tr> 
                                                <td><?php echo $d->training ?></td>
                                                <td><?php echo $d->tmpt_training ?></td>
                                                <td><?php echo $d->mulai_training ?></td>
                                                <td><?php echo $d->selesai_training ?></td>
                                                <td><?php echo $d->penyelenggara ?></td>
                                                <td align="center">
                                                    <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_training="<?php echo $d->id_training ?>"
                                                      data-training="<?php echo $d->training ?>"
                                                      data-tmpt_training="<?php echo $d->tmpt_training ?>"
                                                      data-mulai_training="<?php echo $d->mulai_training ?>"
                                                      data-selesai_training="<?php echo $d->selesai_training ?>"
                                                      data-penyelenggara="<?php echo $d->penyelenggara ?>"
                                                      data-toggle="modal" data-target="#editTraining">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusTraining/<?php echo $d->id_training ?>" onclick="return confirm('Anda yakin akan menghapus data Training ini?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                            <?php } ?>  
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL KURSUS -->

                        <!-- PANEL PRESTASI -->
                        <div id="panel_prestasi" class="tab-pane <?php echo $this->session->flashdata('achiev-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewAchievementModal">
                                        <i class="fas fa-plus"></i> Create a new achievement
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF ACHIEVEMENT</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example8">
                                        <thead>
                                            <tr>      
                                                <th>ACHIEVEMENT</th>
                                                <th>YEAR</th>
                                                <th>ORGANIZER/APPRECIATOR</th>
                                                <th width="150px">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                              foreach($achievement as $d)
                                              {
                                          ?>
                                            <tr>
                                                <td><?php echo $d->achievement ?></td>
                                                <td><?php echo $d->year ?></td>
                                                <td><?php echo $d->organizer ?></td>
                                                <td align="center">  
                                                  <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_achievement="<?php echo $d->id_achievement ?>"
                                                      data-achievement="<?php echo $d->achievement ?>"
                                                      data-year="<?php echo $d->year ?>"
                                                      data-organizer="<?php echo $d->organizer ?>"
                                                      data-toggle="modal" data-target="#editAchievement">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusAchievement/<?php echo $d->id_achievement ?>" onclick="return confirm('Anda yakin akan menghapus data Prestasi ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                            <?php } ?>                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL PRESTASIs -->

                        <!-- PANEL REFERENSI -->
                        <div id="panel_referensi" class="tab-pane <?php echo $this->session->flashdata('reff-tab-active') ?>">
                            <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewRefferenceModal">
                                        <i class="fas fa-plus"></i> Create a new refference
                                    </button>
                                </div>
                            </div> 
                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF REFFERENCE</div>
                                <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="example9">
                                        <thead>
                                            <tr>
                                                <th>FULLNAME</th>
                                                <th>RELATION</th>
                                                <th>NUMBER HANDPHONE</th>
                                                <th>E-MAIL</th> 
                                                <th width="150px">ACTION</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                              foreach($refference as $d)
                                              {
                                          ?>
                                            <tr>  
                                                <td><?php echo $d->fullname ?></td>
                                                <td><?php echo $d->relation ?></td>
                                                <td><?php echo $d->no_hp ?></td>
                                                <td><?php echo $d->email ?></td>
                                                <td align="center">
                                                    <button
                                                      href="javascript:;" 
                                                      type="button" 
                                                      class="btn btn-sm btn-dark"
                                                      data-id_refference="<?php echo $d->id_refference ?>"
                                                      data-fullname="<?php echo $d->fullname ?>"
                                                      data-relation="<?php echo $d->relation ?>"
                                                      data-no_hp="<?php echo $d->no_hp ?>"
                                                      data-email="<?php echo $d->email ?>"
                                                      data-toggle="modal" data-target="#editRefference">
                                                      <i class="fas fa-edit"></i> Edit
                                                  </button>
                                                  <a href="<?php echo base_url();?>index.php/e_filing/employee/hapusRefference/<?php echo $d->id_refference ?>" onclick="return confirm('Anda yakin akan menghapus data Referens ini?')" class="btn btn-danger btn-sm" ><i class="fas fa-trash"></i> Delete</a>
                                                </td>
                                            </tr>
                                            <?php } ?> 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL REFERENSI -->

                        <!-- PANEL E-FILING -->
                        <div id="panel_e-filing" class="tab-pane <?php echo $this->session->flashdata('filling-tab-active') ?>">
                           <!-- Action Card -->
                            <div class="card">
                                <div class="card-body">
                                    <!-- Button trigger modal  -->
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addNewFilingModal">
                                        <i class="fas fa-plus"></i> Create a new E-Filing
                                    </button>
                                </div>
                            </div> 

                            <div class="card card-header-custom">
                                <div class="card-header"><i class="fas fa-list"></i>&#8195;LIST OF E-FILING</div>
                                <div class="card-body">
                                  <div class='alert alert-warning'>
                                                            <strong>
                                                               WARNING! -
                                                            </strong>
                                                            <font color="red">*</font>Please upload all required files
                                                        </div>
                                    <table class="table table-striped table-bordered table-hover" id="example">
                                        <thead>
                                            <tr>
                                                <th width="10px">NO</th>
                                                <th>FILE NAME</th>
                                                <th width="180px">ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                              $no=0;
                                              foreach($data_file_req as $d)
                                              {
                                                  $no++;
                                          ?>
                                          <tr>
                                              <td align="center"><?php echo $no; ?></td>
                                              <td>
                                                  <?php 
                                                      echo $d->required_file_name.'<font color="red">*</font> <span class="badge badge-warning">'.$d->category_name.'</span>'; 

                                                      if(empty($filing_req))
                                                      {
                                                          echo '&#8195;<img src="'.base_url().'/assets/images/false.png">';   
                                                      }
                                                      else
                                                      {
                                                        foreach($filing_req as $a)
                                                        { 
                                                            $cek   = $this->web_app_model->getWhere2($this->session->user_id,'PersNo_req',$d->uuid_employee_required_file,'uuid_employee_required_file','tbl_filing_required');

                                                            if (count($cek) > 0) {
                                                                echo '&#8195;<img src="'.base_url().'/assets/images/checklist.png">';
                                                                break;
                                                            }
                                                            else
                                                            {
                                                                echo '&#8195;<img src="'.base_url().'/assets/images/false.png">';
                                                                break;
                                                            }
                                                        }
                                                      }
                                                  ?>
                                              </td>
                                              <td align="center">
                                                  <?php 
                                                    if(empty($filing_req))
                                                    {
                                                      echo'<a class="btn btn-warning btn-sm">Not&#8195;Found</a>
                                                             <a 
                                                              href="javascript:;" 
                                                              class="btn btn-sm btn-primary"
                                                              data-file_name_req="'.$d->required_file_name.'"
                                                              data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                              data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                              data-toggle="modal" data-target="#uploadFile_add">
                                                             <i class="fa fa-upload"></i> Upload </a>
                                                             ';
                                                    }
                                                    else
                                                    {
                                                      foreach($filing_req as $a)
                                                      { 
                                                        $cek   = $this->web_app_model->getWhere2($this->session->user_id,'PersNo_req',$d->uuid_employee_required_file,'uuid_employee_required_file','tbl_filing_required');

                                                        if (count($cek) > 0) 
                                                        {
                                                          if($a->PersNo_req==$this->session->user_id && $a->uuid_employee_required_file==$d->uuid_employee_required_file)
                                                          {
                                                            echo'<a href="'.base_url().'upload/others/'.$a->attach_req.'" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> View Doc</a>
                                                                 <a 
                                                                  href="javascript:;" 
                                                                  class="btn btn-sm btn-info"
                                                                  data-uuid_filing_req="'.$a->uuid_filing_req.'"
                                                                  data-file_name_req="'.$d->required_file_name.'"
                                                                  data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                                  data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                                  data-toggle="modal" data-target="#uploadFile_update">
                                                                 <i class="fas fa-upload"></i> Change </a>';
                                                            break;
                                                          }
                                                        }
                                                        else
                                                        {
                                                          echo'<a class="btn btn-warning btn-sm">Not&#8195;Found</a>
                                                               <a 
                                                                href="javascript:;" 
                                                                class="btn btn-sm btn-primary"
                                                                data-uuid_filing_req="'.$a->uuid_filing_req.'"
                                                                data-file_name_req="'.$d->required_file_name.'"
                                                                data-uuid_employee_required_file="'.$d->uuid_employee_required_file.'"
                                                                data-uuid_filing_category="'.$d->uuid_filing_category.'"
                                                                data-toggle="modal" data-target="#uploadFile_add">
                                                               <i class="fa fa-upload"></i> Upload </a>
                                                               ';
                                                          break;
                                                        }
                                                      }
                                                    }
                                                  ?>
                                              </td>    
                                          </tr>
                                          <?php } ?>
                                          <?php 
                                            foreach($data_file as $d)
                                            {
                                              $no++;
                                          ?>
                                            <tr>
                                                <td align="center"><?php echo $no;?></td>
                                                <td><?php echo $d->file_name.' <span class="badge badge-warning">'.$d->category.'</span>'?></td>
                                                <td align="center">
                                                    <a href="<?php echo base_url() ?>upload/others/<?php echo $d->attachment ?>" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> View Doc</a>
                                                    <a onclick="return confirm('Anda yakin akan menghapus dokumen ini?')" href="<?php echo base_url();?>index.php/e_filing/upload/deleteDoc/<?php echo $d->uuid_filing ?>/<?php echo $d->attachment ?>" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
                                                </td>       
                                            </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END PANEL E-FILING -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end: PAGE CONTENT-->

        <!-- Modal Upload add File START -->
        <div class="modal fade" id="uploadFile_add" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fa fa-upload"></i></i>&#8195;Upload E-Filing</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/userUploadDoc_req_add" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="file_name_req">File Name Required</label>
                                <input type="hidden" value="<?php echo $employee['First_name'] ?>" name="First_name">
                                <input type="hidden" value="<?php echo $employee['Last_name'] ?>" name="Last_name">
                                <input type="hidden" value="<?php echo $employee['PersNo'] ?>" name="PersNo">
                                <input type="hidden" id="uuid_employee_required_file" name="uuid_employee_required_file">
                                <input type="hidden" id="uuid_filing_category" name="uuid_filing_category">
                                <input 
                                 readonly 
                                 id="file_name_req"
                                 name="file_name_req"
                                 class="form-control post_variable"
                                 type="text" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="attach_req">Choose File</label>
                                <input 
                                 id="attach_req"
                                 name="attach_req"
                                 class="form-control post_variable"
                                 type="file" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Upload add END -->

        <!-- Modal Upload_update File START -->
        <div class="modal fade" id="uploadFile_update" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fa fa-upload"></i></i>&#8195;Upload E-Filing</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/userUploadDoc_req_update" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="file_name_req">File Name Required</label>
                                <input type="hidden" id="uuid_filing_req" name="uuid_filing_req">
                                <input type="hidden" value="<?php echo $employee['First_name'] ?>" name="First_name">
                                <input type="hidden" value="<?php echo $employee['Last_name'] ?>" name="Last_name">
                                <input type="hidden" value="<?php echo $employee['PersNo'] ?>" name="PersNo">
                                <input type="hidden" id="uuid_employee_required_file" name="uuid_employee_required_file">
                                <input type="hidden" id="uuid_filing_category" name="uuid_filing_category">
                                <input 
                                 readonly 
                                 id="file_name_req"
                                 name="file_name_req"
                                 class="form-control post_variable"
                                 type="text" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="attach_req">Choose File</label>
                                <input 
                                 id="attach_req"
                                 name="attach_req"
                                 class="form-control post_variable"
                                 type="file" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload & Change</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Upload_update END -->

        <!-- Modal ORGANIZATION START -->
        <div class="modal fade" id="addNewOrganizationModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Organization Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addOrganizationEx" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="organisasi">Organization</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="organisasi"
                                 name="organisasi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: RTIK INDONESIA" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Position</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jabatan"
                                 name="jabatan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Ketua Divisi HUMAS" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_mulai">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_mulai"
                                 name="thn_mulai"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_berakhir">Stop Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_berakhir"
                                 name="thn_berakhir"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2018" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                    
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editOrganization" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Organization Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editOrganization" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="organisasi">Organization</label>
                                <input type="hidden" id="id_organisasi" name="id_organisasi">
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="organisasi"
                                 name="organisasi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: RTIK INDONESIA" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Position</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jabatan"
                                 name="jabatan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Ketua Divisi HUMAS" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_mulai">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_mulai"
                                 name="thn_mulai"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_berakhir">Stop Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_berakhir"
                                 name="thn_berakhir"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2018" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                  
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal ORGANIZATION END -->

        <!-- Modal Work Ex -->
        <div class="modal fade" id="addNewWorkExModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Work Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addWorkEx" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="perusahaan">Company</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="perusahaan"
                                 name="perusahaan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Company Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Position</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jabatan"
                                 name="jabatan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Position" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_mulai">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_mulai"
                                 name="thn_mulai"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_berakhir">Stop Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_berakhir"
                                 name="thn_berakhir"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2018" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                          
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editWorkEx" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Work Experience</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editWorkEx" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="perusahaan">Company</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" id="id_job" name="id_job">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="perusahaan"
                                 name="perusahaan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Company Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Position</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jabatan"
                                 name="jabatan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Position" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_mulai">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_mulai"
                                 name="thn_mulai"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_berakhir">Stop Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_berakhir"
                                 name="thn_berakhir"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2018" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                          
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Work Ex END -->

        <!-- Modal Education -->
        <div class="modal fade" id="addNewEducationModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Education</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addEducation" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="institusi">Intitution</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="institusi"
                                 name="institusi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Intitution Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jurusan">Majors</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jurusan"
                                 name="jurusan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Major Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="ipk">GPA</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="ipk"
                                 name="ipk"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: IPK/GPA Value" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_masuk">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_masuk"
                                 name="thn_masuk"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_lulus">Graduation Year</label>
                                <input
                                 id="thn_lulus"
                                 name="thn_lulus"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 2019"
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_ijazah">Diploma Cert. Number</label>
                                <input
                                 id="no_ijazah"
                                 name="no_ijazah"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: Cert Number"
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
         <div class="modal fade" id="editEducation" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Education</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editEducation" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="institusi">Intitution</label>
                                <input type="hidden" name="id_education" id="id_education">
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="institusi"
                                 name="institusi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Intitution Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="jurusan">Majors</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="jurusan"
                                 name="jurusan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Major Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="ipk">GPA</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="ipk"
                                 name="ipk"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: IPK/GPA Value" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_masuk">Start Year</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="thn_masuk"
                                 name="thn_masuk"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 2015" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="thn_lulus">Graduation Year</label>
                                <input
                                 id="thn_lulus"
                                 name="thn_lulus"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 2019"
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_ijazah">Diploma Cert. Number</label>
                                <input
                                 id="no_ijazah"
                                 name="no_ijazah"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: Cert Number"
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Educcation END -->

        <!-- Modal certification -->
        <div class="modal fade" id="addNewCertificationModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Certification & Competence</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addCertification" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="kompetensi">Certification / Competence</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="kompetensi"
                                 name="kompetensi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Name of Competence" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select 
                                 id="level"
                                 name="level"
                                 class="form-control post_variable"
                                 required>   
                                 <option value="BASIC">BASIC</option>
                                 <option value="INTERMEDIATE">INTERMEDIATE</option>
                                 <option value="EXPERT">EXPERT</option>
                                </select>
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editCertification" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Certification & Competence</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editCertification" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="kompetensi">Certification / Competence</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" name="id_competence" id="id_competence">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="kompetensi"
                                 name="kompetensi"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Name of Competence" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select 
                                 id="level"
                                 name="level"
                                 class="form-control post_variable"
                                 required>
                                 <option id="cert_option1">SELECT</option>  
                                 <option value="BASIC">BASIC</option>
                                 <option value="INTERMEDIATE">INTERMEDIATE</option>
                                 <option value="EXPERT">EXPERT</option>
                                </select>
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal certification END -->

        <!-- Modal Language -->
        <div class="modal fade" id="addNewLanguageModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Language</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addLanguage" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="bahasa">Language</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="bahasa"
                                 name="bahasa"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: English" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select 
                                 id="level"
                                 name="level"
                                 class="form-control post_variable"
                                 required>   
                                 <option value="BASIC">BASIC</option>
                                 <option value="INTERMEDIATE">INTERMEDIATE</option>
                                 <option value="EXPERT">EXPERT</option>
                                </select>
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editLanguage" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Language</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editLanguage" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="bahasa">Language</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" name="id_language" id="id_language">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="bahasa"
                                 name="bahasa"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: English" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select 
                                 id="level"
                                 name="level"
                                 class="form-control post_variable"
                                 required>   
                                 <option id="lang_option">SELECT</option>
                                 <option value="BASIC">BASIC</option>
                                 <option value="INTERMEDIATE">INTERMEDIATE</option>
                                 <option value="EXPERT">EXPERT</option>
                                </select>
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                           
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Language END -->

        <!-- Modal Family -->
        <div class="modal fade" id="addNewFamilyModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Family</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addFamily" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="nama_sdr">Fullname</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="nama_sdr"
                                 name="nama_sdr"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Your Brother/Sister Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp">Contact</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="no_hp"
                                 name="no_hp"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 6285361885100" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_lhr_sdr">Birthdate</label>
                                <input
                                 id="tgl_lhr_sdr"
                                 name="tgl_lhr_sdr"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Birthdate" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="hubungan">Relation</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="hubungan"
                                 name="hubungan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Father" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan">Job</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="pekerjaan"
                                 name="pekerjaan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Business Man" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editFamily" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Family</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editFamily" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="nama_sdr">Fullname</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" name="id_family" id="id_family">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="nama_sdr"
                                 name="nama_sdr"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Your Brother/Sister Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp">Contact</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="no_hp"
                                 name="no_hp"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: 6285361885100" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="tgl_lhr_sdr">Birthdate</label>
                                <input
                                 id="tgl_lhr_sdr"
                                 name="tgl_lhr_sdr"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Birthdate" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="hubungan">Relation</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="hubungan"
                                 name="hubungan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Father" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan">Job</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="pekerjaan"
                                 name="pekerjaan"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Business Man" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Family END -->

        <!-- Modal Training -->
        <div class="modal fade" id="addNewTrainingModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Training</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addTraining" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="training">Course Name</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="training"
                                 name="training"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Web Development" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="tmpt_training">Course Site</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="tmpt_training"
                                 name="tmpt_training"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Jakarta" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="mulai_training">Start Course</label>
                                <input
                                 id="mulai_training"
                                 name="mulai_training"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Start Course" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="selesai_training">Finish Course</label>
                                <input
                                 id="selesai_training"
                                 name="selesai_training"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Finish Course" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="penyelenggara">Organizer</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="penyelenggara"
                                 name="penyelenggara"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Dicoding Indonesia" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editTraining" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Training</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editTraining" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="training">Course Name</label>
                                <input type="hidden" name="id_training" id="id_training">
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="training"
                                 name="training"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Web Development" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="tmpt_training">Course Site</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="tmpt_training"
                                 name="tmpt_training"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Jakarta" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="mulai_training">Start Course</label>
                                <input
                                 id="mulai_training"
                                 name="mulai_training"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Start Course" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="selesai_training">Finish Course</label>
                                <input
                                 id="selesai_training"
                                 name="selesai_training"
                                 class="form-control post_variable input-tanggal"
                                 type="text"
                                 placeholder="Finish Course" 
                                 data-date-format="yyyy-mm-dd">   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="penyelenggara">Organizer</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="penyelenggara"
                                 name="penyelenggara"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Dicoding Indonesia" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                           
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Training END -->
        
        <!-- Modal Achievement -->
        <div class="modal fade" id="addNewAchievementModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Achievement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addAchievement" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="achievement">Achievement</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="achievement"
                                 name="achievement"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Your Achievement" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input
                                 id="year"
                                 name="year"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 2019" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="organizer">Appreciator</label>
                                <input
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="organizer"
                                 name="organizer"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Appreciator" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>     
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editAchievement" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Achievement</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editAchievement" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="achievement">Achievement</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" name="id_achievement" id="id_achievement">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="achievement"
                                 name="achievement"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Your Achievement" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input
                                 id="year"
                                 name="year"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 2019" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="organizer">Appreciator</label>
                                <input
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="organizer"
                                 name="organizer"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Appreciator" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>     
                          
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Achievement END -->

        <!-- Modal Refference -->
        <div class="modal fade" id="addNewRefferenceModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add Refference</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/addRefference" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="fullname">Fullname</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="fullname"
                                 name="fullname"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Fullname" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="relation">Relation</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="relation"
                                 name="relation"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Relation" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp">Contact</label>
                                <input
                                 id="no_hp"
                                 name="no_hp"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 0853 6188 5100" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input
                                 id="email"
                                 name="email"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: name@email.com" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>     
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editRefference" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Refference</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/employee/editRefference" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="fullname">Fullname</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input type="hidden" name="id_refference" id="id_refference">
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="fullname"
                                 name="fullname"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Fullname" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="relation">Relation</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="relation"
                                 name="relation"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: Relation" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="no_hp">Contact</label>
                                <input
                                 id="no_hp"
                                 name="no_hp"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: 0853 6188 5100" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input
                                 id="email"
                                 name="email"
                                 class="form-control post_variable"
                                 type="text"
                                 placeholder="Ex: name@email.com" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>     
                            
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Refference END -->

        <!-- Modal Refference -->
        <div class="modal fade" id="addNewFilingModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i>&#8195;Add E-Filing</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="<?php echo base_url();?>index.php/e_filing/upload/userUploadDoc" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="file_name">File Name</label>
                                <input type="hidden" name="First_name" value="<?php echo $employee['First_name'] ?>">
                                <input type="hidden" name="Last_name" value="<?php echo $employee['Last_name'] ?>">
                                <input 
                                 pattern="[A-Za-z0-9 -]+"
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="file_name"
                                 name="file_name"
                                 class="form-control post_variable"
                                 type="text" 
                                 placeholder="Ex: File Name" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="control-label">
                                   Category 
                                </label> 
                                <select
                                id="category"
                                name="category"
                                class="form-control post_variable"
                                required>
                                <?php 
                                  foreach($category as $d)
                                  {                                      
                                      echo '<option value="'.$d->category_name.'">'.$d->category_name.'</option>';
                                  } 
                                ?>
                                </select>            
                                <div class="invalid-feedback"></div>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group">
                                <label for="attachment">Choose File</label>
                                <input 
                                 onkeyup="this.value = this.value.toUpperCase()"
                                 id="attachment"
                                 name="attachment"
                                 class="form-control post_variable"
                                 type="file" 
                                 required>   
                                 <div class="invalid-feedback"></div>
                                 <div class="valid-feedback"></div>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-upload"></i> Upload</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Refference END -->
</div>

<script>
       $(document).ready(function() {
          $('#example').DataTable();
          $('#example2').DataTable();
          $('#example3').DataTable();
          $('#example4').DataTable();
          $('#example5').DataTable();
          $('#example6').DataTable();
          $('#example7').DataTable();
          $('#example8').DataTable();
          $('#example9').DataTable();
          $('#example10').DataTable();
      } );
       $(document).ready(function(){
        $('.input-tanggal').datepicker();       
      });

   $('#uploadFile_add').on('show.bs.modal', function (event) {
      var div = $(event.relatedTarget)
      var modal          = $(this)
      modal.find('#uuid_filing_req').attr("value",div.data('uuid_filing_req'));
      modal.find('#file_name_req').attr("value",div.data('file_name_req'));
      modal.find('#uuid_filing_category').attr("value",div.data('uuid_filing_category'));
      modal.find('#uuid_employee_required_file').attr("value",div.data('uuid_employee_required_file'));
  });

 $('#uploadFile_update').on('show.bs.modal', function (event) {
          var div = $(event.relatedTarget)
          var modal          = $(this)
          modal.find('#uuid_filing_req').attr("value",div.data('uuid_filing_req'));
          modal.find('#file_name_req').attr("value",div.data('file_name_req'));
          modal.find('#uuid_filing_category').attr("value",div.data('uuid_filing_category'));
          modal.find('#uuid_employee_required_file').attr("value",div.data('uuid_employee_required_file'));
      });
</script>

<?php $this->load->view('e_filing/employee/jquery_edit');?>