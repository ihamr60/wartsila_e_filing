<script>
      $(document).ready(function() {
          // Untuk sunting
          $('#editOrganization').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_organisasi').attr("value",div.data('id_organisasi'));
              modal.find('#organisasi').attr("value",div.data('organisasi'));
              modal.find('#jabatan').attr("value",div.data('jabatan'));
              modal.find('#thn_mulai').attr("value",div.data('thn_mulai'));
              modal.find('#thn_berakhir').attr("value",div.data('thn_berakhir'));
          });

          $('#editWorkEx').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_job').attr("value",div.data('id_job'));
              modal.find('#perusahaan').attr("value",div.data('perusahaan'));
              modal.find('#jabatan').attr("value",div.data('jabatan'));
              modal.find('#thn_mulai').attr("value",div.data('thn_mulai'));
              modal.find('#thn_berakhir').attr("value",div.data('thn_berakhir'));
          });

          $('#editEducation').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_education').attr("value",div.data('id_education'));
              modal.find('#institusi').attr("value",div.data('institusi'));
              modal.find('#no_ijazah').attr("value",div.data('no_ijazah'));
              modal.find('#jurusan').attr("value",div.data('jurusan'));
              modal.find('#ipk').attr("value",div.data('ipk'));
              modal.find('#thn_masuk').attr("value",div.data('thn_masuk'));
              modal.find('#thn_lulus').attr("value",div.data('thn_lulus'));
          });

          $('#editCertification').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_competence').attr("value",div.data('id_competence'));
              modal.find('#kompetensi').attr("value",div.data('kompetensi'));
              modal.find('#cert_option1').html(div.data('level'));           
          });

          $('#editLanguage').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_language').attr("value",div.data('id_language'));
              modal.find('#bahasa').attr("value",div.data('bahasa'));
              modal.find('#lang_option').html(div.data('level'));           
          });

          $('#editFamily').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_family').attr("value",div.data('id_family'));
              modal.find('#nama_sdr').attr("value",div.data('nama_sdr'));
              modal.find('#tgl_lhr_sdr').attr("value",div.data('tgl_lhr_sdr'));
              modal.find('#hubungan').attr("value",div.data('hubungan'));
              modal.find('#pekerjaan').attr("value",div.data('pekerjaan'));
              modal.find('#no_hp').attr("value",div.data('no_hp'));           
          });

          $('#editTraining').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_training').attr("value",div.data('id_training'));
              modal.find('#training').attr("value",div.data('training'));
              modal.find('#tmpt_training').attr("value",div.data('tmpt_training'));
              modal.find('#mulai_training').attr("value",div.data('mulai_training'));
              modal.find('#selesai_training').attr("value",div.data('selesai_training'));
              modal.find('#penyelenggara').attr("value",div.data('penyelenggara'));           
          });

          $('#editAchievement').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_achievement').attr("value",div.data('id_achievement'));
              modal.find('#achievement').attr("value",div.data('achievement'));
              modal.find('#achiev_option').html(div.data('level')); 
              modal.find('#year').attr("value",div.data('year'));
              modal.find('#organizer').attr("value",div.data('organizer'));           
          });

          $('#editRefference').on('show.bs.modal', function (event) {
              var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
              var modal          = $(this)

              // Isi nilai pada field
              modal.find('#id_refference').attr("value",div.data('id_refference'));
              modal.find('#fullname').attr("value",div.data('fullname'));
              modal.find('#relation').attr("value",div.data('relation')); 
              modal.find('#no_hp').attr("value",div.data('no_hp'));
              modal.find('#email').attr("value",div.data('email'));           
          });

      });
  </script>

