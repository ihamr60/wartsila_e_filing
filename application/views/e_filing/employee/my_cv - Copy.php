<!-- start: MAIN CSS -->
   
    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>vendor/admin/bower_components/bootstrap/dist/css/bootstrap.css" />
  





<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">  
    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Live CV (Curriculum Vitae)</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/e_filing/hr">Employee Data</a></li>
             <li class="breadcrumb-item">Live CV</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal -->
           <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                                <li>
                                    <a data-toggle="tab" href="#panel_overview">
                                        Personal Data
                                    </a>
                                </li>
                          
                                <li>
                                    <a data-toggle="tab" href="#panel_pengalaman_organisasi">
                                        Organization
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_pengalaman_kerja">
                                        Work Experience
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_pengalaman_pendidikan">
                                        Education
                                    </a>
                                </li>
                                 <li>
                                    <a data-toggle="tab" href="#panel_kompetensi">
                                        Certification
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_bahasa">
                                        Language
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_keluarga">
                                        Family
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_kursus">
                                        Training
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_prestasi">
                                        Achievement
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#panel_referensi">
                                        Reference
                                    </a>
                                </li>
                            </ul>
        </div>
    </div>
	
    
    
    
   
    
    
    
    <!-- start: PAGE CONTENT -->
                	<div class="row">
                    <div class="col-sm-12">
                        <div class="tabbable">
                            
                           
                            <div class="tab-content">
                         
                                <div id="panel_overview" class="tab-pane in active">
                                  <div class="card card-header-custom">
                                    			<div class="card-header"><i class="fas fa-list"></i></div>
                                   <div class="card-body">
                                      <div class="row">
                                        <div class="col-md-4">
                                            <div>
                                               <div class="card card-header-custom"> 
                                                <div> 
                                                    
                                                    <p align="center">
                                                    <img style="width: 150; height: 250px; border: 2px #000 solid; border-radius: 6%;" 		src="<?php echo base_url();?>vendor/admin/assets/images/ilham.jpg">
                                                   </p>
                                                   <h4 align="center"></h4>
                                                   <p  align="center"><font size="2"></font></p>		
                                                    <hr>
                                                </div>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Personal Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>PersNo</td>
                                                            <td><span class="label label-sm label-default"></span></a>
                                                            </td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Sex</td>
                                                            <td></td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Birtdate</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Religion</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tribe</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Height</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Weight</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Blood Type</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Passion</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Martital Stats</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nationality</td>
                                                            <td></td>
                                                        </tr>
                                                       
                                                    </tbody>
                                                </table>
                                                
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">General Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       
                                                        <tr>
                                                            <td>Position</td>
                                                            <td></td>  
                                                        </tr>
                                                        <tr>
                                                            <td>Superior</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Departement</td>
                                                            <td></td>
                                                        </tr>
                                                       
                                                    </tbody>
                                                </table>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="3">Contract Information</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Status Employment</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Join Date</td>
                                                            <td></td>                                                        
                                                        </tr>
                                                        <tr>
                                                            <td>Contract Type</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Contract End</td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                              </div>
                                           </div>
                                        </div>
                                        
                                        <div class="col-md-8">
                                        	<div class="row">
                                                <div class="col-md-12">
                                           			<div class="card card-header-custom"> 
                                            			<div class="col-md-12">
                                                			<h4>ID Card Number Information</h4>
                                                			<hr>
                                            			</div>
                                            			<div class="col-md-6">
                                                			<div class="form-group">
                                                    			<label class="control-label">
                                                        			KTP
                                                    			</label>   			
                                                       			<input readonly class="form-control" type="text" value="">   
                                                			</div>
                                                			<div class="form-group">
                                                    			<label class="control-label">
                                                        			NPWP
                                                    			</label>         		
                                                        			<input readonly class="form-control" type="text" value="">    
                                                			</div>
                                               				<div class="form-group">
                                                    			<label class="control-label">
                                                        			BPJS Ketenagakerjaan
                                                    			</label>
                                                        			<input readonly class="form-control" type="text" value="">
                                                			</div>
                                                            <div class="form-group">
                                                    			<label class="control-label">
                                                        			BANK
                                                    			</label>
                                                        			<input readonly class="form-control" type="text" value="">
                                                			</div>
                                            			</div>
                                            				<div class="col-md-6">
                                                				<div class="form-group">
                                                                    <label class="control-label">
                                                                        BPJS Kesehatan
                                                                    </label>
                                                                        <input readonly class="form-control" type="text" value="">    
                                               	 				</div>
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        Pasport
                                                                    </label>
                                                                        <input readonly class="form-control" type="text" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        KK
                                                                    </label>
                                                                        <input readonly class="form-control" type="text" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        DPLK
                                                                    </label>
                                                                        <input readonly class="form-control" type="text" value="">
                                                                </div>
                                            				</div>
                        							</div>
                                  				</div>
                                  
                                                 <div class="col-md-12">
                                                    <br/><br/>
                                                        <div class="card card-header-custom">
                                                            <div class="col-md-12">
                                                                <h4>Attachment</h4>
                                                                <hr>
                                                            </div>
                                                            <div class="card-body">
                                                                <table class="table table-striped table-bordered table-hover" id="projects">
                                                                    <thead>
                                                                        <tr>
                                                                            
                                                                            <th>KTP</th>
                                                                            <th>Passport</th>
                                                                            <th>KK</th>
                                                                            <th>NPWP</th>
                                                                            <th>Married Cert.</th>
                                                                            <th>BANK</th>
                                                                            <th>Education Cert.</th>
                                                                            <th>DPLK</th>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    
                                                                        <tr>
                                                                            
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                           	
                                                                         
                                                                           
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                    <thead>
                                                                        <tr>
                                                                            
                                                                            <th>BPJS Kes.</th>
                                                                            <th>BPJS Naker</th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            <th></th>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    
                                                                        <tr>
                                                                            
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/'" class="btn btn-success btn-sm">Preview</button></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                           	
                                                                         
                                                                           
                                                                        </tr>
                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                    </div>
                                                </div>
                                                
                                    
                                                <div class="col-md-12">
                                                    <div class="card card-header-custom">
                                                        <div class="col-md-12">
                                                            <h4>Annual MCU</h4>
                                                            <hr>
                                                        </div>
                                                            <div class="card-body">
                                                                <table class="table table-striped table-bordered table-hover" id="projects">
                                                                    <thead>
                                                                        <tr> 
                                                                            <th>Years</th>
                                                                            <th>MCU Results</th> 
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>2018</td>
                                                                            <td><button onclick="window.location.href='<?php echo base_url();?>index.php/e_filing/hr/bg_detailEmployee/" class="btn btn-success btn-sm">Preview</button></td>
                                                                        </tr>
                                                                                   
                                                                    </tbody>
                                                                </table>
                                                        	</div>
                                                    </div>
                                                </div>  
                                                
                                                
                                                <div class="col-md-12">
                                           			<div class="card card-header-custom"> 
                                            			<div class="col-md-12">
                                                			<h4>Current Address Information</h4>
                                                			<hr>
                                            			</div>
                                            			
                                            				<div class="col-md-12">
                                                				<div class="form-group">
                                                                    <label class="control-label">
                                                                        Address
                                                                    </label>
                                                                  
                                                                      <br/>
                                                                 
                                               	 				</div>
                                                            </div>
                                                          	<div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        City
                                                                    </label>
                                                             
                                                                        <br/>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        Sub-District
                                                                    </label>
                                                                   
                                                                     <br/>
                                                                        
                                                               
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        Province
                                                                    </label>
                                                                   
                                                                       <br/>
                                                            
                                                                </div>
                                            				</div>
                        							</div>
                                  				</div>                          
                                        	</div> 
                                       </div>   
                                   </div>
                                 </div>
                               </div>
                            </div>
                          

                                <!-- PANEL PENGALAMAN ORGANISASI -->
                                
                                <div id="panel_pengalaman_organisasi" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>
                                                
                                                <th>ORGANIZATION</th>
                                                <th>POSITION</th>
                                                <th>START YEAR</th>
                                                <th>STOP YEAR</th>
                                                <th>ATTACHMENT</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                       		
                                            <tr>
                                                
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                               
                                            </tr>
                                         	
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                
                                <!-- END PANEL PENGALAMAN ORGANISASI -->
                                
                                <!-- START PANEL PENGALAMAN PENDIDIKAN -->
                                <div id="panel_pengalaman_pendidikan" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>     
                                                <th>INSTITUTION</th>
                                                <th>MAJORS</th>
                                                <th>IPK/GPA</th>
                                                <th>STAR YEAR</th>
                                                <th>GRADUATION YEAR</th>
                                                <th>DIPLOMA NUMBER</th>
                                                <th>ATTACHMENT</th>            
                                            </tr>
                                        </thead>
                                        <tbody>
                                      
                                            <tr>    
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>    
                                            </tr>
                                           
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END PENDIDIKAN -->
                                 <!-- START PANEL PENGALAMAN Kerja -->
                               
                                <div id="panel_pengalaman_kerja" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>               
                                                <th>COMPANY</th>
                                                <th>POSITION</th>
                                                <th>START WORKING</th>
                                                <th>STOP WORKING</th>
                                                <th>ATTACHMENT</th>       
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr> 
                                         
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END Pengalaman Kerja -->
                                
                                <!-- START PANEL Kompetensi -->
                                <div id="panel_kompetensi" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>                  
                                                <th>COMPETENCE</th>
                                                <th>LEVEL</th>
                                                <th>ATTACHMENT</th>                                     
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            <tr>
                                                <td></td> 
                                                <td></td> 
                                                <td></td>  
                                            </tr>  
                                        
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END Kompetensi -->
                                
                                <!-- PANEL BAHASA -->
                                
                                <div id="panel_bahasa" class="tab-pane">
                                    <div class="card card-header-custom">
                                        <div class="card-header"><i class="fas fa-list"></i></div>
                                        <div class="card-body">
                                            <table class="table table-striped table-bordered table-hover" id="projects">
                                                <thead>
                                                    <tr>
                                                        <th>LANGUAGE</th>
                                                        <th>LEVEL</th>
                                                        <th>ATTACHMENT</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                               
                                                    <tr> 
                                                        <td></td>      
                                                        <td></td> 
                                                        <td></td> 
                                                    </tr>
                                               
                                                </tbody>
                                            </table>
                                        </div>
                                     </div>
                                </div>
                                
                                <!-- END PANEL BAHASA -->
                                
                                <!-- PANEL KELUARGA -->
                                
                                <div id="panel_keluarga" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                        <div class="card-body">
                                            <table class="table table-striped table-bordered table-hover" id="projects">
                                                <thead>
                                                    <tr>
                                                        <th>FULLNAME</th>
                                                        <th>CONTACT</th>
                                                        <th>BIRTHDATE</th>
                                                        <th>RELATION</th>
                                                        <th>JOB</th>  
                                                    </tr>
                                                </thead>
                                                <tbody>
                									
                                                    <tr>   
                                                        <td></td>                
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>        
                                                    </tr>  
                                                                       
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- END PANEL KELUARGA -->
                                
                                <!-- PANEL KURSUS -->
                                <div id="panel_kursus" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                        <div class="card-body">
                                            <table class="table table-striped table-bordered table-hover" id="projects">
                                                <thead>
                                                    <tr> 
                                                        <th>COURSE NAME</th>
                                                        <th>COURSE SITES</th>
                                                        <th>START COURSE</th>
                                                        <th>FINISH COURSE</th>
                                                        <th>ORGANIZER</th>         
                                                    </tr>
                                                </thead>
                                                <tbody>
                                              
                                                    <tr> 
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                 
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END PANEL KURSUS -->
                                
                                <!-- PANEL PRESTASI -->
                                <div id="panel_prestasi" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>      
                                                <th>ACHIEVEMENT</th>
                                                <th>LEVEL</th>
                                                <th>YEAR</th>
                                                <th>ORGANIZER/APPRECIATOR</th>
                                                <th>ATTACHMENT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                         
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END PANEL PRESTASIs -->

                                <!-- PANEL REFERENSI -->
                                <div id="panel_referensi" class="tab-pane">
                                <div class="card card-header-custom">
                                    <div class="card-header"><i class="fas fa-list"></i></div>
                                    <div class="card-body">
                                    <table class="table table-striped table-bordered table-hover" id="projects">
                                        <thead>
                                            <tr>
                                                <th>FULLNAME</th>
                                                <th>RELATION</th>
                                                <th>NUMBER HANDPHONE</th>
                                                <th>E-MAIL</th> 
                                                <th>ATTACHMENT</th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                       		
                                            <tr>  
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                          
                                        </tbody>
                                    </table>
                                    </div>
                                    </div>
                                </div>
                                <!-- END PANEL REFERENSI -->
                                
                               
                                
                            </div>
                        
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
    
    
    
    
    
    
    
 

    <!-- Modal Add New Site START -->
    <div class="modal fade" id="addNewSiteModal" tabindex="-1" role="dialog" aria-labelledby="addNewSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-new-site" method="POST"  autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add New Site END -->

    <!-- MODAL Edit Site START -->
    <div class="modal fade" id="editSiteModal" tabindex="-1" role="dialog" aria-labelledby="editSiteModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editSiteModalTitle"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-site" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="input-name">Site Name</label>
                            <input type="text" class="form-control" id="input-name" name="inputName" placeholder="Ex : Kantor cabang jakarta selatan" required>
                        </div>
                        <div class="form-group">
                            <label for="input-address">Address</label>
                            <input type="text" class="form-control" id="input-address" name="inputAddress" placeholder="Ex : Halim perdana kusuma" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Edit Site END -->
    
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    