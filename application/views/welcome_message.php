    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        	<h2 class="page-head-title">Dashboard</h2>
    	</div>

    <!-- Start Content -->

	<div class="card card-table card-header-custom">
		<div class="card-header" style="background-color: #FA742F;"><i class="fas fa-info"></i> - My information</div>
		<div class="card-body overflow-auto p-0">
			<table class="table">
				<tbody>
					<tr>
						<th>Employee ID</th>
						<td><?php echo $_SESSION['user']->PersNo ?></td>
					</tr>
					<tr>
						<th>Employee Name</th>
						<td><?php echo  $_SESSION['user']->Last_name && ($_SESSION['user']->First_name !== $_SESSION['user']->Last_name) ? ($_SESSION['user']->First_name." ".$_SESSION['user']->Last_name) :  $_SESSION['user']->First_name ?></td>
					</tr>
					<tr>
						<th>Dept</th>
						<td><?php echo $_SESSION['user']->dept ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>