    <!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Schedule</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">schedule</li>
        </ol>
        </nav>
    </div>

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <button type="button" class="btn btn-success btn-sm btn-add-bulk-schedule" data-toggle="modal" data-target="#addBulkScheduleModal">
                <i class="fas fa-plus"></i> Add bulk schedules
            </button>
            <button type="button" class="btn btn-info btn-sm btn-preview">
                <i class="fas fa-eye"></i> Preview
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-md-12">
            <!-- Date Card -->
            <div class="card card-header-custom">
                <div class="card-header">1. Choose Date</div>
                <div class="card-body">
                    <div id="datepicker" class="datepicker-here" data-language='en' data-today-button='true' data-date-format="yyyy-MM-dd"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <!-- Choose Site Card -->
            <div class="card card-header-custom">
                <div class="card-header">2. Choose Site</div>
                <div class="card-body">
                    <select class="form-control" id="siteSelect" name="siteSelect">
                        <?php foreach ($sites as $site): ?>
                            <option value="<?php echo $site['site_id'] ?>"><?php echo $site['site_name'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <!-- Data Table Card -->
    <div class="card card-table card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i>
            <span class="card-subtitle">** this table shows data based on selected site and date</span>
        </div>
        <div class="card-body">
            <table id="scheduleDataTable" class="table w-100">
                <thead>
                    <tr>
                        <th>Group Name</th>
                        <th>Shift</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Data Table Card -->
    <div class="card card-table card-header-custom">
        <div class="card-header" style="background-color: #FA742F;"><i class="fas fa-thumbtack"></i></div>
        <div class="card-body">
            <table id="scheduleAdvanceDataTable" class="table w-100">
                <thead>
                    <tr>
                        <th>Pers No</th>
                        <th>Name</th>
                        <th>Shift</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add Bulk Schedule Modal -->
    <div class="modal fade" id="addBulkScheduleModal" tabindex="-1" role="dialog" aria-labelledby="addBulkScheduleLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addBulkScheduleLabel"><i class="fas fa-file-signature"></i> New time schedule form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-bulk-schedule-time" method="POST">
                        <div class="form-group">
                            <label for="shift-group">Shift Group Name</label>
                            <select class='form-control select-shift-group' name="inputShiftGroup" placeholder="Ex : Kantor Cabang Jakarta Selatan " id="select-shift-group"> </select>
                        </div>
                        <div class="form-group">
                            <label for="schedule-date">Schedule date</label>
                            <input type='text' class='form-control' name="inputScheduleDate" placeholder="Ex : 2019/05/20 " id="schedule-date" autocomplete="off"/>
                            <!-- <input type='text' class='is-invalid form-control add-bulk-schedule-time datepicker-here' name="inputScheduleDate" placeholder="Ex : 2019/05/20 " id="schedule-date" data-multiple-dates-separator=", " data-multiple-dates="true" data-language='en' data-date-format="yyyy-mm-dd" autocomplete="off"/> -->
                            <!-- <div class="invalid-feedback">
                                Please select group name.
                            </div> -->
                        </div>
                        <div class="form-group">
                            <label for="shift-time">Shift Time Name</label>
                            <select class='form-control select-shift-time' name="inputShiftTime" placeholder="Ex : Shift A " id="select-shift-time"> </select>
                        </div>
                        <div class="form-row pt-3 pl-1">
                            <button type="submit" class="btn btn-primary btn-add-bulk-schedule">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Modal -->

    <!-- advanceOptionModal Modal -->
    <div class="modal fade" id="advanceOptionModal" tabindex="-1" role="dialog" aria-labelledby="advanceOptionLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="advanceOptionLabel"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="advance-option" method="POST">
                        <div class="form-group">
                            <label for="select-employee">Employee Name</label>
                            <select class='form-control select-employee' name="inputEmployee" id="select-employee" require> </select>
                            <div class="invalid-feedback">
                                No selected employee
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="shift-time">Shift Time Name</label>
                            <select class='form-control select-shift-time' name="inputShiftTime" placeholder="Ex : Shift A " id="select-shift-time" require> </select>
                            <div class="invalid-feedback">
                                No selected shift time
                            </div>
                        </div>
                        <div class="form-row pt-3 pl-1">
                            <button type="submit" class="btn btn-primary btn-add-bulk-schedule">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Modal -->