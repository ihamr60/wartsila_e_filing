    <!-- Page Content  -->
    <div id="content">
        <div class="page-head d-print-none">
        <h2 class="page-head-title">Schedule Preview : <?php echo $site_name; ?></h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url('index.php/schedule');?>">schedule</a></li>            
            <li class="breadcrumb-item">schedule preview</li>
        </ol>
        </nav>
    </div>

    <!-- Schedule Preview Card -->
    <div class="card card-table calendar card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i>
        <div class="download-pdf btnPrint"><i class="fas fa-file-download"></i></div>
        </div>
        <div class="card-body">
            <div id="calendar"></div>
        </div>
    </div>

    <style type="text/css" media="print">
        @page { size: landscape; }
    </style>

    <script>
        var view_date = new Date('<?php echo $date; ?>');
        var event_list = $.parseJSON('<?php echo json_encode($events); ?>');
    </script>