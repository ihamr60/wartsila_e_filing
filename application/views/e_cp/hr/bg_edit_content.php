    <!-- Page Content  -->
    <div id="content">
      <div class="page-head">
        <h2 class="page-head-title">Edit Content E - CP</h2>
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">E-CP Content Management</li>
            <li class="breadcrumb-item">Edit Content E - CP</li>
          </ol>
        </nav>
      </div>
      <!-- Start Content -->

      <!-- Action Card -->
    <form method="POST" action="<?php echo base_url();?>index.php/e_cp/hr/edit_content">
      <div class="card">
          <div class="card-body">
              <!-- Button trigger modal  -->
             <div class="form-group">
                <label for="title_id">E-CP Title:</label>
                <input type="hidden" name="content_id" value="<?php echo $data_content['content_id'] ?>">
                <select 
                id="title_id"
                name="title_id" 
                class="form-control post_variable" 
                required>
                <?php
                  $title = $this->web_app_model->getWhereOneItem($data_content['title_id'],'title_id','title');
                  if(!empty($title['title_id']))
                  {
                    echo "<option selected value='".$title['title_id']."'>IND: ".$title['title_ind']."</option>
                          <option value='".$title['title_id']."'>ENG: ".$title['title_eng']."</option>";
                  }
                  else
                  {
                    echo "-";
                  }
                ?>
                <?php 
                  foreach ($data_title as $d) {
                    
                      echo "<option value='".$d->title_id."'>IND: ".$d->title_ind."</option>
                            <option value='".$d->title_id."'>ENG: ".$d->title_eng."</option>";
                  }
                ?> 
                </select>
                <div class="invalid-feedback"></div>
                <div class="valid-feedback"></div>
            </div>
          </div>
      </div> 

      <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-file-signature"></i> &#8195;Bilingual E-CP Content Editor</div>
        <div class="card-body">
          <div class="form-group">
            <label for="ckedtor" class="control-label">Konten B'Indonesia:</label>
            <textarea 
              class="form-control post_variable ckeditor" 
              id="ckedtor"
              name="content_ind">
              <?php echo $data_content['content_ind'] ?>
            </textarea>
            <div class="invalid-feedback"></div>
            <div class="valid-feedback"></div>
          </div>
          <hr>
          <div class="form-group pt-3">
            <label for="ckedtor" class="control-label">English Content:</label>
            <textarea 
              class="form-control post_variable ckeditor" 
              id="ckedtor"
              name="content_eng">
            <?php echo $data_content['content_eng'] ?>
            </textarea>
            <div class="invalid-feedback"></div>
            <div class="valid-feedback"></div>
          </div>
          <hr>
          <button class="btn btn-sm btn-success">
            <i class="fas fa-plus"></i> Update Content
          </button>     
        </div>
      </div>
    </div>
  </form>