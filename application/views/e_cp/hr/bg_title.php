

    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Title E - Company Policy</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">E-CP Content Management</li>
            <li class="breadcrumb-item">Title</li>
        </ol>
        </nav>
    </div>
    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#addTitle">
                <i class="fas fa-plus"></i> Create a new title
            </button>
        </div>
    </div> 

    <!-- Data Tables Card -->    
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Title E - Company Policy </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>Title</th>
                        <th>Lvl</th>
                        <th>Parent</th>
                        <th width="140px">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach($data_title as $d)
                    { $no++;
                  ?>
                  <tr>
                      <td align="center"><?php echo $no; ?></td>
                      <td><?php echo "<b>IND:</b> ".$d->title_ind."<br/><b>ENG:</b> ".$d->title_eng."" ?></td>
                      <td align="center"><?php echo $d->level ?></td>
                      <td>
                        <?php 
                            $title_parent_id = $this->web_app_model->getWhereOneItem($d->parent_id,'title_id','title');
                            if(!empty($title_parent_id['title_id']) && !empty($title_parent_id['title_eng']) )
                            {
                              echo "<b>IND:</b> ".$title_parent_id['title_ind']."<br/><b>ENG:</b> ".$title_parent_id['title_eng']."";   
                            }
                            else
                            {
                              echo "-";
                            }
                        ?>
                      </td>
                      <td>
                          <button
                              class="btn btn-sm btn-primary"
                              data-toggle="modal" data-target="#editTitle<?php echo $d->title_id ?>">
                              <i class="fas fa-edit"></i> Edit
                          </button>
                          <a href="<?php echo base_url(); ?>index.php/e_cp/hr/deleteTitle/<?php echo $d->title_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
              		  </td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal Add New Required File START -->
    <div class="modal fade" id="addTitle" tabindex="-1" role="dialog" aria-labelledby="addNewTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewTitle"><i class="fas fa-file-signature"></i>&#8195;Add New Title E-CP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_cp/hr/add_title">
                        <div class="form-group">
                            <label for="title_ind">Title In Indonesian</label>
                            <input 
                             id="title_ind"
                             name="title_ind"
                             class="form-control post_variable"
                             type="text" 
                             required>   
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="title_eng">Title In English</label>
                            <input 
                             id="title_eng"
                             name="title_eng"
                             class="form-control post_variable"
                             type="text" 
                             required>   
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="parent_id">Parent:</label>
                            <select 
                            id="parent_id" 
                            name="parent_id" 
                            class="form-control" 
                            required> 
                            <option value="">Please Select</option> 
                            <?php
                              foreach($data_title as $d)
                              {
                                echo '<option value="'.$d->title_id.'">ENG: '.$d->title_eng.' / IND: '.$d->title_ind.'</option>';
                              }
                            ?>
                            </select> 
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Add Required File END -->

    <?php 
      foreach ($data_title as $d) {
    ?>
    <!-- MODAL Edit Required File START -->
    <div class="modal fade" id="editTitle<?php echo $d->title_id ?>" tabindex="-1" role="dialog" aria-labelledby="editTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editRequiredFileModalTitle"><i class="fas fa-file-signature"></i>&#8195;Edit Title E-CP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo base_url();?>index.php/e_cp/hr/edit_title">
                        <div class="form-group">
                            <label for="title_ind">Bahasa Indonesia</label>
                            <input 
                             id="title_ind"
                             name="title_ind"
                             value="<?php echo $d->title_ind ?>" 
                             class="form-control post_variable"
                             type="text" 
                             required>   
                             <input type="hidden" name="title_id" value="<?php echo $d->title_id ?>">
                             <input type="hidden" name="level" value="<?php echo $d->level ?>">
                             <input type="hidden" name="parent_id_ori" value="<?php echo $d->parent_id ?>">
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="title_eng">English</label>
                            <input 
                             id="title_eng"
                             name="title_eng"
                             value="<?php echo $d->title_eng ?>" 
                             class="form-control post_variable"
                             type="text" 
                             required>   
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <div class="form-group">
                            <label for="parent_id">Parent:</label>
                            <select 
                            id="parent_id" 
                            name="parent_id" 
                            class="form-control" 
                            required>  
                            <option selected value="<?php echo $d->parent_id ?>">
                                <?php
                                    foreach($data_title as $t)
                                    {   
                                        if($t->title_id==$d->parent_id)
                                        {
                                            echo "ENG: ".$t->title_ind." / IND: ".$t->title_eng."";
                                        }

                                    }
                                ?>
                            </option>
                            <?php
                              foreach ($data_title as $e) {
                            ?>
                                 <option value="<?php echo $e->title_id ?>"><?php echo "ENG: ".$e->title_eng." / IND: ".$e->title_ind.""; ?></option>
                            <?php } ?>
                            </select> 
                             <div class="invalid-feedback"></div>
                             <div class="valid-feedback"></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL Edit Required File END -->
  <?php } ?>

    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
       $('#parent_id').select2();
		} );
    </script>

