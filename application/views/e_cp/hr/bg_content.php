     <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Content E - Company Policy</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">E-CP Content Management</li>
            <li class="breadcrumb-item">Content</li>
        </ol>
        </nav>
    </div>

    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>index.php/e_cp/hr/bg_add_content">
                <i class="fas fa-plus"></i> Create a new content
            </a>
        </div>
    </div> 

    <!-- Data Tables Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; Content E - Company Policy </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th width="140px">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0; 
                    foreach ($data_content as $d) 
                    {
                      $no++;
                  ?>
                  <tr>
                      <td align="center"><?php echo $no; ?></td>
                      <td><?php echo "<b>IND:</b> ".$d->title_ind."<br/><b>ENG:</b> ".$d->title_eng."" ?></td>
                      <td><p><?php echo "<b>IND:</b> ".limit_words($d->content_ind,11).""?>  .   .   .   .</p>
                          <p><?php echo "<b>ENG:</b> ".limit_words($d->content_eng,11).""?>  .   .   .   .</p>
                      </td>
                      <td>
                          <a
                              class="btn btn-sm btn-primary"
                              href="<?php echo base_url();?>index.php/e_cp/hr/bg_edit_content/<?php echo $d->content_id ?>">
                              <i class="fas fa-edit"></i> Edit
                          </a>
                          <a href="<?php echo base_url(); ?>index.php/e_cp/hr/deleteContent/<?php echo $d->content_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
              		  </td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
		} );
    </script>
    