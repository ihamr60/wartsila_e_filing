    <!-- Page Content  -->
    <div id="content">
      <div class="page-head">
        <h2 class="page-head-title">Add Content E - CP</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">E-CP Content Management</li>
            <li class="breadcrumb-item">Add Content E - CP</li>
        </ol>
        </nav>
      </div>
      <!-- Start Content -->

      <!-- Action Card -->
    <form method="POST" action="<?php echo base_url();?>index.php/e_cp/hr/add_content">
      <div class="card">
          <div class="card-body">
              <!-- Button trigger modal  -->
             <div class="form-group">
                <label for="title_id">E-CP Title:</label>
                <select 
                  id="title_id"
                  name="title_id" 
                  class="form-control post_variable" 
                  required>  
                  <option value="">Please Select</option>
                  <?php 
                    foreach ($data_title as $d) 
                    {
                  ?>
                     <option value="<?php echo $d->title_id ?>"><?php echo "ENG: ".$d->title_eng." / IND: ".$d->title_ind."" ?></option>
                  <?php } ?>
                </select>
                <div class="invalid-feedback"></div>
                <div class="valid-feedback"></div>
            </div>
          </div>
      </div> 

      <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-file-signature"></i> &#8195;Bilingual E-CP Content Editor</div>
        <div class="card-body">
          <div class="form-group">
            <label for="ckedtor" class="control-label">Konten B'Indonesia:</label>
            <textarea 
              name="content_ind"
              class="ckeditor" 
              id="ckedtor">
            </textarea>
            <div class="invalid-feedback"></div>
            <div class="valid-feedback"></div>
          </div>
          <hr>
          <div class="form-group pt-3">
            <label for="ckedtor" class="control-label">English Content:</label>
            <textarea 
              name="content_eng"
              class="ckeditor" 
              id="ckedtor">
            </textarea>
            <div class="invalid-feedback"></div>
            <div class="valid-feedback"></div>
          </div>
          <hr>
          <button class="btn btn-sm btn-success">
            <i class="fas fa-plus"></i> Add Content
          </button>     
        </div>
      </div>
    </div>
  </form>

  <script>
       $(document).ready(function() {
       $('#title_id').select2();
    } );
    </script>