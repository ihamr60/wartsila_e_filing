<div id="content">
        <div class="page-head">
            <h2 class="page-head-title">My Overtime Report : <?php echo date('M Y'); ?></h2>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item">my overtime report</li>
                </ol>
            </nav>
        </div>

       <div class="card card-header-custom">
        <div class="card-header">Overtime Report Detail</div>
        <div class="card-body overflow-auto p-0">
            <table id="OvertimeReportDataTable" class="table table-bordered w-100">
                <thead class="text-center">
                    <tr>
                        <th>Date</th>
                        <th>Day</th>
                        <th>Day Status</th>
                        <th>Total Overtime</th>
                        <th colspan="4">Factor Time</th>
                    </tr>
                    <tr>
                        <th colspan="4"></th>
                        <th>1,5</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach ($ot_res as $report) {
                        echo "  <tr>
                                <td>$report->overtime_date</td>
                                <td>$report->day</td>
                                <td ".(($report->day_status == 'HOLIDAY') ? 'style="background:#EF233C"' : '').">$report->day_status</td>
                                <td>$report->total_works</td>
                                <td>$report->time_factor_one_and_half</td>
                                <td>$report->time_factor_two</td>
                                <td>$report->time_factor_three</td>
                                <td>$report->time_factor_four</td>
                                </tr>";
                    } 
                ?>
                <tr>
                    <td colspan="3" class="text-right">Total Hours</th>
                    <td><?php echo $ot_sum->total_works; ?></td>
                    <td><?php echo $ot_sum->oneofive; ?></td>
                    <td><?php echo $ot_sum->two; ?></td>
                    <td><?php echo $ot_sum->three; ?></td>
                    <td><?php echo $ot_sum->four; ?></td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">Acc. Hours</th>
                    <td><?php echo sprintf("%.2f",(($ot_sum->oneofive * 1.5) + ($ot_sum->two * 2) + ($ot_sum->three * 3) + ($ot_sum->four * 4))); ?></td>
                    <td><?php echo sprintf("%.2f",($ot_sum->oneofive * 1.5)); ?></td>
                    <td><?php echo sprintf("%.2f",($ot_sum->two * 2)); ?></td>
                    <td><?php echo sprintf("%.2f",($ot_sum->three * 3)); ?></td>
                    <td><?php echo sprintf("%.2f",($ot_sum->four * 4)); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>