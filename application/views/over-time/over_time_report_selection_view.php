<!-- Page Content  -->
<div id="content">
    <div class="page-head">
    <h2 class="page-head-title">Overtime Site Report</h2>
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
            <li class="breadcrumb-item">overtime site report</li>
        </ol>
    </nav>
</div>

<div class="card card-header-custom">
    <div class="card-header"><i class="fas fa-file-invoice"></i></div>
    <div class="card-body overflow-auto">
        <form id="selection-form">
            <div class="form-row">
                <div class="col">
                    <label for="select-site">Site</label>
                    <select class="form-control" id="select-site" name="selectSite">
                        <?php 
                            foreach ($site_list as $value) {
                                echo "<option value='" . $value['site_id'] . "'>" . $value['site_name'] . "</option>" ;
                            }
                        ?>
                    </select>
                </div>
                <div class="col">
                <label for="select-month">Month</label>
                    <select class="form-control" id="select-month" name="selectMonth">
                        <option value="-1">Select Month</option>
                        <?php foreach ($default_months as $key => $value) : ?>
                            <option value="<?php echo $value['month']; ?>"><?php echo $value['formated_month']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col">
                <label for="exampleFormControlSelect2">Year</label>
                    <select class="form-control" id="year-select" name="selectYear">
                        <option value="-1">Select Year</option>
                        <?php foreach ($default_years as $key => $value) : ?>
                            <option value="<?php echo $value['year']; ?>"><?php echo $value['year']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary mt-3" id="preview" name="preview">Preview</button>
        </form>
    </div>
</div>