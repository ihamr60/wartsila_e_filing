<div id="content">
        <div class="page-head d-print-none">
            <h2 class="page-head-title">Overtime Site Report</h2>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item">overtime site report</li>
                </ol>
            </nav>
        </div>

        <div class="card card-table card-header-custom">
            <div class="card-header" style="background-color: #FA742F;"><i class="fas fa-info"></i></div>
            <div class="card-body overflow-auto p-0">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>Project/Site</th>
                            <td><?php echo $site->site_name ?></td>
                        </tr>
                            <th>Period</th>
                            <td><?php echo "$start_date - $end_date" ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

       <div class="card card-header-custom">
        <div class="card-header">Overtime Site Report
            <div class="download float-right">
                <a class="download-excel pr-1" href="<?php echo base_url() . "index.php/over_time_site_report/exportToExcel/{$site_id}/{$month}/{$year}";?>"><i class="fas fa-file-download"></i> Excel</a>
                <div class="download-pdf btnPrint pl-1"><i class="fas fa-file-download"></i> PDF</div>
            </div>
        </div>
        <div class="card-body overflow-auto p-0">
            <table id="OvertimeSiteReportDataTable" class="table table-bordered w-100">
                <thead class="text-center">
                    <tr>
                        <th data-html2canvas-ignore='true' class="d-print-none"></th>
                        <th>Pers No</th>
                        <th>Name</th>
                        <th colspan="2">Period</th>
                        <th>Total Real Hours</th>
                        <th colspan="4">Factor Time</th>
                        <th>Total Multiple Hours Overtime</th>
                    </tr>
                    <tr>
                        <th class="d-print-none"></th>
                        <th colspan="2"></th>
                        <th>From</th>
                        <th>To</th>
                        <th colspan="1"></th>
                        <th>1,5</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($ot_sum as $value) {
                            $acc = sprintf("%.2f",(($value['oneofive'] * 1.5) + ($value['two'] * 2) + ($value['three'] * 3) + ($value['four'] * 4)));
                            echo "<tr>
                                    <td data-html2canvas-ignore='true' class='d-print-none'><a href='". site_url('over_time_report/view/'.$value['employee_id'] . '/' . $month . '/' . $year) ."' data-html2canvas-ignore='true' class='btn btn-sm btn-info' target='_blank'><i class='fas fa-eye'></i></a></td>
                                    <td>{$value['employee_id']}</td>
                                    <td>{$value['full_name']}</td>
                                    <td>{$start_date}</td>
                                    <td>{$end_date}</td>
                                    <td>". ($value['total_works'] ? $value['total_works'] : '0.0') ."</td>
                                    <td>". ($value['oneofive'] ? $value['oneofive'] : '0.0') ."</td>
                                    <td>". ($value['two'] ? $value['two'] : '0.0') ."</td>
                                    <td>". ($value['three'] ? $value['three'] : '0.0') ."</td>
                                    <td>". ($value['four'] ? $value['four'] : '0.0') ."</td>
                                    <td>$acc</td>
                                  </tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>