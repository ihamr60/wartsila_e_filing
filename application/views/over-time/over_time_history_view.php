<!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Overtime History</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item">overtime history</li>
            </ol>
        </nav>
    </div>

<!-- Data Table Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i></div>
        <div class="card-body overflow-auto">
            <table id="overtimeHistoryDataTable" class="table w-100">
                <thead>
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Purpose</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="modal fade" id="ExtendRequestModal" tabindex="-1" role="dialog" aria-labelledby="ExtendRequestModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ExtendRequestModalLabel">Request Extend</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form id="extend-request">
                        <div class="form-group">
                            <label for="inputOverTimeHours">Overtime Hour(s)</label>
                            <input type='text' name="inputOverTimeHours" class='form-control datepicker-here datepicker-over-time-hours' id="inputOverTimeHours" data-timepicker='true' data-only-timepicker='true' data-time-format='h hour(s) and i minute(s)' data-min-hours="0" data-max-hours = "12" data-language='en' placeholder="Ex : 02:00" value="00:00" readonly="readonly" required>
                        </div>
                        <div class="form-group">
                            <label for="inputPurpose">Purpose</label>
                            <textarea class="form-control" id="inputPurpose" name="inputPurpose" placeholder="Enter your Purpose" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-input-over-time-extend-request">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>