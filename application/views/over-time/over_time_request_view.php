    <!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Overtime Request</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item">Overtime request</li>
            </ol>
        </nav>
    </div>

    <?php if(isset($_SESSION['message'])): ?>
    <div class="card text-white bg-success mb-3">
    <div class="card-header"><i class="fas fa-info-circle"></i> Message</div>
    <div class="card-body">
        <p class="card-text text-white"><?php echo $_SESSION['message']; ?></p>
    </div>
    </div>
    <?php endif; ?>

    <!-- Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-file-signature"></i> Overtime Request Form</div>
        <div class="card-body">
            <div class="card card-table card-header-custom">
                <div class="card-header bg-dark">Requestor</div>
                <div class="card-body overflow-auto p-0">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Employee ID</td>
                                <td><?php echo $_SESSION['user']->PersNo ?></td>
                            </tr>
                            <tr>
                                <td>Employee Name</td>
                                <td><?php echo  $_SESSION['user']->Last_name && ($_SESSION['user']->First_name !== $_SESSION['user']->Last_name) ? ($_SESSION['user']->First_name." ".$_SESSION['user']->Last_name) :  $_SESSION['user']->First_name ?></td>
                            </tr>
                            <tr>
                                <td>Dept</td>
                                <td><?php echo $_SESSION['user']->dept ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card card-table card-header-custom">
                <div class="card-header bg-dark">Request For</div>
                <div class="card-body overflow-auto" style="padding: 1.25rem;">
                    <form id="overTimeRequest" autocomplete="off">
                        <div class="form-group">
                            <label for="inputOverTimeDate">For date</label>
                            <input type="text" class="form-control input-over-time-date" name="inputOverTimeDate" id="inputOverTimeDate" placeholder="Select Date" required onkeypress="return false;">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputHour">Hour(s)</label>
                            <select id="inputHour" class="form-control" style="width: 100%" required>
                                <option value='' selected>Choose...</option>
                            </select>
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputMinute">Minute(s)</label>
                            <select id="inputMinute" class="form-control" style="width: 100%" required>
                                <option value='' selected>Choose...</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPurpose">Purpose</label>
                            <textarea class="form-control" id="inputPurpose" name="inputPurpose" placeholder="Enter your Purpose" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-input-over-time-request"><i class="fas fa-paper-plane"></i> Send OT request</button>
                    </form>
                </div>
            </div>

            </div>
    </div>