
    <!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Overtime Summary</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item">overtime summary</li>
            </ol>
        </nav>
    </div>

    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-file-invoice"></i></div>
        <div class="card-body overflow-auto">
            <form id="selection-form">
                <div class="form-group">
                    <label for="select-year">Choose year</label>
                    <select class="form-control" id="select-year" name="selectYear">
                    </select>
                </div>
                <a href="<?php echo site_url('/over_time_summary/index/'); ?>"name="preview" class="btn btn-primary">Preview</a>
            </form>
        </div>
    </div>