<?php 
// print_r($ot_sum_dept);
if((sizeof($ot_sum_dept) > 0) AND (sizeof($ot_sum_month) > 0)) {
    $header = array_keys(get_object_vars($ot_sum_dept[0]));
}

?>

<div id="content">
        <div class="page-head d-print-none">
            <h2 class="page-head-title">Overtime Summary</h2>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb page-head-nav">
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
                    <li class="breadcrumb-item">overtime summary</li>
                </ol>
            </nav>
        </div>
        <?php if ((sizeof($ot_sum_dept) == 0) AND (sizeof($ot_sum_month) == 0)) : ?>
            <div class="card card-header-custom">
                <div class="card-header">Overtime Summary Per Site</div>
                <div class="card-body overflow-auto p-0">
                    No Data
                </div>
            </div>
        <?php endif; ?>

        <?php if (sizeof($ot_sum_dept) > 0) : ?>
        <div class="card card-header-custom">
            <div class="card-header">Overtime Summary Per Site
            <div class="download float-right">
                <a class="download-excel pr-1" href="<?php echo base_url() . "index.php/over_time_summary/exportToExcel/{$year}";?>"><i class="fas fa-file-download"></i> Excel</a>
                <div class="download-pdf btnPrint pl-1"><i class="fas fa-file-download"></i> PDF</div>
            </div>
            </div>
            <div class="card-body overflow-auto p-0">
                <table id="OvertimeSummaryDataTable" class="table table-bordered w-100">
                    <thead>
                        <tr class="text-capitalize">
                            <?php 
                                foreach ($header as $value) {
                                    echo "<th>$value</th>";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                                <?php 
                                    foreach ($ot_sum_dept as $value ) {
                                        echo "<tr>";
                                        for($i = 0; $i < sizeof($header) ; $i++) {
                                            $obj_name = strval($header[$i]);
                                            echo "<td>" . $value->$obj_name . "</td>";
                                        }
                                        echo "</tr>";
                                    }
                                ?>
                    </tbody>
                </table>
            </div>
            <div class="card-body overflow-auto">
                <canvas id="chartOvertimeSummary" width="400" height="100"></canvas>
            </div>
        </div>
        <?php endif; ?>
        <?php if (sizeof($ot_sum_month) > 0) : ?>
        <div class="card card-header-custom">
            <div class="card-header">Overtime Summary All Site</div>
            <div class="card-body overflow-auto p-0">
                <table id="OvertimeSummaryAllSiteDataTable" class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Total Works</th>
                        </tr>
                    </thead>
                    <tbody>
                                <?php 
                                    foreach ($ot_sum_month as $value ) {
                                        echo "<tr>";
                                        echo "<td>" . $value->date . "</td>";
                                        echo "<td>" . $value->total_works . "</td>";
                                        echo "</tr>";
                                    }
                                ?>
                    </tbody>
                </table>
            </div>
            <div class="card-body overflow-auto">
                <canvas id="chartOvertimeSummaryPerMonth" width="400" height="100"></canvas>
            </div>
        </div>
        <?php endif; ?>
