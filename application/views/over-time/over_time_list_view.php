<!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Overtime Request List</h2>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
                <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Home</a></li>
                <li class="breadcrumb-item">Overtime Request List</li>
            </ol>
        </nav>
    </div>

    <!-- Data Table Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i></div>
        <div class="card-body overflow-auto">
            <table id="overtimeDataTable" class="table w-100">
                <thead>
                    <tr>
                        <th></th>
                        <th>Overtime Date</th>
                        <th>Employee Name</th>
                        <th>Employee Position</th>
                        <th>Request Status</th>
                        <th> </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>