    <!-- Page Content  -->
    <div id="content">
        <div class="page-head">
        <h2 class="page-head-title">Shift Time</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">shift time</li>
        </ol>
        </nav>
    </div>

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <button type="button" class="btn btn-sm btn-success btn-add-new-shift-time" data-toggle="modal" data-target="#addNewShiftTimeModal">
                <i class="fas fa-plus"></i> Add a new shift time
            </button>
        </div>
    </div>

    <!-- Choose Site Card -->
    <div class="card card-header-custom">
        <div class="card-header">Choose Site</div>
        <div class="card-body">
            <select class="form-control" id="siteSelect" name="siteSelect" style="width: 100%">
                <?php foreach ($sites as $site): ?>
                    <option value="<?php echo $site['site_id'] ?>"><?php echo $site['site_name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <!-- Data Table Card -->
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i></div>
        <div class="card-body">
            <table id="shiftTimeDataTable" class="table w-100">
                <thead>
                    <tr>
                        <th>Shift Time Name</th>
                        <th>Working Hours</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <!-- Add New Shift Modal Start -->
    <div class="modal fade" id="addNewShiftTimeModal" tabindex="-1" role="dialog" aria-labelledby="addNewShiftTimeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="addNewShiftTimeLabel"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add-new-shift-time" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="shift-name">Shift Name</label>
                            <input type="text" class="form-control" id="shift-name" name="inputShiftName" placeholder="Ex : Shift A">
                        </div>

                        <div class="form-row">
                            <label class="col-form-label">Working Hours</label>
                        </div>
                        <div class="form-row pt-1">
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">From</span>
                                    </div>
                                    <input type='text' name="inputTimeIn" class='form-control datepicker-here datepicker-time-in' data-timepicker='true' data-only-timepicker='true' data-time-format='hh:ii' data-language='en' style="height: auto;"/>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">To</span>
                                    </div>
                                    <input type='text' name="inputTimeOut" class='form-control datepicker-here datepicker-time-out' data-timepicker='true' data-only-timepicker='true' data-time-format='hh:ii' data-language='en' style="height: auto;"/>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row pt-3 pl-1">
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Add New Shift Modal End -->

    <!-- Edit Shift Modal Start -->
    <div class="modal fade" id="editShiftTimeModal" tabindex="-1" role="dialog" aria-labelledby="editShiftTimeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title" id="editShiftTimeLabel"><i class="fas fa-file-signature"></i></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="edit-shift-time" method="POST" autocomplete="off">
                        <div class="form-group">
                            <label for="shift-name">Shift Name</label>
                            <input type="text" class="form-control" id="shift-name" name="inputShiftName" placeholder="Ex : Shift A">
                        </div>

                        <div class="form-row">
                            <label class="col-form-label">Working Hours</label>
                        </div>
                        <div class="form-row pt-1">
                            <div class="col">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">From</span>
                                    </div>
                                    <input type='text' name="inputTimeIn" class='form-control datepicker-here datepicker-time-in' data-timepicker='true' data-only-timepicker='true' data-time-format='hh:ii' data-language='en' style="height: auto;"/>
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">To</span>
                                    </div>
                                    <input type='text' name="inputTimeOut" class='form-control datepicker-here datepicker-time-out' data-timepicker='true' data-only-timepicker='true' data-time-format='hh:ii' data-language='en' style="height: auto;"/>
                                    <div class="input-group-append">
                                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row pt-3 pl-1">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Shift Modal End -->