    <!-- Page Content  -->
  <div id="content">
    <div class="page-head">
      <h2 class="page-head-title">Edit Admin Permission</h2>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/adm_permiss"> Admin Permission</a></li>
            <li class="breadcrumb-item">Edit Admin Permission</li>
        </ol>
      </nav>
    </div>
    <!-- Start Content -->

    <div class="card card-table card-header-custom">
      <div class="card-header" style="background-color: #FA742F;"><i class="fas fa-user"></i> - Edit Permission</div>
      <div class="card-body overflow-auto p-0">
        <table class="table">
          <tbody>
            <tr>
              <th>Employee ID</th>
              <td><?php echo $data_employee['PersNo']?></td>
            </tr>
            <tr>
              <th>Employee Name</th>
              <td><?php echo $data_employee['First_name']." ".$data_employee['Last_name'] ?></td>
            </tr>
            <tr>
              <th>Dept</th>
              <td><?php echo $data_employee['Cost_Center']?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <!-- Data Tables Card --> 
    <form method="POST" action="<?php echo base_url();?>index.php/adm_permiss/addPermiss">   
      <div class="card card-header-custom">
          <div class="card-header"><i class="fas fa-list"></i> - Permission </div>
          <div class="card-body">
          <?php echo $this->session->flashdata('info'); ?>
            <div class="form-group">
                <label for="site_id">Please select site:</label>
                <select 
                  id="site_id"
                  name="site_id" 
                  class="form-control post_variable" 
                  required>  
                  <option value="">Please Select Site</option>
                  <?php 
                    foreach ($data_site as $d) 
                    {
                  ?>
                     <option value="<?php echo $d->site_id ?>"><?php echo $d->site_name ?></option>
                  <?php } ?>
                </select>
                <input type="hidden" name="employee_id" value="<?php echo $data_employee['PersNo'] ?>">
                <div class="invalid-feedback"></div>
                <div class="valid-feedback"></div>
            </div>
            <div class="card-body pb-3 pt-0 pl-0">
              <button class="btn btn-sm btn-info">
                  <i class="fas fa-plus"></i> Add new access
             </button> 
            </div>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Site</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach($data_adm_permiss as $d)
                    { $no++;
                  ?>
                  <tr>
                    <td width="10px" align="center"><?php echo $no; ?></td>
                    <td width="630px"><?php echo $d->site_name ?></td>
                    <td width="10px">
                      <a href="<?php echo base_url(); ?>index.php/adm_permiss/deletePermiss/<?php echo $d->overtime_site_report_permission_id."/".$d->employee_id ?>" onclick="return confirm('Are you sure you want to delete this data?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
            		    </td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
          </div>
      </div>
    </form>
    
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
       $('#site_id').select2();
		} );
    </script>

