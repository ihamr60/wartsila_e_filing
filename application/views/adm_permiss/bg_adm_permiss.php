

    <!-- Page Content  -->
    <div id="content">

        <div class="page-head">
        <h2 class="page-head-title">Admin Permission</h2>
        <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item">Admin Permission</li>
        </ol>
        </nav>
    </div>
    <!-- Start Content -->

    <!-- Action Card -->
    <div class="card">
        <div class="card-body">
            <!-- Button trigger modal  -->
            <a class="btn btn-sm btn-success" href="<?php echo base_url(); ?>index.php/adm_permiss/bg_add_permiss">
                <i class="fas fa-plus"></i> Create a New Admin
            </a>
        </div>
    </div> 

    <!-- Data Tables Card -->    
    <div class="card card-header-custom">
        <div class="card-header"><i class="fas fa-list"></i> &#8195; List Admin Permission </div>
        <div class="card-body">
        <?php echo $this->session->flashdata('info'); ?>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>PersNo</th>
                        <th>Full Name</th>
                        <th width="140px">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach($data_adm_permiss as $d)
                    { $no++;
                  ?>
                  <tr>
                      <td width="30px" align="center"><?php echo $no; ?></td>
                      <td width="130"><?php echo $d->employee_id ?></td>
                      <td ><?php echo $d->First_name." ".$d->Last_name ?></td>
                      
                      <td width="10px">
                          <a
                            href="<?php echo base_url();?>index.php/adm_permiss/bg_edit_permiss/<?php echo $d->employee_id ?>"
                            class="btn btn-sm btn-primary">
                            <i class="fas fa-edit"></i> Edit
                          </a>
                          <a href="<?php echo base_url(); ?>index.php/adm_permiss/deleteAdminPermiss/<?php echo $d->employee_id ?>" onclick="return confirm('Are you sure you want to delete this data?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
              		  </td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
       $('#parent_id').select2();
		} );
    </script>

