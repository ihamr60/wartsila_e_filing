    <!-- Page Content  -->
  <div id="content">
    <div class="page-head">
      <h2 class="page-head-title">New Admin Permission</h2>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb page-head-nav">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>index.php/adm_permiss"> Admin Permission</a></li>
            <li class="breadcrumb-item">Add Admin Permission</li>
        </ol>
      </nav>
    </div>
    <!-- Start Content -->
    <form method="POST" action="<?php echo base_url();?>index.php/adm_permiss/addPermiss">   
      <div class="card card-table card-header-custom">
        <div class="card-header" style="background-color: #FA742F;"><i class="fas fa-user"></i> - Select employee</div>
        <div class="card-body overflow-auto pt-3 pb-3 pl-3 pr-3">
          <div class="form-group">
            <label for="employee_id">Please select employee:</label>
            <select 
              id="employee_id"
              name="employee_id" 
              class="form-control post_variable" 
              required>  
              <option value="">Please Select Employee</option>
              <?php 
                foreach ($data_employee as $d) 
                {
              ?>
                 <option value="<?php echo $d->PersNo ?>"><?php echo $d->PersNo." - ".$d->First_name." ".$d->Last_name ?></option>
              <?php } ?>
            </select>
            <div class="invalid-feedback"></div>
            <div class="valid-feedback"></div>
          </div>
        </div>
      </div>

      <!-- Data Tables Card -->    
      <div class="card card-header-custom">
          <div class="card-header"><i class="fas fa-list"></i> &#8195; Permission </div>
          <div class="card-body">
          <?php echo $this->session->flashdata('info'); ?>
            <div class="form-group">
                <label for="site_id">Please select site:</label>
                <select 
                  id="site_id"
                  name="site_id" 
                  class="form-control post_variable" 
                  required>  
                  <option value="">Please Select Site</option>
                  <?php 
                    foreach ($data_site as $d) 
                    {
                  ?>
                     <option value="<?php echo $d->site_id ?>"><?php echo $d->site_name ?></option>
                  <?php } ?>
                </select>
                <div class="invalid-feedback"></div>
                <div class="valid-feedback"></div>
            </div>
            <div class="card-body pb-3 pt-0 pl-0">
              <button class="btn btn-sm btn-info">
                  <i class="fas fa-plus"></i> Add new access
              </button>
            </div>
            <table id="example" class="ui celled table" style="width:100%">
                <thead>
                    <tr>
                        <th width="20px">No</th>
                        <th>Site</th>
                        <th width="140px">Action</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                    $no=0;
                    foreach($data_adm_permiss as $d)
                    { $no++;
                  ?>
                  <tr>
                    <td width="30px" align="center"><?php echo $no; ?></td>
                    <td width="130"><?php echo $d->employee_id ?></td>
                    <td width="10px">
                      <a href="<?php echo base_url(); ?>index.php/e_cp/hr/deleteTitle/<?php echo $d->overtime_site_report_permission_id ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete</a>
            		    </td>
                  </tr>
                <?php } ?>
                </tbody>
            </table>
          </div>
      </div>
    </form>
      
    <script>
       $(document).ready(function() {
   		 $('#example').DataTable();
       $('#employee_id').select2();
       $('#site_id').select2();
		} );
    </script>

