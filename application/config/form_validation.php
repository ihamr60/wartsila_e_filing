<?php
    $start = array(
        'field' => 'start', 
        'label' => 'start', 
        'rules' => 'trim|required|strip_tags'
    );

    $search = array(
        'field' => 'search[value]',
        'label' => 'search', 
        'rules' => 'trim|strip_tags|alpha_numeric_spaces'
    );

    $length = array(
        'field' => 'length', 
        'label' => 'length', 
        'rules' => 'trim|required|strip_tags|numeric'
    );

    $draw = array(
        'field' => 'draw', 
        'label' => 'draw', 
        'rules' => 'trim|required|strip_tags|numeric'
    );

    $site_id = array(
        'field' => 'inputSiteID', 
        'label' => 'Site ID', 
        'rules' => 'trim|required|alpha_numeric|strip_tags|max_length[32]'
    );

    $site_name = array(
        'field' => 'inputSiteName', 
        'label' => 'Site Name', 
        'rules' => 'trim|required|strip_tags|min_length[3]|max_length[255]'
    );

    $site_address = array(
        'field' => 'inputSiteAddress', 
        'label' => 'Site Address', 
        'rules' => 'trim|required|strip_tags|min_length[3]|max_length[255]'
    );

    $search_datalist = array(
        'field' => 'search', 
        'label' => 'Keyword', 
        'rules' => 'trim|strip_tags|alpha_numeric_spaces|max_length[255]'
    );

    $config = array(
        'ajax/new_site' => array($site_name,$site_address),
        'ajax/update_site' => array($site_id,$site_name,$site_address),
        'ajax/employee_datalist' => array($search_datalist),
        'ajax/site_datatable' => array($draw, $search, $start, $length),
    );