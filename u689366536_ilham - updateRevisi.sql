-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2020 at 01:50 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u689366536_ilham`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_achievement`
--

CREATE TABLE `tbl_achievement` (
  `id_achievement` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `achievement` varchar(50) DEFAULT NULL,
  `year` varchar(5) DEFAULT NULL,
  `organizer` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_achievement`
--

INSERT INTO `tbl_achievement` (`id_achievement`, `PersNo`, `achievement`, `year`, `organizer`) VALUES
('07660B09D3DB11E9BF852CFDA1251793', '10029402', 'JUARA 1 HACKATHON KEMNAKER', '2018', 'KEMNAKER'),
('1', '10057751', 'JUARA 2 HACKATHON KEMNAKER', '2018', 'KEMNAKER RI'),
('2', '10057751', 'JUARA 1 HACKATHON KMIPN', '2018', 'RISTEKDIKTI & BAKORMA'),
('40EF3893D4A811E99EBE2CFDA1251793', '10027565', 'CERTIFICATE OF JOB ACCOMPLISHMENT', '2015', 'PT. WARTSILA INDONESIA'),
('5202A9A13B4211EABC470CC47AFB3292', '10010208', 'TEAM LEADER MAJOR OVERHAUL ENGINE 6TM410 D-TYPE ', '-', '-'),
('64E5DC9ECF2211E994EE2CFDA1251793', '10029402', '1ST RUNNER UP HACKATHON KEMNAKER RI', '2018', 'KEMNAKER RI'),
('693527EC3B4211EABC470CC47AFB3292', '10010208', 'MAJOR OVERHAUL AND REPLACE CRANKSHAFT ENGINE 6SW28', '-', '-'),
('6999AC2A04EA11EAB5100CC47AFB3292', '10010213', '15 YEAR OF SERVICES', '2016', 'PT WARTSILA INDONESIA'),
('B7D580FFD4A811E99EBE2CFDA1251793', '10027565', 'CERTIFICATE FOR PREVIOUS EMPLOYMENT', '2007', 'BIRO JASA KITA'),
('FFF88156D6B211E9807B2CFDA1251793', '10050504', 'CERTIFICATE OF APPRECIATION', '2007', 'REPUBLICA DEMOCRATICA DE TIMOR-LESTE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_competence`
--

CREATE TABLE `tbl_competence` (
  `id_competence` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `kompetensi` varchar(30) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_competence`
--

INSERT INTO `tbl_competence` (`id_competence`, `PersNo`, `kompetensi`, `level`) VALUES
('07387E35D6B111E9807B2CFDA1251793', '10053515', 'GOOD KNOWLADGE DIESEL 18V46', 'EXPERT'),
('0C2F000F171E11EAB5100CC47AFB3292', '10044528', 'LISENSI K3', 'INTERMEDIATE'),
('1', '10057751', 'PUBLICK SPEAKING', '80'),
('2', '10057751', 'AKUNTANSI', '79'),
('2E5EFA2401D211EAB67E0CC47AFB3292', '10059057', 'SERTIFIKAT BALAI LATIHAN KERJA', 'INTERMEDIATE'),
('30DC600FD64211E9807B2CFDA1251793', '10027565', 'AHLI K3', 'EXPERT'),
('4F0D2BC0D6AA11E9807B2CFDA1251793', '10044271', 'MECHANICAL ENGINEERING', 'INTERMEDIATE'),
('51BF2C0F16FE11EAB5100CC47AFB3292', '10052668', 'SALES NEGOTIATION TECHNIQUES', 'INTERMEDIATE'),
('5680B8EE01BF11EAB67E0CC47AFB3292', '10059057', 'KOMPETENSI TENAGA TEKNIK KETEN', 'EXPERT'),
('57005F1AD6AA11E9807B2CFDA1251793', '10044271', 'AUTOCAD', 'INTERMEDIATE'),
('5C9336A304EB11EAB5100CC47AFB3292', '10050507', 'ENGLISH COURSE ', 'INTERMEDIATE'),
('6111936ED6AA11E9807B2CFDA1251793', '10044271', 'ISO 900', 'BASIC'),
('6F37302C01BB11EAB67E0CC47AFB3292', '10059057', 'PRESENTATION SKILL PROGRAMME', 'EXPERT'),
('74CD23913B4811EABC470CC47AFB3292', '10010221', 'DEMONSTRATING COMPETENCY TO CO', 'EXPERT'),
('8835E6B301C111EAB67E0CC47AFB3292', '10059057', 'OPERATOR ALAT BERAT EXCAVATOR ', 'EXPERT'),
('8BF24DD1D6B211E9807B2CFDA1251793', '10050504', 'MS. WORD', 'INTERMEDIATE'),
('936C84F3D6B211E9807B2CFDA1251793', '10050504', 'MS. EXCEL', 'INTERMEDIATE'),
('95CB586501D111EAB67E0CC47AFB3292', '10059057', 'HHP LEAN BURN GAS CONTROL SYST', 'EXPERT'),
('9C7D119BD6B211E9807B2CFDA1251793', '10050504', 'MS. POWER POINT', 'INTERMEDIATE'),
('9EF203B004EA11EAB5100CC47AFB3292', '10010213', 'CREATIVE PROBLEM SOLVING', 'EXPERT'),
('A7A160D801BE11EAB67E0CC47AFB3292', '10059057', 'QSK60 GAS HE-QUALIFICATION', 'EXPERT'),
('AD876D3401BB11EAB67E0CC47AFB3292', '10059057', 'QSK78 CM500 ENGINE QUALIFICATI', 'EXPERT'),
('B0771CAA01D111EAB67E0CC47AFB3292', '10059057', 'QSV 81/91 NATURAL GAS LEAN BUR', 'EXPERT'),
('BD258AB23B4D11EABC470CC47AFB3292', '10010222', 'PROFESSIONAL PRESENTATIONS', 'EXPERT'),
('CBEFBF97D6B411E9807B2CFDA1251793', '10053589', 'MICROSOFT OFFICE', 'INTERMEDIATE'),
('CE7C0A3AD3D111E9BF852CFDA1251793', '10029402', 'NETWORK JUNIOR 2', 'BASIC'),
('D302DF33D6B411E9807B2CFDA1251793', '10053589', 'MYOB', 'INTERMEDIATE'),
('D9320D0601BF11EAB67E0CC47AFB3292', '10059057', 'LETTER OF ELECTRICAL AUTHORITY', 'EXPERT'),
('DA1B89AFD6B411E9807B2CFDA1251793', '10053589', 'MODBRK', 'INTERMEDIATE'),
('DBAF1147D3D111E9BF852CFDA1251793', '10029402', 'NETWORK JUNIOR 6', 'BASIC'),
('E53B5E113B4111EABC470CC47AFB3292', '10010208', 'OPERATOR K3 KERAN OVERHEAD KEL', 'EXPERT'),
('EB53025A16FD11EAB5100CC47AFB3292', '10052668', 'PROFESSIONAL SELLING SKILLS', 'INTERMEDIATE'),
('F3454EA1CF2111E994EE2CFDA1251793', '10029402', 'JUNIOR WEB DEVELOPER', 'INTERMEDIATE'),
('FB00C12E01BD11EAB67E0CC47AFB3292', '10059057', 'HHP LEAN BURN \'V\' ENGINE GAS C', 'EXPERT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education`
--

CREATE TABLE `tbl_education` (
  `id_education` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `institusi` varchar(40) DEFAULT NULL,
  `no_ijazah` varchar(30) DEFAULT NULL,
  `jurusan` varchar(30) DEFAULT NULL,
  `ipk` varchar(5) DEFAULT NULL,
  `thn_masuk` varchar(5) DEFAULT NULL,
  `thn_lulus` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_education`
--

INSERT INTO `tbl_education` (`id_education`, `PersNo`, `institusi`, `no_ijazah`, `jurusan`, `ipk`, `thn_masuk`, `thn_lulus`) VALUES
('018462DAD6B511E9807B2CFDA1251793', '10053589', 'SCHOOL OF PUBLIC 06 MALURUKUMU', '-', '-', '-', '1984', '1991'),
('0459E0E43B4811EABC470CC47AFB3292', '10010221', 'SDN GUNUNG CUPU 1', '-', '-', '-', '1983', '1989'),
('091FFADB3B4C11EABC470CC47AFB3292', '10010222', 'SDN 12 JAMBI ', '-', '-', '-', '1978', '1983'),
('1', '10057751', 'SMA NEGERI 1 JAKARTA', '1B67667B', 'IPA', '74,3', '2012', '2015'),
('168953DB3B4811EABC470CC47AFB3292', '10010221', 'SMP CIAMIS', '-', '-', '-', '1989', '1992'),
('2', '10057751', 'SMP NEGERI 1 JAKARTA', '3C78DHS', '-', '80,00', '2012', '2019'),
('24CA7988D6B511E9807B2CFDA1251793', '10053589', 'CHATOLIC MISSI BAUCAU', '-', '-', '-', '1991', '1994'),
('259ECDDE3B4811EABC470CC47AFB3292', '10010221', 'SMAN 1 CIAMIS', '-', '-', '-', '1992', '1995'),
('27C839C03B4C11EABC470CC47AFB3292', '10010222', 'SMP PGRI 4 JAMBI', '-', '-', '-', '1983', '1986'),
('389C052D3B4C11EABC470CC47AFB3292', '10010222', 'SMAN 93 JAKARTA', '-', '-', '-', '1986', '1989'),
('38B7C99C16FD11EAB5100CC47AFB3292', '10052668', 'UNIVERSITAS TRISAKTI', '0043/FTI/S1/TE/v/2009', 'TEKNIK ELEKTRO', '2,83', '2005', '2009'),
('3C20E97C3B4811EABC470CC47AFB3292', '10010221', 'MARINE EDUCATION AND TRAINING INSTITUTE', '-', '-', '-', '1995', '2000'),
('46E0C0913B4C11EABC470CC47AFB3292', '10010222', 'AIP/PLAP JAKARTA', '-', '-', '-', '1990', '1994'),
('49D3A087D6B511E9807B2CFDA1251793', '10053589', 'UNIVERSIDADE NACIONAL TIMOR LORASA\'E', '-', 'DIPLOMA MECHANICAL ENGINEERING', '-', '1997', '2003'),
('4FA3359B4CE811EAB4D42CFDA1251793', '10057751', 'EFEF TTTTT', 'awdwad', ' EFEFEF', 'WDAWD', 'AWDAW', 'awdaw'),
('5041E30ED4A411E99EBE2CFDA1251793', '10027565', 'UNIVERSITAS TANJUNG PURA', '16 837/J22/PP/2000', 'ILMU ADMINISTRASI NEGARA', '-', '-', '2000'),
('53F8E4D4D6C811E9807B2CFDA1251793', '10050499', 'SDN MANGKUJAYAN III', '-', '-', '-', '1984', '1990'),
('56428A643B4011EABC470CC47AFB3292', '10010208', 'SD INPRES 17 SORONG', '-', '-', '-', '1981', '1987'),
('65260B44D6C811E9807B2CFDA1251793', '10050499', 'SMPN I PONOROGO', '-', '-', '-', '1990', '1993'),
('6C137D9E3B4011EABC470CC47AFB3292', '10010208', 'SMPN 5 SORONG', '-', '-', '-', '1987', '1990'),
('6D9525D7D6B511E9807B2CFDA1251793', '10053589', 'UNIVERSITY OD WIDYAGAMA MALANG INDONESIA', '-', 'BACHELOR DEGREE MECHANICAL ENG', '-', '2005', '2006'),
('72B85BD0D3D111E9BF852CFDA1251793', '10029402', 'SMA NEGERI 1 LANGSA 2', 'bf-87940e-de', 'IPA', '7.82', '2012', '2015'),
('73150DE33B4411EABC470CC47AFB3292', '10010213', 'SDN SERDANG III', '-', '-', '-', '1982', '1985'),
('73821EE23B5011EABC470CC47AFB3292', '10010224', 'SD TP KENCANA JAKARTA PUSAT', '-', '-', '-', '1980', '1986'),
('7555280CD6B011E9807B2CFDA1251793', '10053515', 'SDN 06 HERA', '-', '-', '-', '1994', '2000'),
('768B912AD6C811E9807B2CFDA1251793', '10050499', 'SMAN II PONOROGO', '-', '-', '-', '1993', '1996'),
('86DBD1523B4411EABC470CC47AFB3292', '10010213', 'SMPN 27 JAKARTA', '-', '-', '-', '1982', '1985'),
('87270BDAD6B011E9807B2CFDA1251793', '10053515', 'SMPN 01 HERA', '-', '-', '-', '2000', '2003'),
('900D50533B4011EABC470CC47AFB3292', '10010208', 'SMAN 1 SORONG', '-', '-', '-', '1990', '1993'),
('90EE0C0ED6C811E9807B2CFDA1251793', '10050499', 'POLITEKNIK GADJAH TUNGGAL', '-', 'ELECTRICAL ENGINEERING', '-', '1997', '2000'),
('91438B56D3D111E9BF852CFDA1251793', '10029402', 'SMA NEGERI 1 LANGSA', 'bf-87940e-de', 'IPA', '7.82', '2012', '2015'),
('93AC247016F711EAB5100CC47AFB3292', '10054002', 'UNIVERSITAS KRISTEN INDONESIA PAULUS MAK', '061030316022017', 'AKUTANSI', '3,60', '2013', '2014'),
('949BB75F3B5011EABC470CC47AFB3292', '10010224', 'SMPN 137 , JAKARTA PUSAT', '-', '-', '-', '1989', '1992'),
('98ADDFAFD6B011E9807B2CFDA1251793', '10053515', 'PUBLICA 4 DE SETEMBRO', '-', '-', '-', '2003', '2006'),
('9D2F51183B4411EABC470CC47AFB3292', '10010213', 'SMAN 54 JAKARTA', '-', '-', '-', '1985', '1988'),
('A1380D803B5911EABC470CC47AFB3292', '10010229', 'SMPN 7 SIANTAR', '-', '-', '-', '1985', '1988'),
('A9F1404E3B4011EABC470CC47AFB3292', '10010208', 'STIP JAKARTA', '-', '-', '-', '1995', '2000'),
('B3AD919501D311EAB67E0CC47AFB3292', '10059057', 'UNIVERSITAS BATAM', '018/S-1/TE/UNIBA-10', 'TEKNIK ELEKTRO', '-', '2006', '2010'),
('B5BA68883B5011EABC470CC47AFB3292', '10010224', 'SMAN 27, JAKARTA PUSAT ', '-', '-', '-', '1986', '1989'),
('BA0670233B4411EABC470CC47AFB3292', '10010213', 'ATMAJAYA CATHOLIC UNIVERSITY', '-', '-', '-', '-', '1994'),
('C3F6B4FB3B5911EABC470CC47AFB3292', '10010229', 'SMAK BUDIMOUCIA', '-', '-', '-', '1988', '1991'),
('DE4B9578CF2111E994EE2CFDA1251793', '10029402', 'POLITEKNIK NEGERI LHOKSEUMAWE', 'BU-9373CC', 'TEKNIK INFORMATIKA', '3.71', '2015', '2019'),
('E3244A4F3B5411EABC470CC47AFB3292', '10010229', 'SDN 12 SIANTAR', '-', '-', '-', '1982', '1985'),
('E85933D23B5011EABC470CC47AFB3292', '10010224', 'DIII POLITEKNIK MANUFAKTUR BANDUNG - ITB', '-', '-', '-', '-', '1996'),
('FD491F79D65811E9807B2CFDA1251793', '10044271', 'UNIVERSITAS TARUMANAGARA', '0006/TM-U AR/II/2005', 'TEKNIK MESIN', '-', '-', '2005');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_ex`
--

CREATE TABLE `tbl_job_ex` (
  `id_job` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `perusahaan` varchar(30) DEFAULT NULL,
  `jabatan` varchar(30) DEFAULT NULL,
  `thn_mulai` varchar(5) DEFAULT NULL,
  `thn_berakhir` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_job_ex`
--

INSERT INTO `tbl_job_ex` (`id_job`, `PersNo`, `perusahaan`, `jabatan`, `thn_mulai`, `thn_berakhir`) VALUES
('0530A5E33B3D11EABC470CC47AFB3292', '10010208', 'PELICAN VISION', 'RANK 3 ENGINEER', '2000', '2001'),
('057D00F33B4D11EABC470CC47AFB3292', '10010222', 'PT BOGASARI', 'CADET ENGINEER ', '1992', '1993'),
('0A2372B5D6A911E9807B2CFDA1251793', '10044271', 'PT. CHANG JUL FANG INDONESIA', 'SUPERVISOR', '2009', '2011'),
('1', '10057751', 'PT. Perta Arun Gas', 'HR OFFICER', '2016', '2019'),
('16675EEB3B5A11EABC470CC47AFB3292', '10010229', 'PT WARTSILA INDONESIA', 'SENIOR ENGINEER', '2002', '2010'),
('2', '10057751', 'PT. MULTI DATA SISTEM', 'HR ADMIN', '2013', '2015'),
('25482C763B3D11EABC470CC47AFB3292', '10010208', 'PELICAN VISION', 'RANK 2 ENGINEER', '2001', '2002'),
('38A2F209D6A911E9807B2CFDA1251793', '10044271', 'PT. SPACE INDONESIA', 'SUPERVISOR', '2012', '2013'),
('3FB396703B4511EABC470CC47AFB3292', '10010213', 'PT GERMAN MOTOR MANUFACTURING', 'TRAINEE ENGINEER', '1990', '1991'),
('56C8CB7B3B4511EABC470CC47AFB3292', '10010213', 'CV TAURUS UTAMA', 'TECH. SUPPORT', '1992', '1995'),
('5D13644B3B5111EABC470CC47AFB3292', '10010224', 'PT LIPPO MELOC AUTOPARTS', 'PRODUCTION ENGINEERING', '1996', '1997'),
('66D2937ED6B411E9807B2CFDA1251793', '10053589', 'PT. WARTSILA INDONESIA', 'STAFF OPERATION', '2016', '2017'),
('6887D50ED64111E9807B2CFDA1251793', '10027565', 'BIRO JASA KITA', 'MECHANIC', '2002', '2007'),
('69B723323B4511EABC470CC47AFB3292', '10010213', 'MULIA GLASS FLOAT', 'SUPERVISOR', '1995', 'N.A.'),
('6FC76B783B3D11EABC470CC47AFB3292', '10010208', 'PT WARSILA INDONESIA', 'SENIOR SERVICE ENGINEER', '2002', '2012'),
('7A01E3E43B5111EABC470CC47AFB3292', '10010224', 'PT BATARA KUTANG PRIMA', 'MAINTANCE COORDINATOR', '1997', '1999'),
('7EC15C37D6B411E9807B2CFDA1251793', '10053589', 'CROCODILE AGENCY', 'TNT ADMINISTRATOR', '2009', '2014'),
('8B3E31ABD64111E9807B2CFDA1251793', '10027565', 'PT. WARTSILA INDONESIA', 'LOGISTIC COORDINATOR', '2014', '2015'),
('8C9DBFED3B5111EABC470CC47AFB3292', '10010224', 'PT CIKARANG PERKASA', 'ASS. PLAT MANAGER', '-', '-'),
('8DC3F083D64211E9807B2CFDA1251793', '10027565', 'PT. WARTSILA INDONESIA', 'JR. LOGISTIC ENGINEER', '2008', '2014'),
('902CEBA63B3D11EABC470CC47AFB3292', '10010208', 'PT GOLTENS JAKARTA', 'SUPERVISOR', '2014', '2014'),
('9119CA7AD6B411E9807B2CFDA1251793', '10053589', 'ARIANA AGENCY', 'MARKETING', '2008', '2009'),
('A3BC375C3B4A11EABC470CC47AFB3292', '10010221', 'NYK TINE', '3RD ENGINEER', '2000', '2002'),
('AEE15555D6B411E9807B2CFDA1251793', '10053589', 'HARITOS SHIPPING AND TNT', 'TNT ADMINISTRATOR', '2002', '2005'),
('B720E1F6CF2111E994EE2CFDA1251793', '10029402', 'PT MULTI DATA SISTEM', 'SOFTWARE ENGINEER', '2015', '2019'),
('B906D0904CE311EAB4D42CFDA1251793', '10057751', '2', 'TES', '2015', '2020'),
('C21D6124D6C811E9807B2CFDA1251793', '10050499', 'PT. WACHYUNI MANDIRA', 'ELECTRICAL ENGINEER ', '2000', '2000'),
('C69E2B38D6B011E9807B2CFDA1251793', '10053515', 'PT. WARTSILA INDONESIA', 'SUPERVISOR OPERATION', '2016', '2019'),
('C84C389E3B3D11EABC470CC47AFB3292', '10010208', 'CHARLES TAYLOR ADJUSTING ', 'MARINE SURVEYOR AND ADJUSTER', '2014', '2014'),
('C982BF153B4A11EABC470CC47AFB3292', '10010221', 'WARTSITA INDONESIA', 'SERVICE ENGINNER', '2002', 'N.A.'),
('CC9AF218D6A811E9807B2CFDA1251793', '10044271', 'BLK DA\'I AN-NUR', 'HEAD OF DEPARTMENT', '2005', '2009'),
('D6A10DEAD3D011E9BF852CFDA1251793', '10029402', 'PT PKLE 2', 'PROGRAMMER', '2019', '2020'),
('E0102229D6B011E9807B2CFDA1251793', '10053515', 'PT. WARTSILA INDONESIA', 'ASSISTANCE OF MECHANICAL', '2011', '2016'),
('E30C7BF6D6C811E9807B2CFDA1251793', '10050499', 'PT. WACHYUNI MANDIRA', 'AUTOMATION ENGINEER', '-', '-'),
('F0C5DEAD3B3C11EABC470CC47AFB3292', '10010208', 'MT. SAPTA SAMUDERA', 'TANKER', '1998', '1999'),
('F1FE31C0D6A811E9807B2CFDA1251793', '10044271', 'MAN POWER & TRANSMIGRATION MIN', 'FREELANCE INTSRUCTOR', '2008', '2009'),
('F25C82963B3D11EABC470CC47AFB3292', '10010208', 'WINTERMAR OFFSHORE', 'TECHNICAL SUPERINTENDENT', '2015', '2020'),
('F3433DE93B4C11EABC470CC47AFB3292', '10010222', 'PT STOWINDO POWER', 'SERVICE ENGINEER', '1995', '1998'),
('F999D24E3B5911EABC470CC47AFB3292', '10010229', 'PT INDO MATRA POWER', 'TEKNISI MANAGER', '2010', '2016');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE `tbl_language` (
  `id_language` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `bahasa` varchar(30) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`id_language`, `PersNo`, `bahasa`, `level`) VALUES
('01667610D6B411E9807B2CFDA1251793', '10053589', 'TETUM', 'EXPERT'),
('01AFE668CF2211E994EE2CFDA1251793', '10029402', 'ENGLISH 2', 'BASIC'),
('0D5B78903B4111EABC470CC47AFB3292', '10010208', 'ENGLISH', 'INTERMEDIATE'),
('10032BA9D6B411E9807B2CFDA1251793', '10053589', 'PORTUGUESE', 'INTERMEDIATE'),
('170FC19D3B4111EABC470CC47AFB3292', '10010208', 'BAHASA INDONESIA', 'INTERMEDIATE'),
('556749AED3D211E9BF852CFDA1251793', '10029402', 'INDONESIA', 'EXPERT'),
('6594D7134D9011EA8A6A2CFDA1251793', '10057751', 'EFEFEF', 'INTERMEDIATE'),
('93444A84D6AA11E9807B2CFDA1251793', '10044271', 'ENGLISH', 'INTERMEDIATE'),
('9BB1AD27D6AA11E9807B2CFDA1251793', '10044271', 'JAPANESE', 'INTERMEDIATE'),
('A3B8DF47D6AA11E9807B2CFDA1251793', '10044271', 'ARABIC', 'BASIC'),
('D0E5CE233B5911EABC470CC47AFB3292', '10010229', 'BAHASA INDONESIA', 'EXPERT'),
('DAEA21F43B5911EABC470CC47AFB3292', '10010229', 'ENGLISH', 'INTERMEDIATE'),
('EDF7E0C5D6B311E9807B2CFDA1251793', '10053589', 'ENGLISH', 'INTERMEDIATE'),
('F65EB473D6B311E9807B2CFDA1251793', '10053589', 'INDONESIA', 'EXPERT');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_organisasi`
--

CREATE TABLE `tbl_organisasi` (
  `id_organisasi` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `organisasi` varchar(30) DEFAULT NULL,
  `jabatan` varchar(30) DEFAULT NULL,
  `thn_mulai` varchar(5) DEFAULT NULL,
  `thn_berakhir` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_organisasi`
--

INSERT INTO `tbl_organisasi` (`id_organisasi`, `PersNo`, `organisasi`, `jabatan`, `thn_mulai`, `thn_berakhir`) VALUES
('1', '10057751', 'KOMUNITAS MAHASISWA IT', 'KETUA HUMAS', '2017', '2019'),
('2', '10057751', 'RELAWAN TIK INDONESIA', 'KETUA LITBANG', '2016', '2018'),
('8C1DECAF4B1D11EAB4D42CFDA1251793', '10057751', 'TES', 'TES', '2015', '2020'),
('9CD9B669CF2111E994EE2CFDA1251793', '10029402', 'RELAWAN TIK INDONESIA', 'KETUA DIVISI HUMAS ', '2015', '2017'),
('B425688E4CDF11EAB4D42CFDA1251793', '10057751', 'ERER DWDWD WD', 'ERER', 'SEFSE', 'SEFSE'),
('C84733DDD3C911E9BF852CFDA1251793', '10029402', 'HIMATIK', 'KETUA UMUM', '2016', '2018'),
('CDC049E04B1D11EAB4D42CFDA1251793', '10057751', 'TES', 'TES', '2015', '2020');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_refference`
--

CREATE TABLE `tbl_refference` (
  `id_refference` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `fullname` varchar(30) DEFAULT NULL,
  `relation` varchar(20) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_refference`
--

INSERT INTO `tbl_refference` (`id_refference`, `PersNo`, `fullname`, `relation`, `no_hp`, `email`) VALUES
('1', '10057751', 'MUHAMMAD RIZKA', 'DOSEN PEMBIMBING', '0853645346', 'rizka@pnl.co.id'),
('1BF7FC71D6AA11E9807B2CFDA1251793', '10044271', 'HENRY', 'EX WORK PARTNER', '628997308059', '-'),
('2', '10057751', 'KHADAVI', 'ATASAN', '0852647564754', 'davi@gmail.com'),
('319373C2D6B411E9807B2CFDA1251793', '10053589', 'ADELIA DA COSTA RUAS', '-', '670 77298046', '-'),
('36ACEEA03B5A11EABC470CC47AFB3292', '10010229', 'OKA ROZANO S.', 'TEMAN', '081345294446', '-'),
('428C9D71D6B411E9807B2CFDA1251793', '10053589', 'EDUARDO DA SILVA', '-', '670 77391332', '-'),
('703E9A153B4111EABC470CC47AFB3292', '10010208', 'OKKE SHINTYA DEWI', 'WIFE', '081232224499', '-'),
('80EE9C51CF2211E994EE2CFDA1251793', '10029402', 'HARY KUSWANTO', 'PARTNER', '085364536432', 'email@email.com'),
('865E78C9D3DB11E9BF852CFDA1251793', '10029402', 'HENDRI 2', 'DIRUT PKLE', '085345342435', 'hendir@email.com'),
('8F63598A3B4111EABC470CC47AFB3292', '10010208', 'POLY HERMANUS WTHER', 'TEMAN ', '08118706974', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_training`
--

CREATE TABLE `tbl_training` (
  `id_training` varchar(32) NOT NULL,
  `PersNo` varchar(255) DEFAULT NULL,
  `training` varchar(30) DEFAULT NULL,
  `tmpt_training` varchar(30) DEFAULT NULL,
  `mulai_training` varchar(20) DEFAULT NULL,
  `selesai_training` varchar(20) DEFAULT NULL,
  `penyelenggara` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_training`
--

INSERT INTO `tbl_training` (`id_training`, `PersNo`, `training`, `tmpt_training`, `mulai_training`, `selesai_training`, `penyelenggara`) VALUES
('01BD581E3B4511EABC470CC47AFB3292', '10010213', 'INTERNAL QUALITY AUDIT', '-', '-', '1997', 'MGF'),
('036A13643B3F11EABC470CC47AFB3292', '10010208', 'LEADERSHIP TRAINING BY DALE CA', 'CIKARANG JAKARTA', '-', '-', 'PT WARTSILA'),
('07BD0B843B5111EABC470CC47AFB3292', '10010224', 'MATSUBISHI ELECT CORP', 'JAPAN', '-', '1997', 'PT LIMA BEKASI'),
('087363F301D211EAB67E0CC47AFB3292', '10059057', '4-DAYS HIGH & LOW VOLTAGE SWTI', 'SINGAPORE', '2013-09-09', '2013-09-12', 'ASSETS TRAINING & TECHNICAL SE'),
('1D6560503B3F11EABC470CC47AFB3292', '10010208', 'ADVANCED TRAINING ENGINE W46 ', 'SUBIC - FILIPINA', '-', '-', 'PT WARTSILA'),
('1F167225D64211E9807B2CFDA1251793', '10027565', 'TRAINING K3', 'CAWANG', '2018-01-08', '2018-01-20', 'PT. PHITAGORAS GLOBAL DUTA'),
('1FD34A003B4511EABC470CC47AFB3292', '10010213', 'ASSEMBLE AND DISASSEMBLE ENGIN', '-', '-', '1996', 'PT. TRAKINDO UTAMA'),
('22EC914B3B5111EABC470CC47AFB3292', '10010224', 'LAMCOR LAGUNA', 'PHILIPINES', '-', '1997', 'PT LIMA BEKASI'),
('2E62FC2704E811EAB5100CC47AFB3292', '10057751', 'TRAINING FOR LOSS CONTROL PROB', 'CIKARANG', '1997-07-28', '1997-07-28', 'PT MULIA INDUSTRINDO'),
('2FEC7AAA01C011EAB67E0CC47AFB3292', '10059057', 'HIGH VOLTAGE OPERATION & SAFET', '-', '2011-05-31', '2011-06-02', 'AGGREKO'),
('35519D713B5111EABC470CC47AFB3292', '10010224', 'GUIDED CONV. LEVEL C', 'JAKARTA', '-', '1991', '-'),
('3AA8B004CF2211E994EE2CFDA1251793', '10029402', 'WEB DEVELOPMENT', 'JAKARTA', '2019-05-10', '2019-09-03', 'DICODING INDONESIA'),
('4BCF82A74D9211EA8A6A2CFDA1251793', '10057751', ' W DW WD WD', 'WDWD', '2020-02-05', '2020-01-02', 'WDWDWS'),
('53FEC919D6B011E9807B2CFDA1251793', '10053515', 'DON BOSCO TRAINING - TECHNICIA', '-', '2006-10-20', '2007-10-15', 'DON BOSCO TRAINING'),
('559C775201BD11EAB67E0CC47AFB3292', '10059057', 'QSK78 CM500 ENGINE QUALIFICATI', '-', '2014-07-08', '2014-07-11', 'CUMMINS INC'),
('66E14E873B5A11EABC470CC47AFB3292', '10010229', 'BASIC ELECTRICAL & PLC TRAININ', '-', '-', '-', 'PT WACHYUNI MANDIRA'),
('6E83442A3B4C11EABC470CC47AFB3292', '10010222', 'WARTSILA SALM W200', 'FRANCE', '-', '1996', 'PT.STOWINDO'),
('71A0223701C611EAB67E0CC47AFB3292', '10059057', 'PEMBINAAN KESELEMATAN DAN KESE', 'JAKARTA', '2018-03-12', '2018-03-14', 'PT MITRA SINERGI INTERNASIONAL'),
('74CB808401C111EAB67E0CC47AFB3292', '10059057', 'OPERATOR ALAT BERAT EXCAVATOR ', 'TANJUNG PINANG', '2006-04-26', '2006-05-21', 'PT HEXINDO ADI PERKASA'),
('773259873B4A11EABC470CC47AFB3292', '10010221', 'HAND ON TRAINING W-32 ADVANCED', '-', '-', '2006', 'WARSILA TRAINING CENTRE'),
('7E7F5CF04D9111EA8A6A2CFDA1251793', '10057751', ' WDW ', 'WDWD', '2020-01-29', '2020-01-02', 'WDWD '),
('87FA444F3B4C11EABC470CC47AFB3292', '10010222', 'WARTSILA SALM UD 25', 'SURGERES', '-', '1996', 'PT. STOWINDO'),
('96DA5F45012D11EAB67E0CC47AFB3292', '10053550', 'INTRODUCTION OF OCCUPATIONAL S', 'BANDUNG', '2011-02-10', '2011-02-10', 'MINISTRY OF MAN POWER AND TRAN'),
('A8204629D3DA11E9BF852CFDA1251793', '10029402', 'JARINGAN 2', 'LHOKSEUMAWE', '2019-09-01', '2019-09-04', 'LSP POLITEKNIK LSM'),
('A8D3DAEB01BC11EAB67E0CC47AFB3292', '10059057', 'PRESENTATION SKILL PROGRAMME', '-', '2014-08-19', '2014-08-20', 'LOXTON CONSULTING GROUP'),
('ADD6F50604EB11EAB5100CC47AFB3292', '10050507', 'MICROSOFT WORD XP - LEVEL: 1', 'TIMOR LESTE', '2009-10-07', '2009-10-07', 'ITTC UNTL'),
('CAF73FC3012C11EAB67E0CC47AFB3292', '10054290', 'INTRODUCTION OF OCCUPATIONAL S', 'BANDUNG', '2011-02-10', '2011-02-10', 'MINISTRY OF MAN POWER AND TRAN'),
('DFA6C1953B3E11EABC470CC47AFB3292', '10010208', 'LINK TO SALES TRAINING', 'MUMBAI - INDIA', '-', '-', 'PT WARTSILA'),
('E95D756B01C611EAB67E0CC47AFB3292', '10059057', 'GOAL TECH WORKSHOP', 'SINGAPORE', '2013-11-18', '2013-11-22', 'ESB SINGAPORE'),
('F043BE4B3B4411EABC470CC47AFB3292', '10010213', 'LOSS CONTROL PROGRAM ', '-', '', '1997', 'MGF');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_achievement`
--
ALTER TABLE `tbl_achievement`
  ADD PRIMARY KEY (`id_achievement`) USING BTREE;

--
-- Indexes for table `tbl_competence`
--
ALTER TABLE `tbl_competence`
  ADD PRIMARY KEY (`id_competence`) USING BTREE;

--
-- Indexes for table `tbl_education`
--
ALTER TABLE `tbl_education`
  ADD PRIMARY KEY (`id_education`) USING BTREE;

--
-- Indexes for table `tbl_job_ex`
--
ALTER TABLE `tbl_job_ex`
  ADD PRIMARY KEY (`id_job`) USING BTREE;

--
-- Indexes for table `tbl_language`
--
ALTER TABLE `tbl_language`
  ADD PRIMARY KEY (`id_language`) USING BTREE;

--
-- Indexes for table `tbl_organisasi`
--
ALTER TABLE `tbl_organisasi`
  ADD PRIMARY KEY (`id_organisasi`) USING BTREE;

--
-- Indexes for table `tbl_refference`
--
ALTER TABLE `tbl_refference`
  ADD PRIMARY KEY (`id_refference`) USING BTREE;

--
-- Indexes for table `tbl_training`
--
ALTER TABLE `tbl_training`
  ADD PRIMARY KEY (`id_training`) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
